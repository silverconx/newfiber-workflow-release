package com.newfiber.workflow.enums;

import lombok.Getter;

/**
 * @author xiongkai
 */
@Getter
public enum EDataSourceType {

    /**
     */
    KingbaseES("KingbaseES", "人大金仓"),
	DM("DM DBMS", "达梦"),

;

    EDataSourceType(String key, String value) {
        this.key = key;
        this.value = value;
    }

    private final String key;

    private final String value;

}
