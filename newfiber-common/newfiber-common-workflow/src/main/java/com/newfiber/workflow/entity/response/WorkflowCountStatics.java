package com.newfiber.workflow.entity.response;

import lombok.Data;

/**
 * @author : X.K
 * @since : 2023/3/16 下午2:57
 */
@Data
public class WorkflowCountStatics {

	private Long allCount;

	private Long doneCount;

	private Long todoCount;

	public WorkflowCountStatics(Long doneCount, Long todoCount) {
		this.doneCount = doneCount;
		this.todoCount = todoCount;
		this.allCount = doneCount + todoCount;
	}

	public WorkflowCountStatics(Integer doneCount, Integer todoCount) {
		this.doneCount = doneCount.longValue();
		this.todoCount = todoCount.longValue();
		this.allCount = doneCount.longValue() + todoCount.longValue();
	}

	public static WorkflowCountStatics zero(){
		return new WorkflowCountStatics(0L, 0L);
	}
}
