package com.newfiber.workflow.support.request;

import com.newfiber.common.core.web.request.BasePageRequest;
import com.newfiber.workflow.service.FlowableProcessService;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

/**
 * 工作流任务分页查询请求，业务请求继承该类
 * @see FlowableProcessService#pageTodoBusinessKey(String, String, Object, Object, WorkflowPageRequest)
 */
@Data
public class WorkflowPageRequest extends BasePageRequest {

    /**
     * 工作流实例编号
     */
    @ApiModelProperty(name = "workflowInstanceId", value = "工作流实例编号")
    private String workflowInstanceId;

    /**
     * 状态
     */
    @ApiModelProperty(name = "status", value = "状态")
    private String status;

    /**
     * 工作流用户编号(查询该用户的待办/已完成)
     */
    @ApiModelProperty(name = "workflowUserId", value = "工作流用户编号(查询该用户的待办/已完成)")
    private String workflowUserId;

	/**
	 * 工作流用户角色编号(查询该用户角色的待办/已完成)
	 */
	@ApiModelProperty(name = "workflowUserId", value = "工作流用户角色编号(查询该用户角色的待办/已完成)")
	private List<String> workflowUserRoleIdList;

	/**
	 * 查询范围
	 */
	@ApiModelProperty(name = "queryScope", value = "查询范围（代办 todo / 已办 done / 全部 all，默认all）")
	private String queryScope = "all";

}
