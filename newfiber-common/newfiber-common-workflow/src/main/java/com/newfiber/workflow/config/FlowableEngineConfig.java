package com.newfiber.workflow.config;

import org.flowable.spring.SpringProcessEngineConfiguration;
import org.flowable.spring.boot.ProcessEngineConfigurationConfigurer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * @author : X.K
 * @since : 2022/4/1 下午5:13
 */

@Configuration
public class FlowableEngineConfig implements ProcessEngineConfigurationConfigurer {

	@Value("${flowable.url}")
	private String url;

	@Value("${flowable.username}")
	private String username;

	@Value("${flowable.password}")
	private String password;

	@Value("${flowable.driver-class-name}")
	private String driverClassName;

	@Override
	public void configure(SpringProcessEngineConfiguration springProcessEngineConfiguration) {
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource(url, username, password);
		driverManagerDataSource.setDriverClassName(driverClassName);
		springProcessEngineConfiguration.setDataSource(driverManagerDataSource);

		springProcessEngineConfiguration.setActivityFontName("宋体");
		springProcessEngineConfiguration.setAnnotationFontName("宋体");
		springProcessEngineConfiguration.setLabelFontName("宋体");
	}
}
