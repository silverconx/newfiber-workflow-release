package com.newfiber.workflow.service;

import java.util.Set;
import org.flowable.cmmn.model.Task;
import org.flowable.identitylink.api.IdentityLink;

public interface FlowableCommonService {
    Set<String> listTaskUserId(Task task);
    Set<String> listTaskUserId(Set<IdentityLink> identityLinkSet);
}
