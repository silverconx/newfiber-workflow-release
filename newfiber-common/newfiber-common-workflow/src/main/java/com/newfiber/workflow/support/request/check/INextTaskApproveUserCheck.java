package com.newfiber.workflow.support.request.check;


import com.newfiber.common.core.enums.EBoolean;

/**
 * 下一节点审核人填写检查
 * @author : X.K
 * @since : 2022/5/19 上午11:30
 */
public interface INextTaskApproveUserCheck {

	/**
	 * 检查类型
	 */
	ENextTaskApproveUserCheckType getCheckType();

	/**
	 * 任务Key
	 */
	String getTaskKey();

	/**
	 * 告警信息
	 */
	String getWarnMessage();

	/**
	 * 审核结果
	 */
	default String getApproveResult(){
		return EBoolean.True.getBoolString();
	}
}
