package com.newfiber.workflow.config.diagram;

import java.awt.Color;

/**
 * @author : X.K
 * @since : 2022/2/24 下午1:42
 */
public final class FlowableWorkflowConstants {

	/** 动态流程图颜色定义 **/
	public static final Color COLOR_NORMAL = new Color(0, 205, 0);
	public static final Color COLOR_CURRENT = new Color(255, 0, 0);

	/** 定义生成流程图时的边距(像素) **/
	public static final int PROCESS_PADDING = 5;

}
