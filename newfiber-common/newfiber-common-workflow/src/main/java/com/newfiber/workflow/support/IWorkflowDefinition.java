package com.newfiber.workflow.support;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.newfiber.workflow.enums.EConstantValue;

/**
 * 工作流定义
 * @see IWorkflowCallback#getWorkflowDefinition()
 */
public interface IWorkflowDefinition {

    /**
     * 工作流编号
     * @return 工作流编号
     */
    String getWorkflowKey();

    /**
     * 工作流名称
     * @return 工作流名称
     */
    String getWorkflowName();

    /**
     * 业务实体表别名
     * @return 业务实体表别名
     */
    default String getTableAlias() {
        return "t";
    }

	/**
	 * 业务实体表主键
	 * @return 业务实体表主键
	 */
	default String getTablePrimary(){
		return "id";
	}

	/**
	 * 工作流实例编号字段名
	 * @return 工作流实例编号字段名
	 */
	default String getWorkflowInstanceFieldName() {
		return EConstantValue.WorkflowInstanceFieldName.getValue();
	}

    /**
     * 业务实体表主键类型
     * @return 业务实体表主键类型
     */
    default Class<?> getTableIdType(){
        return String.class;
    }

	/**
	 * 业务服务
	 * @return 业务服务
	 */
	default Class<? extends ServiceImpl<?, ?>> getBusinessService(){
    	return null;
    }
}
