package com.newfiber.workflow.controller;

import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.workflow.entity.WorkflowModel;
import com.newfiber.workflow.entity.request.WorkflowModeCreateRequest;
import com.newfiber.workflow.entity.request.WorkflowModelNextTaskRequest;
import com.newfiber.workflow.entity.request.WorkflowModelPageRequest;
import com.newfiber.workflow.entity.response.WorkflowModelNextTaskResponse;
import com.newfiber.workflow.service.FlowableModelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.annotation.Resource;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@Api(value = "WF02-工作流模型管理", tags = "WF02-工作流模型管理")
@RequestMapping("/workflow-model")
public class WorkflowModelController extends BaseController {

    @Resource
    private FlowableModelService flowableModelService;

    @ApiOperation(value = "创建工作流模型")
    @PostMapping(value = "/create")
    public Result<Object> create(@RequestBody @Valid WorkflowModeCreateRequest request) {
        return success(flowableModelService.create(request));
    }

    @ResponseBody
    @ApiOperation(value = "保存工作流模型")
    @PutMapping(value = "/save")
    public Result<String> save(@RequestParam("modelId") String modelId, @RequestParam("key") String key, @RequestParam("name") String name,
            @RequestParam("jsonXml") String jsonXml, @RequestParam("svgXml") String svgXml,
            @RequestParam("description") String description) {
        flowableModelService.save(modelId, key, name, jsonXml, svgXml, description);
        return success();
    }

    @ApiOperation(value = "部署工作流模型")
    @PostMapping(value = "/deploy/{modelId}")
    public Result<String> deploy(@PathVariable String modelId) {
        flowableModelService.deploy(modelId);
        return success();
    }

    @ApiOperation(value = "删除工作流模型")
    @PostMapping(value = "/delete/{modelId}")
    public Result<String> delete(@PathVariable String modelId) {
        flowableModelService.delete(modelId);
        return success();
    }

	@ApiOperation(value = "上传工作流文件")
	@PostMapping(value = "/upload")
	public Result<String> upload(@RequestParam String workflowKey, @RequestParam MultipartFile multipartFile) {
		flowableModelService.upload(workflowKey, multipartFile);
		return success();
	}

	@ApiOperation(value = "重新上传工作流文件")
	@PostMapping(value = "/deployWebActivitiServerFile")
	public Result<String> reUpload(@RequestParam String workflowKey, @RequestParam MultipartFile multipartFile) {
		flowableModelService.reUpload(workflowKey, multipartFile);
		return success();
	}

    @ApiOperation(value = "下一步任务信息")
    @PostMapping(value = "/nextTasks")
    public Result<List<WorkflowModelNextTaskResponse>> nextTasks(@RequestBody @Valid WorkflowModelNextTaskRequest request) {
        return success(flowableModelService.nextTasks(request.getWorkflowKey(), request.getCurrentTask()));
    }

    @ApiOperation(value = "详细查询工作流模型")
    @PostMapping(value = "/detail/{modelId}")
    public Result<WorkflowModel> detail(@PathVariable String modelId) {
        return success(flowableModelService.detail(modelId));
    }

    @ApiOperation(value = "分页查询工作流模型")
    @PostMapping(value = "/page")
    public PageResult<List<WorkflowModel>> page(@RequestBody @Valid WorkflowModelPageRequest request) {
        return pageResult(flowableModelService.pageWorkflowModel(request));
    }

}
