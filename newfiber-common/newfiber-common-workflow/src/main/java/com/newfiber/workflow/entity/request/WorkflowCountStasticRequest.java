package com.newfiber.workflow.entity.request;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class WorkflowCountStasticRequest {

    /**
     * 工作流编号
     */
    @NotBlank
    @ApiModelProperty(name = "workflowKey", value = "工作流编号", required = true)
    private String workflowKey;

	/**
	 * 工作流用户编号
	 */
	@NotBlank
	@ApiModelProperty(name = "workflowUserId", value = "工作流用户编号", required = true)
	private String workflowUserId;

    /**
     * 状态
     */
    @ApiModelProperty(name = "status", value = "状态")
    private String status;

    /**
     *  查询字段
     */
    @ApiModelProperty(value = "查询字段（根据group by分组统计）")
    private String queryField;

    /**
     *  查询字段值
     */
    @ApiModelProperty(value = "查询字段值")
    private String queryFieldValue;

}
