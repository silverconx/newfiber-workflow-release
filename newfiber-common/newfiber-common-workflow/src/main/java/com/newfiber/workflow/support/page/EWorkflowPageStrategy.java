package com.newfiber.workflow.support.page;

/**
 * 工作流分页策略
 * @author xiongkai
 */
public enum EWorkflowPageStrategy {

    /**
     *
     */
    IdIn("IdIn", "通过 'id in ($)' 查询"),
	JoinTable("JoinTable", "通过连表查询"),

;

    EWorkflowPageStrategy(String key, String stringValue) {
        this.key = key;
        this.stringValue = stringValue;
    }

    private final String key;

    private final String stringValue;

    public String getKey() {
        return key;
    }

    public String getStringValue() {
        return stringValue;
    }
}
