package com.newfiber.workflow.support;

import com.newfiber.workflow.service.FlowableProcessService;
import com.newfiber.workflow.support.notification.IWorkflowNotification;
import com.newfiber.workflow.support.request.check.INextTaskApproveUserCheck;
import com.newfiber.workflow.utils.ReflectionKit;

/**
 * 工作流回调接口，业务service实现该接口
 * @see FlowableProcessService
 * @param <T> 业务实体
 */
public interface IWorkflowCallback<T> {

    /**
     * 业务实体类型
     * @return 业务实体类型
     */
     default Class<?> getEntityClass(){
        return ReflectionKit.getInterfaceGeneric(this);
    }

    /**
     * 工作流定义
     * @return 工作流定义
     */
    IWorkflowDefinition getWorkflowDefinition();

    /**
     * 更新业务实体的工作流实体编号
     * @param businessKey 业务实体编号
     * @param workflowInstanceId 工作流实体编号
     */
    void refreshWorkflowInstanceId(Object businessKey, String workflowInstanceId);

    /**
     * 更新业务数据状态
     * @param businessKey 业务实体编号
     * @param status 状态
     */
    void refreshStatus(Object businessKey, String status);

	/**
	 * 更新业务驳回状态
	 * 1、拒绝则更新为驳回来源状态，eg：A驳回到B，则更新为A
	 * 2、通过则更新为空
	 * @param businessKey 业务实体编号
	 * @param rejectFromStatus 驳回来源状态
	 */
	default void refreshRefuseFromStatus(Object businessKey, String rejectFromStatus){};

    /**
     * 工作流通知
     * @return 工作流通知
     */
    default IWorkflowNotification[] getWorkflowNotification(){
        return null;
    }

	/**
	 * 下一步审核人检查
	 * @return 下一步审核人检查
	 */
	default INextTaskApproveUserCheck[] getNextTaskApproveUserCheck(){
    	return null;
    }
}
