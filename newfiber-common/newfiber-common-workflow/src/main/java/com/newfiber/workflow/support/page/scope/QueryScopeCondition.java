package com.newfiber.workflow.support.page.scope;

import lombok.Data;
import org.w3c.dom.Element;

/**
 * @author : X.K
 * @since : 2022/6/20 下午3:15
 */
@Data
public class QueryScopeCondition {

	/**
	 * 范围编号
	 */
	private String scopeId;

	/**
	 * 连表条件
	 */
	private String joinCondition;

	/**
	 * 用户查询条件
	 */
	private String userCondition;

	/**
	 * 用户组/角色查询条件
	 */
	private String groupCondition;

	public QueryScopeCondition(String scopeId, Element joinConditionElement, Element userConditionElement, Element groupConditionElement){
		this.scopeId = scopeId;
		if(null != joinConditionElement){
			this.joinCondition = joinConditionElement.getTextContent();
		}
		if(null != userConditionElement){
			this.userCondition = userConditionElement.getTextContent();
		}
		if(null != groupConditionElement){
			this.groupCondition = groupConditionElement.getTextContent();
		}
	}

}
