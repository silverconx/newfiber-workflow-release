package com.newfiber.workflow.support.page;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ReflectUtil;
import com.newfiber.workflow.enums.EQueryScope;
import com.newfiber.workflow.service.FlowableProcessService;
import com.newfiber.workflow.support.IWorkflowCallback;
import com.newfiber.workflow.support.IWorkflowDefinition;
import com.newfiber.workflow.support.page.scope.EQueryScopeKey;
import com.newfiber.workflow.support.page.scope.QueryScopeCondition;
import com.newfiber.workflow.support.page.scope.QueryScopeUtil;
import com.newfiber.workflow.utils.ApplicationContextProvider;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.springframework.util.CollectionUtils;

@Slf4j
@Intercepts({
        @Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}),}
)
public class WorkflowPageInterceptor implements Interceptor{

    private final FlowableProcessService flowableProcessService;

    private final Map<Class<?>, EntityInfo> classEntityInfoMap = new HashMap<>();{
		flowableProcessService = ApplicationContextProvider.getBean(FlowableProcessService.class);
	}

	@Override
    public Object intercept(Invocation invocation) throws Throwable {
        WorkflowPage workflowPage = WorkflowPageHelper.getWorkflowPage();
        if(null == workflowPage){
            return invocation.proceed();
        }

        if((StringUtils.isBlank(workflowPage.getTaskKey()) && StringUtils.isBlank(workflowPage.getUserId()))){
            return invocation.proceed();
        }

		Object[] args = invocation.getArgs();
		MappedStatement mappedStatement = (MappedStatement) args[0];
		Object parameterObject = args[1];
		BoundSql boundSql = mappedStatement.getBoundSql(parameterObject);
		String executeSql = boundSql.getSql();

		EQueryScope queryScope = EQueryScope.match(workflowPage.getQueryScope());
		IWorkflowCallback<?> workflowCallback = workflowPage.getWorkflowCallback();
		IWorkflowDefinition workflowDefinition = workflowCallback.getWorkflowDefinition();

		switch (workflowPage.getWorkflowPageStrategy()){
	        case IdIn:
		        // 待办/已完成的业务编号
		        List<String> businessKeyList = getQueryScopeBusinessKeyList(workflowPage, queryScope);

		        // 拼接新SQL
		        executeSql = appendWhereIdCondition(executeSql, businessKeyList, workflowPage.getWorkflowCallback());

		        break;

	        case JoinTable:

		        QueryScopeCondition queryScopeCondition = QueryScopeUtil.match(queryScope);
		        if(null == queryScopeCondition){
			        return invocation.proceed();
		        }

		        String joinCondition = parseSqlTemplateCondition(queryScopeCondition.getJoinCondition(), workflowPage, workflowDefinition);
		        String userCondition = parseSqlTemplateCondition(queryScopeCondition.getUserCondition(), workflowPage, workflowDefinition);
		        String groupCondition = parseSqlTemplateCondition(queryScopeCondition.getGroupCondition(), workflowPage, workflowDefinition);

	        	break;

	        default: break;
        }

        BoundSql newBoundSql = new BoundSql(mappedStatement.getConfiguration(), executeSql,
                boundSql.getParameterMappings(), boundSql.getParameterObject());

        MappedStatement newMappedStatement = buildMappedStatement(mappedStatement, new BoundSqlSqlSource(newBoundSql));
        for (ParameterMapping mapping : boundSql.getParameterMappings()) {
            String prop = mapping.getProperty();
            if (boundSql.hasAdditionalParameter(prop)) {
                newBoundSql.setAdditionalParameter(prop, boundSql.getAdditionalParameter(prop));
            }
        }

        invocation.getArgs()[0] = newMappedStatement;
        WorkflowPageHelper.clearPage();

        return invocation.proceed();
    }

    private String parseSqlTemplateCondition(String condition, WorkflowPage workflowPage, IWorkflowDefinition workflowDefinition){
		if(StringUtils.isBlank(condition)){
			return condition;
		}
	    condition = condition.replace(EQueryScopeKey.WorkflowKey.getKey(), workflowDefinition.getWorkflowKey());
	    condition = condition.replace(EQueryScopeKey.TableAlias.getKey(), workflowDefinition.getTableAlias());
	    condition = condition.replace(EQueryScopeKey.WorkflowInstanceFieldName.getKey(), workflowDefinition.getWorkflowInstanceFieldName());
	    condition = condition.replace(EQueryScopeKey.UserId.getKey(), workflowPage.getUserId());
		if(CollectionUtil.isNotEmpty(workflowPage.getUserRoleIdList())){
			condition = condition.replace(EQueryScopeKey.UserRoleIds.getKey(), String.join(",", workflowPage.getUserRoleIdList()));
		}
		return condition;
    }

	private List<String> getQueryScopeBusinessKeyList(WorkflowPage workflowPage, EQueryScope queryScope) {
		List<String> businessProcessInstanceIdList = Collections.emptyList();
		switch (queryScope){
			case All:
				businessProcessInstanceIdList = flowableProcessService.listInvolvedBusinessProcessInstanceIdByUser(workflowPage.getWorkflowCallback(), workflowPage.getTaskKey(), workflowPage.getUserId());
			    break;
			case Todo:
				businessProcessInstanceIdList = flowableProcessService.listTodoBusinessProcessInstanceIdByUser(workflowPage.getWorkflowCallback(), workflowPage.getTaskKey(), workflowPage.getUserId());
				break;
			case Done:
				businessProcessInstanceIdList = flowableProcessService.listTaskDoneBusinessProcessInstanceIdByUser(workflowPage.getWorkflowCallback(), workflowPage.getTaskKey(), workflowPage.getUserId());
				break;
			default: break;
		}
		return businessProcessInstanceIdList;
	}

	@Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {

    }

    private String appendWhereIdCondition(String sql, List<String> idList, IWorkflowCallback<?> workflowCallback){
        if(CollectionUtils.isEmpty(idList)){
            idList.add("0");
        }

        String appendSql = " %s.%s in ( %s ) ";
        appendSql = String.format(appendSql, workflowCallback.getWorkflowDefinition().getTableAlias(),
	            workflowCallback.getWorkflowDefinition().getWorkflowInstanceFieldName(),
                parseIdListByIdType(String.class, idList));

        try{
            Expression appendWhere = CCJSqlParserUtil.parseCondExpression(appendSql);

            Select select = (Select) CCJSqlParserUtil.parse(sql);
            PlainSelect plainSelect = (PlainSelect) select.getSelectBody();

            if (plainSelect.getWhere() == null) {
                plainSelect.setWhere(appendWhere);
            } else {
                plainSelect.setWhere(new AndExpression(appendWhere, plainSelect.getWhere()));
            }

            sql = plainSelect.toString();
        }catch (Exception e){
            log.error("【工作流】拼接SQL错误:{}-->{}", e.getMessage(), e.getStackTrace());
        }

        return sql;
    }

    private String appendSqlTemplateCondition(String sql, String joinCondition, String userCondition, String groupCondition){

	    try {
		    Select select = (Select) CCJSqlParserUtil.parse(sql);
		    PlainSelect plainSelect = (PlainSelect) select.getSelectBody();

		    // TODO 拼接用户查询条件
		    userCondition = userCondition.replace(EQueryScopeKey.GroupCondition.getKey(), groupCondition);
		    Expression appendWhere = CCJSqlParserUtil.parseCondExpression(userCondition);
		    plainSelect.setWhere(new AndExpression(appendWhere, plainSelect.getWhere()));

	    } catch (JSQLParserException e) {
		    log.error("【工作流】拼接SQL错误:{}-->{}", e.getMessage(), e.getStackTrace());
	    }

		return sql;
    }

    /**
     * 基于MyBatisPlus解析表名
     * @param entityClass 实体
     * @return 表名
     */
    @Deprecated
    @SuppressWarnings("unchecked")
    private String parseTableName(Class<?> entityClass) {
        String tableName = "t";
        if(null == entityClass){
            return tableName;
        }

        EntityInfo entityInfo = classEntityInfoMap.get(entityClass);
        if(null != entityInfo && null != entityInfo.getTableName()){
            tableName = entityInfo.getTableName();
        }else{
            Annotation[] annotations = entityClass.getAnnotations();
            for(Annotation annotation : annotations){
                if("TableName".equals(annotation.annotationType().getSimpleName())){
                    try{
                        Object o = ReflectUtil.getFieldValue(annotation, "h");
                        if(o instanceof InvocationHandler){
                            LinkedHashMap<String, Object> memberValues = (LinkedHashMap<String, Object>) ReflectUtil.getFieldValue(o, "memberValues");
                            tableName = memberValues.get("value").toString();
                            classEntityInfoMap.put(entityClass, new EntityInfo(tableName));
                        }
                    }catch (Exception ignore){ }
                }
            }
        }

        return tableName;
    }

    /**
     * 基于MyBatisPlus解析主键编号
     * @param entityClass 实体
     * @param idList 编号列表
     * @return 编号
     */
    @Deprecated
    private String parseIdList(Class<?> entityClass, List<String> idList){
        if(CollectionUtils.isEmpty(idList)){
            idList.add("0");
        }
        String idListString = String.join(",", idList);

        if(null == entityClass){
            return idListString;
        }

        Class<?> tableIdType = null;
        EntityInfo entityInfo = classEntityInfoMap.get(entityClass);

        if(null != entityInfo && null != entityInfo.getTableIdType()){
            tableIdType = entityInfo.getTableIdType();
        }else{
            Field[] fields = entityClass.getDeclaredFields();
            for(Field field : fields){
                Annotation[] annotations = field.getAnnotations();
                for(Annotation annotation : annotations){
                    if("TableId".equals(annotation.annotationType().getSimpleName())){
                        tableIdType = field.getType();

                        if(null != classEntityInfoMap.get(entityClass)){
                            classEntityInfoMap.get(entityClass).setTableIdType(tableIdType);
                        }else{
                            classEntityInfoMap.put(entityClass, new EntityInfo(tableIdType));
                        }

                    }
                }
            }
        }

        if(ReflectUtil.newInstance(tableIdType, "0") instanceof String){
            idListString = String.join("','", idList);
            idListString = "'" + idListString + "'";
        }

        return idListString;
    }

    private String parseIdListByIdType(Class<?> tableIdType, List<String> idList){
        String idListString = String.join(",", idList);

        if(null != tableIdType && ReflectUtil.newInstance(tableIdType, "0") instanceof String){
            idListString = String.join("','", idList);
            idListString = "'" + idListString + "'";
        }

        return idListString;
    }

    private MappedStatement buildMappedStatement (MappedStatement ms, SqlSource newSqlSource) {
        MappedStatement.Builder builder = new
                MappedStatement.Builder(ms.getConfiguration(), ms.getId(), newSqlSource, ms.getSqlCommandType());
        builder.resource(ms.getResource());
        builder.fetchSize(ms.getFetchSize());
        builder.statementType(ms.getStatementType());
        builder.keyGenerator(ms.getKeyGenerator());
        if (ms.getKeyProperties() != null && ms.getKeyProperties().length > 0) {
            builder.keyProperty(ms.getKeyProperties()[0]);
        }
        builder.timeout(ms.getTimeout());
        builder.parameterMap(ms.getParameterMap());
        builder.resultMaps(ms.getResultMaps());
        builder.resultSetType(ms.getResultSetType());
        builder.cache(ms.getCache());
        builder.flushCacheRequired(ms.isFlushCacheRequired());
        builder.useCache(ms.isUseCache());
        return builder.build();
    }

    static class BoundSqlSqlSource implements SqlSource {
        private final BoundSql boundSql;

        public BoundSqlSqlSource(BoundSql boundSql) {
            this.boundSql = boundSql;
        }

        @Override
        public BoundSql getBoundSql(Object parameterObject) {
            return boundSql;
        }
    }

    @Data
    static class EntityInfo{
        String tableName;
        Class<?> tableIdType;

        public EntityInfo(String tableName) {
            this.tableName = tableName;
        }

        public EntityInfo(Class<?> tableIdType) {
            this.tableIdType = tableIdType;
        }
    }
}
