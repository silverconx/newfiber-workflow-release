package com.newfiber.workflow.support.page;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.newfiber.workflow.support.IWorkflowCallback;
import com.newfiber.workflow.support.request.WorkflowPageRequest;

/**
 * 工作流分页工具类
 */
public class WorkflowPageHelper extends PageHelper {

    protected static final ThreadLocal<WorkflowPage> LOCAL_WORKFLOW_PAGE = new ThreadLocal<>();

    /**
     * 开始分页
     * @param workflowPageRequest 分页接口参数
     * @param workflowCallback 回调接口
     * @return 分页参数实体
     */
    public static <E> Page<E> startPage(WorkflowPageRequest workflowPageRequest, IWorkflowCallback<?> workflowCallback) {
	    PageHelper.clearPage();
	    WorkflowPageHelper.clearPage();
        Page<E> page = startPage(workflowPageRequest.getPageNum(), workflowPageRequest.getPageSize(), workflowPageRequest.getOrderBy());
        LOCAL_WORKFLOW_PAGE.set(WorkflowPage.build(workflowPageRequest, workflowCallback));
        return page;
    }

	/**
	 * 开始分页
	 * @param workflowPageRequest 分页接口参数
	 * @param workflowPageStrategy 工作流分页策略
	 * @param workflowCallback 回调接口
	 * @return 分页参数实体
	 */
	public static <E> Page<E> startPage(WorkflowPageRequest workflowPageRequest, EWorkflowPageStrategy workflowPageStrategy, IWorkflowCallback<?> workflowCallback) {
		PageHelper.clearPage();
		WorkflowPageHelper.clearPage();
		Page<E> page = startPage(workflowPageRequest.getPageNum(), workflowPageRequest.getPageSize(), workflowPageRequest.getOrderBy());
		LOCAL_WORKFLOW_PAGE.set(WorkflowPage.build(workflowPageRequest, workflowPageStrategy, workflowCallback));
		return page;
	}

    public static WorkflowPage getWorkflowPage(){
        return LOCAL_WORKFLOW_PAGE.get();
    }

    public static void clearPage() {
        LOCAL_WORKFLOW_PAGE.remove();
    }

}
