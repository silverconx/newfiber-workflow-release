package com.newfiber.workflow.support.page.scope;

/**
 * @author xiongkai
 */
public enum EQueryScopeKey {

    /**
     */
    WorkflowKey("$WorkflowKey", "工作流编号"),
	TableAlias("$TableAlias", "表别名"),
	WorkflowInstanceFieldName("$WorkflowInstanceFieldName", "工作流实例编号字段名"),
	UserId("$UserId", "用户编号"),
	GroupCondition("$GroupCondition", "用户组/角色查询条件"),
	UserRoleIds("$UserRoleIds", "角色编号"),
;

    EQueryScopeKey(String key, String stringValue) {
        this.key = key;
        this.stringValue = stringValue;
    }

    private final String key;

    private final String stringValue;

    public String getKey() {
        return key;
    }

    public String getStringValue() {
        return stringValue;
    }
}
