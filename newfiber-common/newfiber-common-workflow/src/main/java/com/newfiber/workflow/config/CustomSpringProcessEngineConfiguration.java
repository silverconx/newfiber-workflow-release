package com.newfiber.workflow.config;

import com.newfiber.workflow.enums.EDataSourceType;
import java.util.Properties;
import org.flowable.spring.SpringProcessEngineConfiguration;

/**
 * @author : X.K
 * @since : 2023/7/10 下午5:28
 */
public class CustomSpringProcessEngineConfiguration extends SpringProcessEngineConfiguration {

	public CustomSpringProcessEngineConfiguration() {
		super();

		Properties databaseTypeMappings = getDefaultDatabaseTypeMappings();
		databaseTypeMappings.setProperty(EDataSourceType.KingbaseES.getKey(), DATABASE_TYPE_POSTGRES);
		databaseTypeMappings.setProperty(EDataSourceType.DM.getKey(), DATABASE_TYPE_ORACLE);
		super.databaseTypeMappings = databaseTypeMappings;
	}
}
