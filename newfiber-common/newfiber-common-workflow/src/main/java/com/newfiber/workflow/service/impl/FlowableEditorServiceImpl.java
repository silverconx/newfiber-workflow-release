package com.newfiber.workflow.service.impl;

import cn.hutool.core.io.IoUtil;
import com.alibaba.fastjson2.JSONObject;
import com.newfiber.workflow.service.FlowableEditorService;
import java.io.InputStream;
import lombok.extern.slf4j.Slf4j;
import org.flowable.common.engine.api.FlowableException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class FlowableEditorServiceImpl implements FlowableEditorService {

    @Override
    public Object stencilset() {
        InputStream stencilsetStream = this.getClass().getClassLoader().getResourceAsStream("stencilset.json.zh-cn");
        try {
            if(null != stencilsetStream){
                String result = IoUtil.readUtf8(stencilsetStream);
                return JSONObject.parse(result);
            }else{
                return "";
            }
        } catch (Exception e) {
            log.error("加载BPMN按钮失败：{}-->{}", e.getMessage(), e.getStackTrace());
            throw new FlowableException(String.format("加载BPMN按钮失败【%s】", e.getMessage()));
        }
    }
}
