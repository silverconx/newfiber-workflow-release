package com.newfiber.workflow.controller;

import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.workflow.entity.WorkflowHistoricActivity;
import com.newfiber.workflow.entity.WorkflowUser;
import com.newfiber.workflow.entity.request.WorkflowCountStasticRequest;
import com.newfiber.workflow.entity.request.WorkflowHistoryActivityListRequest;
import com.newfiber.workflow.entity.request.WorkflowTodoBusinessExecutorListRequest;
import com.newfiber.workflow.entity.request.WorkflowTodoBusinessKeyListRequest;
import com.newfiber.workflow.service.FlowableProcessService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(value = "WF04-工作流流程管理", tags = "WF04-工作流流程管理")
@RequestMapping("/workflow-process")
public class WorkflowProcessController extends BaseController {

    @Resource
    private FlowableProcessService flowableProcessService;

	@ApiOperation(value = "获取流程节点进程图")
	@GetMapping(value = "diagram-view/{workflowInstanceId}")
	public void diagramView(@PathVariable("workflowInstanceId") String workflowInstanceId, HttpServletResponse httpServletResponse) {
		flowableProcessService.diagram(workflowInstanceId, httpServletResponse);
	}

	@ApiOperation(value = "列表查询待办业务的可执行人")
    @PostMapping(value = "list-todo-business-executor")
    public Result<List<WorkflowUser>> listTodoBusinessExecutor(@Valid @RequestBody WorkflowTodoBusinessExecutorListRequest request) {
        return success(flowableProcessService
	        .listTodoBusinessExecutor(request.getWorkflowKey(), request.getBusinessKey()));
    }

    @ApiOperation(value = "列表查询待办业务编号")
    @PostMapping(value = "list-todo-businessKey")
    public Result<List<String>> listTodoBusinessKey(@Valid @RequestBody WorkflowTodoBusinessKeyListRequest request) {
        return success(flowableProcessService.listTodoBusinessKey(
                request.getUserId(), request.getGroupId(), request.getWorkflowKey(), request.getTaskKey()));
    }

    @ApiOperation(value = "列表查询历史活动记录")
    @PostMapping(value = "list-history-activity")
    public Result<List<WorkflowHistoricActivity>> listHistoryActivity(@Valid @RequestBody WorkflowHistoryActivityListRequest request) {
        return success(flowableProcessService.listHistoricActivity(
                request.getWorkflowKey(), request.getBusinessKey(), request.getWorkflowUserId(), request.getStatus(), request.getFileRefFieldPattern()));
    }

	@ApiOperation(value = "工作流任务各状态数量统计")
	@PostMapping(value = "workflow-count-statics")
	public Result<Object> workflowCountStatics(@Valid @RequestBody WorkflowCountStasticRequest request) {
		return success(flowableProcessService.workflowCountStatics(
			request.getWorkflowKey(), request.getWorkflowUserId(), request.getStatus(), request.getQueryField(), request.getQueryFieldValue()));
	}


}
