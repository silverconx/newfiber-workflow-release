package com.newfiber.workflow.entity.request;

import com.newfiber.common.core.web.request.BasePageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WorkflowModelPageRequest extends BasePageRequest {

    /**
     * 名称
     */
    @ApiModelProperty(name = "name", value = "名称")
    private String name;

}
