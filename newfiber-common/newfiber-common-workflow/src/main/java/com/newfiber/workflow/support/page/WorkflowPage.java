package com.newfiber.workflow.support.page;

import com.newfiber.workflow.support.IWorkflowCallback;
import com.newfiber.workflow.support.request.WorkflowPageRequest;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WorkflowPage {

    /**
     * 用户编号
     */
    private String userId;

	/**
	 * TODO 用户角色编号
	 */
	private List<String> userRoleIdList;

    /**
     * 任务主键/状态
     */
    private String taskKey;

	/**
	 * 查询范围（EQueryScope）
	 */
	private String queryScope;

	/**
	 * 分页策略
	 */
	private EWorkflowPageStrategy workflowPageStrategy = EWorkflowPageStrategy.IdIn;

    /**
     * 回调接口
     */
    private IWorkflowCallback<?> workflowCallback;

	public static WorkflowPage build(WorkflowPageRequest pageReq, IWorkflowCallback<?> workflowCallback){
		WorkflowPage workflowPage = new WorkflowPage();
		workflowPage.setUserId(pageReq.getWorkflowUserId());
		workflowPage.setUserRoleIdList(pageReq.getWorkflowUserRoleIdList());
//		workflowPage.setTaskKey(pageReq.getStatus());
		workflowPage.setQueryScope(pageReq.getQueryScope());
		workflowPage.setWorkflowCallback(workflowCallback);
		return workflowPage;
	}

	public static WorkflowPage build(WorkflowPageRequest pageReq, EWorkflowPageStrategy workflowPageStrategy, IWorkflowCallback<?> workflowCallback){
		WorkflowPage workflowPage = build(pageReq, workflowCallback);
		workflowPage.setWorkflowPageStrategy(workflowPageStrategy);
		return workflowPage;
	}
}
