package com.newfiber.workflow.support.request.check;

/**
 * @author xiongkai
 */
public enum ENextTaskApproveUserCheckType {

    /**
     *
     */
    User("User", "用户"),
	Users("Users", "多用户"),
	Role("Role", "角色"),

;

    ENextTaskApproveUserCheckType(String key, String stringValue) {
        this.key = key;
        this.stringValue = stringValue;
    }

    private final String key;

    private final String stringValue;

    public String getKey() {
        return key;
    }

    public String getStringValue() {
        return stringValue;
    }
}
