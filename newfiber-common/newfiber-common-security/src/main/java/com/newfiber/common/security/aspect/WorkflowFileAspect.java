package com.newfiber.common.security.aspect;

import cn.hutool.core.util.ReflectUtil;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.system.api.RemoteFileService;
import com.newfiber.system.api.domain.SysFile;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author : X.K
 * @since : 2023/3/17 下午2:07
 */
@Aspect
@Component
public class WorkflowFileAspect {

	@Autowired(required = false)
	private RemoteFileService remoteFileService;

	@AfterReturning(pointcut = "@annotation(com.newfiber.common.core.annotation.WorkflowFileWrapper)", returning = "workflowHistoricActivityList")
	public void around(JoinPoint joinPoint, List<?> workflowHistoricActivityList) throws Throwable{
		if(CollectionUtils.isEmpty(workflowHistoricActivityList) || null == remoteFileService){
			return;
		}

		// 业务编号
		Long businessKey = Long.parseLong(ReflectUtil.getFieldValue(workflowHistoricActivityList.get(0), EWorkflowHistoricActivityField.BusinessKey.code).toString());

		// 查询匹配模式
		String fileRefFieldPattern = ReflectUtil.getFieldValue(workflowHistoricActivityList.get(0), EWorkflowHistoricActivityField.FileRefFieldPattern.code).toString();

		// 文件列表
		Result<List<SysFile>> businessFileListResult = remoteFileService.list(businessKey);
		List<SysFile> businessFileList = businessFileListResult.getData();

		if(CollectionUtils.isEmpty(businessFileList)){
			return;
		}

		for(Object workflowHistoricActivity : workflowHistoricActivityList){
			// 节点状态
			String taskStatus = ReflectUtil.getFieldValue(workflowHistoricActivity, EWorkflowHistoricActivityField.ActivityId.code).toString();

			// 过滤文件
			List<SysFile> activityFileList = businessFileList.stream().filter(t -> t.getRefField().startsWith(taskStatus)).collect(Collectors.toList());
			if(StringUtils.isNotBlank(fileRefFieldPattern)){
				activityFileList = activityFileList.stream().filter(t -> t.getRefField().contains(fileRefFieldPattern)).collect(Collectors.toList());
			}

			ReflectUtil.setFieldValue(workflowHistoricActivity, EWorkflowHistoricActivityField.AttachFileList.code, activityFileList);
		}
	}

	enum EWorkflowHistoricActivityField {

		/**
		 *
		 */
		BusinessKey("businessKey", "业务数据编号"),
		AttachFileList("attachFileList", "附件"),
		ActivityId("activityId", "活动编号/节点状态"),
		FileRefFieldPattern("fileRefFieldPattern", "查询关联文件匹配符（例refField like '%picture%', 则传picture）"),

		;

		EWorkflowHistoricActivityField(String code, String value) {
			this.code = code;
			this.value = value;
		}

		private final String code;

		private final String value;

		public String getCode() {
			return code;
		}

		public String getValue() {
			return value;
		}

	}
}
