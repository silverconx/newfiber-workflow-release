package com.newfiber.common.security.utils;

import com.newfiber.common.core.utils.SpringUtils;
import com.newfiber.common.redis.service.RedisService;
import com.newfiber.system.api.enums.ESystemRedisKey;

/**
 * 配置工具类
 * 
 * @author newfiber
 */
public class ConfigUtils {

    /**
     * 获取配置缓存
     */
    public static <T> T getConfig(final String key){
    	RedisService redisService = SpringUtils.getBean(RedisService.class);
	    return redisService.getCacheObject(ESystemRedisKey.Config, key);
    }

}
