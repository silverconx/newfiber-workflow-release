package com.newfiber.common.security.utils;

import com.alibaba.fastjson2.JSONArray;
import com.newfiber.common.core.utils.SpringUtils;
import com.newfiber.common.core.utils.StringUtils;
import com.newfiber.common.redis.service.RedisService;
import com.newfiber.system.api.domain.SysDictData;
import com.newfiber.system.api.enums.ESystemRedisKey;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.apache.commons.collections4.CollectionUtils;

/**
 * 字典工具类
 * 
 * @author newfiber
 */
public class DictUtils{
    /**
     * 设置字典缓存
     * 
     * @param key 参数键
     * @param dictDatas 字典数据列表
     */
    public static void setDictCache(String key, List<SysDictData> dictDatas){
        SpringUtils.getBean(RedisService.class).setCacheObject(ESystemRedisKey.Dict, key, dictDatas);
    }

    /**
     * 获取字典缓存
     * 
     * @param key 参数键
     * @return dictDatas 字典数据列表
     */
    public static List<SysDictData> getDictCache(String key){
        JSONArray arrayCache = SpringUtils.getBean(RedisService.class).getCacheObject(ESystemRedisKey.Dict, key);
        if (StringUtils.isNotNull(arrayCache)){
            return arrayCache.toList(SysDictData.class);
        }
        return Collections.emptyList();
    }

	/**
	 * 获取字典缓存
	 *
	 * @param key 参数键
	 * @return dictDatas 字典数据列表
	 */
	public static String getDictCache(String key, String value){
		List<SysDictData> sysDictDataList = getDictCache(key);
		if(CollectionUtils.isNotEmpty(sysDictDataList)){
			Optional<SysDictData> optional = sysDictDataList.stream().filter(t -> t.getDictValue().equals(value)).findAny();
			if(optional.isPresent()){
				return optional.get().getDictLabel();
			}
		}
		return "";
	}

    /**
     * 删除指定字典缓存
     * 
     * @param key 字典键
     */
    public static void removeDictCache(String key){
        SpringUtils.getBean(RedisService.class).deleteObject(ESystemRedisKey.Dict, key);
    }

    /**
     * 清空字典缓存
     */
    public static void clearDictCache(){
        Collection<String> keys = SpringUtils.getBean(RedisService.class).keys(ESystemRedisKey.Dict);
        SpringUtils.getBean(RedisService.class).deleteObject(keys);
    }
}
