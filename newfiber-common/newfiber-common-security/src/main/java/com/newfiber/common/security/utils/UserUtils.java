package com.newfiber.common.security.utils;

import com.newfiber.common.core.utils.SpringUtils;
import com.newfiber.common.redis.service.RedisService;
import com.newfiber.system.api.domain.SysUser;
import com.newfiber.system.api.enums.ESystemRedisKey;

/**
 *
 * @author newfiber
 */
public class UserUtils {

    /**
     * 获取配置缓存
     */
    public static SysUser getUser(final Long userId){
    	RedisService redisService = SpringUtils.getBean(RedisService.class);
	    return redisService.getCacheObject(ESystemRedisKey.User, userId.toString());
    }

}
