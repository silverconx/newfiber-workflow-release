package com.newfiber.common.security.interceptor;

import com.newfiber.common.core.constant.SecurityConstants;
import com.newfiber.common.core.context.SecurityContextHolder;
import com.newfiber.common.core.utils.StringUtils;
import com.newfiber.common.security.auth.AuthUtil;
import com.newfiber.common.security.utils.SecurityUtils;
import com.newfiber.system.api.model.LoginUser;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.AsyncHandlerInterceptor;

/**
 * 自定义请求头拦截器，将Header数据封装到线程变量中方便获取
 * 注意：此拦截器会同时验证当前用户有效期自动刷新有效期
 *
 * @author newfiber
 */
public class HeaderInterceptor implements AsyncHandlerInterceptor{
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception{
        if (!(handler instanceof HandlerMethod)){
            return true;
        }

        String token = SecurityUtils.getToken();
        if (StringUtils.isNotEmpty(token)){
            LoginUser loginUser = AuthUtil.getLoginUser(token);
            if (StringUtils.isNotNull(loginUser)){
                AuthUtil.verifyLoginUserExpire(loginUser);
                SecurityContextHolder.set(SecurityConstants.LOGIN_USER, loginUser);

	            SecurityContextHolder.setUserId(loginUser.getUserid().toString());
	            SecurityContextHolder.setUserName(loginUser.getUsername());
	            SecurityContextHolder.setNickName(loginUser.getSysUser().getNickName());
                SecurityContextHolder.setUserDeptId(loginUser.getSysUser().getDeptId());
            }
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception{
        SecurityContextHolder.remove();
    }
}
