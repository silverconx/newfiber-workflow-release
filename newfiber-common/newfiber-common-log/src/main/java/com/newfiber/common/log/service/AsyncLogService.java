package com.newfiber.common.log.service;

import com.newfiber.common.core.constant.SecurityConstants;
import com.newfiber.system.api.RemoteLogService;
import com.newfiber.system.api.domain.SysOperLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * 异步调用日志服务
 * 
 * @author newfiber
 */
@Service
public class AsyncLogService{

    @Autowired(required = false)
    private RemoteLogService remoteLogService;

    /**
     * 保存系统日志记录
     */
    @Async
    public void saveSysLog(SysOperLog sysOperLog){
        remoteLogService.saveLog(sysOperLog, SecurityConstants.INNER);
    }
}
