package com.newfiber.common.log.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import com.newfiber.common.log.enums.BusinessType;
import com.newfiber.common.log.enums.OperatorType;

/**
 * 自定义操作日志记录注解
 * 
 * @author newfiber
 *
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log{
    /**
     * 模块
     */
    public String title() default "";

    /**
     * 功能
     */
    public BusinessType businessType() default BusinessType.OTHER;

    /**
     * 操作人类别
     */
    public OperatorType operatorType() default OperatorType.MANAGE;

    /**
     * 是否保存请求的参数
     */
    public boolean isSaveRequestData() default true;

    /**
     * 是否保存响应的参数
     */
    public boolean isSaveResponseData() default true;

    // 日志内容如：用户:{0},修改了名称为:{1},年龄:{2}
    String content() default "-";

    // 参数名通过逗号分割如：id, name, age
    String params() default "";


    /**
     * 示例
     * @Log(content = "新增资料类型:{0},备注:{1}",params = "DocumentTypeSaveRequest.typeName,DocumentTypeSaveRequest.remark")
     * */

}
