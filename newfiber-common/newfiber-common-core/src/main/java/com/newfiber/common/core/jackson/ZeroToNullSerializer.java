package com.newfiber.common.core.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;

public class ZeroToNullSerializer extends JsonSerializer<Number> {

	@Override
	public void serialize(Number o, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
		if(o.longValue() == 0L){
			jsonGenerator.writeString("");
		}else{
			jsonGenerator.writeString(o.toString());
		}
	}
}
