package com.newfiber.common.core.exception;

/**
 * 权限异常
 * 
 * @author newfiber
 */
public class PreAuthorizeException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public PreAuthorizeException(){
    }
}
