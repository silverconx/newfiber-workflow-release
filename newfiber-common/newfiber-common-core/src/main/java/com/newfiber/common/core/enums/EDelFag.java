package com.newfiber.common.core.enums;

/**
 * @author : Silver
 * @since : 2019-01-11 22:10
 */
public enum EDelFag {

	/** 删除标志（0代表存在 2代表删除） */

    NO("0", "否"),

    YES("2", "是");

    EDelFag(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public Integer getIntegerCode(){
        return Integer.valueOf(code);
    }

    public String getValue() {
        return value;
    }

}
