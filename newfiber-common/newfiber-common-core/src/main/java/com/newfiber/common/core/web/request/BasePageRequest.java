package com.newfiber.common.core.web.request;

import com.newfiber.common.core.web.page.PageDomain;
import com.newfiber.common.core.web.page.PageSupport;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author : X.K
 * @since : 2023/2/17 上午9:54
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BasePageRequest extends BaseQueryRequest{

	PageDomain pageDomain = PageSupport.getPageDomain();

	/**
	 * 起始页
	 */
	@ApiModelProperty(name = "pageNum", value = "起始页")
	@NotNull(message = "起始页不能为空")
	@Min(value = 0, message = "起始页要大于0")
	private Integer pageNum;

	/**
	 * 每页数量
	 */
	@ApiModelProperty(name = "pageSize", value = "每页数量")
	@NotNull(message = "每页数量不能为空")
	@Min(value = 0, message = "每页数量要大于0")
	private Integer pageSize;

	public Integer pageStart(){
		return pageNum * pageSize;
	}

	public Integer getPageNum() {
		return null == pageNum ? pageDomain.getPageNum() : pageNum;
	}

	public Integer getPageSize() {
		return null == pageSize ? pageDomain.getPageSize() : pageSize;
	}
}
