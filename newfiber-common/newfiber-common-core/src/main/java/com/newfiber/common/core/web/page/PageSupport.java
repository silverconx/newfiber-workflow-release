package com.newfiber.common.core.web.page;

import cn.hutool.core.util.NumberUtil;
import com.github.pagehelper.PageInfo;
import com.newfiber.common.core.constant.HttpStatus;
import com.newfiber.common.core.text.Convert;
import com.newfiber.common.core.utils.ServletUtils;
import java.util.List;

/**
 * 表格数据处理
 * 
 * @author newfiber
 */
public class PageSupport{
    /**
     * 当前记录起始索引
     */
    public static final String PAGE_NUM = "pageNum";

    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "pageSize";

    /**
     * 排序列
     */
    public static final String ORDER_BY_COLUMN = "orderByColumn";

    /**
     * 排序的方向 "desc" 或者 "asc".
     */
    public static final String IS_ASC = "isAsc";

    /**
     * 分页参数合理化
     */
    public static final String REASONABLE = "reasonable";

    /**
     * 封装分页对象
     */
    public static PageDomain getPageDomain(){
        PageDomain pageDomain = new PageDomain();
        pageDomain.setPageNum(Convert.toInt(ServletUtils.getParameter(PAGE_NUM), 1));
        pageDomain.setPageSize(Convert.toInt(ServletUtils.getParameter(PAGE_SIZE), 10));
        pageDomain.setOrderByColumn(ServletUtils.getParameter(ORDER_BY_COLUMN));
        pageDomain.setIsAsc(ServletUtils.getParameter(IS_ASC));
        pageDomain.setReasonable(ServletUtils.getParameterToBool(REASONABLE));
        return pageDomain;
    }

	/**
	 * 响应请求分页数据
	 */
	public static <T> PageResult<List<T>> pageResult(List<T> list){
		PageResult<List<T>> rspData = new PageResult();
		rspData.setCode(HttpStatus.SUCCESS);
		rspData.setData(list);
		rspData.setMsg("查询成功");
		rspData.setTotal(NumberUtil.parseInt(String.valueOf(new PageInfo(list).getTotal())));
		return rspData;
	}

	public static PageDomain buildPageRequest(){
        return getPageDomain();
    }
}
