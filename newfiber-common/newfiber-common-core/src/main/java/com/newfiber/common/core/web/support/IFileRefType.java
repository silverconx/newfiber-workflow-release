package com.newfiber.common.core.web.support;

/**
 * @author : X.K
 * @since : 2023/2/28 下午3:01
 */
public interface IFileRefType {

	/**
	 * @return 关联类型
	 */
	String getRefType();

	/**
	 * @return 关联字段
	 */
	String getRefField();

	/**
	 * @return 关联类型
	 */
	String getRefTypeName();

}
