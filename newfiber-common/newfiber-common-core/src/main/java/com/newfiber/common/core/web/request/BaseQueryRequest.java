package com.newfiber.common.core.web.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : X.K
 * @since : 2023/2/17 上午9:54
 */
@Data
public class BaseQueryRequest {

	@ApiModelProperty(value = "id")
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private Long id;

	@ApiModelProperty(value = "排序字段")
	private String orderBy = "id desc";

	@ApiModelProperty(value = "状态")
	private String status;

	@ApiModelProperty(value = "数据权限")
	private String dataScope;

}
