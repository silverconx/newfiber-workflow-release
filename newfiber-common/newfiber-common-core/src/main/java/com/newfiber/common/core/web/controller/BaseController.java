package com.newfiber.common.core.web.controller;

import com.newfiber.common.core.utils.DateUtils;
import com.newfiber.common.core.utils.PageUtils;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.core.web.page.PageSupport;
import java.beans.PropertyEditorSupport;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

/**
 * web层通用数据处理
 * 
 * @author newfiber
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class BaseController{
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder(WebDataBinder binder){
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport(){
            @Override
            public void setAsText(String text){
                setValue(DateUtils.parseDate(text));
            }
        });
    }

    /**
     * 设置请求分页数据
     */
    protected void startPage(){
        PageUtils.startPage();
    }

    /**
     * 清理分页的线程变量
     */
    protected void clearPage(){
        PageUtils.clearPage();
    }

    /**
     * 响应请求分页数据
     */
    protected <T> PageResult<List<T>> pageResult(List<T> list){
	    return PageSupport.pageResult(list);
    }

    /**
     * 返回成功
     */
    public Result<String> success(){
        return Result.success();
    }


    /**
     * 返回成功消息
     */
    public <T> Result<T> success(T data){
        return Result.success(data);
    }

    /**
     * 返回失败消息
     */
    public Result error(){
        return Result.error();
    }

    /**
     * 返回失败消息
     */
    public <T> Result<T> error(String message){
        return Result.error(message);
    }

    /**
     * 返回警告消息
     */
    public Result warn(String message){
        return Result.warn(message);
    }

    /**
     * 响应返回结果
     * 
     * @param rows 影响行数
     * @return 操作结果
     */
    protected Result<String> result(int rows){
        return rows > 0 ? Result.success() : Result.error();
    }

    /**
     * 响应返回结果
     * 
     * @param result 结果
     * @return 操作结果
     */
    protected Result result(boolean result){
        return result ? success() : error();
    }
}
