package com.newfiber.common.core.web.support;

import com.newfiber.common.core.exception.ServiceException;
import org.apache.commons.lang3.StringUtils;

/**
 * @author : X.K
 * @since : 2023/2/28 下午3:01
 */
public interface IRedisPrefix {

	/**
	 * @return 模块名
	 */
	String getModuleKey();

	/**
	 * @return 模块名
	 */
	String getModuleName();

	default String wrapperRedisKey(String key){
		if(StringUtils.isBlank(getModuleKey())){
			throw new ServiceException("Redis缓存模块名不能为空");
		}

		return getModuleKey() + ":" + key;
	}
}
