//package com.newfiber.common.core.geometry;
//
//import cn.hutool.core.util.ReflectUtil;
//import com.newfiber.common.core.utils.SpringUtils;
//import com.vividsolutions.jts.geom.Geometry;
//import com.vividsolutions.jts.geom.GeometryFactory;
//import com.vividsolutions.jts.geom.PrecisionModel;
//import com.vividsolutions.jts.io.WKBReader;
//import com.vividsolutions.jts.io.WKBWriter;
//import com.vividsolutions.jts.io.WKTReader;
//import com.zaxxer.hikari.pool.HikariProxyConnection;
//import dm.jdbc.driver.DmdbBlob;
//import dm.jdbc.driver.DmdbConnection;
//import java.sql.Blob;
//import java.sql.CallableStatement;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Struct;
//import org.apache.ibatis.session.SqlSessionFactory;
//import org.apache.ibatis.type.BaseTypeHandler;
//import org.apache.ibatis.type.JdbcType;
//import org.apache.ibatis.type.MappedJdbcTypes;
//import org.apache.ibatis.type.MappedTypes;
//import org.jetbrains.annotations.Nullable;
//import org.springframework.stereotype.Component;
//
///**
// * @author : X.K
// * @since : 2023/7/26 下午2:41
// */
//@Component
//@MappedTypes({String.class})
//@MappedJdbcTypes({JdbcType.STRUCT})
//public class DmGeoTypeHandler extends BaseTypeHandler<String> {
//
//	private Connection connection;
//
//	@Override
//	public void setNonNullParameter(PreparedStatement preparedStatement, int i, String s, JdbcType jdbcType) throws SQLException {
//
//		try {
//			Geometry geometry = new WKTReader(new GeometryFactory(new PrecisionModel())).read(s);
//			Connection connection = getConnect();
//
//			// 创建ST_Multipolygon空间类型的struct类型对象
//			Object[] attrs = new Object[3];
//			attrs[0] = geometry.getSRID();
//			attrs[1] = DmdbBlob.newInstanceOfLocal(new WKBWriter().write(geometry), (DmdbConnection)connection);
//			attrs[2] = 6;
//
//			Struct struct = connection.createStruct("ST_MULTIPOLYGON", attrs);
//
//			preparedStatement.setObject(i, struct);
//		}catch (Exception e){
//			e.printStackTrace();
//		}
//
//	}
//
//	@Override
//	public String getNullableResult(ResultSet resultSet, String s) throws SQLException {
//		Object object = resultSet.getObject(s);
//		return getGeometryString((Struct) object);
//	}
//
//	private Connection getConnect(){
//		if(connection == null){
//			connection = SpringUtils.getBean(SqlSessionFactory.class).openSession().getConnection();
//			if(connection instanceof HikariProxyConnection){
//				Object poolEntry = ReflectUtil.getFieldValue(connection, "poolEntry");
//				connection = (Connection) ReflectUtil.getFieldValue(poolEntry, "connection");
//			}
//		}
//		return connection;
//	}
//
//	@Override
//	public String getNullableResult(ResultSet resultSet, int i) throws SQLException {
//		Object object = resultSet.getObject(i);
//		return getGeometryString((Struct) object);
//	}
//
//	@Nullable
//	private String getGeometryString(Struct object) throws SQLException {
//		if(null == object){
//			return null;
//		}
//
//		Object[] attrs = object.getAttributes();
//		Blob blob = (Blob) attrs[1];
//
//		// 解析GEO_WKB属性信息
//		try {
//			WKBReader reader = new WKBReader();
//			Geometry geometry = reader.read(blob.getBytes(1, (int) blob.length()));
//			return geometry.toString();
//		} catch (com.vividsolutions.jts.io.ParseException e) {
//			e.printStackTrace();
//		}
//
//		return null;
//	}
//
//	@Override
//	public String getNullableResult(CallableStatement callableStatement, int i) throws SQLException {
//		return "";
//	}
//}
