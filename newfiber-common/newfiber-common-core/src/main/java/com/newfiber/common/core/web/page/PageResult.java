package com.newfiber.common.core.web.page;

import com.newfiber.common.core.constant.HttpStatus;
import com.newfiber.common.core.web.domain.Result;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 表格分页数据对象
 * 
 * @author newfiber
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PageResult<T> extends Result<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 总记录数 */
    private Integer total = 0;

    /**
     * 表格数据对象
     */
    public PageResult(){
    }

	/**
	 * 分页
	 *
	 * @param data 列表数据
	 */
	public PageResult(T data){
		this.data = data;
	}

    /**
     * 分页
     * 
     * @param data 列表数据
     * @param total 总记录数
     */
    public PageResult(T data, int total){
        this.data = data;
        this.total = total;
		this.code = HttpStatus.SUCCESS;
		this.msg="查询成功";
    }

	/**
	 * 分页
	 *
	 * @param data 列表数据
	 * @param total 总记录数
	 */
	public PageResult(T data, Long total){
		this.data = data;
		this.total = Integer.parseInt(total.toString());
		this.code = HttpStatus.SUCCESS;
		this.msg="查询成功";
	}

}