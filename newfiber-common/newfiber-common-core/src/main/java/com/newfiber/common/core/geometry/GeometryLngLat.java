package com.newfiber.common.core.geometry;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author : X.K
 * @since : 2023/3/15 下午8:02
 */
@Data
@AllArgsConstructor
public class GeometryLngLat {

	private String lng;

	private String lat;

	private Long time;

	public GeometryLngLat(String lng, String lat) {
		this.lng = lng;
		this.lat = lat;
	}

	public Long getTime() {
		if(null == time){
			return 5L;
		}
		return time;
	}
}
