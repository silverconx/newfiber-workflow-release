package com.newfiber.common.core.web.domain;

import com.newfiber.common.core.constant.HttpStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 操作消息提醒
 * 
 * @author newfiber
 */
@Data
@NoArgsConstructor
@SuppressWarnings({ "rawtypes", "unchecked" })
public class Result<T>{
    private static final long serialVersionUID = 1L;

	/** 状态码 */
	protected int code;

	/** 返回内容 */
	protected String msg;

	/** 数据对象 */
	protected T data;

	/**
	 * 初始化一个新创建的 AjaxResult 对象
	 *
	 * @param code 状态码
	 * @param msg 返回内容
	 */
	public Result(int code, String msg)
	{
		this.code = code;
		this.msg = msg;
	}

	/**
	 * 初始化一个新创建的 AjaxResult 对象
	 *
	 * @param code 状态码
	 * @param msg 返回内容
	 * @param data 数据对象
	 */
	public Result(int code, String msg, T data)
	{
		this.code = code;
		this.msg = msg;
		this.data = data;
	}


    /**
     * 返回成功消息
     * 
     * @return 成功消息
     */
    public static Result<String> success(){
        return Result.success("操作成功");
    }

    /**
     * 返回成功数据
     * 
     * @return 成功消息
     */
    public static <T> Result success(T data){
        return Result.success("操作成功", data);
    }

    /**
     * 返回成功消息
     * 
     * @param msg 返回内容
     * @return 成功消息
     */
    public static Result<String> success(String msg){
        return Result.success(msg, null);
    }

    /**
     * 返回成功消息
     * 
     * @param msg 返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static Result success(String msg, Object data){
        return new Result(HttpStatus.SUCCESS, msg, data);
    }

    /**
     * 返回警告消息
     *
     * @param msg 返回内容
     * @return 警告消息
     */
    public static Result warn(String msg){
        return Result.warn(msg, null);
    }

    /**
     * 返回警告消息
     *
     * @param msg 返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static Result warn(String msg, Object data){
        return new Result(HttpStatus.WARN, msg, data);
    }

    /**
     * 返回错误消息
     * 
     * @return 错误消息
     */
    public static Result<String> error(){
        return Result.error("操作失败");
    }

    /**
     * 返回错误消息
     * 
     * @param msg 返回内容
     * @return 错误消息
     */
    public static <T> Result<T> error(String msg){
        return Result.error(msg, null);
    }

    /**
     * 返回错误消息
     * 
     * @param msg 返回内容
     * @param data 数据对象
     * @return 错误消息
     */
    public static <T> Result<T> error(String msg, Object data){
        return new Result(HttpStatus.ERROR, msg, data);
    }

    /**
     * 返回错误消息
     * 
     * @param code 状态码
     * @param msg 返回内容
     * @return 错误消息
     */
    public static Result error(int code, String msg){
        return new Result(code, msg, null);
    }

	/**
	 * 返回错误消息
	 *
	 * @param code 状态码
	 * @param msg 返回内容
	 * @return 错误消息
	 */
	public static <T> Result error(int code, String msg, T data){
		return new Result(code, msg, data);
	}
}
