package com.newfiber.common.core.enums;

import lombok.Getter;

/**
 * @author xiongkai
 */
@Getter
public enum EBoolean {

    /**
     */
    False(false, "false", 0, "0"),
    True(true, "true", 1, "1"),

;

    EBoolean(boolean key, String boolString, int intValue, String stringValue) {
        this.key = key;
        this.boolString = boolString;
        this.intValue = intValue;
        this.stringValue = stringValue;
    }

    public static EBoolean reverse(String stringValue){
    	if(True.stringValue.equals(stringValue)){
    		return False;
	    }else{
		    return True;
	    }
    }

	public boolean getKey() {
		return key;
	}

	private final boolean key;

	private final String boolString;

	private final int intValue;

    private final String stringValue;

    public Long getLongValue(){
    	return Long.parseLong(stringValue);
    }
}
