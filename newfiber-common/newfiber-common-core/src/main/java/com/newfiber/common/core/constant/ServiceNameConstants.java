package com.newfiber.common.core.constant;

/**
 * 服务名称
 *
 * @author newfiber
 */
public class ServiceNameConstants {

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "newfiber-system";

    /**
     * 业务模块的serviceid
     */
    public static final String BUSINESS_SERVICE = "newfiber-business";

	/**
	 * 海绵业务模块的serviceid
	 */
	public static final String SPONGE_SERVICE = "newfiber-sponge";

    /**
     * webgis业务模块的serviceid
     */
    public static final String WEBGIS_SERVICE = "newfiber-webgis";

}
