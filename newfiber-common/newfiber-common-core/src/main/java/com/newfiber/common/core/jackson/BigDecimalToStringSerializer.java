package com.newfiber.common.core.jackson;

import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializerBase;

import java.math.BigDecimal;

@JacksonStdImpl
public class BigDecimalToStringSerializer extends ToStringSerializerBase {

    public static final BigDecimalToStringSerializer instance = new BigDecimalToStringSerializer();

    public BigDecimalToStringSerializer() {
        super(Object.class);
    }
    public BigDecimalToStringSerializer(Class<?> handledType) {
        super(handledType);
    }

    @Override
    public String valueToString(Object o) {
        if(null == o){
            return "";
        }
        BigDecimal num=(BigDecimal) o;
        if(BigDecimal.ZERO.compareTo(num)== 0 ){
            return BigDecimal.ZERO.toPlainString();
        }
        return num.stripTrailingZeros().toPlainString();
    }

}
