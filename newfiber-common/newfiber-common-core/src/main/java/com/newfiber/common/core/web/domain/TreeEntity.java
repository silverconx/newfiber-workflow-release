package com.newfiber.common.core.web.domain;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 * Tree基类
 * 
 * @author newfiber
 */
@Data
public class TreeEntity {

	private Long id;

	private String name;

    private String code;

    private String assistField1;

    private List<TreeEntity> children = new ArrayList<>();

}
