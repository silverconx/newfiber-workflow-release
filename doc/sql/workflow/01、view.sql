---------ACT_ID_USER
CREATE
    ALGORITHM = UNDEFINED
    DEFINER = `root`@`%`
    SQL SECURITY DEFINER
VIEW `ACT_ID_USER` AS
    SELECT
        `newfiber_standard`.`sys_users`.`user_name` AS `ID_`,
        1 AS `REV_`,
        `newfiber_standard`.`sys_users`.`nick_name` AS `FIRST_`,
        `newfiber_standard`.`sys_users`.`nick_name` AS `LAST_`,
        `newfiber_standard`.`sys_users`.`nick_name` AS `DISPLAY_NAME_`,
        `newfiber_standard`.`sys_users`.`email` AS `EMAIL_`,
        '123456' AS `PWD_`,
        NULL AS `PICTURE_ID_`,
        NULL AS `TENANT_ID_`
    FROM
        `newfiber_standard`.`sys_users`
    WHERE
        (`newfiber_standard`.`sys_users`.`del_flag` = 0)

---------ACT_ID_MEMBERSHIP
CREATE
    ALGORITHM = UNDEFINED
    DEFINER = `root`@`%`
    SQL SECURITY DEFINER
VIEW `ACT_ID_MEMBERSHIP` AS
    SELECT
        `su`.`user_name` AS `USER_ID_`, `t`.`role_id` AS `GROUP_ID_`
    FROM
        (`newfiber_standard`.`sys_user_role` `t`
        LEFT JOIN `newfiber_standard`.`sys_users` `su` ON ((`t`.`user_id` = `su`.`user_id`)));

---------ACT_ID_GROUP
CREATE
    ALGORITHM = UNDEFINED
    DEFINER = `root`@`%`
    SQL SECURITY DEFINER
VIEW `ACT_ID_GROUP` AS
    SELECT
        `newfiber_standard`.`sys_role`.`role_id` AS `ID_`,
        1 AS `REV_`,
        `newfiber_standard`.`sys_role`.`role_name` AS `NAME_`
    FROM
        `newfiber_standard`.`sys_role`
    WHERE
        (`newfiber_standard`.`sys_role`.`del_flag` = 0);
