package com.newfiber.system.controller;

import com.newfiber.common.core.utils.StringUtils;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import com.newfiber.common.redis.service.RedisService;
import com.newfiber.common.security.annotation.RequiresPermissions;
import com.newfiber.system.api.enums.ESystemRedisKey;
import com.newfiber.system.api.model.LoginUser;
import com.newfiber.system.domain.SysUserOnline;
import com.newfiber.system.service.ISysUserOnlineService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 在线用户监控
 * 
 * @author newfiber
 */
@RestController
@RequestMapping("/online")
public class SysUserOnlineController extends BaseController{
    @Autowired
    private ISysUserOnlineService userOnlineService;

    @Autowired
    private RedisService redisService;

    @RequiresPermissions("monitor:online:list")
    @GetMapping("/list")
    public PageResult list(String ipaddr, String userName){
        Collection<String> keys = redisService.keys(ESystemRedisKey.Token);
        List<SysUserOnline> userOnlineList = new ArrayList<SysUserOnline>();
        for (String key : keys){
            LoginUser user = redisService.getCacheObject(key);
            if (StringUtils.isNotEmpty(ipaddr) && StringUtils.isNotEmpty(userName)){
                if (StringUtils.equals(ipaddr, user.getIpaddr()) && StringUtils.equals(userName, user.getUsername())){
                    userOnlineList.add(userOnlineService.selectOnlineByInfo(ipaddr, userName, user));
                }
            }
            else if (StringUtils.isNotEmpty(ipaddr)){
                if (StringUtils.equals(ipaddr, user.getIpaddr())){
                    userOnlineList.add(userOnlineService.selectOnlineByIpaddr(ipaddr, user));
                }
            }
            else if (StringUtils.isNotEmpty(userName)){
                if (StringUtils.equals(userName, user.getUsername())){
                    userOnlineList.add(userOnlineService.selectOnlineByUserName(userName, user));
                }
            }
            else{
                userOnlineList.add(userOnlineService.loginUserToUserOnline(user));
            }
        }
        Collections.reverse(userOnlineList);
        userOnlineList.removeAll(Collections.singleton(null));
        return pageResult(userOnlineList);
    }

    /**
     * 强退用户
     */
    @RequiresPermissions("monitor:online:forceLogout")
    @Log(title = "在线用户", businessType = BusinessType.FORCE)
    @DeleteMapping("/{tokenId}")
    public Result forceLogout(@PathVariable String tokenId){
        redisService.deleteObject(ESystemRedisKey.Token, tokenId);
        return success();
    }
}
