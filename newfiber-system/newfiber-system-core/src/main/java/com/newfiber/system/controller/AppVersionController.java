package com.newfiber.system.controller;

import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.system.domain.AppVersion;
import com.newfiber.system.domain.pgyer.AppVersionRequest;
import com.newfiber.system.domain.pgyer.AppVersionResponse;
import com.newfiber.system.domain.pgyer.AppView;
import com.newfiber.system.service.IAppVersionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * date: 2023/5/27 上午 10:11
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@RestController
@RequestMapping("/version")
@Api(value = "APP版本更新", tags = "APP版本更新")
public class AppVersionController extends BaseController {

    @Autowired
    private IAppVersionService appVersionService;

    @GetMapping("/checkAndUpdate")
    @ApiOperation(value = "检查更新")
    public Result<AppVersion> checkAndUpdate(@RequestParam String version) {
        return success(appVersionService.checkAndUpdate(version));
    }

    @GetMapping("/getCurrentVersionInfo")
    @ApiOperation(value = "获取当前版本信息")
    public Result<AppVersion> getCurrentVersionInfo(@RequestParam String version) {
        return success(appVersionService.getCurrentVersionInfo(version));
    }

	@GetMapping("/pgyerVersion")
	@ApiOperation(value = "获取当前蒲公英APP版本")
	public Result<AppView> pgyerVersion() {
		return success(appVersionService.pgyerVersion());
	}

	@GetMapping("/pgyerVersionCheck")
	@ApiOperation(value = "检测App是否有更新")
	public Result<AppVersionResponse> pgyerVersionCheck(AppVersionRequest request) {
		return success(appVersionService.pgyerVersionCheck(request));
	}
}
