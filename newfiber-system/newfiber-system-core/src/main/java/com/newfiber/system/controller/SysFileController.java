package com.newfiber.system.controller;

import com.newfiber.common.core.domain.R;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import com.newfiber.system.api.domain.SysFile;
import com.newfiber.system.api.domain.request.SysFileSaveRequest;
import com.newfiber.system.domain.request.sysFile.SysFileQueryRequest;
import com.newfiber.system.service.ISysFileService;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件请求处理
 *
 * @author newfiber
 */
@RestController
public class SysFileController extends BaseController {

    @Resource
    private ISysFileService sysFileService;

    @PostMapping("upload")
    @ApiOperation(value = "上传附件", position = 10)
    @Log(title = "附件", businessType = BusinessType.INSERT)
    public R<SysFile> upload(MultipartFile file) {
        return sysFileService.upload(file);
    }

    /**
     * 新增附件
     */
    @PostMapping("upload_ref")
    @ApiOperation(value = "上传附件", position = 10, hidden = true)
    @Log(title = "附件", businessType = BusinessType.INSERT)
    public Result<SysFile> add(@RequestParam String refType, @RequestParam String refField,
                               @RequestParam Long refId, @RequestParam String remark, @RequestParam("file") MultipartFile file) {
        return success(sysFileService.uploadRef(refType, refField, refId, remark, file));
    }

    /**
     * 新增附件
     */
    @PostMapping("add")
    @ApiOperation(value = "新增附件", position = 20, hidden = true)
    @Log(title = "附件", businessType = BusinessType.INSERT)
    public Result<Long> add(@RequestParam("refId") Long refId, @RequestBody SysFileSaveRequest request) {
        return success(sysFileService.insert(refId, request));
    }

    /**
     * 新增附件
     */
    @PostMapping("addBatch")
    @ApiOperation(value = "批量新增附件", position = 30, hidden = true)
    @Log(title = "附件", businessType = BusinessType.INSERT)
    public Result<Boolean> addBatch(@RequestParam("refId") Long refId, @RequestBody List<SysFileSaveRequest> request) {
        return success(sysFileService.insert(null, refId, request));
    }

    /**
     * 新增附件
     */
    @PostMapping("addBatchByType")
    @ApiOperation(value = "批量新增附件", position = 40, hidden = true)
    @Log(title = "附件", businessType = BusinessType.INSERT)
    public Result<Boolean> addBatchByType(@RequestParam("refType") String refType, @RequestParam("refId") Long refId, @RequestBody List<SysFileSaveRequest> request) {
        return success(sysFileService.insert(refType, refId, request));
    }

    /**
     * 删除附件
     */
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除附件", notes = "传入ids(,隔开)", position = 50)
    @Log(title = "附件", businessType = BusinessType.DELETE)
    public Result<Object> remove(@PathVariable String ids) {
        return success(sysFileService.delete(ids));
    }

    /**
     * 更新附件
     */
    @PostMapping("update")
    @ApiOperation(value = "更新附件", position = 60, hidden = true)
    @Log(title = "附件", businessType = BusinessType.UPDATE)
    public Result<Boolean> update(@RequestParam("refType") String refType, @RequestParam("refId") Long refId, @RequestBody List<SysFileSaveRequest> request) {
        return success(sysFileService.update(refType, refId, request));
    }

    /**
     * 详细查询附件
     */
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "详细查询附件", position = 70)
    public Result<SysFile> detail(@PathVariable("id") Long id) {
        return success(sysFileService.selectDetail(id));
    }

    /**
     * 详细查询附件
     */
    @GetMapping(value = "/listByRef/{refId}")
    @ApiOperation(value = "详细查询附件", position = 80)
    public Result<List<SysFile>> list(@PathVariable("refId") Long id) {
        return success(sysFileService.selectList(id));
    }

    /**
     * 分页查询附件
     */
    @GetMapping("/page")
    @ApiOperation(value = "分页查询附件", position = 90)
    public PageResult<List<SysFile>> page(SysFileQueryRequest request) {
        startPage();
        List<SysFile> list = sysFileService.selectPage(request);
        return pageResult(list);
    }

    /**
     * 列表查询附件
     */
    @GetMapping("/list")
    @ApiOperation(value = "列表查询附件", position = 100)
    public Result<List<SysFile>> list(SysFileQueryRequest request) {
        List<SysFile> list = sysFileService.selectList(request);
        return success(list);
    }

    @GetMapping("/selectByRefIds/{refIds}")
    public Result<List<SysFile>> selectByRefIds(@PathVariable(value = "refIds") String refIds){
        List<SysFile> list = sysFileService.selectByRefIds(refIds);
        return success(list);
    }

}