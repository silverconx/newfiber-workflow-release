package com.newfiber.system.domain.request.sysFile;

import com.newfiber.common.core.web.request.BaseQueryRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 附件对象 sys_file
 *
 * @author X.K
 * @date 2023-02-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysFileQueryRequest extends BaseQueryRequest {

    /**
     * 关联类型
     */
    @ApiModelProperty(value = "关联类型")
    private String refType;

    /**
     * 关联字段
     */
    @ApiModelProperty(value = "关联字段")
    private String refField;

    /**
     * 关联编号
     */
    @ApiModelProperty(value = "关联编号")
    private Long refId;

    /**
     * 名称（不带扩展名）
     */
    @ApiModelProperty(value = "名称（不带扩展名）")
    private String name;

    /**
     * 扩展名
     */
    @ApiModelProperty(value = "扩展名")
    private String extension;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    private String status;

}
