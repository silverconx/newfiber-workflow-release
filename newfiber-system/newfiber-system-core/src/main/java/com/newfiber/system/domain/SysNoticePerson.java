package com.newfiber.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 通知公告-已读未读对象 sys_notice_person
 * date: 2023/4/24 下午 08:17
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
@TableName("sys_notice_person")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "通知公告-已读未读", description = "通知公告-已读未读")
public class SysNoticePerson extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 公告ID
     */
    @ApiModelProperty(value = "公告ID")
    private Long noticeId;

    /**
     * 通知人
     */
    @ApiModelProperty(value = "通知人")
    private String noticePerson;

    /**
     * 是否已读（0未读 1已读)
     */
    @ApiModelProperty(value = "是否已读（0未读 1已读)")
    private String readStatus;

    /**
     * 读取时间
     */
    @ApiModelProperty(value = "读取时间")
    private Date readTime;


}
