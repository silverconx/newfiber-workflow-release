package com.newfiber.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * APP菜单收藏对象 sys_app_menu_collect
 * 
 * @author X.K
 * @date 2023-03-29
 */
@Data
@TableName("sys_app_menu_collect")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "APP菜单收藏", description = "APP菜单收藏")
public class AppMenuCollect extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /**
     *  APP菜单编号
     */
    @ApiModelProperty(value = "APP菜单编号")
    private Long appMenuId;

    /**
     *  用户编号
     */
    @ApiModelProperty(value = "用户编号")
    private Long userId;


}
