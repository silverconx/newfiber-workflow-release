package com.newfiber.system.domain.request.region;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * 中国行政地区对象 sys_region
 *
 * @author 张鸿志
 * @date 2023-07-18
 */
@Data
public class RegionSaveRequest{

    /**
     * 
     */
    @ApiModelProperty(value = "")
    private Integer id;

    /**
     * 层级
     */
    @NotNull(message = "层级 不能为空")
    @ApiModelProperty(value = "层级", required = true)
    private Integer level;

    /**
     * 父级行政代码
     */
    @NotNull(message = "父级行政代码 不能为空")
    @ApiModelProperty(value = "父级行政代码", required = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long parentCode;

    /**
     * 行政代码
     */
    @NotNull(message = "行政代码 不能为空")
    @ApiModelProperty(value = "行政代码", required = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long areaCode;

    /**
     * 邮政编码
     */
    @NotNull(message = "邮政编码 不能为空")
    @ApiModelProperty(value = "邮政编码", required = true)
    private Integer zipCode;

    /**
     * 区号
     */
    @NotBlank(message = "区号 不能为空")
    @ApiModelProperty(value = "区号", required = true)
    private String cityCode;

    /**
     * 名称
     */
    @NotBlank(message = "名称 不能为空")
    @ApiModelProperty(value = "名称", required = true)
    private String name;

    /**
     * 简称
     */
    @NotBlank(message = "简称 不能为空")
    @ApiModelProperty(value = "简称", required = true)
    private String shortName;

    /**
     * 组合名
     */
    @NotBlank(message = "组合名 不能为空")
    @ApiModelProperty(value = "组合名", required = true)
    private String mergerName;

    /**
     * 拼音
     */
    @NotBlank(message = "拼音 不能为空")
    @ApiModelProperty(value = "拼音", required = true)
    private String pinyin;

    /**
     * 经度
     */
    @NotNull(message = "经度 不能为空")
    @ApiModelProperty(value = "经度", required = true)
    private BigDecimal lng;

    /**
     * 纬度
     */
    @NotNull(message = "纬度 不能为空")
    @ApiModelProperty(value = "纬度", required = true)
    private BigDecimal lat;

}
