package com.newfiber.system.domain.request.sysNews;

import com.newfiber.common.core.web.request.BaseQueryRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 新闻信息对象 sys_news_info
 *
 * @author newfiber
 * @date 2023-03-31
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class NewsInfoQueryRequest extends BaseQueryRequest {

    /**
     * 新闻标题
     */
    @ApiModelProperty(value = "新闻标题")
    private String newsTitle;

    /**
     * 新闻类别
     */
    @ApiModelProperty(value = "新闻类别")
    private String newsType;

    /**
     * 发布时间
     */
    @ApiModelProperty(value = "发布时间")
    private Date publishTime;

    /**
     * 是否置顶  0 否 1是
     */
    @ApiModelProperty(value = "是否置顶  0 否 1是")
    private Long isTop;

    /**
     * 浏览次数
     */
    @ApiModelProperty(value = "浏览次数")
    private Long browseNum;

    /**
     * 是否显示  0 否 1是
     */
    @ApiModelProperty(value = "是否显示  0 否 1是")
    private Long isShow;

    /**
     * 新闻内容
     */
    @ApiModelProperty(value = "新闻内容")
    private String newsContent;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    private String status;

}
