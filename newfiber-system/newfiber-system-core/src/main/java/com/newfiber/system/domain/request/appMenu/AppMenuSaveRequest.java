package com.newfiber.system.domain.request.appMenu;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;
import lombok.Data;

/**
 * APP菜单对象 sys_app_menu
 *
 * @author X.K
 * @date 2023-03-20
 */
@Data
public class AppMenuSaveRequest{

    /**
     * 父级菜单
     */
    @ApiModelProperty(value = "父级菜单")
    private Long parentId;

    /**
     * 菜单类型
     */
    @ApiModelProperty(value = "菜单类型")
    private String category;

    /**
     * 菜单编号
     */
    @NotBlank
    @ApiModelProperty(value = "菜单编号", required = true)
    private String code;

    /**
     * 菜单名称
     */
    @NotBlank
    @ApiModelProperty(value = "菜单名称", required = true)
    private String name;

    /**
     * 请求地址
     */
    @NotBlank
    @ApiModelProperty(value = "请求地址")
    private String path;

    /**
     * 图片地址
     */
    @ApiModelProperty(value = "图片地址")
    private String picUrl;

    /**
     * 图片地址（选中）
     */
    @ApiModelProperty(value = "图片地址（选中）")
    private String picUrlChecked;

    /**
     * 是否隐藏（0否 1是）
     */
    @ApiModelProperty(value = "是否隐藏（0否 1是）")
    private String isHide;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序")
    private Long sort;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

}
