package com.newfiber.system.service;

import com.newfiber.common.core.domain.R;
import com.newfiber.system.api.domain.SysFile;
import com.newfiber.system.api.domain.request.SysFileSaveRequest;
import com.newfiber.system.domain.request.sysFile.SysFileQueryRequest;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

/**
 * 附件Service接口
 *
 * @author X.K
 * @date 2023-02-22
 */
public interface ISysFileService {

    /**
     * 新增附件
     *
     * @return 结果
     */
    R<SysFile> upload(MultipartFile file);

    /**
     * 新增附件
     *
     * @return 结果
     */
    SysFile uploadRef(String refType, String refField, Long refId, String remark, MultipartFile file);

    /**
     * 新增附件
     *
     * @param request 新增参数
     * @return 结果
     */
    long insert(Long refId, SysFileSaveRequest request);

    /**
     * 新增附件
     *
     * @param request 新增参数
     * @return 结果
     */
    boolean insert(String refType, Long refId, List<SysFileSaveRequest> request);

    /**
     * 批量删除附件
     *
     * @param ids 编号(,隔开)
     * @return 结果
     */
    boolean delete(String ids);

    /**
     * 更新附件
     *
     * @author: LuFan
     * @since JDK 1.8
     */
    boolean update(String refType, Long refId, List<SysFileSaveRequest> request);

    /**
     * 详细查询附件
     *
     * @param id 主键
     * @return 附件
     */
    SysFile selectDetail(Long id);

    /**
     * 分页查询附件
     *
     * @param request 分页参数
     * @return 附件集合
     */
    List<SysFile> selectPage(SysFileQueryRequest request);

    /**
     * 列表查询附件
     *
     * @param request 列表参数
     * @return 附件集合
     */
    List<SysFile> selectList(SysFileQueryRequest request);

    /**
     * 列表查询附件
     *
     * @return 附件集合
     */
    List<SysFile> selectList(Long refId);

    /**
     * 根据ref id  批量查询附件数据
     *
     * @return 附件集合
     */
    List<SysFile> selectByRefIds(String refIds);
}
