package com.newfiber.system.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.newfiber.common.core.web.domain.BaseEntity;
import com.newfiber.common.core.exception.ServiceException;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.service.BaseServiceImpl;
import com.newfiber.common.security.utils.SecurityUtils;
import com.newfiber.system.api.RemoteFileService;
import com.newfiber.system.api.domain.SysFile;
import com.newfiber.system.domain.NewsInfo;
import com.newfiber.system.domain.request.sysNews.NewsInfoQueryRequest;
import com.newfiber.system.domain.request.sysNews.NewsInfoSaveRequest;
import com.newfiber.system.domain.request.sysNews.NewsInfoUpdateRequest;
import com.newfiber.system.mapper.NewsInfoMapper;
import com.newfiber.system.service.INewsInfoService;
import com.newfiber.system.service.ISysFileService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

/**
 * 新闻信息Service业务层处理
 *
 * @author newfiber
 * @date 2023-03-31
 */
@Service
public class NewsInfoServiceImpl extends BaseServiceImpl<NewsInfoMapper, NewsInfo> implements INewsInfoService {

    @Resource
    private NewsInfoMapper newsInfoMapper;

    @Resource
    private RemoteFileService remoteFileService;

    @Resource
    private ISysFileService sysFileService;






    @Override
    public long insert(NewsInfoSaveRequest request) {
        NewsInfo newsInfo = new NewsInfo();
        BeanUtils.copyProperties(request, newsInfo);
        newsInfo.setCreateBy(SecurityUtils.getUsername());

        save(newsInfo);
        // 文件
        if (CollectionUtils.isNotEmpty(request.getFileSaveRequestList())) {
            remoteFileService.addBatch("news", newsInfo.getId(), request.getFileSaveRequestList());
        }
        return Optional.of(newsInfo).map(BaseEntity::getId).orElse(0L);
    }

    @Override
    public boolean update(NewsInfoUpdateRequest request) {
        NewsInfo newsInfo = new NewsInfo();
        BeanUtils.copyProperties(request, newsInfo);


        List<SysFile> sysFiles = sysFileService.selectList(request.getId());

        List<Long> idList = sysFiles.stream().map(SysFile::getId).collect(Collectors.toList());
        if(idList.size() > 0 ){
            String str = Arrays.toString(idList.toArray());   //[1,2,3,3,4]
            String replace = str.replace("[", "").replace("]", "").replace(" ","");

            //先删除
            sysFileService.delete(replace);
        }

        // 文件
        if (CollectionUtils.isNotEmpty(request.getFileSaveRequestList())) {
            remoteFileService.addBatch("news", newsInfo.getId(), request.getFileSaveRequestList());
        }
        return updateById(newsInfo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(String ids) {
        return deleteLogic(ids);
    }

    @Override
    public NewsInfo selectDetail(Long id) {
        NewsInfo newsInfo = newsInfoMapper.selectOneById(id);
        if(null == newsInfo){
            throw new ServiceException(String.format("%s ID=%s 的记录不存在", this.getClass().getSimpleName(), id));
        }
        //更新浏览次数
        newsInfo.setBrowseNum(newsInfo.getBrowseNum()+1);
        updateById(newsInfo);
        return newsInfo;
    }

    @Override
    public List<NewsInfo> selectPage(NewsInfoQueryRequest request) {
        return newsInfoMapper.selectByCondition(request);
    }

    @Override
    public List<NewsInfo> selectList(NewsInfoQueryRequest request) {
        return newsInfoMapper.selectByCondition(request);
    }

}
