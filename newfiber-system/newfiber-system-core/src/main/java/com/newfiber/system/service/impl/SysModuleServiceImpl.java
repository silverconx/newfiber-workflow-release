package com.newfiber.system.service.impl;

import java.util.List;
import com.newfiber.common.core.utils.DateUtils;
import java.util.Optional;
import java.util.ArrayList;
import com.newfiber.common.core.exception.ServiceException;
import com.newfiber.common.core.web.domain.BaseEntity;
import com.newfiber.common.core.web.service.BaseServiceImpl;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import com.newfiber.system.mapper.SysModuleMapper;
import com.newfiber.system.domain.SysModule;
import com.newfiber.system.domain.request.sysModule.SysModuleSaveRequest;
import com.newfiber.system.domain.request.sysModule.SysModuleUpdateRequest;
import com.newfiber.system.domain.request.sysModule.SysModuleQueryRequest;
import com.newfiber.system.service.ISysModuleService;

/**
 * 系统模块Service业务层处理
 * 
 * @author lufan
 * @date 2023-02-21
 */
@Service
public class SysModuleServiceImpl extends BaseServiceImpl<SysModuleMapper, SysModule> implements ISysModuleService {

    @Resource
    private SysModuleMapper sysModuleMapper;

    @Override
    public long insert(SysModuleSaveRequest request) {
        SysModule sysModule = new SysModule();
        BeanUtils.copyProperties(request, sysModule);
        save(sysModule);
        return Optional.of(sysModule).map(BaseEntity::getId).orElse(0L);
    }

    @Override
    public boolean update(SysModuleUpdateRequest request) {
        SysModule sysModule = new SysModule();
        BeanUtils.copyProperties(request, sysModule);
        return updateById(sysModule);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(String ids) {
	    return deleteLogic(ids);
    }

    @Override
    public SysModule selectDetail(Long id) {
        SysModule sysModule = sysModuleMapper.selectById(id);
        if(null == sysModule){
	        throw new ServiceException(String.format("%s ID=%s 的记录不存在", this.getClass().getSimpleName(), id));
        }
        return sysModule;
    }

    @Override
    public List<SysModule> selectPage(SysModuleQueryRequest request) {
        return sysModuleMapper.selectByCondition(request);
    }

    @Override
    public List<SysModule> selectList(SysModuleQueryRequest request) {
        return sysModuleMapper.selectByCondition(request);
    }

}
