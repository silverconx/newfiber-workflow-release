package com.newfiber.system.service;

import java.util.List;
import com.newfiber.system.domain.SysModule;
import com.newfiber.system.domain.request.sysModule.SysModuleSaveRequest;
import com.newfiber.system.domain.request.sysModule.SysModuleUpdateRequest;
import com.newfiber.system.domain.request.sysModule.SysModuleQueryRequest;

/**
 * 系统模块Service接口
 * 
 * @author lufan
 * @date 2023-02-21
 */
public interface ISysModuleService {

    /**
     * 新增系统模块
     *
     * @param request 新增参数
     * @return 结果
     */
    long insert(SysModuleSaveRequest request);

    /**
     * 修改系统模块
     *
     * @param request 修改参数
     * @return 结果
     */
    boolean update(SysModuleUpdateRequest request);

    /**
     * 批量删除系统模块
     *
     * @param  ids 编号(,隔开)
     * @return 结果
     */
    boolean delete(String ids);

    /**
     * 详细查询系统模块
     *
     * @param id 主键
     * @return 系统模块
     */
     SysModule selectDetail(Long id);

     /**
      * 分页查询系统模块
      *
      * @param request 分页参数
      * @return 系统模块集合
      */
     List<SysModule> selectPage(SysModuleQueryRequest request);

     /**
      * 列表查询系统模块
      *
      * @param request 列表参数
      * @return 系统模块集合
      */
     List<SysModule> selectList(SysModuleQueryRequest request);

}
