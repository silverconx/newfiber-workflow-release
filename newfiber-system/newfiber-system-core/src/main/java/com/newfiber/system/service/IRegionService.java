package com.newfiber.system.service;


import com.newfiber.system.domain.Region;
import com.newfiber.system.domain.request.region.RegionQueryRequest;

import java.util.List;

/**
 * 中国行政地区Service接口
 * 
 * @author 张鸿志
 * @date 2023-07-18
 */
public interface IRegionService {



     /**
      * 列表查询中国行政地区
      *
      * @param request 列表参数
      * @return 中国行政地区集合
      */
     List<Region> selectList(RegionQueryRequest request);

}
