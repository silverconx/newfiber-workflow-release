package com.newfiber.system.domain.request.sysModule;

import com.newfiber.common.core.web.request.BaseQueryRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统模块对象 sys_module
 *
 * @author lufan
 * @date 2023-02-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysModuleQueryRequest extends BaseQueryRequest {

    /**
     * 模块编号
     */
    @ApiModelProperty(value = "模块编号")
    private String moduleKey;

    /**
     * 模块名称
     */
    @ApiModelProperty(value = "模块名称")
    private String moduleName;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    private String status;

}
