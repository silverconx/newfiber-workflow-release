package com.newfiber.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import com.newfiber.system.domain.request.appRoleMenu.AppRoleMenuQueryRequest;
import com.newfiber.system.domain.AppRoleMenu;
import org.apache.ibatis.annotations.Param;

/**
 * APP角色菜单关联Mapper接口
 * 
 * @author X.K
 * @date 2023-03-20
 */
public interface AppRoleMenuMapper extends BaseMapper<AppRoleMenu>{

    /**
     * 条件查询APP角色菜单关联列表
     * 
     * @param request 查询条件
     * @return APP角色菜单关联集合
     */
    List<AppRoleMenu> selectByCondition(@Param("request") AppRoleMenuQueryRequest request);

	/**
	 * 条件查询APP角色菜单关联列表
	 *
	 * @param id 查询条件
	 * @return APP角色菜单关联集合
	 */
	AppRoleMenu selectOneById(@Param("id") Long id);

}
