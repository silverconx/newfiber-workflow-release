package com.newfiber.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newfiber.system.api.domain.SysFile;
import com.newfiber.system.domain.request.sysFile.SysFileQueryRequest;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 附件Mapper接口
 * 
 * @author X.K
 * @date 2023-02-22
 */
public interface SysFileMapper extends BaseMapper<SysFile>{

    /**
     * 条件查询附件列表
     * 
     * @param request 查询条件
     * @return 附件集合
     */
    List<SysFile> selectByCondition(@Param("request") SysFileQueryRequest request);

}
