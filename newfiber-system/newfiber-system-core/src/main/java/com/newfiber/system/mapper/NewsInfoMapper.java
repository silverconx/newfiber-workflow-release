package com.newfiber.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newfiber.system.domain.NewsInfo;
import com.newfiber.system.domain.request.sysNews.NewsInfoQueryRequest;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 新闻信息Mapper接口
 *
 * @author newfiber
 * @date 2023-03-31
 */
public interface NewsInfoMapper extends BaseMapper<NewsInfo> {

    /**
     * 条件查询新闻信息列表
     *
     * @param request 查询条件
     * @return 新闻信息集合
     */
    List<NewsInfo> selectByCondition(@Param("request") NewsInfoQueryRequest request);

	/**
	 * 条件查询新闻信息列表
	 *
	 * @param id 查询条件
	 * @return 新闻信息集合
	 */
	NewsInfo selectOneById(@Param("id") Long id);

}
