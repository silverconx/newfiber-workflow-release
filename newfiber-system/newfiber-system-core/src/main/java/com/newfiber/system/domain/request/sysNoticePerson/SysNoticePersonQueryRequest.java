package com.newfiber.system.domain.request.sysNoticePerson;

import com.newfiber.common.core.web.request.BaseQueryRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 通知公告-已读未读对象 sys_notice_person
 * date: 2023/4/24 下午 08:13
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysNoticePersonQueryRequest extends BaseQueryRequest {

    /**
     * 公告ID
     */
    @ApiModelProperty(value = "公告ID")
    private Integer noticeId;

    /**
     * 通知人
     */
    @ApiModelProperty(value = "通知人")
    private String noticePerson;

    /**
     * 是否已读（0未读 1已读)
     */
    @ApiModelProperty(value = "是否已读（0未读 1已读)")
    private String readStatus;

    /**
     * 读取时间
     */
    @ApiModelProperty(value = "读取时间")
    private Date readTime;

}
