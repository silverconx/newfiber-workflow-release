package com.newfiber.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newfiber.system.domain.Region;
import com.newfiber.system.domain.request.region.RegionQueryRequest;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 中国行政地区Mapper接口
 * 
 * @author 张鸿志
 * @date 2023-07-18
 */
public interface RegionMapper extends BaseMapper<Region> {

    /**
     * 条件查询中国行政地区列表
     * 
     * @param request 查询条件
     * @return 中国行政地区集合
     */
    List<Region> selectByCondition(@Param("request") RegionQueryRequest request);

	/**
	 * 条件查询中国行政地区列表
	 *
	 * @param id 查询条件
	 * @return 中国行政地区集合
	 */
	Region selectOneById(@Param("id") Long id);

}
