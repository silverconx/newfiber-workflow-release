package com.newfiber.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import com.newfiber.system.domain.request.appMenuCollect.AppMenuCollectQueryRequest;
import com.newfiber.system.domain.AppMenuCollect;
import org.apache.ibatis.annotations.Param;

/**
 * APP菜单收藏Mapper接口
 * 
 * @author X.K
 * @date 2023-03-29
 */
public interface AppMenuCollectMapper extends BaseMapper<AppMenuCollect>{

    /**
     * 条件查询APP菜单收藏列表
     * 
     * @param request 查询条件
     * @return APP菜单收藏集合
     */
    List<AppMenuCollect> selectByCondition(@Param("request") AppMenuCollectQueryRequest request);

	/**
	 * 条件查询APP菜单收藏列表
	 *
	 * @param id 查询条件
	 * @return APP菜单收藏集合
	 */
	AppMenuCollect selectOneById(@Param("id") Long id);

}
