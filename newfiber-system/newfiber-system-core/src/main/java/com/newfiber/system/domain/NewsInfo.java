package com.newfiber.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;



/**
 * 新闻信息对象 sys_news_info
 *
 * @author newfiber
 * @date 2023-03-31
 */
@Data
@TableName("sys_news_info")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "新闻信息", description = "新闻信息")
public class NewsInfo extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /**
     *  新闻标题
     */
    @ApiModelProperty(value = "新闻标题")
    private String newsTitle;

    /**
     *  新闻类别
     */
    @ApiModelProperty(value = "新闻类别")
    private String newsType;

    /**
     *  发布时间
     */
    @ApiModelProperty(value = "发布时间")
    private Date publishTime;

    /**
     *  是否置顶  0 否 1是
     */
    @ApiModelProperty(value = "是否置顶  0 否 1是")
    private Long isTop;

    /**
     *  浏览次数
     */
    @ApiModelProperty(value = "浏览次数")
    private Long browseNum;

    /**
     *  是否显示  0 否 1是
     */
    @ApiModelProperty(value = "是否显示  0 否 1是")
    private Long isShow;

    /**
     *  新闻内容
     */
    @ApiModelProperty(value = "新闻内容")
    private String newsContent;


}
