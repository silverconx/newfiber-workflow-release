package com.newfiber.system.domain.request.appMenuCollect;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * APP菜单收藏对象 sys_app_menu_collect
 *
 * @author X.K
 * @date 2023-03-29
 */
@Data
public class AppMenuCollectSaveRequest{

    /**
     * APP菜单编号
     */
    @NotNull
    @ApiModelProperty(value = "APP菜单编号", required = true)
    private Long appMenuId;

    /**
     * 用户编号
     */
    @NotNull
    @ApiModelProperty(value = "用户编号", required = true)
    private Long userId;

}
