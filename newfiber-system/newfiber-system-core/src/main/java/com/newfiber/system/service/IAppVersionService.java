package com.newfiber.system.service;

import com.newfiber.system.domain.AppVersion;
import com.newfiber.system.domain.pgyer.AppVersionRequest;
import com.newfiber.system.domain.pgyer.AppVersionResponse;
import com.newfiber.system.domain.pgyer.AppView;

/**
 * 移动端app版本更新接口
 *
 * date: 2023/5/27 上午 10:59
 * @author: LuFan
 * @since JDK 1.8
 */
public interface IAppVersionService {

    /**
     * 检查更新
     *
     * @param version
     * @return Version列表数据
     */
    AppVersion checkAndUpdate(String version);

    /**
     * 根据版本号查询版本详细信息
     *
     * @param version 查询条件
     * @return Version列表数据
     */
    AppVersion getCurrentVersionInfo(String version);

	/**
	 * 蒲公英最新版本
	 */
	AppView pgyerVersion();

	/**
	 * 蒲公英最新版本
	 */
	AppVersionResponse pgyerVersionCheck(AppVersionRequest request);

}
