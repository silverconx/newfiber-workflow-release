package com.newfiber.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * app-版本更新表 实体类
 *
 * @author admin
 * @since 2022-08-11 10:08:53
 */
@Data
@TableName("sys_app_version")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "移动APP Version对象", description = "app-版本更新表")
public class AppVersion extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 版本号
     */
    @ApiModelProperty(value = "版本号")
    private String versions;

    /**
     * url地址
     */
    @ApiModelProperty(value = "url地址")
    private String url;

    /**
     * 平台
     */
    @ApiModelProperty(value = "平台")
    private String platform;

    /**
     * 类型（0-最新版本 1-历史版本）
     */
    @ApiModelProperty(value = "类型（0-最新版本 1-历史版本）")
    private String type;


    /**
     * app名称
     */
    @ApiModelProperty(value = "app名称")
    private String appName;


    /**
     * app描述
     */
    @ApiModelProperty(value = "app描述")
    private String appDesc;


    /**
     * 更新内容描述
     */
    @ApiModelProperty(value = "更新内容描述")
    private String updateDesc;
}
