package com.newfiber.system.controller;


import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.system.domain.Region;
import com.newfiber.system.domain.request.region.RegionQueryRequest;
import com.newfiber.system.service.IRegionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 中国行政地区Controller
 * 
 * @author 张鸿志
 * @date 2023-07-18
 */
@RestController
@RequestMapping("/region")
@Api(value = "中国行政地区", tags = "中国行政地区")
public class RegionController extends BaseController {

    @Resource
    private IRegionService regionService;
    
    /**
     * 列表查询中国行政地区
     */
    @GetMapping("/list")
    //@RequiresPermissions("business:Region:list")
    @ApiOperation(value = "列表查询中国行政地区", position = 60)
    public Result<List<Region>> list(RegionQueryRequest request) {
        List<Region> list = regionService.selectList(request);
        return success(list);
    }

}
