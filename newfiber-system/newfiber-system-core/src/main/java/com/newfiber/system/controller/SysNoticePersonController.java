package com.newfiber.system.controller;

import java.util.List;
import javax.annotation.Resource;
import javax.validation.Valid;

import com.newfiber.system.service.ISysNoticePersonService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import com.newfiber.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.newfiber.system.domain.SysNoticePerson;
import com.newfiber.system.domain.request.sysNoticePerson.SysNoticePersonSaveRequest;
import com.newfiber.system.domain.request.sysNoticePerson.SysNoticePersonUpdateRequest;
import com.newfiber.system.domain.request.sysNoticePerson.SysNoticePersonQueryRequest;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;

/**
 * 通知公告-已读未读Controller
 * date: 2023/4/24 下午 08:12
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@RestController
@RequestMapping("/sysNoticePerson")
@Api(value = "通知公告-已读未读", tags = "通知公告-已读未读")
public class SysNoticePersonController extends BaseController {

    @Resource
    private ISysNoticePersonService sysNoticePersonService;

    /**
     * 新增通知公告-已读未读
     */
    @PostMapping("add")
    @RequiresPermissions("system:patrolNoticePerson:add")
    @ApiOperation(value = "新增通知公告-已读未读", position = 10)
    @Log(title = "通知公告-已读未读", businessType = BusinessType.INSERT)
    public Result<Long> add(@Valid @RequestBody SysNoticePersonSaveRequest request) {
        return success(sysNoticePersonService.insert(request));
    }

    /**
     * 修改通知公告-已读未读
     */
    @PutMapping("edit")
    @RequiresPermissions("system:patrolNoticePerson:edit")
    @ApiOperation(value = "修改通知公告-已读未读", position = 20)
    @Log(title = "通知公告-已读未读", businessType = BusinessType.UPDATE)
    public Result<Object> edit(@Valid @RequestBody SysNoticePersonUpdateRequest request) {
        return success(sysNoticePersonService.update(request));
    }

    /**
     * 删除通知公告-已读未读
     */
    @DeleteMapping("/{ids}")
    @RequiresPermissions("system:patrolNoticePerson:remove")
    @ApiOperation(value = "删除通知公告-已读未读", notes = "传入ids(,隔开)", position = 30)
    @Log(title = "通知公告-已读未读", businessType = BusinessType.DELETE)
    public Result<Object> remove(@PathVariable String ids) {
        return success(sysNoticePersonService.delete(ids));
    }

    /**
     * 详细查询通知公告-已读未读
     */
    @GetMapping(value = "/{id}")
    @RequiresPermissions("system:patrolNoticePerson:detail")
    @ApiOperation(value = "详细查询通知公告-已读未读", position = 40)
    public Result<SysNoticePerson> detail(@PathVariable("id") Long id) {
        return success(sysNoticePersonService.selectDetail(id));
    }

    /**
     * 分页查询通知公告-已读未读
     */
    @GetMapping("/page")
    @RequiresPermissions("system:patrolNoticePerson:page")
    @ApiOperation(value = "分页查询通知公告-已读未读", position = 50)
    public PageResult<List<SysNoticePerson>> page(SysNoticePersonQueryRequest request) {
        startPage();
        List<SysNoticePerson> list = sysNoticePersonService.selectPage(request);
        return pageResult(list);
    }

    /**
     * 列表查询通知公告-已读未读
     */
    @GetMapping("/list")
    @RequiresPermissions("system:patrolNoticePerson:list")
    @ApiOperation(value = "列表查询通知公告-已读未读", position = 60)
    public Result<List<SysNoticePerson>> list(SysNoticePersonQueryRequest request) {
        List<SysNoticePerson> list = sysNoticePersonService.selectList(request);
        return success(list);
    }

}
