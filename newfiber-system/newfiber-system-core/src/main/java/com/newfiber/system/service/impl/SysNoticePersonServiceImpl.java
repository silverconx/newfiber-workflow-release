package com.newfiber.system.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.newfiber.common.core.web.domain.BaseEntity;
import com.newfiber.common.core.exception.ServiceException;
import com.newfiber.common.core.web.service.BaseServiceImpl;
import com.newfiber.common.security.utils.SecurityUtils;
import com.newfiber.system.api.domain.SysUser;
import com.newfiber.system.api.model.LoginUser;
import com.newfiber.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import com.newfiber.system.mapper.SysNoticePersonMapper;
import com.newfiber.system.domain.SysNoticePerson;
import com.newfiber.system.domain.request.sysNoticePerson.SysNoticePersonSaveRequest;
import com.newfiber.system.domain.request.sysNoticePerson.SysNoticePersonUpdateRequest;
import com.newfiber.system.domain.request.sysNoticePerson.SysNoticePersonQueryRequest;
import com.newfiber.system.service.ISysNoticePersonService;

/**
 * 通知公告-已读未读Service业务层处理
 * date: 2023/4/24 下午 08:13
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@Service
public class SysNoticePersonServiceImpl extends BaseServiceImpl<SysNoticePersonMapper, SysNoticePerson> implements ISysNoticePersonService {

    @Resource
    private SysNoticePersonMapper sysNoticePersonMapper;

    @Autowired
    private ISysUserService iSysUserService;

    @Override
    public long insert(SysNoticePersonSaveRequest request) {
        SysNoticePerson sysNoticePerson = new SysNoticePerson();
        BeanUtils.copyProperties(request, sysNoticePerson);
        save(sysNoticePerson);
        return Optional.of(sysNoticePerson).map(BaseEntity::getId).orElse(0L);
    }

    @Override
    public int insertBatch(Long noticeId) {
        List<SysUser> sysUsers = iSysUserService.selectUserList(new SysUser());
        sysUsers.forEach(e -> {
            SysNoticePerson sysNoticePerson = new SysNoticePerson();
            sysNoticePerson.setNoticeId(noticeId);
            sysNoticePerson.setNoticePerson(e.getUserName());
            sysNoticePerson.setReadStatus("0");
            save(sysNoticePerson);
        });
        return sysUsers.size();
    }

    @Override
    public boolean update(SysNoticePersonUpdateRequest request) {
        SysNoticePerson sysNoticePerson = new SysNoticePerson();
        BeanUtils.copyProperties(request, sysNoticePerson);
        return updateById(sysNoticePerson);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(String ids) {
        return deleteLogic(ids);
    }

    @Override
    public boolean deleteByNoticeId(Long noticeId) {
        QueryWrapper<SysNoticePerson> sysNoticePersonQueryWrapper = new QueryWrapper<SysNoticePerson>().
                eq("notice_id", noticeId);
        return remove(sysNoticePersonQueryWrapper);
    }

    @Override
    public SysNoticePerson selectDetail(Long id) {
        SysNoticePerson sysNoticePerson = sysNoticePersonMapper.selectById(id);
        if (null == sysNoticePerson) {
            throw new ServiceException(String.format("%s ID=%s 的记录不存在", this.getClass().getSimpleName(), id));
        }
        return sysNoticePerson;
    }

    @Override
    public List<SysNoticePerson> selectPage(SysNoticePersonQueryRequest request) {
        return sysNoticePersonMapper.selectByCondition(request);
    }

    @Override
    public List<SysNoticePerson> selectList(SysNoticePersonQueryRequest request) {
        return sysNoticePersonMapper.selectByCondition(request);
    }

    @Override
    public boolean readNotice(Long noticeId) {
        // 获取当前用户，标记当前用户为已读
        LoginUser loginUser = SecurityUtils.getLoginUser();
        final String username = loginUser.getUsername();
        UpdateWrapper<SysNoticePerson> sysNoticePersonUpdateWrapper = new UpdateWrapper<SysNoticePerson>().
                set("read_status", "1").
                set("read_time", new Date()).
                set("update_time", new Date()).
                eq("read_status", "0").
                eq("notice_id", noticeId).
                eq("notice_person", username);
        return update(sysNoticePersonUpdateWrapper);
    }

}
