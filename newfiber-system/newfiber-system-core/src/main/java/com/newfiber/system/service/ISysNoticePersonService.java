package com.newfiber.system.service;

import java.util.List;

import com.newfiber.system.domain.SysNoticePerson;
import com.newfiber.system.domain.request.sysNoticePerson.SysNoticePersonSaveRequest;
import com.newfiber.system.domain.request.sysNoticePerson.SysNoticePersonUpdateRequest;
import com.newfiber.system.domain.request.sysNoticePerson.SysNoticePersonQueryRequest;

/**
 * 通知公告-已读未读Service接口
 * <p>
 * date: 2023/4/24 下午 08:12
 *
 * @author: LuFan
 * @since JDK 1.8
 */
public interface ISysNoticePersonService {

    /**
     * 新增通知公告-已读未读
     *
     * @param request 新增参数
     * @return 结果
     */
    long insert(SysNoticePersonSaveRequest request);

    /**
     * 批量插入用户读取初始化信息
     *
     * @param noticeId
     * @return 用户数量
     */
    int insertBatch(Long noticeId);

    /**
     * 修改通知公告-已读未读
     *
     * @param request 修改参数
     * @return 结果
     */
    boolean update(SysNoticePersonUpdateRequest request);

    /**
     * 批量删除通知公告-已读未读
     *
     * @param ids 编号(,隔开)
     * @return 结果
     */
    boolean delete(String ids);

    /**
     * 根据noticeId删除用户已读未读数据
     *
     * @param noticeId
     * @return boolean
     */
    boolean deleteByNoticeId(Long noticeId);

    /**
     * 详细查询通知公告-已读未读
     *
     * @param id 主键
     * @return 通知公告-已读未读
     */
    SysNoticePerson selectDetail(Long id);

    /**
     * 分页查询通知公告-已读未读
     *
     * @param request 分页参数
     * @return 通知公告-已读未读集合
     */
    List<SysNoticePerson> selectPage(SysNoticePersonQueryRequest request);

    /**
     * 列表查询通知公告-已读未读
     *
     * @param request 列表参数
     * @return 通知公告-已读未读集合
     */
    List<SysNoticePerson> selectList(SysNoticePersonQueryRequest request);

    /**
     * 某个用户读取通知以后，刷新sys_notice_person
     * @param
     * @return 结果
     */
    boolean readNotice(Long noticeId);

}
