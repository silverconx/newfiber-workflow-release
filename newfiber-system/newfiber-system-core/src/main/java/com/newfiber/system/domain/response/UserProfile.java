package com.newfiber.system.domain.response;

import com.newfiber.system.api.domain.SysUser;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author : X.K
 * @since : 2023/2/13 上午10:28
 */
@Data
@AllArgsConstructor
public class UserProfile {
	SysUser user;
	String roleGroup;
	String postGroup;
}
