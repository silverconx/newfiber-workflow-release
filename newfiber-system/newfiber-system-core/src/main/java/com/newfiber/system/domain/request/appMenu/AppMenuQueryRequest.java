package com.newfiber.system.domain.request.appMenu;

import com.newfiber.common.core.web.request.BaseQueryRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * APP菜单对象 sys_app_menu
 *
 * @author X.K
 * @date 2023-03-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AppMenuQueryRequest extends BaseQueryRequest {

    /**
     * 父级菜单
     */
    @ApiModelProperty(value = "父级菜单")
    private Long parentId;

    /**
     * 菜单类型
     */
    @ApiModelProperty(value = "菜单类型")
    private String category;

    /**
     * 菜单编号
     */
    @ApiModelProperty(value = "菜单编号")
    private String code;

    /**
     * 菜单名称
     */
    @ApiModelProperty(value = "菜单名称")
    private String name;

    /**
     * 是否隐藏（0否 1是）
     */
    @ApiModelProperty(value = "是否隐藏（0否 1是）")
    private String isHide;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    private String status;

	/**
	 * 用户编号
	 */
	@ApiModelProperty(value = "用户编号")
    private Long userId;

}
