package com.newfiber.system.service.impl;

import com.newfiber.common.core.web.service.BaseServiceImpl;
import com.newfiber.common.security.utils.SecurityUtils;
import com.newfiber.system.api.domain.SysNotice;
import com.newfiber.system.api.model.LoginUser;
import com.newfiber.system.mapper.SysNoticeMapper;
import com.newfiber.system.service.ISysNoticePersonService;
import com.newfiber.system.service.ISysNoticeService;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 公告 服务层实现
 *
 * @author newfiber
 */
@Service
public class SysNoticeServiceImpl extends BaseServiceImpl<SysNoticeMapper, SysNotice> implements ISysNoticeService {
    @Autowired
    private SysNoticeMapper noticeMapper;

    @Autowired
    private ISysNoticePersonService iSysNoticePersonService;

    /**
     * 新增公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    @Override
    public void insertNotice(SysNotice notice) {
    	save(notice);
    }

    /**
     * 删除公告对象
     *
     * @param noticeId 公告ID
     * @return 结果
     */
    @Override
    public int deleteNoticeById(Long noticeId) {
        // 删除公告之前删除对应用户已读未读数据
        iSysNoticePersonService.deleteByNoticeId(noticeId);
        return noticeMapper.deleteNoticeById(noticeId);
    }

    /**
     * 批量删除公告信息
     *
     * @param noticeIds 需要删除的公告ID
     * @return 结果
     */
    @Override
    public int deleteNoticeByIds(Long[] noticeIds) {
        final List<Long> longs = Arrays.asList(noticeIds);
        longs.forEach(e -> {
            // 删除公告之前删除对应用户已读未读数据
            iSysNoticePersonService.deleteByNoticeId(e);
        });
        return noticeMapper.deleteNoticeByIds(noticeIds);
    }

	@Override
	public void handleNotice(Long noticeId) {
		SysNotice sysNotice = noticeMapper.selectNoticeById(noticeId);
		// 刷新浏览量数字 read_count
		sysNotice.setReadCount(sysNotice.getReadCount() + 1);
		boolean result = iSysNoticePersonService.readNotice(noticeId);
		if (result) {
			// 更新sys_notice_person表对应用户为已读，如果更新成功，read_person_count+1 ，unread_person_count-1
			sysNotice.setReadPersonCount(sysNotice.getReadPersonCount() + 1);
			sysNotice.setUnreadPersonCount(sysNotice.getUnreadPersonCount() - 1);
		}
		noticeMapper.updateNotice(sysNotice);
	}

	/**
     * 修改公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    @Override
    public int updateNotice(SysNotice notice) {
        return noticeMapper.updateNotice(notice);
    }

    /**
     * 查询公告信息
     *
     * @param noticeId 公告ID
     * @return 公告信息
     */
    @Override
    public SysNotice selectNoticeById(Long noticeId) {
        SysNotice sysNotice = noticeMapper.selectNoticeById(noticeId);
        // 刷新浏览量数字 read_count
        sysNotice.setReadCount(sysNotice.getReadCount() + 1);
        boolean result = iSysNoticePersonService.readNotice(noticeId);
        if (result) {
            // 更新sys_notice_person表对应用户为已读，如果更新成功，read_person_count+1 ，unread_person_count-1
            sysNotice.setReadPersonCount(sysNotice.getReadPersonCount() + 1);
            sysNotice.setUnreadPersonCount(sysNotice.getUnreadPersonCount() - 1);
        }
        noticeMapper.updateNotice(sysNotice);
        return sysNotice;
    }

    /**
     * 查询公告列表
     *
     * @param notice 公告信息
     * @return 公告集合
     */
    @Override
    public List<SysNotice> selectNoticeList(SysNotice notice) {
	    LoginUser loginUser = SecurityUtils.getLoginUser();
	    if(null != loginUser){
		    notice.setNoticePerson(loginUser.getUsername());
	    }
        return noticeMapper.selectNoticeList(notice);
    }
}
