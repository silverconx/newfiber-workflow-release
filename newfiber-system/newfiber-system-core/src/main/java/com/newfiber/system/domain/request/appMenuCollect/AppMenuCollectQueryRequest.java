package com.newfiber.system.domain.request.appMenuCollect;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * APP菜单收藏对象 sys_app_menu_collect
 *
 * @author X.K
 * @date 2023-03-29
 */
@Data
public class AppMenuCollectQueryRequest{

    /**
     * APP菜单编号
     */
    @ApiModelProperty(value = "APP菜单编号")
    private Long appMenuId;

    /**
     * 用户编号
     */
    @ApiModelProperty(value = "用户编号")
    private Long userId;

}
