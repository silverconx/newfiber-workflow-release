package com.newfiber.system.domain.response;

import com.newfiber.system.domain.vo.TreeSelect;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author : X.K
 * @since : 2023/2/13 上午10:24
 */
@Data
@AllArgsConstructor
public class RoleMenuTree {
	List<Long> checkedKeys;
	List<TreeSelect> menus;
}
