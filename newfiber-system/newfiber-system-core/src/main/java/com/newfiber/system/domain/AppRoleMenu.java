package com.newfiber.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * APP角色菜单关联对象 sys_app_role_menu
 * 
 * @author X.K
 * @date 2023-03-20
 */
@Data
@TableName("sys_app_role_menu")
@ApiModel(value = "APP角色菜单关联", description = "APP角色菜单关联")
public class AppRoleMenu {

    private static final long serialVersionUID = 1L;

	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "主键id")
	@TableId(value = "id", type = IdType.ASSIGN_ID)
	private Long id;

    /**
     *  菜单id
     */
    @ApiModelProperty(value = "菜单id")
    private Long menuId;

    /**
     *  角色id
     */
    @ApiModelProperty(value = "角色id")
    private Long roleId;


}
