package com.newfiber.system.domain.request.appRoleMenu;

import com.newfiber.common.core.web.request.BaseQueryRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * APP角色菜单关联对象 sys_app_role_menu
 *
 * @author X.K
 * @date 2023-03-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AppRoleMenuQueryRequest extends BaseQueryRequest {

    /**
     * 菜单id
     */
    @ApiModelProperty(value = "菜单id")
    private Long menuId;

    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id")
    private Long roleId;

}
