package com.newfiber.system.domain.pgyer;

import lombok.Data;

/**
 * @author : X.K
 * @since : 2023/7/31 上午10:45
 */
@Data
public class AppVersionResponse {

	private Integer buildBuildVersion;
	private String forceUpdateVersion;
	private String forceUpdateVersionNo;
	private String needForceUpdate;
	private String downloadURL;
	private String buildHaveNewVersion;
	private String buildVersionNo;
	private String buildVersion;
	private String buildShortcutUrl;
	private String buildUpdateDescription;
}
