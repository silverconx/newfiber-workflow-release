package com.newfiber.system.controller;

import com.newfiber.common.core.constant.UserConstants;
import com.newfiber.common.core.domain.R;
import com.newfiber.common.core.utils.StringUtils;
import com.newfiber.common.core.utils.poi.ExcelUtil;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import com.newfiber.common.security.annotation.InnerAuth;
import com.newfiber.common.security.annotation.RequiresPermissions;
import com.newfiber.common.security.utils.PwdUtil;
import com.newfiber.common.security.utils.SecurityUtils;
import com.newfiber.system.api.domain.SysDept;
import com.newfiber.system.api.domain.SysRole;
import com.newfiber.system.api.domain.SysUser;
import com.newfiber.system.api.model.LoginUser;
import com.newfiber.system.domain.SysPost;
import com.newfiber.system.domain.response.UserInfo;
import com.newfiber.system.service.ISysConfigService;
import com.newfiber.system.service.ISysDeptService;
import com.newfiber.system.service.ISysPermissionService;
import com.newfiber.system.service.ISysPostService;
import com.newfiber.system.service.ISysRoleService;
import com.newfiber.system.service.ISysUserService;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 用户信息
 * 
 * @author newfiber
 */
@RestController
@RequestMapping("/user")
public class SysUserController extends BaseController{
    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysDeptService deptService;

    @Autowired
    private ISysPostService postService;

    @Autowired
    private ISysPermissionService permissionService;

    @Autowired
    private ISysConfigService configService;

    /**
     * 分页获取用户列表
     */
    @GetMapping("/page")
    public PageResult<List<SysUser>> page(SysUser user){
        startPage();
        List<SysUser> list = userService.selectUserList(user);
        return pageResult(list);
    }

    /**
     * 获取用户列表
     */
    @GetMapping("/list")
    public Result<List<SysUser>> list(SysUser user){
        List<SysUser> list = userService.selectUserList(user);
        return success(list);
    }

    @Log(title = "用户管理", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:user:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysUser user){
        List<SysUser> list = userService.selectUserList(user);
        ExcelUtil<SysUser> util = new ExcelUtil<SysUser>(SysUser.class);
        util.exportExcel(response, list, "用户数据");
    }

    @Log(title = "用户管理", businessType = BusinessType.IMPORT)
    @RequiresPermissions("system:user:import")
    @PostMapping("/importData")
    public Result importData(MultipartFile file, boolean updateSupport) throws Exception{
        ExcelUtil<SysUser> util = new ExcelUtil<SysUser>(SysUser.class);
        List<SysUser> userList = util.importExcel(file.getInputStream());
        String operName = SecurityUtils.getUsername();
        String message = userService.importUser(userList, updateSupport, operName);
        return success(message);
    }

    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response) throws IOException{
        ExcelUtil<SysUser> util = new ExcelUtil<SysUser>(SysUser.class);
        util.importTemplateExcel(response, "用户数据");
    }

    /**
     * 获取当前用户信息
     */
    @InnerAuth
    @GetMapping("/info/{username}")
    public R<LoginUser> info(@PathVariable("username") String username){
        SysUser sysUser = userService.selectUserByUserName(username);
        if (StringUtils.isNull(sysUser)){
            return R.fail("用户名或密码错误");
        }
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(sysUser);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(sysUser);
        LoginUser sysUserVo = new LoginUser();
        sysUserVo.setSysUser(sysUser);
        sysUserVo.setRoles(roles);
        sysUserVo.setPermissions(permissions);
        return R.ok(sysUserVo);
    }

    /**
     * 注册用户信息
     */
    @InnerAuth
    @PostMapping("/register")
    public R<Boolean> register(@RequestBody SysUser sysUser){
        String username = sysUser.getUserName();
        if (!("true".equals(configService.selectConfigByKey("sys.account.registerUser")))){
            return R.fail("当前系统没有开启注册功能！");
        }
        if (UserConstants.NOT_UNIQUE.equals(userService.checkUserNameUnique(sysUser))){
            return R.fail("保存用户'" + username + "'失败，注册账号已存在");
        }
        return R.ok(userService.registerUser(sysUser));
    }

    /**
     * 获取用户信息
     * 
     * @return 用户信息
     */
    @GetMapping("getInfo")
    public Result<UserInfo> getInfo(){
        SysUser user = userService.selectUserById(SecurityUtils.getUserId());
        // 角色集合
        Set<String> roleIds = permissionService.getRolePermission(user);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(user);
        // 岗位
	    List<SysPost> sysPostList = postService.selectPostInfoListByUserId(user.getUserId());

	    UserInfo userInfo = new UserInfo();
	    userInfo.setUser(user);
	    userInfo.setRoleIds(roleIds);
	    userInfo.setPermissions(permissions);

	    if(CollectionUtils.isNotEmpty(sysPostList)){
		    userInfo.setPosts(sysPostList);
		    userInfo.setPostIds(sysPostList.stream().map(SysPost::getPostId).collect(Collectors.toList()));
	    }

        return Result.success(userInfo);
    }

    /**
     * 根据用户编号获取详细信息
     */
    @RequiresPermissions("system:user:query")
    @GetMapping(value = { "/", "/{userId}" })
    public Result getInfo(@PathVariable(value = "userId", required = false) Long userId){
        userService.checkUserDataScope(userId);
	    UserInfo userInfo = new UserInfo();
        List<SysRole> roles = roleService.selectRoleAll();
	    userInfo.setRoles(SysUser.isAdmin(userId) ? roles : roles.stream().filter(r -> !r.isAdmin()).collect(Collectors.toList()));
	    userInfo.setPosts(postService.selectPostAll());
        if (StringUtils.isNotNull(userId)){
            SysUser sysUser = userService.selectUserById(userId);
	        userInfo.setUser(sysUser);
	        userInfo.setPostIds(postService.selectPostListByUserId(userId));
	        userInfo.setRoleIds(sysUser.getRoles().stream().map(t -> String.valueOf(t.getRoleId())).collect(Collectors.toSet()));
        }
        return Result.success(userInfo);
    }

    /**
     * 新增用户
     */
    @RequiresPermissions("system:user:add")
    @Log(title = "用户管理", businessType = BusinessType.INSERT)
    @PostMapping
    public Result add(@Validated @RequestBody SysUser user){
        if (UserConstants.NOT_UNIQUE.equals(userService.checkUserNameUnique(user))){
            return error("新增用户'" + user.getUserName() + "'失败，登录账号已存在");
        }
        else if (StringUtils.isNotEmpty(user.getPhonenumber())
                && UserConstants.NOT_UNIQUE.equals(userService.checkPhoneUnique(user))){
            return error("新增用户'" + user.getUserName() + "'失败，手机号码已存在");
        }
        else if (StringUtils.isNotEmpty(user.getEmail())
                && UserConstants.NOT_UNIQUE.equals(userService.checkEmailUnique(user))){
            return error("新增用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }
	    String password = SecurityUtils.transferAesEncrypt(user.getPassword());
	    PwdUtil.passwordCheck(password);
        user.setCreateBy(SecurityUtils.getUsername());
        user.setPassword(SecurityUtils.encryptPassword(password));
        return result(userService.insertUser(user));
    }

    /**
     * 修改用户
     */
    @RequiresPermissions("system:user:edit")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public Result edit(@Validated @RequestBody SysUser user){
        userService.checkUserAllowed(user);
        userService.checkUserDataScope(user.getUserId());
        if (UserConstants.NOT_UNIQUE.equals(userService.checkUserNameUnique(user))){
            return error("修改用户'" + user.getUserName() + "'失败，登录账号已存在");
        }
        else if (StringUtils.isNotEmpty(user.getPhonenumber())
                && UserConstants.NOT_UNIQUE.equals(userService.checkPhoneUnique(user))){
            return error("修改用户'" + user.getUserName() + "'失败，手机号码已存在");
        }
        else if (StringUtils.isNotEmpty(user.getEmail())
                && UserConstants.NOT_UNIQUE.equals(userService.checkEmailUnique(user))){
            return error("修改用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }
        user.setUpdateBy(SecurityUtils.getUsername());
        return result(userService.updateUser(user));
    }

    /**
     * 删除用户
     */
    @RequiresPermissions("system:user:remove")
    @Log(title = "用户管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{userIds}")
    public Result remove(@PathVariable Long[] userIds){
        if (ArrayUtils.contains(userIds, SecurityUtils.getUserId())){
            return error("当前用户不能删除");
        }
        return result(userService.deleteUserByIds(userIds));
    }

    /**
     * 重置密码
     */
    @RequiresPermissions("system:user:edit")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PutMapping("/resetPwd")
    public Result resetPwd(@RequestBody SysUser user){
        userService.checkUserAllowed(user);
        userService.checkUserDataScope(user.getUserId());

	    String password = SecurityUtils.transferAesEncrypt(user.getPassword());
	    PwdUtil.passwordCheck(password);
        user.setPassword(SecurityUtils.encryptPassword(password));
        user.setUpdateBy(SecurityUtils.getUsername());
        return result(userService.resetPwd(user));
    }

    /**
     * 状态修改
     */
    @RequiresPermissions("system:user:edit")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public Result changeStatus(@RequestBody SysUser user){
        userService.checkUserAllowed(user);
        userService.checkUserDataScope(user.getUserId());
        user.setUpdateBy(SecurityUtils.getUsername());
        return result(userService.updateUserStatus(user));
    }

    /**
     * 根据用户编号获取授权角色
     */
    @RequiresPermissions("system:user:query")
    @GetMapping("/authRole/{userId}")
    public Result authRole(@PathVariable("userId") Long userId){
        SysUser user = userService.selectUserById(userId);
        List<SysRole> roles = roleService.selectRolesByUserId(userId);
        UserInfo userInfo = new UserInfo();
	    userInfo.setUser(user);
	    userInfo.setRoles(SysUser.isAdmin(userId) ? roles : roles.stream().filter(r -> !r.isAdmin()).collect(Collectors.toList()));
        return Result.success(userInfo);
    }

    /**
     * 用户授权角色
     */
    @RequiresPermissions("system:user:edit")
    @Log(title = "用户管理", businessType = BusinessType.GRANT)
    @PutMapping("/authRole")
    public Result insertAuthRole(Long userId, Long[] roleIds){
        userService.checkUserDataScope(userId);
        userService.insertUserAuth(userId, roleIds);
        return success();
    }

    /**
     * 获取部门树列表
     */
    @RequiresPermissions("system:user:list")
    @GetMapping("/deptTree")
    public Result deptTree(SysDept dept){
        return success(deptService.selectDeptTreeList(dept));
    }
}
