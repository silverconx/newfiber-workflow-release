package com.newfiber.system.service.impl;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.newfiber.common.core.exception.ServiceException;
import com.newfiber.common.core.web.service.BaseServiceImpl;
import com.newfiber.common.security.utils.ConfigUtils;
import com.newfiber.system.domain.AppVersion;
import com.newfiber.system.domain.pgyer.AppVersionRequest;
import com.newfiber.system.domain.pgyer.AppVersionResponse;
import com.newfiber.system.domain.pgyer.AppView;
import com.newfiber.system.mapper.AppVersionMapper;
import com.newfiber.system.service.IAppVersionService;
import java.util.HashMap;
import java.util.Map;
import lombok.Data;
import org.springframework.stereotype.Service;

/**
 * 移动端app版本更新实现类
 * date: 2023/5/27 上午 10:58
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@Service
public class AppVersionServiceImpl extends BaseServiceImpl<AppVersionMapper, AppVersion> implements IAppVersionService {

    /**
     * 查询版本更新
     * date: 2023/5/27 上午 10:55
     *
     * @author: LuFan
     * @since JDK 1.8
     */
    @Override
    public AppVersion checkAndUpdate(String version) {
        QueryWrapper<AppVersion> appVersionQueryWrapper = new QueryWrapper<>();
        appVersionQueryWrapper.orderByDesc("versions").last("limit 1");
        AppVersion appVersion = getOne(appVersionQueryWrapper);
        if (version.equals(appVersion.getVersions())) {
            throw new ServiceException("当前版本已经是最新版本，无需更新");
        }
        return appVersion;
    }

    /**
     * 获取当前版本信息
     * date: 2023/5/27 上午 10:55
     *
     * @author: LuFan
     * @since JDK 1.8
     */
    @Override
    public AppVersion getCurrentVersionInfo(String version) {
        QueryWrapper<AppVersion> appVersionQueryWrapper = new QueryWrapper<>();
        appVersionQueryWrapper.eq("versions", version);
        AppVersion appVersion = getOne(appVersionQueryWrapper);
        if(null == appVersion){
            throw new ServiceException("当前版本：" + version + "不存在。");
        }
        return appVersion;
    }

	@Override
	public AppView pgyerVersion() {
    	String apiUrl = "https://www.pgyer.com/apiv2/app/view";

		String apiKye = ConfigUtils.getConfig("pgyer_apiKey");
		String appKey = ConfigUtils.getConfig("pgyer_appKey");

		Map<String, Object> body = new HashMap<>();
		body.put("_api_key", apiKye);
		body.put("appKey", appKey);
		body.put("buildKey", "");
		String httpResult = HttpUtil.createPost(apiUrl).form(body).execute().body();
		Result<AppView> tokenResult = JSONObject.parseObject(httpResult, new TypeReference<Result<AppView>>(){});
		return tokenResult.data;
	}

	@Override
	public AppVersionResponse pgyerVersionCheck(AppVersionRequest request) {
		String apiUrl = "https://www.pgyer.com/apiv2/app/check";

		String apiKye = ConfigUtils.getConfig("pgyer_apiKey");
		String appKey = ConfigUtils.getConfig("pgyer_appKey");

		Map<String, Object> body = new HashMap<>();
		body.put("_api_key", apiKye);
		body.put("appKey", appKey);
		body.put("buildKey", request.getBuildVersion());
		body.put("buildBuildVersion", request.getBuildBuildVersion());
		body.put("channelKey", request.getChannelKey());

		String httpResult = HttpUtil.createPost(apiUrl).form(body).execute().body();
		Result<AppVersionResponse> tokenResult = JSONObject.parseObject(httpResult, new TypeReference<Result<AppVersionResponse>>(){});
		return tokenResult.data;
	}

	@Data
	public static class Result<T>{
		int code;
		String message;
		T data;
	}

}
