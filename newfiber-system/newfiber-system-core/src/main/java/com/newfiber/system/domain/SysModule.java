package com.newfiber.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统模块对象 sys_module
 * 
 * @author lufan
 * @date 2023-02-21
 */
@Data
@TableName("sys_module")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "系统模块", description = "系统模块")
public class SysModule extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /**
     *  模块编号
     */
    @ApiModelProperty(value = "模块编号")
    private String moduleKey;

    /**
     *  模块名称
     */
    @ApiModelProperty(value = "模块名称")
    private String moduleName;


}
