package com.newfiber.system.controller;

import com.newfiber.common.core.utils.poi.ExcelUtil;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import com.newfiber.common.security.annotation.InnerAuth;
import com.newfiber.common.security.annotation.RequiresPermissions;
import com.newfiber.system.api.domain.SysOperLog;
import com.newfiber.system.service.ISysOperLogService;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 操作日志记录
 * 
 * @author newfiber
 */
@RestController
@RequestMapping("/operlog")
public class SysOperlogController extends BaseController{
    @Autowired
    private ISysOperLogService operLogService;

    @RequiresPermissions("system:operlog:list")
    @GetMapping("/list")
    public PageResult list(SysOperLog operLog){
        startPage();
        List<SysOperLog> list = operLogService.selectOperLogList(operLog);
        return pageResult(list);
    }

    @Log(title = "操作日志", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:operlog:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysOperLog operLog){
        List<SysOperLog> list = operLogService.selectOperLogList(operLog);
        ExcelUtil<SysOperLog> util = new ExcelUtil<SysOperLog>(SysOperLog.class);
        util.exportExcel(response, list, "操作日志");
    }

    @Log(title = "操作日志", businessType = BusinessType.DELETE)
    @RequiresPermissions("system:operlog:remove")
    @DeleteMapping("/{operIds}")
    public Result remove(@PathVariable Long[] operIds){
        return result(operLogService.deleteOperLogByIds(operIds));
    }

    @RequiresPermissions("system:operlog:remove")
    @Log(title = "操作日志", businessType = BusinessType.CLEAN)
    @DeleteMapping("/clean")
    public Result clean(){
        operLogService.cleanOperLog();
        return success();
    }

    @GetMapping("/queryList")
    public List<SysOperLog> queryList(SysOperLog operLog){
        List<SysOperLog> list = operLogService.selectOperLogList(operLog);
        return list;
    }

    @InnerAuth
    @PostMapping
    public Result add(@RequestBody SysOperLog operLog){
        return result(operLogService.insertOperlog(operLog));
    }
}
