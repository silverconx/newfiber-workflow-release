package com.newfiber.system.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * APP菜单对象 sys_app_menu
 * 
 * @author X.K
 * @date 2023-03-20
 */
@Data
@TableName("sys_app_menu")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "APP菜单", description = "APP菜单")
public class AppMenu extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /**
     *  父级菜单
     */
    @ApiModelProperty(value = "父级菜单")
    private Long parentId;

    /**
     *  菜单类型（数据字典：sys_app_category）
     */
    @ApiModelProperty(value = "菜单类型（数据字典：sys_app_category）")
    private String category;

    /**
     *  菜单编号
     */
    @ApiModelProperty(value = "菜单编号")
    private String code;

    /**
     *  菜单名称
     */
    @ApiModelProperty(value = "菜单名称")
    private String name;

    /**
     *  请求地址
     */
    @ApiModelProperty(value = "请求地址")
    private String path;

    /**
     *  图片地址
     */
    @ApiModelProperty(value = "图片地址")
    private String picUrl;

    /**
     *  图片地址（选中）
     */
    @ApiModelProperty(value = "图片地址（选中）")
    private String picUrlChecked;

    /**
     *  是否隐藏（0否 1是）
     */
    @ApiModelProperty(value = "是否隐藏（0否 1是）")
    private String isHide;

    /**
     *  排序
     */
    @ApiModelProperty(value = "排序")
    private Long sort;

    // DB Property

	/** 子菜单 */
	@TableField(exist = false)
	private List<AppMenu> children = new ArrayList<>();


}
