package com.newfiber.system.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * 中国行政地区对象 sys_region
 * 
 * @author 张鸿志
 * @date 2023-07-18
 */
@Data
@TableName("sys_region")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "中国行政地区", description = "中国行政地区")
public class Region extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /**
     *  层级
     */
    @ApiModelProperty(value = "层级")
    private Integer level;

    /**
     *  父级行政代码
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "父级行政代码")
    private Long parentCode;

    /**
     *  行政代码
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "行政代码")
    private Long areaCode;

    /**
     *  邮政编码
     */
    @ApiModelProperty(value = "邮政编码")
    private Integer zipCode;

    /**
     *  区号
     */
    @ApiModelProperty(value = "区号")
    private String cityCode;

    /**
     *  名称
     */
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     *  简称
     */
    @ApiModelProperty(value = "简称")
    private String shortName;

    /**
     *  组合名
     */
    @ApiModelProperty(value = "组合名")
    private String mergerName;

    /**
     *  拼音
     */
    @ApiModelProperty(value = "拼音")
    private String pinyin;

    /**
     *  经度
     */
    @ApiModelProperty(value = "经度")
    private BigDecimal lng;

    /**
     *  纬度
     */
    @ApiModelProperty(value = "纬度")
    private BigDecimal lat;


}
