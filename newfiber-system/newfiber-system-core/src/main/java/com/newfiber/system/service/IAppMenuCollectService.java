package com.newfiber.system.service;

import com.newfiber.system.domain.AppMenuCollect;
import com.newfiber.system.domain.request.appMenuCollect.AppMenuCollectQueryRequest;
import com.newfiber.system.domain.request.appMenuCollect.AppMenuCollectSaveRequest;
import java.util.List;

/**
 * APP菜单收藏Service接口
 * 
 * @author X.K
 * @date 2023-03-29
 */
public interface IAppMenuCollectService {


    /**
     * 新增APP菜单收藏
     *
     * @param request 新增参数
     * @return 结果
     */
    boolean collectAndUnCollect(AppMenuCollectSaveRequest request);

    /**
     * 批量删除APP菜单收藏
     *
     * @param  ids 编号(,隔开)
     * @return 结果
     */
    boolean delete(String ids);

    /**
     * 详细查询APP菜单收藏
     *
     * @param id 主键
     * @return APP菜单收藏
     */
     AppMenuCollect selectDetail(Long id);

     /**
      * 分页查询APP菜单收藏
      *
      * @param request 分页参数
      * @return APP菜单收藏集合
      */
     List<AppMenuCollect> selectPage(AppMenuCollectQueryRequest request);

     /**
      * 列表查询APP菜单收藏
      *
      * @param request 列表参数
      * @return APP菜单收藏集合
      */
     List<AppMenuCollect> selectList(AppMenuCollectQueryRequest request);

}
