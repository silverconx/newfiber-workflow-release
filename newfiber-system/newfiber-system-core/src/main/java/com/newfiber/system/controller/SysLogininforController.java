package com.newfiber.system.controller;

import com.newfiber.common.core.utils.poi.ExcelUtil;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import com.newfiber.common.redis.service.RedisService;
import com.newfiber.common.security.annotation.InnerAuth;
import com.newfiber.common.security.annotation.RequiresPermissions;
import com.newfiber.system.api.domain.SysLogininfor;
import com.newfiber.system.api.enums.ESystemRedisKey;
import com.newfiber.system.service.ISysLogininforService;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统访问记录
 * 
 * @author newfiber
 */
@RestController
@RequestMapping("/logininfor")
public class SysLogininforController extends BaseController{
    @Autowired
    private ISysLogininforService logininforService;

    @Autowired
    private RedisService redisService;

    @RequiresPermissions("system:logininfor:list")
    @GetMapping("/list")
    public PageResult list(SysLogininfor logininfor){
        startPage();
        List<SysLogininfor> list = logininforService.selectLogininforList(logininfor);
        return pageResult(list);
    }

    @Log(title = "登录日志", businessType = BusinessType.EXPORT)
    @RequiresPermissions("system:logininfor:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysLogininfor logininfor){
        List<SysLogininfor> list = logininforService.selectLogininforList(logininfor);
        ExcelUtil<SysLogininfor> util = new ExcelUtil<SysLogininfor>(SysLogininfor.class);
        util.exportExcel(response, list, "登录日志");
    }

    @RequiresPermissions("system:logininfor:remove")
    @Log(title = "登录日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{infoIds}")
    public Result remove(@PathVariable Long[] infoIds){
        return result(logininforService.deleteLogininforByIds(infoIds));
    }

    @RequiresPermissions("system:logininfor:remove")
    @Log(title = "登录日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/clean")
    public Result clean(){
        logininforService.cleanLogininfor();
        return success();
    }

    @RequiresPermissions("system:logininfor:unlock")
    @Log(title = "账户解锁", businessType = BusinessType.OTHER)
    @GetMapping("/unlock/{userName}")
    public Result unlock(@PathVariable("userName") String userName){
        redisService.deleteObject(ESystemRedisKey.PwdErrCnt, userName);
        return success();
    }

    @InnerAuth
    @PostMapping
    public Result add(@RequestBody SysLogininfor logininfor){
        return result(logininforService.insertLogininfor(logininfor));
    }
}
