package com.newfiber.system.enums;

import com.newfiber.common.core.web.support.IRedisPrefix;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author : X.K
 * @since : 2023/2/28 下午3:03
 */
@Getter
@AllArgsConstructor
public enum EManaggeRedisKey implements IRedisPrefix {

	/**
	 *
	 */


	PlatformTump("PlatformTump", "项目跳转")

	;

	private final String moduleKey;

	private final String moduleName;

}
