package com.newfiber.system.controller;

import java.util.List;
import javax.annotation.Resource;

import com.newfiber.system.domain.NewsInfo;
import com.newfiber.system.domain.request.sysNews.NewsInfoQueryRequest;
import com.newfiber.system.domain.request.sysNews.NewsInfoSaveRequest;
import com.newfiber.system.domain.request.sysNews.NewsInfoUpdateRequest;
import com.newfiber.system.service.INewsInfoService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import com.newfiber.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;

/**
 * 新闻信息Controller
 *
 * @author newfiber
 * @date 2023-03-31
 */
@RestController
@RequestMapping("/sysNewsInfo")
@Api(value = "新闻信息", tags = "新闻信息")
public class SysNewsInfoController extends BaseController {

    @Resource
    private INewsInfoService newsInfoService;

    /**
     * 新增新闻信息
     */
    @PostMapping("add")
    //@RequiresPermissions("system:info:add")
    @ApiOperation(value = "新增新闻信息", position = 10)
    @Log(title = "新闻信息", businessType = BusinessType.INSERT)
    public Result<Long> add(@RequestBody NewsInfoSaveRequest request) {
        return success(newsInfoService.insert(request));
    }


    /**
     * 修改新闻信息
     */
    @PutMapping("edit")
    @RequiresPermissions("system:info:edit")
    @ApiOperation(value = "修改新闻信息", position = 20)
    @Log(title = "新闻信息", businessType = BusinessType.UPDATE)
    public Result<Object> edit(@RequestBody NewsInfoUpdateRequest request) {
        return success(newsInfoService.update(request));
    }

    /**
     * 删除新闻信息
     */
    @DeleteMapping("/{ids}")
    @RequiresPermissions("system:info:remove")
    @ApiOperation(value = "删除新闻信息", notes = "传入ids(,隔开)", position = 30)
    @Log(title = "新闻信息", businessType = BusinessType.DELETE)
    public Result<Object> remove(@PathVariable String ids) {
        return success(newsInfoService.delete(ids));
    }

    /**
     * 详细查询新闻信息
     */
    @GetMapping(value = "/{id}")
    @RequiresPermissions("system:info:detail")
    @ApiOperation(value = "详细查询新闻信息", position = 40)
    public Result<NewsInfo> detail(@PathVariable("id") Long id) {
        return success(newsInfoService.selectDetail(id));
    }

    /**
     * 分页查询新闻信息
     */
    @GetMapping("/page")
    @RequiresPermissions("system:info:page")
    @ApiOperation(value = "分页查询新闻信息", position = 50)
    public PageResult<List<NewsInfo>> page(NewsInfoQueryRequest request) {
        startPage();
        List<NewsInfo> list = newsInfoService.selectPage(request);
        return pageResult(list);
    }

    /**
     * 列表查询新闻信息
     */
    @GetMapping("/list")
    @RequiresPermissions("system:info:list")
    @ApiOperation(value = "列表查询新闻信息", position = 60)
    public Result<List<NewsInfo>> list(NewsInfoQueryRequest request) {
        List<NewsInfo> list = newsInfoService.selectList(request);
        return success(list);
    }

}
