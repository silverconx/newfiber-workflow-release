package com.newfiber.system.service;

import com.newfiber.system.domain.AppMenu;
import com.newfiber.system.domain.request.appMenu.AppMenuQueryRequest;
import com.newfiber.system.domain.request.appMenu.AppMenuSaveRequest;
import com.newfiber.system.domain.request.appMenu.AppMenuUpdateRequest;
import com.newfiber.system.domain.vo.TreeSelect;
import java.util.List;

/**
 * APP菜单Service接口
 * 
 * @author X.K
 * @date 2023-03-20
 */
public interface IAppMenuService {

    /**
     * 新增APP菜单
     *
     * @param request 新增参数
     * @return 结果
     */
    long insert(AppMenuSaveRequest request);

    /**
     * 修改APP菜单
     *
     * @param request 修改参数
     * @return 结果
     */
    boolean update(AppMenuUpdateRequest request);

	/**
	 *
	 */
	void updateAppRoleMenu(Long roleId, List<Long> appMenuIdList);

    /**
     * 批量删除APP菜单
     *
     * @param  ids 编号(,隔开)
     * @return 结果
     */
    boolean delete(String ids);

    /**
     * 详细查询APP菜单
     *
     * @param id 主键
     * @return APP菜单
     */
     AppMenu selectDetail(Long id);

	/**
	 * 分页查询APP菜单
	 *
	 * @param request 分页参数
	 * @return APP菜单集合
	 */
	List<AppMenu> selectMenuList(AppMenuQueryRequest request, Long userId);

	/**
	 *
	 */
	List<TreeSelect> buildMenuTreeSelect(List<AppMenu> menus);

	/**
	 * 根据用户ID查询菜单树信息
	 *
	 * @param userId 用户ID
	 * @return 菜单列表
	 */
	List<AppMenu> selectMenuTreeByUserId(Long userId);

     /**
      * 分页查询APP菜单
      *
      * @param request 分页参数
      * @return APP菜单集合
      */
     List<AppMenu> selectPage(AppMenuQueryRequest request);

     /**
      * 列表查询APP菜单
      *
      * @param request 列表参数
      * @return APP菜单集合
      */
     List<AppMenu> selectList(AppMenuQueryRequest request);

	/**
	 * 列表查询APP菜单
	 *
	 * @return APP菜单集合
	 */
	List<AppMenu> selectList(Long userId);

	/**
	 *
	 */
	List<Long> selectMenuListByRoleId(Long roleId);

	/**
	 *
	 */
	List<AppMenu> buildMenuTree(List<AppMenu> menus);
}
