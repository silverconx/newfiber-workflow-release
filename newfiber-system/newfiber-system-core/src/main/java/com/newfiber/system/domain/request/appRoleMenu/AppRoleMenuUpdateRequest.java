package com.newfiber.system.domain.request.appRoleMenu;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * APP角色菜单关联对象 sys_app_role_menu
 *
 * @author X.K
 * @date 2023-03-20
 */
@Data
public class AppRoleMenuUpdateRequest{

    /**
     * 主键
     */
    @NotNull
    @ApiModelProperty(value = "主键", required = true)
    private Long id;

    /**
     * 菜单id
     */
    @ApiModelProperty(value = "菜单id")
    private Long menuId;

    /**
     * 角色id
     */
    @ApiModelProperty(value = "角色id")
    private Long roleId;
}
