package com.newfiber.system.controller;

import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import com.newfiber.common.security.annotation.RequiresPermissions;
import com.newfiber.common.security.utils.SecurityUtils;
import com.newfiber.system.domain.AppMenu;
import com.newfiber.system.domain.request.appMenu.AppMenuQueryRequest;
import com.newfiber.system.domain.request.appMenu.AppMenuSaveRequest;
import com.newfiber.system.domain.request.appMenu.AppMenuUpdateRequest;
import com.newfiber.system.domain.request.appMenuCollect.AppMenuCollectSaveRequest;
import com.newfiber.system.domain.response.RoleMenuTree;
import com.newfiber.system.domain.vo.TreeSelect;
import com.newfiber.system.service.IAppMenuCollectService;
import com.newfiber.system.service.IAppMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * APP菜单Controller
 * 
 * @author X.K
 * @date 2023-03-20
 */
@RestController
@RequestMapping("/appMenu")
@Api(value = "APP菜单", tags = "APP菜单")
public class AppMenuController extends BaseController {

    @Resource
    private IAppMenuService appMenuService;

    @Resource
    private IAppMenuCollectService appMenuCollectService;

    /**
     * 新增APP菜单
     */
    @PostMapping("add")
    @RequiresPermissions("system:appMenu:add")
    @ApiOperation(value = "新增APP菜单", position = 10)
    @Log(title = "APP菜单", businessType = BusinessType.INSERT)
    public Result<Long> add(@RequestBody AppMenuSaveRequest request) {
        return success(appMenuService.insert(request));
    }

    /**
     * 修改APP菜单
     */
    @PutMapping("edit")
    @RequiresPermissions("system:appMenu:edit")
    @ApiOperation(value = "修改APP菜单", position = 20)
    @Log(title = "APP菜单", businessType = BusinessType.UPDATE)
    public Result<Object> edit(@RequestBody AppMenuUpdateRequest request) {
        return success(appMenuService.update(request));
    }

    /**
     * 删除APP菜单
     */
    @DeleteMapping("/{ids}")
    @RequiresPermissions("system:appMenu:remove")
    @ApiOperation(value = "删除APP菜单", notes = "传入ids(,隔开)", position = 30)
    @Log(title = "APP菜单", businessType = BusinessType.DELETE)
    public Result<Object> remove(@PathVariable String ids) {
        return success(appMenuService.delete(ids));
    }

    /**
     * 详细查询APP菜单
     */
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "详细查询APP菜单", position = 40)
    public Result<AppMenu> detail(@PathVariable("id") Long id) {
        return success(appMenuService.selectDetail(id));
    }

    /**
     * 分页查询APP菜单
     */
    @GetMapping("/page")
    @ApiOperation(value = "分页查询APP菜单", position = 50)
    public PageResult<List<AppMenu>> page(AppMenuQueryRequest request) {
        startPage();
        List<AppMenu> list = appMenuService.selectPage(request);
        return pageResult(list);
    }

    /**
     * 列表查询APP菜单
     */
    @GetMapping("/list")
    @ApiOperation(value = "列表查询APP菜单", position = 60)
    public Result<List<AppMenu>> list(AppMenuQueryRequest request) {
        List<AppMenu> list = appMenuService.selectList(request);
        return success(list);
    }

	/**
	 * 获取菜单下拉树列表
	 */
	@GetMapping("/treeselect")
	@ApiOperation(value = "获取菜单下拉树列表", position = 60)
	public Result<List<TreeSelect>> treeselect(AppMenuQueryRequest request){
		Long userId = null != request.getUserId() ? request.getUserId() : SecurityUtils.getUserId();
		List<AppMenu> menus = appMenuService.selectMenuList(request, userId);
		return success(appMenuService.buildMenuTreeSelect(menus));
	}

	/**
	 * 获取用户菜单
	 */
	@GetMapping("/treeUserMenu")
	@ApiOperation(value = "获取用户菜单", position = 60)
	public Result<List<AppMenu>> treeUserMenu(AppMenuQueryRequest request){
		Long userId = null != request.getUserId() ? request.getUserId() : SecurityUtils.getUserId();
		List<AppMenu> menus = appMenuService.selectMenuList(request, userId);
		return success(appMenuService.buildMenuTree(menus));
	}

	/**
	 * 加载对应角色菜单列表树
	 */
	@GetMapping(value = "/roleMenuTreeselect/{roleId}")
	@ApiOperation(value = "加载对应角色菜单列表树", position = 60)
	public Result<RoleMenuTree> roleMenuTreeselect(@PathVariable("roleId") Long roleId){
		Long userId = SecurityUtils.getUserId();
		List<AppMenu> menus = appMenuService.selectList(userId);
		List<Long> checkedKeys = appMenuService.selectMenuListByRoleId(roleId);
		List<TreeSelect> treeMenus = appMenuService.buildMenuTreeSelect(menus);
		return Result.success(new RoleMenuTree(checkedKeys, treeMenus));
	}

	/**
	 * 收藏/取消收藏菜单
	 */
	@PostMapping("/collect_un_collect")
	@ApiOperation(value = "收藏/取消收藏菜单", position = 60)
	public Result<Boolean> collectAndUnCollect(@RequestBody AppMenuCollectSaveRequest request){
		return success(appMenuCollectService.collectAndUnCollect(request));
	}

	/**
	 * 获取路由信息
	 *
	 * @return 路由信息
	 */
	@GetMapping("getRouters")
	public Result<List<AppMenu>> getRouters(){
		Long userId = SecurityUtils.getUserId();
		List<AppMenu> menus = appMenuService.selectMenuTreeByUserId(userId);
		return success(menus);
	}
}
