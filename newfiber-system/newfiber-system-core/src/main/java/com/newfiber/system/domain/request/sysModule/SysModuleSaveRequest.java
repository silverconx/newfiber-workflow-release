package com.newfiber.system.domain.request.sysModule;

import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;

/**
 * 系统模块对象 sys_module
 *
 * @author newfiber
 * @date 2023-02-21
 */
@Data
public class SysModuleSaveRequest{

    /**
     * 模块编号
     */
    @NotBlank
    @ApiModelProperty(value = "模块编号", required = true)
    private String moduleKey;

    /**
     * 模块名称
     */
    @NotBlank
    @ApiModelProperty(value = "模块名称", required = true)
    private String moduleName;

    /**
     * 备注
     */
    @NotBlank
    @ApiModelProperty(value = "备注", required = true)
    private String remark;

    /**
     * 状态
     */
    @NotBlank
    @ApiModelProperty(value = "状态", required = true)
    private String status;

}
