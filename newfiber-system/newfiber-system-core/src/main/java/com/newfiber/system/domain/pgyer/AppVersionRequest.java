package com.newfiber.system.domain.pgyer;

import lombok.Data;

/**
 * @author : X.K
 * @since : 2023/7/31 上午10:45
 */
@Data
public class AppVersionRequest {

	private String buildVersion;
	private String channelKey;
	private int buildBuildVersion;
}
