package com.newfiber.system.controller;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import com.newfiber.common.security.annotation.RequiresPermissions;
import com.newfiber.system.domain.SysModule;
import com.newfiber.system.domain.request.sysModule.SysModuleSaveRequest;
import com.newfiber.system.domain.request.sysModule.SysModuleUpdateRequest;
import com.newfiber.system.domain.request.sysModule.SysModuleQueryRequest;
import com.newfiber.system.service.ISysModuleService;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;

/**
 * 系统模块Controller
 * 
 * @author lufan
 * @date 2023-02-21
 */
@RestController
@RequestMapping("/sysModule")
public class SysModuleController extends BaseController {

    @Resource
    private ISysModuleService sysModuleService;

    /**
     * 新增系统模块
     */
    @PostMapping
    @RequiresPermissions("system:module:add")
    @Log(title = "系统模块", businessType = BusinessType.INSERT)
    public Result<Long> add(@RequestBody SysModuleSaveRequest request) {
        return success(sysModuleService.insert(request));
    }

    /**
     * 修改系统模块
     */
    @PutMapping
    @RequiresPermissions("system:module:edit")
    @Log(title = "系统模块", businessType = BusinessType.UPDATE)
    public Result<Object> edit(@RequestBody SysModuleUpdateRequest request) {
        return success(sysModuleService.update(request));
    }

    /**
     * 删除系统模块
     */
    @DeleteMapping("/{ids}")
    @RequiresPermissions("system:module:remove")
    @Log(title = "系统模块", businessType = BusinessType.DELETE)
    public Result<Object> remove(@PathVariable String ids) {
        return success(sysModuleService.delete(ids));
    }

    /**
     * 详细查询系统模块
     */
    @GetMapping(value = "/{id}")
    @RequiresPermissions("system:module:detail")
    public Result<SysModule> detail(@PathVariable("id") Long id) {
        return success(sysModuleService.selectDetail(id));
    }

    /**
     * 分页查询系统模块
     */
    @GetMapping("/page")
    @RequiresPermissions("system:module:page")
    public PageResult<List<SysModule>> page(SysModuleQueryRequest request) {
        startPage();
        List<SysModule> list = sysModuleService.selectPage(request);
        return pageResult(list);
    }

    /**
     * 列表查询系统模块
     */
    @GetMapping("/list")
    @RequiresPermissions("system:module:list")
    public Result<List<SysModule>> list(SysModuleQueryRequest request) {
        List<SysModule> list = sysModuleService.selectList(request);
        return success(list);
    }

}
