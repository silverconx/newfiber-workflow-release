package com.newfiber.system.domain.request.sysNoticePerson;

import lombok.Data;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import static cn.hutool.core.date.DatePattern.NORM_DATETIME_PATTERN;

/**
 * 通知公告-已读未读对象 sys_notice_person
 * date: 2023/4/24 下午 08:14
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
public class SysNoticePersonSaveRequest {

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

    /**
     * 公告ID
     */
    @ApiModelProperty(value = "公告ID")
    private Integer noticeId;

    /**
     * 通知人
     */
    @ApiModelProperty(value = "通知人")
    private String noticePerson;

    /**
     * 是否已读（0未读 1已读)
     */
    @ApiModelProperty(value = "是否已读（0未读 1已读)")
    private String readStatus;

    /**
     * 读取时间
     */
    @ApiModelProperty(value = "读取时间")
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    private Date readTime;

}
