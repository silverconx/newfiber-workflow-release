package com.newfiber.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newfiber.system.domain.AppVersion;

/**
 * APP菜单收藏Mapper接口
 * 
 * @author X.K
 * @date 2023-03-29
 */
public interface AppVersionMapper extends BaseMapper<AppVersion>{

}
