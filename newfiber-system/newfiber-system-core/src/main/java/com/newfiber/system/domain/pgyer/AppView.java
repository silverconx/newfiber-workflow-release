package com.newfiber.system.domain.pgyer;

import java.util.Date;
import java.util.List;
import lombok.Data;

/**
 * @author : X.K
 * @since : 2023/7/31 上午10:45
 */
@Data
public class AppView {

	private String buildKey;
	private String buildType;
	private int buildIsFirst;
	private String buildIsLastest;
	private String buildFileSize;
	private String buildName;
	private String buildPassword;
	private String buildVersion;
	private String buildVersionNo;
	private String buildQrcodeShowAppIcon;
	private String buildVersionType;
	private String buildBuildVersion;
	private String buildIdentifier;
	private String buildIcon;
	private String buildDescription;
	private String buildUpdateDescription;
	private String buildScreenshots;
	private String buildShortcutUrl;
	private String buildSignatureType;
	private String buildIsAcceptFeedback;
	private String buildIsUploadCrashlog;
	private String buildIsOriginalBuildInHouse;
	private String buildAdhocUuids;
	private String buildTemplate;
	private String buildInstallType;
	private String buildManuallyBlocked;
	private String buildIsPlaceholder;
	private String buildCates;
	private Date buildCreated;
	private Date buildUpdated;
	private String buildQRCodeURL;
	private int isOwner;
	private int isJoin;
	private String buildFollowed;
	private Date appExpiredDate;
	private boolean isImmediatelyExpired;
	private int appExpiredStatus;
	private List<String> otherApps;
	private String otherAppsCount;
	private int todayDownloadCount;
	private String appKey;
	private String appAutoSync;
	private String appShowPgyerCopyright;
	private String appDownloadPay;
	private String appDownloadDescription;
	private String appGameLicenseStatus;
	private String appLang;
	private String appIsTestFlight;
	private String appIsInstallDate;
	private String appInstallStartDate;
	private String appInstallEndDate;
	private String appInstallQuestion;
	private String appInstallAnswer;
	private String appFeedbackStatus;
	private int isMerged;
	private String mergeAppInfo;
	private int canPayDownload;
	private String iconUrl;
	private List<String> buildScreenshotsUrl;
}
