package com.newfiber.system.controller;

import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import com.newfiber.common.security.utils.SecurityUtils;
import com.newfiber.system.api.domain.SysNotice;
import com.newfiber.system.service.ISysNoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 公告 信息操作处理
 * 
 * @author newfiber
 */
@RestController
@RequestMapping("/notice")
@Api(value = "通知公告", tags = "通知公告")
public class SysNoticeController extends BaseController{
    @Autowired
    private ISysNoticeService noticeService;

    /**
     * 获取通知公告列表
     */
    @GetMapping("/list")
    public PageResult list(SysNotice notice){
        startPage();
        List<SysNotice> list = noticeService.selectNoticeList(notice);
        return pageResult(list);
    }

    /**
     * 根据通知公告编号获取详细信息
     */
    @GetMapping(value = "/{noticeId}")
    public Result getInfo(@PathVariable Long noticeId){
        return success(noticeService.selectNoticeById(noticeId));
    }

    /**
     * 新增通知公告
     */
    @PostMapping("/add")
    public Result<String> add(@RequestBody SysNotice notice){
        notice.setCreateBy(SecurityUtils.getUsername());
	    noticeService.insertNotice(notice);
        return success();
    }

    /**
     * 修改通知公告
     */
    @Log(title = "通知公告", businessType = BusinessType.UPDATE)
    @PutMapping
    public Result edit(@Validated @RequestBody SysNotice notice){
        notice.setUpdateBy(SecurityUtils.getUsername());
        return result(noticeService.updateNotice(notice));
    }

	/**
	 * 处理通知公告
	 */
	@ApiOperation(value = "处理通知公告", position = 10)
	@GetMapping("/handle/{noticeIds}")
	public Result handle(@PathVariable Long[] noticeIds){
		return result(noticeService.deleteNoticeByIds(noticeIds));
	}

    /**
     * 删除通知公告
     */
    @Log(title = "通知公告", businessType = BusinessType.DELETE)
    @DeleteMapping("/{noticeIds}")
    public Result remove(@PathVariable Long[] noticeIds){
        return result(noticeService.deleteNoticeByIds(noticeIds));
    }
}
