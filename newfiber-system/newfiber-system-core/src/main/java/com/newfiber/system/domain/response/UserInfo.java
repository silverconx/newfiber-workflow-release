package com.newfiber.system.domain.response;

import com.newfiber.system.api.domain.SysRole;
import com.newfiber.system.api.domain.SysUser;
import com.newfiber.system.domain.SysPost;
import java.util.List;
import java.util.Set;
import lombok.Data;

/**
 * @author : X.K
 * @since : 2023/2/13 上午10:35
 */
@Data
public class UserInfo {
	SysUser user;
	Set<String> roleIds;
	Set<String> permissions;
	List<SysRole> roles;
	List<SysPost> posts;
	List<Long> postIds;
}
