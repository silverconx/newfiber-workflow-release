package com.newfiber.system.domain.request.region;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.newfiber.common.core.web.request.BaseQueryRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 中国行政地区对象 sys_region
 *
 * @author 张鸿志
 * @date 2023-07-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RegionQueryRequest extends BaseQueryRequest {

    /**
     * 层级
     */
    @ApiModelProperty(value = "层级")
    private Integer level;

    /**
     * 父级行政代码
     */
    @ApiModelProperty(value = "父级行政代码")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long parentCode;

    /**
     * 行政代码
     */
    @ApiModelProperty(value = "行政代码")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long areaCode;


    /**
     * 区号
     */
    @ApiModelProperty(value = "区号")
    private String cityCode;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String name;

}
