package com.newfiber.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

import com.newfiber.system.domain.request.sysNoticePerson.SysNoticePersonQueryRequest;
import com.newfiber.system.domain.SysNoticePerson;
import org.apache.ibatis.annotations.Param;

/**
 * 通知公告-已读未读Mapper接口
 * date: 2023/4/24 下午 08:13
 *
 * @author: LuFan
 * @since JDK 1.8
 */
public interface SysNoticePersonMapper extends BaseMapper<SysNoticePerson> {

    /**
     * 条件查询通知公告-已读未读列表
     *
     * @param request 查询条件
     * @return 通知公告-已读未读集合
     */
    List<SysNoticePerson> selectByCondition(@Param("request") SysNoticePersonQueryRequest request);

    /**
     * 条件查询通知公告-已读未读列表
     *
     * @param id 查询条件
     * @return 通知公告-已读未读集合
     */
    SysNoticePerson selectOneById(@Param("id") Long id);

}
