package com.newfiber.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.newfiber.system.domain.AppRoleMenu;
import com.newfiber.system.mapper.AppRoleMenuMapper;
import com.newfiber.system.service.IAppRoleMenuService;
import org.springframework.stereotype.Service;

/**
 * APP角色菜单关联Service业务层处理
 * 
 * @author X.K
 * @date 2023-03-20
 */
@Service
public class AppRoleMenuServiceImpl extends ServiceImpl<AppRoleMenuMapper, AppRoleMenu> implements IAppRoleMenuService {

}
