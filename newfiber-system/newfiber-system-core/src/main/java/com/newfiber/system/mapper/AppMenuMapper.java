package com.newfiber.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newfiber.system.domain.AppMenu;
import com.newfiber.system.domain.request.appMenu.AppMenuQueryRequest;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * APP菜单Mapper接口
 * 
 * @author X.K
 * @date 2023-03-20
 */
public interface AppMenuMapper extends BaseMapper<AppMenu>{

    /**
     * 条件查询APP菜单列表
     * 
     * @param request 查询条件
     * @return APP菜单集合
     */
    List<AppMenu> selectByCondition(@Param("request") AppMenuQueryRequest request);

	/**
	 * 条件查询APP菜单列表
	 *
	 * @param id 查询条件
	 * @return APP菜单集合
	 */
	AppMenu selectOneById(@Param("id") Long id);

	/**
	 * 根据角色ID查询菜单树信息
	 *
	 * @param roleId 角色ID
	 * @param menuCheckStrictly 菜单树选择项是否关联显示
	 * @return 选中菜单列表
	 */
	List<Long> selectMenuListByRoleId(@Param("roleId") Long roleId, @Param("menuCheckStrictly") boolean menuCheckStrictly);

	/**
	 * 根据用户ID查询菜单
	 *
	 * @param userId 用户ID
	 * @return 菜单列表
	 */
	List<AppMenu> selectMenuTreeByUserId(Long userId);

}
