package com.newfiber.system.service;

import java.util.List;
import com.newfiber.system.domain.NewsInfo;
import com.newfiber.system.domain.request.sysNews.NewsInfoQueryRequest;
import com.newfiber.system.domain.request.sysNews.NewsInfoSaveRequest;
import com.newfiber.system.domain.request.sysNews.NewsInfoUpdateRequest;

/**
 * 新闻信息Service接口
 *
 * @author newfiber
 * @date 2023-03-31
 */
public interface INewsInfoService {

    /**
     * 新增新闻信息
     *
     * @param request 新增参数
     * @return 结果
     */
    long insert(NewsInfoSaveRequest request);

    /**
     * 修改新闻信息
     *
     * @param request 修改参数
     * @return 结果
     */
    boolean update(NewsInfoUpdateRequest request);

    /**
     * 批量删除新闻信息
     *
     * @param  ids 编号(,隔开)
     * @return 结果
     */
    boolean delete(String ids);

    /**
     * 详细查询新闻信息
     *
     * @param id 主键
     * @return 新闻信息
     */
     NewsInfo selectDetail(Long id);

     /**
      * 分页查询新闻信息
      *
      * @param request 分页参数
      * @return 新闻信息集合
      */
     List<NewsInfo> selectPage(NewsInfoQueryRequest request);

     /**
      * 列表查询新闻信息
      *
      * @param request 列表参数
      * @return 新闻信息集合
      */
     List<NewsInfo> selectList(NewsInfoQueryRequest request);

}
