package com.newfiber.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import com.newfiber.system.domain.request.sysModule.SysModuleQueryRequest;
import com.newfiber.system.domain.SysModule;
import org.apache.ibatis.annotations.Param;

/**
 * 系统模块Mapper接口
 * 
 * @author lufan
 * @date 2023-02-21
 */
public interface SysModuleMapper extends BaseMapper<SysModule>{

    /**
     * 条件查询系统模块列表
     * 
     * @param request 查询条件
     * @return 系统模块集合
     */
    List<SysModule> selectByCondition(@Param("request") SysModuleQueryRequest request);

}
