package com.newfiber.system.domain.request.sysNews;

import com.newfiber.system.api.domain.request.SysFileSaveRequest;
import lombok.Data;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;
import static cn.hutool.core.date.DatePattern.NORM_DATETIME_PATTERN;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import java.util.List;


/**
 * 新闻信息对象 sys_news_info
 *
 * @author newfiber
 * @date 2023-03-31
 */
@Data
public class NewsInfoSaveRequest{

    /**
     * 主键id
     */
    @ApiModelProperty(value = "主键id")
    private Long id;

    /**
     * 新闻标题
     */
    @ApiModelProperty(value = "新闻标题")
    private String newsTitle;

    /**
     * 新闻类别
     */
    @ApiModelProperty(value = "新闻类别")
    private String newsType;

    /**
     * 发布时间
     */
    @ApiModelProperty(value = "发布时间")
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    private Date publishTime;

    /**
     * 是否置顶  0 否 1是
     */
    @ApiModelProperty(value = "是否置顶  0 否 1是")
    private Long isTop;

    /**
     * 浏览次数
     */
    @ApiModelProperty(value = "浏览次数")
    private Long browseNum;

    /**
     * 是否显示  0 否 1是
     */
    @ApiModelProperty(value = "是否显示  0 否 1是")
    private Long isShow;

    /**
     * 新闻内容
     */
    @ApiModelProperty(value = "新闻内容")
    private String newsContent;

    /**
     * 创建者
     */
    @ApiModelProperty(value = "创建者")
    private String createBy;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    private Date createTime;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    private String status;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除）")
    private String delFlag;

    /**
     * 更新者
     */
    @ApiModelProperty(value = "更新者")
    private String updateBy;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    private Date updateTime;


    /**
     * 文件
     */
    @ApiModelProperty(value = "文件")
    private List<SysFileSaveRequest> fileSaveRequestList;

}
