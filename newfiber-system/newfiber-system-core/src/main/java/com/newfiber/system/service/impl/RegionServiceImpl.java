package com.newfiber.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.newfiber.common.core.exception.ServiceException;
import com.newfiber.common.core.web.domain.BaseEntity;
import com.newfiber.common.core.web.service.BaseServiceImpl;
import com.newfiber.system.domain.Region;
import com.newfiber.system.domain.request.region.RegionQueryRequest;
import com.newfiber.system.domain.request.region.RegionSaveRequest;
import com.newfiber.system.domain.request.region.RegionUpdateRequest;
import com.newfiber.system.mapper.RegionMapper;
import com.newfiber.system.service.IRegionService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * 中国行政地区Service业务层处理
 * 
 * @author 张鸿志
 * @date 2023-07-18
 */
@Service
public class RegionServiceImpl extends BaseServiceImpl<RegionMapper, Region> implements IRegionService {

    @Resource
    private RegionMapper regionMapper;



    @Override
    public List<Region> selectList(RegionQueryRequest request) {
        //如果父节点为空 的情况下，level就设置为0，防止查询数据量大，卡死系统
        if(ObjectUtil.isEmpty(request.getParentCode()) && ObjectUtil.isEmpty(request.getLevel())){
            request.setLevel(0);
        }
        return regionMapper.selectByCondition(request);
    }

}
