import com.alibaba.fastjson2.JSONObject;
import java.util.HashMap;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.junit.jupiter.api.Test;

/**
 * @author : X.K
 * @since : 2023/2/9 下午2:27
 */
public class CommonTest {

	@Test
	public void mapTest(){
		MapTest mapTest = new MapTest("123");
		System.out.println(mapTest.getField());
		System.out.println(JSONObject.toJSONString(mapTest));
	}

}

@Data
@AllArgsConstructor
class MapTest extends HashMap<String, Object>{
	private String field;
}
