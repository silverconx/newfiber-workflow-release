package com.newfiber.system;

import com.newfiber.common.security.annotation.EnableCustomConfig;
import com.newfiber.common.security.annotation.EnableCustomFeignClients;
import com.newfiber.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 业务模块
 * 
 * @author newfiber
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableCustomFeignClients
@EnableScheduling
@SpringBootApplication(scanBasePackages = {"com.newfiber.system", "com.newfiber.business.api"})
public class NewfiberSystemApplication {
    public static void main(String[] args){
        SpringApplication.run(NewfiberSystemApplication.class, args);
	    System.out.println("(♥◠‿◠)ﾉﾞ  系统模块启动成功   ლ(´ڡ`ლ)ﾞ  ");
    }
}
