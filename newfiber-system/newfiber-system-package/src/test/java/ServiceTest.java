import com.newfiber.common.core.web.domain.Result;
import com.newfiber.system.NewfiberSystemApplication;
import com.newfiber.system.controller.SmsEndpoint;
import javax.annotation.Resource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author : X.K
 * @since : 2023/10/13 上午9:29
 */
//@Slf4j
@SpringBootTest(classes = NewfiberSystemApplication.class)
@RunWith(SpringRunner.class)
public class ServiceTest {

	@Resource
	private SmsEndpoint smsEndpoint;

	@Test
	public void aliyunSmsTest(){
//		smsEndpoint.sendMessage("SMS_257847870", "{\"code\":\"1111\"}", "18772346904");

//		smsEndpoint.sendValidate( "SMS_257847870", "18772346904");

		Result<String> result = smsEndpoint.validateMessage("SMS_257847870", "18772346904", "9315");

		System.out.println(1);
	}
}
