//import com.vividsolutions.jts.geom.Geometry;
//import com.vividsolutions.jts.io.WKBReader;
//import com.vividsolutions.jts.io.WKBWriter;
//import dm.jdbc.driver.DmdbBlob;
//import dm.jdbc.driver.DmdbConnection;
//import java.sql.Blob;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.Statement;
//import java.sql.Struct;
//import org.junit.Test;
//
///**
// * @author : X.K
// * @since : 2023/7/26 下午2:03
// */
//public class DmTest {
//
//	@Test
//	public void test1() throws Exception{
//		String URL = "jdbc:dm://192.168.30.93:5236/newfiber_standard?schema=newfiber_standard";
//		String USER_NAME = "SYSDBA";
//		String PASSWORD = "SYSDBA";
//
//		// 1、通过反射加载驱动
//		// 1.1、即使注释这行依然能够运行，这是因为Java SPI机制会自动加载驱动
//		Class.forName("dm.jdbc.driver.DmDriver");
//
//		// 2、创建连接Collection
//		Connection connection = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
//
//		Statement stmt = connection.createStatement();
//
//		//获取数据表sub_types中的空间类型数据
//		ResultSet rs = stmt.executeQuery("select geometry from pat_patrol_section;");
//		while (rs.next()) {
//			// 将空间类型数据转换为STRUCT类型后，获取结构体中的成员属性信息
//			Struct struct = (Struct)rs.getObject(1);
//			Object[] attrs = struct.getAttributes();
//			int srid = (Integer)attrs[0];
//			Blob blob = (Blob)attrs[1];
//			int type = (Integer)attrs[2];
//
//			// 解析GEO_WKB属性信息
//			WKBReader reader = new WKBReader();
//			Geometry geometry = reader.read(blob.getBytes(1, (int)blob.length()));
//			System.out.println(geometry);
//
//			// 创建ST_Multipolygon空间类型的struct类型对象
//			WKBWriter writer = new WKBWriter();
//			attrs[1] = DmdbBlob.newInstanceOfLocal(writer.write(geometry), (DmdbConnection)connection);
//			struct = connection.createStruct("ST_MULTIPOLYGON", attrs);
//
//			//将创建好的struct类型对象插入数据表sub_types
//			PreparedStatement pstmt = connection.prepareStatement("insert into sub_types(sub_mpolygon) values(?)");
//			pstmt.setObject(1, struct);
//			pstmt.execute();
//		}
//		rs.close();
//		stmt.close();
//		connection.close();
//
//	}
//
//}