import cn.hutool.core.util.NumberUtil;
import com.alibaba.fastjson2.JSONObject;
import com.alibaba.fastjson2.TypeReference;
import com.newfiber.business.service.impl.TencentVoiceMessageServiceImpl;
import com.newfiber.common.security.utils.PwdUtil;
import com.newfiber.common.security.utils.SecurityUtils;
import java.math.BigDecimal;
import java.util.Map;
import org.junit.Test;

/**
 * @author : X.K
 * @since : 2023/7/5 上午9:03
 */
public class CommonTest {

//	@Test
//	public void dataSupportTest(){
//		System.out.println(DataSupport.nanMonitorPropertyCodeString());
//	}

	@Test
	public void commonTest1(){
		String str = "{\n"
			+ "    \"pdinfo\": {\n"
			+ "      \"D0\": \"||\",\n"
			+ "      \"D1\": \"&&\"\n"
			+ "    },\n"
			+ "    \"ruleId\": {\n"
			+ "      \"D0\": \">\",\n"
			+ "      \"D1\": \"<=\"\n"
			+ "    },\n"
			+ "    \"zhiVlue\": {\n"
			+ "      \"D0\": \"2\",\n"
			+ "      \"D1\": \"23\"\n"
			+ "    }\n"
			+ "  }";

		Map<String, Object> map = JSONObject.parseObject(str, new TypeReference<Map<String, Object>>(){});

		System.out.println(1);
	}

	@Test
	public void commonTest2(){
		String str = "{\n"
			+ "  \"warnFactor\": \"z,wlih1\",\n"
			+ "  \"warnFormula\": \"z>2||wlih1<=23\",\n"
			+ "  \"pcParseAssist\": {\n"
			+ "    \"pdinfo\": {\n"
			+ "      \"D0\": \"||\",\n"
			+ "      \"D1\": \"&&\"\n"
			+ "    },\n"
			+ "    \"ruleId\": {\n"
			+ "      \"D0\": \">\",\n"
			+ "      \"D1\": \"<=\"\n"
			+ "    },\n"
			+ "    \"zhiVlue\": {\n"
			+ "      \"D0\": \"2\",\n"
			+ "      \"D1\": \"23\"\n"
			+ "    }\n"
			+ "  },\n"
			+ "  \"type\": 1,\n"
			+ "  \"stCode\": \"0555010055\",\n"
			+ "  \"warnType\": \"water_level\",\n"
			+ "  \"timeThreshold\": \"2\",\n"
			+ "  \"warnLevel\": \"early_warn\",\n"
			+ "  \"remark\": \"222222\"\n"
			+ "}";
//		RtuWarnConfigSaveRequest request = JSONObject.parseObject(str, RtuWarnConfigSaveRequest.class);

		System.out.println(1);
	}

	@Test
	public void tencentVoiceCallTest(){
		TencentVoiceMessageServiceImpl tencentVoiceMessageService = new TencentVoiceMessageServiceImpl();
		String[] templateParamSet = {"熊凯", "7月31号11点", "光谷软件园"};
		tencentVoiceMessageService.call("1478317", templateParamSet, "18772346904");
	}

	@Test
	public void pwdTest(){
//		PwdUtil.passwordCheck("123456");
//		PwdUtil.passwordCheck("123456abd");
		PwdUtil.passwordCheck("123456abd@");
	}

	@Test
	public void pwdTest1(){
		System.out.println(SecurityUtils.matchesPassword("admin123", "$2a$10$EfK11Q..fzLkVX8oA1H2qu/7kO9IEbK0OiD59qZajjbPNJ.xnsYyS"));
		System.out.println(SecurityUtils.encryptPassword("Newfiber2023@"));
	}

	@Test
	public void div(){
		BigDecimal progress = BigDecimal.valueOf(NumberUtil.div(3F, 100f) * 100).setScale(2, BigDecimal.ROUND_DOWN);
		System.out.println(1);
	}

}
