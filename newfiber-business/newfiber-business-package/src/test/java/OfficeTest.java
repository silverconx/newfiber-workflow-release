import com.newfiber.business.NewfiberBusinessApplication;
import com.newfiber.business.domain.request.patrolExamine.PatrolExamineQueryRequest;
import com.newfiber.business.service.IPatrolExamineService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static cn.hutool.core.date.DatePattern.NORM_DATE_PATTERN;

@SpringBootTest(classes = NewfiberBusinessApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
//@AutoConfigureMockMvc
public class OfficeTest {

    @Resource
    private IPatrolExamineService patPatrolExamineService;

    @Test
    public void testExamineService() throws ParseException {
        PatrolExamineQueryRequest patrolExamine = new PatrolExamineQueryRequest();
        SimpleDateFormat sdf = new SimpleDateFormat(NORM_DATE_PATTERN);
        patrolExamine.setStartDate(sdf.parse("2022-03-01"));
        patrolExamine.setEndDate(sdf.parse("2023-03-31"));
//        patPatrolExamineService.export(patrolExamine);
    }

}
