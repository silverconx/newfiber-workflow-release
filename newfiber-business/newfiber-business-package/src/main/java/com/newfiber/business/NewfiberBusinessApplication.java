package com.newfiber.business;

import com.newfiber.common.security.annotation.EnableCustomConfig;
import com.newfiber.common.security.annotation.EnableCustomFeignClients;
import com.newfiber.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 业务模块
 * 
 * @author newfiber
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableCustomFeignClients
@EnableScheduling
@SpringBootApplication
	(scanBasePackages = {"com.newfiber.workflow", "com.newfiber.business", "com.newfiber.system.api", "com.newfiber.business.api"})
public class NewfiberBusinessApplication {
    public static void main(String[] args){
//        SpringApplication.run(NewfiberBusinessApplication.class, args);
//	    System.out.println("(♥◠‿◠)ﾉﾞ  业务模块启动成功   ლ(´ڡ`ლ)ﾞ  ");

	    SpringApplication springApplication = new SpringApplication(NewfiberBusinessApplication.class);
	    springApplication.setAllowBeanDefinitionOverriding(true);
	    springApplication.run(args);
	    System.out.println("(♥◠‿◠)ﾉﾞ  业务模块启动成功   ლ(´ڡ`ლ)ﾞ  ");
    }
}
