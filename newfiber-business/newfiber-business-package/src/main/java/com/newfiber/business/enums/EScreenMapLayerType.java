package com.newfiber.business.enums;

import com.newfiber.business.api.service.IScreenMapLayerService;
import com.newfiber.business.domain.response.ScreenMapLayType;
import com.newfiber.business.service.impl.RtuSiteInfoServiceImpl;
import com.newfiber.common.security.utils.ConfigUtils;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EScreenMapLayerType{

	/**
	 *
	 */
	RtuSitePipeline("RtuSite-Pipeline", "管网监测点", RtuSiteInfoServiceImpl.class, ERtuSiteType.Pipeline.getCode()),
	RtuSiteWaterlogging("RtuSite-Waterlogging", "内涝监测点", RtuSiteInfoServiceImpl.class, ERtuSiteType.Waterlogging.getCode()),
	RtuSiteWaterLevel("RtuSite-WaterLevel", "水位监测点", RtuSiteInfoServiceImpl.class, ERtuSiteType.WaterLevel.getCode()),
	RtuSiteQuality("RtuSite-Quality", "水质监测点", RtuSiteInfoServiceImpl.class, ERtuSiteType.Quality.getCode()),

	Project("Project", "工程项目", null, null),
	River("River", "河湖水系", null, null),
	Drainage("Drainage", "汇水分区", null, null),
	RtuRain("Rtu-Rain", "降雨", RtuSiteInfoServiceImpl.class, ERtuSiteType.Rain.getCode()),
	RtuFlow("Rtu-Flow", "流量", RtuSiteInfoServiceImpl.class, ERtuSiteType.Flow.getCode()),
	WaterloggingRisk("Waterlogging-Risk", "内涝风险", null, null),

	;

    private final String layerType;

    private final String layerName;

    private final Class<? extends IScreenMapLayerService> businessService;

    private final String businessType;

    public static List<ScreenMapLayType> parseType(){
	    List<ScreenMapLayType> screenMapLayTypeList = new ArrayList<>();
	    String iconPrefix = ConfigUtils.getConfig("iconPrefixUrl");
	    for(EScreenMapLayerType screenMapLayerType : EScreenMapLayerType.values()){
		    screenMapLayTypeList.add(new ScreenMapLayType(screenMapLayerType.getLayerType(), screenMapLayerType.layerName, iconPrefix.concat(screenMapLayerType.layerName).concat(".png")));
	    }
	    return screenMapLayTypeList;
    }
}
