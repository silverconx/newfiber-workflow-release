package com.newfiber.business.domain.response;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author : X.K
 * @since : 2023/10/7 上午10:47
 */
@Data
@AllArgsConstructor
public class ScreenMapLayType {

	private String layerType;

	private String layerName;

	private String iconUrl;

}
