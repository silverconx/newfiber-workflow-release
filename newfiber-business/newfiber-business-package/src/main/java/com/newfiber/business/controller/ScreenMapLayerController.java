package com.newfiber.business.controller;

import com.newfiber.business.api.domain.ScreenMapLayer;
import com.newfiber.business.domain.response.ScreenMapLayType;
import com.newfiber.business.enums.EScreenMapLayerType;
import com.newfiber.business.service.impl.ScreenMapLayerServiceImpl;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@RestController
@RequestMapping("/screen-map-layer")
@Api(value = "大屏图例", tags = "大屏图例")
public class ScreenMapLayerController extends BaseController {

    @Resource
    private ScreenMapLayerServiceImpl screenMapLayerService;

    @GetMapping("/list")
    @ApiOperation(value = "列表查询大屏图例")
    public Result<List<ScreenMapLayer>> list() {
        List<ScreenMapLayer> list = screenMapLayerService.parseScreenMapLayer();
        return success(list);
    }

	@GetMapping("/list-type")
	@ApiOperation(value = "列表查询大屏图例类型")
	public Result<List<ScreenMapLayType>> listType() {
		return success(EScreenMapLayerType.parseType());
	}

}
