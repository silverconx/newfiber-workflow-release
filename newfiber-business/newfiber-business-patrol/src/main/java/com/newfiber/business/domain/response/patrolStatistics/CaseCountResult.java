package com.newfiber.business.domain.response.patrolStatistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 封装通用返回结果
 * date: 2023/3/1 下午 04:53
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
@EqualsAndHashCode
public class CaseCountResult {

    @ApiModelProperty(value = "统计项目")
    private String item;

    @ApiModelProperty(value = "具体的值")
    private int count;

}
