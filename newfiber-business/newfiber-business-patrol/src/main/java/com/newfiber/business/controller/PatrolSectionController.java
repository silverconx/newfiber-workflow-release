package com.newfiber.business.controller;

import com.newfiber.business.domain.PatrolSection;
import com.newfiber.business.domain.request.patrolSection.PatrolSectionQueryRequest;
import com.newfiber.business.domain.request.patrolSection.PatrolSectionSaveRequest;
import com.newfiber.business.domain.request.patrolSection.PatrolSectionUpdateRequest;
import com.newfiber.business.service.IPatrolSectionService;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import com.newfiber.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 巡查段配置Controller
 * 
 * @author X.K
 * @date 2023-02-17
 */
@RestController
@RequestMapping("/patrolSection")
@Api(value = "PAT02-巡查段配置", tags = "PAT02-巡查段配置")
public class PatrolSectionController extends BaseController {

    @Resource
    private IPatrolSectionService patrolSectionService;

    /**
     * 新增巡查段配置
     */
    @PostMapping
    @RequiresPermissions("businessPatrol:patrolSection:add")
    @ApiOperation(value = "新增巡查段配置", position = 10)
    @Log(title = "巡查段配置", businessType = BusinessType.INSERT)
    public Result<Long> add(@RequestBody PatrolSectionSaveRequest request) {
        return success(patrolSectionService.insert(request));
    }

    /**
     * 修改巡查段配置
     */
    @PutMapping
    @RequiresPermissions("businessPatrol:patrolSection:edit")
    @ApiOperation(value = "修改巡查段配置", position = 20)
    @Log(title = "巡查段配置", businessType = BusinessType.UPDATE)
    public Result<Object> edit(@RequestBody PatrolSectionUpdateRequest request) {
        return success(patrolSectionService.update(request));
    }

    /**
     * 删除巡查段配置
     */
    @DeleteMapping("/{ids}")
    @RequiresPermissions("businessPatrol:patrolSection:remove")
    @ApiOperation(value = "删除巡查段配置", notes = "传入ids(,隔开)", position = 30)
    @Log(title = "巡查段配置", businessType = BusinessType.DELETE)
    public Result<Object> remove(@PathVariable String ids) {
        return success(patrolSectionService.delete(ids));
    }

    /**
     * 详细查询巡查段配置
     */
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "详细查询巡查段配置", position = 40)
    public Result<PatrolSection> detail(@PathVariable("id") Long id) {
        return success(patrolSectionService.selectDetail(id));
    }

    /**
     * 分页查询巡查段配置
     */
    @GetMapping("/page")
    @RequiresPermissions("businessPatrol:patrolSection:page")
    @ApiOperation(value = "分页查询巡查段配置", position = 50)
    public PageResult<List<PatrolSection>> page(PatrolSectionQueryRequest request) {
        startPage();
        List<PatrolSection> list = patrolSectionService.selectPage(request);
        return pageResult(list);
    }

    /**
     * 列表查询巡查段配置
     */
    @GetMapping("/list")
    @RequiresPermissions("businessPatrol:patrolSection:list")
    @ApiOperation(value = "列表查询巡查段配置", position = 60)
    public Result<List<PatrolSection>> list(PatrolSectionQueryRequest request) {
        List<PatrolSection> list = patrolSectionService.selectList(request);
        return success(list);
    }

}
