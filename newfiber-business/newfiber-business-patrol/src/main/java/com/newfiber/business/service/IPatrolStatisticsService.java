package com.newfiber.business.service;

import com.newfiber.business.domain.request.patrolStatistics.PatrolStatisticsQueryRequest;
import com.newfiber.business.domain.request.patrolStatistics.PatrolUserStatisticRequest;
import com.newfiber.business.domain.response.patrolStatistics.CaseCountResult;
import com.newfiber.business.domain.response.patrolStatistics.CaseFinishRateResult;
import com.newfiber.business.domain.response.patrolStatistics.PatrolAttendanceRate;
import com.newfiber.business.domain.response.patrolStatistics.PatrolPerson;
import com.newfiber.business.domain.response.patrolStatistics.PatrolStatisticsCaseResult;
import com.newfiber.business.domain.response.patrolStatistics.PatrolUserStatisticResponse;
import java.util.List;

/**
 * date: 2023/3/1 下午 05:43
 *
 * @author: LuFan
 * @since JDK 1.8
 */
public interface IPatrolStatisticsService {

    /**
     * 巡查出勤率统计
     *
     * @param request 通用统计查询入参
     * @return PatrolAttendanceRate
     */
    PatrolAttendanceRate attendanceRate(PatrolStatisticsQueryRequest request);

    /**
     * 优秀巡查人统计
     *
     * @param request 通用统计查询入参
     * @return PatrolPerson
     */
    List<PatrolPerson> patrolPerson(PatrolStatisticsQueryRequest request);

    /**
     * 案件来源统计
     * 需要区分不同的巡查内容
     *
     * @param request 通用统计查询入参
     * @return PatrolStatisticsCaseResult
     */
    PatrolStatisticsCaseResult caseSource(PatrolStatisticsQueryRequest request);

    /**
     * 项目问题统计
     *
     * @param request 通用统计查询入参
     * @return PatrolStatisticsCaseResult
     */
    PatrolStatisticsCaseResult patrolCaseType(PatrolStatisticsQueryRequest request);

    /**
     * 问题类型统计
     *
     * @param request 通用统计查询入参
     * @return PatrolStatisticsCaseResult
     */
    List<CaseCountResult> caseType(PatrolStatisticsQueryRequest request);

    /**
     * 问题结案率统计
     *
     * @param request 通用统计查询入参
     * @return CaseFinishRateResult
     */
    CaseFinishRateResult caseFinishRate(PatrolStatisticsQueryRequest request);

    /**
     * 来源
     * 问题来源总数统计，不区分巡查内容
     *
     * @param request 通用统计查询入参
     * @return CaseCountResult
     */
    List<CaseCountResult> caseSourceTotal(PatrolStatisticsQueryRequest request);

    /**
     * 巡查人员统计
     */
    PatrolUserStatisticResponse patrolUserStatistic(PatrolUserStatisticRequest request);
}
