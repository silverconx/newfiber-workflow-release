package com.newfiber.business.domain.response.patrolStatistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 优秀巡查人统计结果
 * date: 2023/3/2 下午 3:10
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
@EqualsAndHashCode
public class PatrolPerson {

    @ApiModelProperty(value = "巡查人姓名")
    private String name;

    @ApiModelProperty(value = "巡查次数")
    private int count;
}
