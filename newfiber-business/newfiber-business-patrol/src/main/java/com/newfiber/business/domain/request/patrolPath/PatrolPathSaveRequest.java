package com.newfiber.business.domain.request.patrolPath;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * 巡查路径对象 pat_patrol_path
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolPathSaveRequest{

    /**
     * 关联类型
     */
    @NotBlank
    @ApiModelProperty(value = "关联类型", required = true)
    private String refType;

    /**
     * 关联编号
     */
    @NotNull
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "关联编号", required = true)
    private Object refId;

    /**
     * 经纬度
     */
    @NotBlank
    @ApiModelProperty(value = "经纬度", required = true)
    private String lonLat;

	/**
	 * 定位时间
	 */
	@ApiModelProperty(value = "定位时间", hidden = true)
    private Date patrolDatetime;

	/**
	 * 用户名称
	 */
	@NotNull
	@ApiModelProperty(value = "用户名称", required = true)
	private String userName;

	public Double parseLon(){
		if(StringUtils.isNotBlank(lonLat)){
			return Double.parseDouble(lonLat.split(",")[0]);
		}
		return null;
	}

	public Double parseLat(){
		if(StringUtils.isNotBlank(lonLat)){
			return Double.parseDouble(lonLat.split(",")[1]);
		}
		return null;
	}

}
