package com.newfiber.business.domain.response.patrolTask;

import com.newfiber.business.enums.EPatrolTaskStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : X.K
 * @since : 2023/3/8 上午10:59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PatrolTaskStatusStatistics {

	@ApiModelProperty("状态")
	private String status;

	@ApiModelProperty("状态名称")
	private String statusName;

	@ApiModelProperty("数量")
	private Long count;

	public PatrolTaskStatusStatistics(EPatrolTaskStatus taskStatus){
		this.setStatus(taskStatus.getCode());
		this.setStatusName(taskStatus.getValue());
		this.setCount(0L);
	}
}
