package com.newfiber.business.domain.response.patrolStatistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 巡查统计返回结果封装
 *
 * date: 2023/3/3 上午 11:08
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
@EqualsAndHashCode
public class PatrolStatisticsTarget {

    /**
     * category
     */
    @ApiModelProperty(value="category")
    private String type;

    /**
     * 具体统计项数据
     */
    @ApiModelProperty(value = "data")
    private List<String> data;
}
