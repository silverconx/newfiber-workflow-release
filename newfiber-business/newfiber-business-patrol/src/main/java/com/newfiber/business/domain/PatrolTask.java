package com.newfiber.business.domain;

import static cn.hutool.core.date.DatePattern.NORM_DATETIME_PATTERN;

import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.newfiber.common.core.geometry.GeometryLngLat;
import com.newfiber.common.core.geometry.GeometryTypeHandler;
import com.newfiber.common.core.geometry.GeometryUtils;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 巡查任务对象 pat_patrol_task
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
@TableName("pat_patrol_task")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "巡查任务", description = "巡查任务")
public class PatrolTask extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号")
    private String number;

    /**
     * 任务来源（巡查 patrol | 报警 warning）
     */
    @ApiModelProperty(value = "任务来源（巡查 patrol | 报警 warning）")
    private String taskSource;

    /**
     * 巡查类型（数据字典 patrol_type）
     */
    @ApiModelProperty(value = "巡查类型（数据字典 patrol_type）")
    private String patrolType;

    /**
     * 巡查基础信息编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "巡查基础信息编号")
    private Long basicInfoId;

    /**
     * 巡查段编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "巡查段编号")
    private Long patrolSectionId;

    /**
     * 巡查计划编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "巡查计划编号")
    private Long planId;

    /**
     * 任务名称
     */
    @ApiModelProperty(value = "任务名称")
    private String taskName;

    /**
     * 巡查人
     */
    @ApiModelProperty(value = "巡查人")
    private Long taskUserId;

    /**
     * 计划开始时间
     */
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    @ApiModelProperty(value = "计划开始时间")
    private Date planStartDatetime;

    /**
     * 计划应结束时间
     */
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    @ApiModelProperty(value = "计划应结束时间")
    private Date planEndDatetime;

    /**
     * 任务实际开始时间
     */
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    @ApiModelProperty(value = "任务实际开始时间")
    private Date realStartDatetime;

    /**
     * 任务实际结束时间
     */
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    @ApiModelProperty(value = "任务实际结束时间")
    private Date realEndDatetime;

    /**
     * 任务下发时间
     */
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    @ApiModelProperty(value = "任务下发时间")
    private Date issuedDatetime;

    /**
     * 状态（未开始 to_start | 进行中 proceed | 已完成 done | 超时 expired）
     */
    @ApiModelProperty(value = "状态（未开始 to_start | 进行中 proceed | 已完成 done | 超时 expired）")
    private String status;

    /**
     * 超时时间（单位：天）
     */
    @ApiModelProperty(value = "超时时间（单位：天）")
    private Long expiredDays;

    // DB Property
	/**
	 * 巡查轨迹
	 */
	@TableField(value = "patrol_path", exist = false, typeHandler = GeometryTypeHandler.class, updateStrategy = FieldStrategy.IGNORED)
	@ApiModelProperty(value = "巡查轨迹")
	private String patrolPath = null;

	/**
	 * 巡查轨迹
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "巡查轨迹")
	private List<GeometryLngLat> patrolPathLngLat;

	/**
	 * 巡查日志编号
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "巡查日志编号")
	@JsonSerialize(using = ToStringSerializer.class)
	private Long patrolLogId;


    /**
     * 巡查目标/巡查内容名称
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查目标/巡查内容名称")
    private String patrolTarget;

    /**
     * 巡查段类型（点 point | 线 line | 面 area）
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查段类型（点 point | 线 line | 面 area）")
    private String sectionType;

    /**
     * 巡查段详情（JSON）
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查段详情（JSON）")
    private String sectionDetail;

    /**
     * 巡查段名称
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查段名称")
    private String patrolSectionName;

    /**
     * 巡查人
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查人")
    private String taskUserName;

    /**
     * 巡查人手机号
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查人手机号")
    private String taskUserPhone;

	public String getPatrolSectionName() {
        return PatrolSection.parsePatrolSectionName(patrolTarget, sectionType, sectionDetail);
    }

	public List<GeometryLngLat> getPatrolPathLngLat() {
		return GeometryUtils.parseLineStringLngLat(patrolPath);
	}
}
