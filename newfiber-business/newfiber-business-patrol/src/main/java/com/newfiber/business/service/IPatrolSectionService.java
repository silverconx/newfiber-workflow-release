package com.newfiber.business.service;

import com.newfiber.business.domain.PatrolSection;
import com.newfiber.business.domain.request.patrolSection.PatrolSectionQueryRequest;
import com.newfiber.business.domain.request.patrolSection.PatrolSectionSaveRequest;
import com.newfiber.business.domain.request.patrolSection.PatrolSectionUpdateRequest;
import java.util.List;

/**
 * 巡查段配置Service接口
 * 
 * @author X.K
 * @date 2023-02-17
 */
public interface IPatrolSectionService {

    /**
     * 新增巡查段配置
     *
     * @param request 新增参数
     * @return 结果
     */
    long insert(PatrolSectionSaveRequest request);

    /**
     * 修改巡查段配置
     *
     * @param request 修改参数
     * @return 结果
     */
    boolean update(PatrolSectionUpdateRequest request);

    /**
     * 批量删除巡查段配置
     *
     * @param  ids 编号(,隔开)
     * @return 结果
     */
    boolean delete(String ids);

	/**
	 * 计算数量
     *
     * @param basicInfoId 巡查基本信息id
     * @return 结果
	 */
	long countByBasicInfo(Long basicInfoId);

    /**
     * 详细查询巡查段配置
     *
     * @param id 主键
     * @return 巡查段配置
     */
     PatrolSection selectDetail(Long id);

     /**
      * 分页查询巡查段配置
      *
      * @param request 分页参数
      * @return 巡查段配置集合
      */
     List<PatrolSection> selectPage(PatrolSectionQueryRequest request);

     /**
      * 列表查询巡查段配置
      *
      * @param request 列表参数
      * @return 巡查段配置集合
      */
     List<PatrolSection> selectList(PatrolSectionQueryRequest request);

}
