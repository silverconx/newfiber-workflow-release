package com.newfiber.business.enums;

import com.newfiber.common.core.exception.ServiceException;

/**
 * @author : Silver
 * @since : 2019-01-11 22:10
 */
public enum EPatrolTaskStatus {

	/**
	 * 状态（未启用 to_use | 未开始 to_start | 进行中 proceed | 已完成 done | 超时 expired）
	 */
	ToUse("to_use", "未启用"),

	ToStart("to_start", "未开始"),

	Proceed("proceed", "进行中"),

	Done("done", "已完成"),

	Expired("expired", "超时"),

	Stopped("stopped", "已停止"),

	;

	public static EPatrolTaskStatus match(String code){
		for(EPatrolTaskStatus sectionType : EPatrolTaskStatus.values()){
			if(sectionType.code.equals(code)){
				return sectionType;
			}
		}
		throw new ServiceException("任务状态不匹配");
	}

    EPatrolTaskStatus(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

}
