package com.newfiber.business.controller;

import com.newfiber.business.domain.PatrolBasicInfo;
import com.newfiber.business.domain.request.patrolBasicInfo.PatrolBasicInfoQueryRequest;
import com.newfiber.business.domain.request.patrolBasicInfo.PatrolBasicInfoSaveRequest;
import com.newfiber.business.domain.request.patrolBasicInfo.PatrolBasicInfoUpdateRequest;
import com.newfiber.business.service.IPatrolBasicInfoService;
import com.newfiber.business.service.IPatrolSectionService;
import com.newfiber.common.core.exception.ServiceException;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import com.newfiber.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 巡查基础信息Controller
 *
 * @author X.K
 * @date 2023-02-10
 */
@RestController
@RequestMapping("/patrolBasicInfo")
@Api(value = "PAT01-巡查基础信息", tags = "PAT01-巡查基础信息")
public class PatrolBasicInfoController extends BaseController {

    @Resource
    private IPatrolBasicInfoService patrolBasicInfoService;

    @Resource
    private IPatrolSectionService patrolSectionService;

    /**
     * 新增巡查基础信息
     */
    @PostMapping
    @RequiresPermissions("businessPatrol:patrolBasicInfo:add")
    @ApiOperation(value = "新增巡查基础信息", position = 10)
    @Log(title = "巡查基础信息", businessType = BusinessType.INSERT)
    public Result<Long> add(@RequestBody PatrolBasicInfoSaveRequest request) {
        return success(patrolBasicInfoService.insert(request));
    }

    /**
     * 修改巡查基础信息
     */
    @PutMapping
    @RequiresPermissions("businessPatrol:patrolBasicInfo:edit")
    @ApiOperation(value = "修改巡查基础信息", position = 20)
    @Log(title = "巡查基础信息", businessType = BusinessType.UPDATE)
    public Result<Object> edit(@RequestBody PatrolBasicInfoUpdateRequest request) {
        return success(patrolBasicInfoService.update(request));
    }

    /**
     * 删除巡查基础信息
     */
    @DeleteMapping("/{ids}")
    @RequiresPermissions("businessPatrol:patrolBasicInfo:remove")
    @ApiOperation(value = "删除巡查基础信息", notes = "传入ids(,隔开)", position = 30)
    @Log(title = "巡查基础信息", businessType = BusinessType.DELETE)
    public Result<Object> remove(@PathVariable String ids) {
        Arrays.asList(ids.split(",")).forEach(id -> {
		    if(patrolSectionService.countByBasicInfo(Long.parseLong(id)) > 0){
                throw new ServiceException("巡查基础信息已绑定巡查段，无法删除");
            }
        });
        return success(patrolBasicInfoService.delete(ids));
    }

    /**
     * 详细查询巡查基础信息
     */
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "详细查询巡查基础信息", position = 40)
    public Result<PatrolBasicInfo> detail(@PathVariable("id") Long id) {
        return success(patrolBasicInfoService.selectDetail(id));
    }

    /**
     * 分页查询巡查基础信息
     */
    @GetMapping("/page")
    @RequiresPermissions("businessPatrol:patrolBasicInfo:page")
    @ApiOperation(value = "分页查询巡查基础信息", position = 50)
    public PageResult<List<PatrolBasicInfo>> page(PatrolBasicInfoQueryRequest request) {
        startPage();
        List<PatrolBasicInfo> list = patrolBasicInfoService.selectPage(request);
        return pageResult(list);
    }

    /**
     * 列表查询巡查基础信息
     */
    @GetMapping("/list")
    @RequiresPermissions("businessPatrol:patrolBasicInfo:list")
    @ApiOperation(value = "列表查询巡查基础信息", position = 60)
    public Result<List<PatrolBasicInfo>> list(PatrolBasicInfoQueryRequest request) {
        List<PatrolBasicInfo> list = patrolBasicInfoService.selectList(request);
        return success(list);
    }

}
