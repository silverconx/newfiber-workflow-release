package com.newfiber.business.domain.request.patrolLog;

import static cn.hutool.core.date.DatePattern.NORM_DATETIME_PATTERN;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 巡查日志对象 pat_patrol_log
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolLogUpdateRequest{

    /**
     * 主键
     */
    @NotNull
    @ApiModelProperty(value = "主键", required = true)
    private Long id;

    /**
     * 巡查名称
     */
    @NotBlank
    @ApiModelProperty(value = "巡查名称", required = true)
    private String patrolName;

    /**
     * 巡查长度
     */
    @ApiModelProperty(value = "巡查长度")
    private BigDecimal patrolLength;

    /**
     * 巡查起点
     */
    @ApiModelProperty(value = "巡查起点")
    private String startPlace;

    /**
     * 巡查终点
     */
    @ApiModelProperty(value = "巡查终点")
    private String endPlace;

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    private Date startDatetime;

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    private Date endDatetime;

    /**
     * 巡查内容
     */
    @ApiModelProperty(value = "巡查内容")
    private String logContent;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

	/**
	 * 图片
	 */
	@ApiModelProperty(value = "图片")
	private String picture;

	/**
	 * 视频
	 */
	@ApiModelProperty(value = "视频")
	private String video;

	/**
	 * 音频
	 */
	@ApiModelProperty(value = "音频")
	private String audio;

}
