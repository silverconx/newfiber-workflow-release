package com.newfiber.business.controller;

import com.newfiber.business.domain.PatrolPlan;
import com.newfiber.business.domain.request.patrolPlan.PatrolPlanQueryRequest;
import com.newfiber.business.domain.request.patrolPlan.PatrolPlanSaveRequest;
import com.newfiber.business.domain.request.patrolPlan.PatrolPlanUpdateRequest;
import com.newfiber.business.service.IPatrolPlanService;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import com.newfiber.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 巡查计划配置Controller
 * 
 * @author X.K
 * @date 2023-02-17
 */
@RestController
@RequestMapping("/patrolPlan")
@Api(value = "PAT04-巡查计划配置", tags = "PAT04-巡查计划配置")
public class PatrolPlanController extends BaseController {

    @Resource
    private IPatrolPlanService patrolPlanService;

    /**
     * 新增巡查计划配置
     */
    @PostMapping("add")
    @RequiresPermissions("businessPatrol:patrolPlan:add")
    @ApiOperation(value = "新增巡查计划配置", position = 10)
    @Log(title = "巡查计划配置", businessType = BusinessType.INSERT)
    public Result<Long> add(@RequestBody PatrolPlanSaveRequest request) {
        return success(patrolPlanService.insert(request));
    }

    /**
     * 修改巡查计划配置
     */
    @PutMapping("edit")
    @RequiresPermissions("businessPatrol:patrolPlan:edit")
    @ApiOperation(value = "修改巡查计划配置", position = 20, hidden = true)
    @Log(title = "巡查计划配置", businessType = BusinessType.UPDATE)
    public Result<Object> edit(@RequestBody PatrolPlanUpdateRequest request) {
        return success(patrolPlanService.update(request));
    }

	/**
	 * 停止巡查计划配置
	 */
	@PutMapping("/stop/{ids}")
	@RequiresPermissions("businessPatrol:patrolPlan:edit")
	@ApiOperation(value = "停止巡查计划配置", position = 30)
	@Log(title = "巡查计划配置", businessType = BusinessType.UPDATE)
	public Result<Object> stop(@PathVariable String ids) {
		return success(patrolPlanService.stop(ids));
	}

    /**
     * 启用巡查计划
     */
    @PutMapping("/startUse/{planId}")
    @RequiresPermissions("businessPatrol:patrolPlan:edit")
    @ApiOperation(value = "启用巡查计划", position = 40)
    @Log(title = "启用巡查计划", businessType = BusinessType.UPDATE)
    public Result<Object> startUse(@PathVariable Long planId) {
        return success(patrolPlanService.startUse(planId));
    }

    /**
     * 删除巡查计划配置
     */
    @DeleteMapping("/{ids}")
    @RequiresPermissions("businessPatrol:patrolPlan:remove")
    @ApiOperation(value = "删除巡查计划配置", notes = "传入ids(,隔开)", position = 50, hidden = true)
    @Log(title = "巡查计划配置", businessType = BusinessType.DELETE)
    public Result<Object> remove(@PathVariable String ids) {
        return success(patrolPlanService.delete(ids));
    }

    /**
     * 详细查询巡查计划配置
     */
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "详细查询巡查计划配置", position = 60)
    public Result<PatrolPlan> detail(@PathVariable("id") Long id) {
        return success(patrolPlanService.selectDetail(id));
    }

    /**
     * 分页查询巡查计划配置
     */
    @GetMapping("/page")
    @RequiresPermissions("businessPatrol:patrolPlan:page")
    @ApiOperation(value = "分页查询巡查计划配置", position = 70)
    public PageResult<List<PatrolPlan>> page(PatrolPlanQueryRequest request) {
        startPage();
        List<PatrolPlan> list = patrolPlanService.selectPage(request);
        return pageResult(list);
    }

    /**
     * 列表查询巡查计划配置
     */
    @GetMapping("/list")
    @RequiresPermissions("businessPatrol:patrolPlan:list")
    @ApiOperation(value = "列表查询巡查计划配置", position = 80)
    public Result<List<PatrolPlan>> list(PatrolPlanQueryRequest request) {
        List<PatrolPlan> list = patrolPlanService.selectList(request);
        return success(list);
    }

}
