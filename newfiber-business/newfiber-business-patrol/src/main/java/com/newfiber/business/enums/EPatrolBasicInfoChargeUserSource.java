package com.newfiber.business.enums;

import com.newfiber.common.core.exception.ServiceException;
import org.apache.commons.lang3.StringUtils;

/**
 * @author : Silver
 * @since : 2019-01-11 22:10
 */
public enum EPatrolBasicInfoChargeUserSource {

	/**
	 * 负责人来源（内部 inner | 外部 outer）
	 */
	Inner("inner", "内部"),

	Outer("outer", "外部"),

	;

	public static EPatrolBasicInfoChargeUserSource match(String code){
		if(StringUtils.isBlank(code)){
			return null;
		}
		for(EPatrolBasicInfoChargeUserSource sectionType : EPatrolBasicInfoChargeUserSource.values()){
			if(sectionType.code.equals(code)){
				return sectionType;
			}
		}
		throw new ServiceException("负责人来源不匹配");
	}

    EPatrolBasicInfoChargeUserSource(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

}
