package com.newfiber.business.domain.request.patrolCase;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.newfiber.common.core.web.request.BaseQueryRequest;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import static cn.hutool.core.date.DatePattern.NORM_DATE_PATTERN;

/**
 * 巡查案件对象 pat_patrol_case
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PatrolCaseQueryRequest extends BaseQueryRequest {

    /**
     * id
     */
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号")
    private String number;

    /**
     * 巡查类型（数据字典 patrol_type）
     */
    @ApiModelProperty(value = "巡查类型（数据字典 patrol_type）")
    private String patrolType;

    /**
     * 巡查段编号
     */
    @ApiModelProperty(value = "巡查段编号")
    private Long patrolSectionId;

    /**
     * 巡查任务id
     */
    @ApiModelProperty(value = "巡查任务id")
    private String patrolTaskIds;

    /**
     * 巡查河道/巡查内容名称/所属河道
     */
    @ApiModelProperty(value = "巡查河道/巡查内容名称/所属河道")
    private Long patrolTargetId;

    /**
     * 巡查日志编号
     */
    @ApiModelProperty(value = "巡查日志编号")
    private Long patrolLogId;

    /**
     * 案件名称
     */
    @ApiModelProperty(value = "案件名称")
    private String caseName;

    /**
     * 案件等级（数据字典 case_level）
     */
    @ApiModelProperty(value = "案件等级（数据字典 case_level）")
    private String caseLevel;

    /**
     * 案件类型（数据字典  case_type）
     */
    @ApiModelProperty(value = "案件类型（数据字典  case_type）")
    private String caseType;

    /**
     * 案件内容
     */
    @ApiModelProperty(value = "案件内容")
    private String caseContent;

    /**
     * 案件来源
     */
    @ApiModelProperty(value = "案件来源")
    private String caseSource;

    /**
     * 截止日期
     */
    @ApiModelProperty(value = "截止日期")
    private Date deadline;

    /**
     * 案件地址
     */
    @ApiModelProperty(value = "案件地址")
    private String caseAddress;

    /**
     * 经纬度
     */
    @ApiModelProperty(value = "经纬度")
    private String lonLat;

    /**
     * 是否上报（0 否 | 1 是）
     */
    @ApiModelProperty(value = "是否上报（0 否 | 1 是）")
    private String reportFlag;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    private String status;

    /**
     * 综合搜索条件
     */
    @ApiModelProperty(value = "综合搜索条件")
    private String queryStr;

    /**
     * 开始日期
     */
    @ApiModelProperty(value = "开始日期")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date startDate;

    /**
     * 结束日期
     */
    @ApiModelProperty(value = "结束日期")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date endDate;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    private String createBy;
}
