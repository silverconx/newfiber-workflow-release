package com.newfiber.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newfiber.business.domain.PatrolUser;
import com.newfiber.business.domain.request.patrolUser.PatrolUserQueryRequest;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 巡查人员Mapper接口
 * 
 * @author X.K
 * @date 2023-02-17
 */
public interface PatrolUserMapper extends BaseMapper<PatrolUser>{

    /**
     * 条件查询巡查人员列表
     * 
     * @param request 查询条件
     * @return 巡查人员集合
     */
    List<PatrolUser> selectByCondition(@Param("request") PatrolUserQueryRequest request);

	/**
	 * 查询出没有被配置过的用户
	 *
	 * date: 2023/3/30 下午 04:05
	 * @author: LuFan
	 * @since JDK 1.8
	 */
    List<PatrolUser> queryUnusedUser(@Param("request") PatrolUserQueryRequest request);

	/**
	 * 巡查用户数量（去重）
	 *
	 * @param patrolType
	 * @return 结果
	 */
	long patrolUserDistinctCount(@Param("patrolType") String patrolType);
}
