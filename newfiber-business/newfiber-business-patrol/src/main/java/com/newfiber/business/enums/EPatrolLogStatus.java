package com.newfiber.business.enums;

import com.newfiber.common.core.exception.ServiceException;

/**
 * date: 2023/2/24 上午 09:15
 * @author: LuFan
 * @since JDK 1.8
 */
public enum EPatrolLogStatus {
    /**
     * 提交状态(未提交 draft |  已提交 committed)
     * 巡查计划配置以后就会生成巡查任务一对一，对应的一条日志记录
     * 数据存放在pat_patrol_log表里面
     */

	/**
	 * 移动端上报日志状态
	 */
	Temp("temp", "临时"),

    Draft("draft", "未提交"),

    /**
     * status字段默认状态 draft，结束巡查后状态更新为 committed
     */
    Committed("committed", "已提交"),
    ;

    public static EPatrolLogStatus match(String code) {
        for (EPatrolLogStatus ePatrolLogStatus : EPatrolLogStatus.values()) {
            if (ePatrolLogStatus.code.equals(code)) {
                return ePatrolLogStatus;
            }
        }
        throw new ServiceException("日志提交状态不匹配");
    }

    EPatrolLogStatus(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
