package com.newfiber.business.controller;

import com.newfiber.business.domain.PatrolLog;
import com.newfiber.business.domain.request.patrolLog.PatrolLogQueryRequest;
import com.newfiber.business.domain.request.patrolLog.PatrolLogSaveRequest;
import com.newfiber.business.domain.request.patrolLog.PatrolLogSaveTempRequest;
import com.newfiber.business.domain.request.patrolLog.PatrolLogSubmitTempRequest;
import com.newfiber.business.domain.request.patrolLog.PatrolLogUpdateRequest;
import com.newfiber.business.service.IPatrolCaseService;
import com.newfiber.business.service.IPatrolLogService;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import com.newfiber.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.concurrent.Executors;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 巡查日志Controller
 * 
 * @author X.K
 * @date 2023-02-17
 */
@RestController
@RequestMapping("/patrolLog")
@Api(value = "PAT07-巡查日志", tags = "PAT07-巡查日志")
public class PatrolLogController extends BaseController {

    @Resource
    private IPatrolLogService patrolLogService;

    @Resource
    private IPatrolCaseService patrolCaseService;

    /**
     * 新增巡查日志
     */
    @PostMapping("add")
    @RequiresPermissions("businessPatrol:patrolLog:add")
    @ApiOperation(value = "新增巡查日志", position = 10)
    @Log(title = "巡查日志", businessType = BusinessType.INSERT)
    public Result<Long> add(@RequestBody PatrolLogSaveRequest request) {
        return success(patrolLogService.insert(request));
    }

	/**
	 * 新增巡查日志
	 */
	@PostMapping("addTemp")
	@RequiresPermissions("businessPatrol:patrolLog:add")
	@ApiOperation(value = "新增临时巡查日志", position = 10)
	@Log(title = "巡查日志", businessType = BusinessType.INSERT)
	public Result<Long> addTemp(@RequestBody PatrolLogSaveTempRequest request) {
		return success(patrolLogService.insertTemp(request));
	}

    /**
     * 修改巡查日志
     */
    @PutMapping("edit")
    @RequiresPermissions("businessPatrol:patrolLog:edit")
    @ApiOperation(value = "修改巡查日志", position = 20)
    @Log(title = "巡查日志", businessType = BusinessType.UPDATE)
    public Result<Object> edit(@RequestBody PatrolLogUpdateRequest request) {
        return success(patrolLogService.update(request));
    }

	/**
	 * 提交临时巡查日志
	 */
	@PutMapping("submitTemp")
	@RequiresPermissions("businessPatrol:patrolLog:edit")
	@ApiOperation(value = "提交临时巡查日志", position = 20)
	@Log(title = "巡查日志", businessType = BusinessType.UPDATE)
	public Result<Object> submitTemp(@RequestBody PatrolLogSubmitTempRequest request) {
		// 提交临时日志
		patrolLogService.submitTemp(request);

		// 启动临时日志的缓存案件
		Executors.newSingleThreadExecutor().submit(() -> patrolCaseService.startCachedCaseByLog(request.getId()));

		return success(true);
	}

	/**
	 * 取消临时巡查日志
	 */
	@PutMapping("cancelTemp/{id}")
	@RequiresPermissions("businessPatrol:patrolLog:edit")
	@ApiOperation(value = "取消临时巡查日志", position = 20)
	@Log(title = "巡查日志", businessType = BusinessType.UPDATE)
	public Result<Object> cancelTemp(@PathVariable("id") Long id) {
		return success(patrolLogService.cancelTemp(id));
	}

    /**
     * 删除巡查日志
     */
    @DeleteMapping("/{ids}")
    @RequiresPermissions("businessPatrol:patrolLog:remove")
    @ApiOperation(value = "删除巡查日志", notes = "传入ids(,隔开)", position = 30)
    @Log(title = "巡查日志", businessType = BusinessType.DELETE)
    public Result<Object> remove(@PathVariable String ids) {
        return success(patrolLogService.delete(ids));
    }

    /**
     * 详细查询巡查日志
     */
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "详细查询巡查日志", position = 40)
    public Result<PatrolLog> detail(@PathVariable("id") Long id) {
        return success(patrolLogService.selectDetail(id));
    }

	/**
	 * 详细查询临时巡查日志
	 */
	@GetMapping(value = "/detailTemp/{userId}")
	@ApiOperation(value = "详细查询临时巡查日志", position = 40)
	public Result<PatrolLog> detailTemp(@PathVariable("userId") Long userId) {
		return success(patrolLogService.selectTempByUserId(userId));
	}

    /**
     * 分页查询巡查日志
     */
    @GetMapping("/page")
    @RequiresPermissions("businessPatrol:patrolLog:page")
    @ApiOperation(value = "分页查询巡查日志", position = 50)
    public PageResult<List<PatrolLog>> page(PatrolLogQueryRequest request) {
        startPage();
        List<PatrolLog> list = patrolLogService.selectPage(request);
        return pageResult(list);
    }

    /**
     * 列表查询巡查日志
     */
    @GetMapping("/list")
    @RequiresPermissions("businessPatrol:patrolLog:list")
    @ApiOperation(value = "列表查询巡查日志", position = 60)
    public Result<List<PatrolLog>> list(PatrolLogQueryRequest request) {
        List<PatrolLog> list = patrolLogService.selectList(request);
        return success(list);
    }

	/**
	 * 列表查询实时巡查任务
	 */
	@GetMapping("/realtime_list")
	@RequiresPermissions("businessPatrol:patrolLog:list")
	@ApiOperation(value = "列表查询实时巡查任务", position = 60)
	public Result<List<PatrolLog>> realtimeList(PatrolLogQueryRequest request) {
		List<PatrolLog> list = patrolLogService.selectRealtimeList(request);
		return success(list);
	}

    /**
     * 导出巡查日志数据
     */
    @PostMapping("/export")
    @ApiOperation(value = "导出巡查日志数据", position = 70)
    public void export(HttpServletResponse response, PatrolLogQueryRequest request) {
        patrolLogService.export(response, request);
    }
}
