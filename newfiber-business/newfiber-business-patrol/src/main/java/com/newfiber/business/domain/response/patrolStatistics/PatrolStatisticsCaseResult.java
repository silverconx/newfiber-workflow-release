package com.newfiber.business.domain.response.patrolStatistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * 巡查统计-案件相关统计通用返回结果
 * <
 * date: 2023/3/1 上午 10:09
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
@EqualsAndHashCode
public class PatrolStatisticsCaseResult implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 巡查内容名称
     */
    @ApiModelProperty(value = "巡查目标")
    private List<PatrolStatisticsTarget> xAxis;

    /**
     * 具体统计项数据
     */
    @ApiModelProperty(value = "具体统计项数据")
    private List<PatrolStatisticsSeries> series;
}
