package com.newfiber.business.domain.request.patrolLog;

import com.newfiber.system.api.domain.request.SysFileSaveRequest;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.List;
import lombok.Data;

/**
 * 巡查日志对象 pat_patrol_log
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolLogSubmitTempRequest {

	/**
	 * 编号
	 */
	@ApiModelProperty(value = "编号")
	private Long id;

	/**
	 * 巡查名称
	 */
	@ApiModelProperty(value = "巡查名称")
	private String patrolName;

	/**
	 * 巡查内容
	 */
	@ApiModelProperty(value = "巡查内容")
	private String logContent;

	/**
	 * 文件
	 */
	@ApiModelProperty(value = "文件")
	private List<SysFileSaveRequest> fileSaveRequestList;

	/**
	 * 巡查长度
	 */
	@ApiModelProperty(value = "巡查长度")
	private BigDecimal patrolLength;

	/**
	 * 巡查终点
	 */
	@ApiModelProperty(value = "巡查终点")
	private String endPlace;



}
