package com.newfiber.business.enums;

import com.newfiber.common.core.exception.ServiceException;

/**
 * @author : Silver
 * @since : 2019-01-11 22:10
 */
public enum EPatrolSectionType {

	/**
	 * 巡查段类型（点 point | 线 line | 面 area）
	 */
	Point("point", "点"),

	Line("line", "线"),

	Area("area", "面")
	;

	public static EPatrolSectionType match(String code){
		for(EPatrolSectionType sectionType : EPatrolSectionType.values()){
			if(sectionType.code.equals(code)){
				return sectionType;
			}
		}
		throw new ServiceException("巡查段类型不匹配");
	}

    EPatrolSectionType(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

}
