package com.newfiber.business.domain;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 巡查轨迹停留记录对象 pat_patrol_path_stay
 * 
 * @author X.K
 * @date 2023-03-08
 */
@Data
@TableName("pat_patrol_path_stay")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "巡查轨迹停留记录", description = "巡查轨迹停留记录")
public class PatrolPathStay extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /**
     *  关联类型
     */
    @ApiModelProperty(value = "关联类型")
    private String refType;

    /**
     *  关联编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "关联编号")
    private Long refId;

	/**
	 *  关联人员
	 */
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "关联人员")
    private Long userId;

    /**
     *  停留经纬度
     */
    @ApiModelProperty(value = "停留经纬度")
    private String stayLonLat;

    /**
     *  停留时间
     */
    @ApiModelProperty(value = "停留时间")
    private Date stayDatetime;

    /**
     *  停留时长
     */
    @ApiModelProperty(value = "停留时长")
    private Double stayDuration;

	// DB Property

	/**
	 * 巡查目标
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "巡查目标")
	private String patrolTarget;

	/**
	 * 巡查段类型（点 point | 线 line | 面 area）
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "巡查段类型（点 point | 线 line | 面 area）")
	private String sectionType;

	/**
	 * 巡查段详情（JSON）
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "巡查段详情（JSON）")
	private String sectionDetail;

	/**
	 * 巡查段名称
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "巡查段名称")
	private String patrolSectionName;

	/**
	 * 巡查轨迹
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "巡查轨迹")
	private String patrolPath;

	/**
	 * 关联人员
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "关联人员")
	private String userName;

	/**
	 *  停留结束时间
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "停留结束时间")
	private Date stayEndDatetime;

	/**
	 * 巡查段编号
	 */
	@TableField(exist = false)
	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "巡查段编号")
	private Long patrolSectionId;

	public String getPatrolSectionName() {
		return PatrolSection.parsePatrolSectionName(patrolTarget, sectionType, sectionDetail);
	}

	public String getStayReason(){
		return "原地停留时间过长";
	}

	public String getReportFlag(){
		return "0";
	}

	public Date getStayEndDatetime() {
		if(null != stayDatetime && null != stayDuration){
			return DateUtil.offsetMinute(stayDatetime, (int) (stayDuration * 60));
		}
		return stayEndDatetime;
	}
}
