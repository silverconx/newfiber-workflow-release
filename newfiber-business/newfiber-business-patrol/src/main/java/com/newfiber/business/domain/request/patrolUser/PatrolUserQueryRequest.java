package com.newfiber.business.domain.request.patrolUser;

import com.newfiber.common.core.utils.StringUtils;
import com.newfiber.common.core.web.request.BaseQueryRequest;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 巡查人员对象 pat_patrol_user
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PatrolUserQueryRequest extends BaseQueryRequest {

    /**
     * 关联类型（巡查段 section | 巡查任务 task）
     */
    @ApiModelProperty(value = "关联类型（巡查段 section | 巡查任务 task）")
    private String refType;

    /**
     * 关联编号
     */
    @ApiModelProperty(value = "关联编号")
    private Long refId;

	/**
	 * 关联编号（全部：*  |  多段用,隔开）
	 */
	@ApiModelProperty(value = "关联编号（全部：*  |  多段用,隔开）")
	private String refIds;

	/**
	 * 关联编号
	 */
	@ApiModelProperty(value = "关联编号")
	private List<String> refIdList = new ArrayList<>();

    /**
     *  巡查类型（数据字典 patrol_type）
     */
    @ApiModelProperty(value = "巡查类型（数据字典 patrol_type）")
    private String patrolType;

    public List<String> getRefIdList(){
        if(StringUtils.isBlank(this.refIds)){
            return new ArrayList<>();
        }
        return Arrays.asList(this.refIds.split(","));
    }

    /**
     * 用户类型（巡查人员 patrol | 立案人员 register | 核查人员 approve | 处理人员 handle | 验证人员 verify | 结案人员 close）
     */
    @ApiModelProperty(value = "用户类型（巡查人员 patrol | 立案人员 register | 核查人员 approve | 处理人员 handle | 验证人员 verify | 结案人员 close）")
    private String userType;

    /**
     * 用户编号
     */
    @ApiModelProperty(value = "用户编号")
    private Long userId;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    private String status;

	/**
	 * 是否转义关联名称（0 否 | 1 是）
	 */
	@ApiModelProperty("是否转义关联名称（0 否 | 1 是）")
	private String parseRefNameFlag;

    /**
     * 是否已选用
     */
    @ApiModelProperty(value = "是否已选用")
    private String used;

}
