package com.newfiber.business.domain.request.patrolLog;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * 巡查日志对象 pat_patrol_log
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolLogSaveTempRequest {

	/**
	 * 巡查类型（河道 river | 管网 pipeline）
	 */
	@NotBlank
	@ApiModelProperty(value = "巡查类型（河道 river | 管网 pipeline）", required = true)
	private String patrolType;

	/**
	 * 巡查段编号
	 */
	@NotNull
	@ApiModelProperty(value = "巡查段编号", required = true)
	private Long patrolSectionId;

	/**
	 * 日志巡查人
	 */
	@NotNull
	@ApiModelProperty(value = "日志巡查人", required = true)
	private Long logUserId;

	/**
	 * 巡查起点
	 */
	@ApiModelProperty(value = "巡查起点")
	private String startPlace;


}
