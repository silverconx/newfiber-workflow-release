package com.newfiber.business.service;

import com.newfiber.business.domain.PatrolBasicInfo;
import com.newfiber.business.domain.request.patrolBasicInfo.PatrolBasicInfoQueryRequest;
import com.newfiber.business.domain.request.patrolBasicInfo.PatrolBasicInfoSaveRequest;
import com.newfiber.business.domain.request.patrolBasicInfo.PatrolBasicInfoUpdateRequest;
import java.util.List;

/**
 * 巡查基础信息Service接口
 * 
 * @author X.K
 * @date 2023-02-10
 */
public interface IPatrolBasicInfoService {

    /**
     * 新增巡查基础信息
     *
     * @param request 新增参数
     * @return 结果
     */
    long insert(PatrolBasicInfoSaveRequest request);

    /**
     * 修改巡查基础信息
     *
     * @param request 修改参数
     * @return 结果
     */
    boolean update(PatrolBasicInfoUpdateRequest request);

    /**
     * 批量删除巡查基础信息
     *
     * @param  ids 编号(,隔开)
     * @return 结果
     */
    boolean delete(String ids);

    /**
     * 详细查询巡查基础信息
     *
     * @param id 主键
     * @return 巡查基础信息
     */
     PatrolBasicInfo selectDetail(Long id);

     /**
      * 分页查询巡查基础信息
      *
      * @param request 分页参数
      * @return 巡查基础信息集合
      */
     List<PatrolBasicInfo> selectPage(PatrolBasicInfoQueryRequest request);

     /**
      * 列表查询巡查基础信息
      *
      * @param request 列表参数
      * @return 巡查基础信息集合
      */
     List<PatrolBasicInfo> selectList(PatrolBasicInfoQueryRequest request);

}
