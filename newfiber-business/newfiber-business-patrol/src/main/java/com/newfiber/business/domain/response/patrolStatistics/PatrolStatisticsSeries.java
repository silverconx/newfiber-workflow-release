package com.newfiber.business.domain.response.patrolStatistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 巡查统计-案件相关统计通用返回结果
 * <
 * date: 2023/3/1 上午 10:09
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
@EqualsAndHashCode
public class PatrolStatisticsSeries {

    /**
     * 统计项名称
     */
    @ApiModelProperty(value = "统计项名称")
    private String name;

    /**
     * 具体统计的值
     */
    @ApiModelProperty(value = "具体统计的值")
    private List<Integer> data;
}
