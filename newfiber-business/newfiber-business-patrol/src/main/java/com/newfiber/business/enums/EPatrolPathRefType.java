package com.newfiber.business.enums;

import com.newfiber.common.core.exception.ServiceException;

/**
 * @author : Silver
 * @since : 2019-01-11 22:10
 */
public enum EPatrolPathRefType {

	/**
	 * 巡查轨迹关联类型
	 */
	PatrolLog("PatrolLog", "巡查日志"),


	;

	public static EPatrolPathRefType match(String code){
		for(EPatrolPathRefType sectionType : EPatrolPathRefType.values()){
			if(sectionType.code.equals(code)){
				return sectionType;
			}
		}
		throw new ServiceException("巡查轨迹关联类型不匹配");
	}

    EPatrolPathRefType(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

}
