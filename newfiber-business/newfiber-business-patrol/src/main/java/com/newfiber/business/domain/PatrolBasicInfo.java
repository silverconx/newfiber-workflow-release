package com.newfiber.business.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.newfiber.business.enums.EPatrolBasicInfoChargeUserSource;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 巡查基础信息对象 pat_patrol_basic_info
 * 
 * @author X.K
 * @date 2023-02-10
 */
@Data
@TableName("pat_patrol_basic_info")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "巡查基础信息", description = "巡查基础信息")
public class PatrolBasicInfo extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     *  巡查类型（数据字典 patrol_type）
     */
    @ApiModelProperty(value = "巡查类型（数据字典 patrol_type）")
    private String patrolType;

    /**
     *  巡查目标
     */
    @ApiModelProperty(value = "巡查目标")
    private String patrolTarget;

    /**
     *  巡查长度
     */
    @ApiModelProperty(value = "巡查长度")
    private String patrolTargetLength;

	/**
	 *  负责人来源（内部 inner | 外部 outer）
	 */
	@ApiModelProperty(value = "负责人来源（内部 inner | 外部 outer）")
	private String chargeUserSource;

    /**
     *  负责人（内部为ID | 外部为名称）
     */
    @ApiModelProperty(value = "负责人（内部为ID | 外部为名称）")
    private Object chargeUser;

	/**
	 *  外部负责人手机号
	 */
	@ApiModelProperty(value = "外部负责人手机号")
	private String chargeUserMobile;

    /**
     *  排序
     */
    @ApiModelProperty(value = "排序")
    private Long sort;

    // DB Property

	/**
	 *  内部负责人名称
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "内部负责人名称")
	private String chargeUserName;

	public Object getChargeUser() {
		EPatrolBasicInfoChargeUserSource chargeUserType = EPatrolBasicInfoChargeUserSource.match(getChargeUserSource());
		if(EPatrolBasicInfoChargeUserSource.Inner.equals(chargeUserType)){
			return Long.parseLong(chargeUser.toString());
		}
		return chargeUser;
	}
}
