package com.newfiber.business.service;

import com.newfiber.business.domain.PatrolLog;
import com.newfiber.business.domain.PatrolSection;
import com.newfiber.business.domain.PatrolTask;
import com.newfiber.business.domain.request.patrolLog.PatrolLogQueryRequest;
import com.newfiber.business.domain.request.patrolLog.PatrolLogSaveRequest;
import com.newfiber.business.domain.request.patrolLog.PatrolLogSaveTempRequest;
import com.newfiber.business.domain.request.patrolLog.PatrolLogSubmitTempRequest;
import com.newfiber.business.domain.request.patrolLog.PatrolLogUpdateRequest;
import com.newfiber.business.domain.request.patrolTask.PatrolTaskBeginRequest;
import com.newfiber.business.domain.request.patrolTask.PatrolTaskFinishRequest;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 巡查日志Service接口
 *
 * @author X.K
 * @date 2023-02-17
 */
public interface IPatrolLogService {

    /**
     * 新增巡查日志
     *
     * @param request 新增参数
     * @return 结果
     */
    long insert(PatrolLogSaveRequest request);

    /**
     * 新增巡查日志
     *
     * @param request 新增参数
     * @return 结果
     */
    long insertTemp(PatrolLogSaveTempRequest request);

    /**
     * 批量新增巡查日志
     *
     * @param patrolTasks 新增参数列表
     * @return 结果
     */
    boolean insertBatch(List<PatrolTask> patrolTasks);

    /**
     * 批量删除巡查日志
     *
     * @param ids 编号(,隔开)
     * @return 结果
     */
    boolean delete(String ids);

    /**
     * 更新日志表
     *
     * @param taskId taskId
     * @param date
     */
    void updateCanceledTaskLog(Long taskId, Date date);

    /**
     * 更新日志的开始时间
     *
     * @param request
     */
    void beginPatrolLog(PatrolTaskBeginRequest request);

    /**
     * 结束巡查
     *
     * @param request
     */
    void finishPatrolLog(PatrolTaskFinishRequest request);

    /**
     * 手动停止巡查
     *
     * @param taskId
     */
    void stopPatrolLogBySystem(Long taskId);

    /**
     * 强制提交日志
     *
     * @param patrolTask
     */
    void forceCommitPatrolLog(PatrolTask patrolTask);

    /**
     * 提交巡查轨迹
     *
     * @param taskId
     * @param patrolLength
     * @return 结果
     */
    String submitPatrolPath(Long taskId, BigDecimal patrolLength);

    /**
     * 计算巡查有效率
     *
     * @param patrolSection
     * @param patrolTaskId
     * @param patrolPath
     * @return 结果
     */
    boolean calcPatrolCoverRate(PatrolSection patrolSection, Long patrolTaskId, String patrolPath);

    /**
     * 修改巡查日志
     *
     * @param request 修改参数
     * @return 结果
     */
    boolean update(PatrolLogUpdateRequest request);

    /**
     * 提交临时巡查日志
     *
     * @param request
     * @return 结果
     */
    boolean submitTemp(PatrolLogSubmitTempRequest request);

    /**
     * 取消临时巡查日志
     *
     * @param id
     * @return 结果
     */
    boolean cancelTemp(Long id);

    /**
     * 更新案件数量
     *
     * @param patrolLogId
     * @param operation + 或者 -
     * @return 结果
     */
    boolean updateCaseCount(Long patrolLogId, String operation);

    /**
     * 查询巡查日志
     *
     * @param taskId
     * @return 结果
     */
    Long selectLogIdByTask(Long taskId);

    /**
     * 查询巡查日志
     * @param taskId
     * @return 结果
     */
    PatrolLog selectByTask(Long taskId);

    /**
     * 获取redis巡查轨迹
     * @param patrolLogId
     * @return 结果
     */
    String parseRedisPatrolPath(Long patrolLogId);

    /**
     * 获取巡查轨迹
     * @param patrolLogId
     * @return 结果
     */
    String getPatrolPath(Long patrolLogId);

    /**
     * 详细查询巡查日志
     *
     * @param id 主键
     * @return 巡查日志
     */
    PatrolLog selectDetail(Long id);

    /**
     * 详细查询巡查日志
     *
     * @param id 主键
     * @return 巡查日志
     */
    PatrolLog selectDetailBrief(Long id);

    /**
     * 查询临时日志
     * @param userId
     * @return 巡查日志
     */
    PatrolLog selectTempByUserId(Long userId);

    /**
     * 分页查询巡查日志
     *
     * @param request 分页参数
     * @return 巡查日志集合
     */
    List<PatrolLog> selectPage(PatrolLogQueryRequest request);

    /**
     * 列表查询巡查日志
     *
     * @param request 列表参数
     * @return 巡查日志集合
     */
    List<PatrolLog> selectList(PatrolLogQueryRequest request);

    /**
     * 列表查询巡查任务
     *
     * @param request 列表参数
     * @return 巡查任务集合
     */
    List<PatrolLog> selectRealtimeList(PatrolLogQueryRequest request);

    /**
     * 导出
     *
     * @param response
     * @param request
     */
    void export(HttpServletResponse response, PatrolLogQueryRequest request);

}
