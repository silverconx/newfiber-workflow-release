package com.newfiber.business.enums;

import com.newfiber.common.core.exception.ServiceException;

/**
 * date: 2023/3/23 下午 08:04
 * @author: LuFan
 * @since JDK 1.8
 */
public enum EPatrolType {
    /**
     * 巡查类型
     */
    River("river","河道"),

    Pipeline("pipeline","管网"),
    ;

    public static EPatrolType match(String code){
        for(EPatrolType sectionType : EPatrolType.values()){
            if(sectionType.code.equals(code)){
                return sectionType;
            }
        }
        throw new ServiceException("巡查类型不匹配");
    }

    EPatrolType(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
