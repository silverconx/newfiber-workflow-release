package com.newfiber.business.domain;

import static cn.hutool.core.date.DatePattern.NORM_DATETIME_MINUTE_PATTERN;
import static cn.hutool.core.date.DatePattern.NORM_DATETIME_PATTERN;
import static cn.hutool.core.date.DatePattern.NORM_DATE_PATTERN;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.newfiber.common.core.web.domain.BaseEntity;
import com.newfiber.system.api.domain.request.SysFileSaveRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 巡查案件对象 pat_patrol_case
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
@TableName("pat_patrol_case")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "巡查案件", description = "巡查案件")
public class PatrolCase extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 工作流实例编号
     */
    @ApiModelProperty(value = "工作流实例编号")
    private String workflowInstanceId;

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号")
    private String number;

    /**
     * 巡查类型（数据字典 patrol_type）
     */
    @ApiModelProperty(value = "巡查类型（数据字典 patrol_type）")
    private String patrolType;

    /**
     *  巡查基础信息编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "巡查基础信息编号")
    private Long basicInfoId;

    /**
     * 巡查段编号
     */
    @ApiModelProperty(value = "巡查段编号")
    private Long patrolSectionId;

    /**
     * 巡查任务编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "巡查任务编号")
    private Long patrolTaskId;

    /**
     * 巡查日志编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "巡查日志编号")
    private Long patrolLogId;

    /**
     * 案件名称
     */
    @ApiModelProperty(value = "案件名称")
    private String caseName;

    /**
     * 案件等级（数据字典 case_level）
     */
    @ApiModelProperty(value = "案件等级（数据字典 case_level）")
    private String caseLevel;

    /**
     * 问题类型/案件类型（数据字典  case_type）
     */
    @ApiModelProperty(value = "问题类型/案件类型（数据字典  case_type）")
    private String caseType;

    /**
     * 案件内容/问题详情/问题描述
     */
    @ApiModelProperty(value = "案件内容/问题详情/问题描述")
    private String caseContent;

    /**
     * 案件来源
     */
    @ApiModelProperty(value = "案件来源")
    private String caseSource;

    /**
     * 案件数据来源
     * 1巡查任务录入的案件task 2巡查日志录入的案件log 3单独录入的案件case
     */
    @ApiModelProperty(value = "案件数据来源")
    private String caseDataSource;

    /**
     * 截止日期
     */
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    @ApiModelProperty(value = "截止日期")
    private Date deadline;

    /**
     * 案件地址
     */
    @ApiModelProperty(value = "案件地址")
    private String caseAddress;

    /**
     * 经纬度
     */
    @ApiModelProperty(value = "经纬度")
    private String lonLat;

    /**
     * 是否上报（0 否 | 1 是）
     */
    @ApiModelProperty(value = "是否上报（0 否 | 1 是）")
    private String reportFlag;

    /**
     * 是否作废（0 否 | 1 是）
     */
    @ApiModelProperty(value = "是否作废（0 否 | 1 是）")
    private String deprecateFlag;

    /**
     * 作废原因
     */
    @ApiModelProperty(value = "作废原因")
    private String deprecateReason;

    /**
     * 期望完成日期
     */
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    @ApiModelProperty(value = "期望完成日期（yyyy-MM-dd）")
    private Date expectEndDate;

    // DB Property

    /**
     * 计划应结束时间
     */
    @TableField(exist = false)
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    @ApiModelProperty(value = "计划应结束时间")
    private Date planEndDatetime;

	/**
	 * 文件
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "文件")
	private List<SysFileSaveRequest> fileSaveRequestList;

	/**
	 * 巡查段名称
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "巡查段名称")
	private String patrolSectionName;

	/**
	 * 巡查段类型（点 point | 线 line | 面 area）
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "巡查段类型（点 point | 线 line | 面 area）")
	private String sectionType;

	/**
	 * 问题类型/案件类型
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "问题类型/案件类型")
	private String caseTypeStr;

	/**
	 * 巡查段详情（JSON）
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "巡查段详情（JSON）")
	private String sectionDetail;

	/**
	 * 问题状态
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "问题状态")
	private String statusStr;

	/**
	 * 上报人
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "上报人")
	private String reportPerson;

	/**
	 * 上报人电话
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "上报人电话")
	private String reportPersonPhone;

	/**
	 * 上报时间
	 */
	@TableField(exist = false)
	@JsonFormat(pattern = NORM_DATETIME_MINUTE_PATTERN)
	@DateTimeFormat(pattern = NORM_DATETIME_MINUTE_PATTERN)
	@ApiModelProperty(value = "上报时间")
	private Date reportTime;

	/**
	 * 巡查河道/巡查内容名称/所属河道
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "巡查河道/巡查内容名称/所属河道")
	private Long patrolTarget;

	/**
	 * 巡查内容名称
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "巡查内容名称")
	private String patrolTargetName;

    public String getPatrolSectionName() {
        return PatrolSection.parsePatrolSectionName(patrolTargetName, sectionType, sectionDetail);
    }
}
