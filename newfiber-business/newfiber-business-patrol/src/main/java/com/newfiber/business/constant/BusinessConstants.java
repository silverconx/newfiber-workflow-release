package com.newfiber.business.constant;

/**
 * date: 2023/2/27 下午 06:08
 *
 * @author: LuFan
 * @since JDK 1.8
 */
public class BusinessConstants {
    /**
     * 逗号
     */
    public final static String COMMA = ",";

    /**
     * 正在进行的任务数量
     */
    public final static String TASK_PROCEED_COUNT = "task_proceed_count";

    /**
     * 已经结束的任务数量
     */
    public final static String TASK_DONE_COUNT = "task_done_count";

    /**
     * ALL
     */
    public final static String ALL = "all";

    /**
     * CASE_SOURCE
     */
    public final static String CASE_SOURCE = "case_source";

    /**
     * CASE_TYPE
     */
    public final static String CASE_TYPE = "case_type";

    /**
     * PIPELINE_CASE_TYPE
     */
    public final static String PIPELINE_CASE_TYPE = "pipeline_case_type";

    /**
     * PATROL_TASK_ID
     */
    public final static String PATROL_TASK_ID = "patrol_task_id";

    /**
     * YEAR
     */
    public final static String YEAR = "year";

    /**
     * MONTH
     */
    public final static String MONTH = "month";

    /**
     * WEEK
     */
    public final static String WEEK = "week";

    /**
     * PAT_LOG
     */
    public final static String PAT_LOG= "巡查日志";

    /**
     * PAT_LOG
     */
    public final static String PAT_EXAMINE= "巡查考核";

    /**
     * STATUS_1 启用
     */
    public final static String STATUS_1 = "1";

    /**
     * STATUS_0 未启用
     */
    public final static String STATUS_0 = "0";

    public final static String FALSE_STR = "false";

    /**
     * 巡查考核导出模板文件
     */
    public static final String EXAMINE_EXPORT_TEMPLATE = "/template/examineExport.docx";

    /**
     * 巡查考核导出文件名称
     */
    public static final String EXAMINE_EXPORT_FILE_NAME = "examineExport.docx";


    /**
     * 巡查日志导出模板文件
     */
    public static final String PATROL_LOG_EXPORT_TEMPLATE = "/template/patrolLogExport.docx";

    /**
     * 巡查日志导出文件名称
     */
    public static final String PATROL_LOG_EXPORT_FILE_NAME = "patrolLogExport.docx";
}
