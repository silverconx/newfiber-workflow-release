package com.newfiber.business.service;

import com.newfiber.business.domain.PatrolSection;
import com.newfiber.business.domain.PatrolTask;
import com.newfiber.business.domain.WorkCal;
import com.newfiber.business.domain.request.patrolTask.PatrolTaskBeginRequest;
import com.newfiber.business.domain.request.patrolTask.PatrolTaskFinishRequest;
import com.newfiber.business.domain.request.patrolTask.PatrolTaskGeneratorDraftRequest;
import com.newfiber.business.domain.request.patrolTask.PatrolTaskQueryRequest;
import com.newfiber.business.domain.request.patrolTask.PatrolTaskSaveRequest;
import com.newfiber.business.domain.request.patrolTask.PatrolTaskStaticsRequest;
import com.newfiber.business.domain.request.patrolTask.PatrolTaskUpdateRequest;
import com.newfiber.business.domain.response.patrolHomePage.PatrolHomePageCount;
import com.newfiber.business.domain.response.patrolTask.PatrolTaskStatusStatistics;
import com.newfiber.business.enums.EPatrolTaskStatus;
import java.util.Date;
import java.util.List;

/**
 * 巡查任务Service接口
 *
 * @author X.K
 * @date 2023-02-17
 */
public interface IPatrolTaskService {

    /**
     * 新增巡查任务
     *
     * @param request 新增参数
     * @return 结果
     */
    long insert(PatrolTaskSaveRequest request);

    /**
     * 新增巡查任务
     *
     * @param planId         计划ID
     * @param status         是否启用
     * @param patrolTaskList 任务列表
     * @return 结果
     */
    List<PatrolTask> insertBatch(Long planId, String status, List<PatrolTask> patrolTaskList);

    /**
     * 生成巡查任务草稿
     *
     * @param request 生成巡查任务草稿
     * @return 结果
     */
    List<PatrolTask> generatorDraft(PatrolTaskGeneratorDraftRequest request);

    /**
     * 批量删除巡查任务
     *
     * @param ids 编号(,隔开)
     * @return 结果
     */
    boolean delete(String ids);

    /**
     * 开始巡查
     *
     * @return 结果
     */
    boolean beginPatrol(PatrolTaskBeginRequest request);

    /**
     * 取消巡查
     *
     * @param taskId 巡查任务id
     * @return 结果
     */
    boolean cancelPatrol(Long taskId);

    /**
     * 重新巡查
     *
     * @param taskId 巡查任务id
     * @return 结果
     */
    boolean restartPatrol(Long taskId);

    /**
     * 结束巡查
     *
     * @return 结果
     */
    boolean finishPatrol(PatrolTaskFinishRequest request, PatrolSection patrolSection);

    /**
     * 停止巡查
     *
     * @param taskId 巡查任务id
     * @return 结果
     */
    boolean stopPatrol(Long taskId);

    /**
     * 强制提交
     *
     * @param patrolTask 巡查任务id
     * @return 结果
     */
    boolean forceCommit(PatrolTask patrolTask);

    /**
     * 修改巡查任务
     *
     * @param request 修改参数
     * @return 结果
     */
    boolean update(PatrolTaskUpdateRequest request);

    /**
     * 启用巡查任务
     *
     * @param planId 巡查计划id
     * @return 结果
     */
    boolean startUseTask(Long planId);

    /**
     * 修改巡查任务 包装方法
     *
     * @param request 修改参数
     * @return 结果
     */
    boolean updateWrapper(PatrolTaskUpdateRequest request);

    /**
     * 计算数量
     *
     * @param patrolSectionId 巡查段id
     * @return 结果
     */
    long countBySection(Long patrolSectionId);

    /**
     * 计算数量
     *
     * @param planId 计划id
     * @param status 状态
     * @return long 结果
     */
    long count(Long planId, EPatrolTaskStatus status);

    /**
     * 计算数量
     *
     * @param status 状态
     * @return long 结果
     */
    long countByUser(Long taskUserId, String patrolType, EPatrolTaskStatus status);

    /**
     * 详细查询巡查任务
     *
     * @param id 主键
     * @return 巡查任务
     */
    PatrolTask selectDetail(Long id);

    /**
     * 详细查询巡查任务
     *
     * @param id 主键
     * @return 巡查任务
     */
    PatrolTask selectDetailBrief(Long id);

    /**
     * 分页查询巡查任务
     *
     * @param request 分页参数
     * @return 巡查任务集合
     */
    List<PatrolTask> selectPage(PatrolTaskQueryRequest request);

    /**
     * 列表查询巡查任务
     *
     * @param request 列表参数
     * @return 巡查任务集合
     */
    List<PatrolTask> selectList(PatrolTaskQueryRequest request);

    /**
     * 列表查询巡查任务
     *
     * @return 巡查任务集合
     */
    List<PatrolTask> selectList(EPatrolTaskStatus taskStatus);

    /**
     * 状态统计
     */
    List<PatrolTaskStatusStatistics> statusStatistics(PatrolTaskStaticsRequest request);

    /**
     * 截止结束时间列表
     *
     * @param planEndDatetime
     * @return 巡查任务集合
     */
    List<PatrolTask> untilEndDateList(Date planEndDatetime, String patrolType);

    /**
     * 截止结束时间列表
     *
     * @return 巡查任务集合
     */
    List<PatrolTask> selectList(List<Long> idList);

    /**
     * 查询即将开始任务列表
     *
     * @return PatrolTask
     */
    List<PatrolTask> queryTodoTasks();

    /**
     * 查询并更新超时任务
     *
     * @return PatrolTask
     */
    List<PatrolTask> queryAndUpdateExpiredTasks();

    /**
     * 首页/工作台统计
     *
     * @return PatrolTask
     */
    List<PatrolHomePageCount> patrolCount();

    /**
     * 首页工作日历统计
     *
     * @param startDate
     * @param endDate
     * @return WorkCal
     */
    WorkCal workCal(Date startDate, Date endDate);

    /**
     * 根据巡查计划创建巡查任务之后，系统将提前一天通知相关的巡查人员。
     * date: 2023/4/19 下午 04:32
     *
     * @author: LuFan
     * @since JDK 1.8
     */
    void queryAndNoticeTodoTasks();

    /**
     * 检测超时的任务
     * date: 2023/4/19 下午 04:34
     * @author: LuFan
     * @since JDK 1.8
     */
    void findExpiredTasks();

}
