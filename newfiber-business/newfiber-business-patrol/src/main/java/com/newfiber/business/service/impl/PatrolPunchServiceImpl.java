package com.newfiber.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.newfiber.business.domain.PatrolPunch;
import com.newfiber.business.domain.request.patrolPunch.PatrolPunchQueryRequest;
import com.newfiber.business.domain.request.patrolPunch.PatrolPunchSaveRequest;
import com.newfiber.business.domain.request.patrolPunch.PatrolPunchUpdateRequest;
import com.newfiber.business.mapper.PatrolPunchMapper;
import com.newfiber.business.service.IPatrolPunchService;
import com.newfiber.common.core.exception.ServiceException;
import com.newfiber.common.core.web.domain.BaseEntity;
import com.newfiber.common.core.web.service.BaseServiceImpl;
import com.newfiber.system.api.RemoteFileService;
import java.util.List;
import java.util.Optional;
import javax.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 巡查打卡Service业务层处理
 * 
 * @author X.K
 * @date 2023-02-17
 */
@Service
public class PatrolPunchServiceImpl extends BaseServiceImpl<PatrolPunchMapper, PatrolPunch> implements IPatrolPunchService {

    @Resource
    private PatrolPunchMapper patrolPunchMapper;

    @Resource
    private RemoteFileService remoteFileService;

    @Override
    public long insert(PatrolPunchSaveRequest request) {
        PatrolPunch patrolPunch = new PatrolPunch();
        BeanUtils.copyProperties(request, patrolPunch);
        save(patrolPunch);

	    remoteFileService.addBatch("patrolPunch", patrolPunch.getId(), request.getFileSaveRequestList());

        return Optional.of(patrolPunch).map(BaseEntity::getId).orElse(0L);
    }

    @Override
    public boolean update(PatrolPunchUpdateRequest request) {
        PatrolPunch patrolPunch = new PatrolPunch();
        BeanUtils.copyProperties(request, patrolPunch);
        return updateById(patrolPunch);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(String ids) {
	    return deleteLogic(ids);
    }

	@Override
	public boolean deleteByTaskId(Long patrolTaskId) {
    	// 由任务生成的日志，id相同
		deleteByLogId(patrolTaskId);
		QueryWrapper<PatrolPunch> patrolCaseWrapper = new QueryWrapper<PatrolPunch>().
			eq("patrol_task_id", patrolTaskId);
		return remove(patrolCaseWrapper);
	}

	@Override
	public boolean deleteByLogId(Long patrolLogId) {
		QueryWrapper<PatrolPunch> patrolCaseWrapper = new QueryWrapper<PatrolPunch>().
			eq("patrol_log_id", patrolLogId);
		return remove(patrolCaseWrapper);
	}

	@Override
    public PatrolPunch selectDetail(Long id) {
        PatrolPunch patrolPunch = patrolPunchMapper.selectOneById(id);
        if(null == patrolPunch){
	        throw new ServiceException(String.format("%s ID=%s 的记录不存在", this.getClass().getSimpleName(), id));
        }
        return patrolPunch;
    }

    @Override
    public List<PatrolPunch> selectPage(PatrolPunchQueryRequest request) {
        return patrolPunchMapper.selectByCondition(request);
    }

    @Override
    public List<PatrolPunch> selectList(PatrolPunchQueryRequest request) {
        return patrolPunchMapper.selectByCondition(request);
    }

}
