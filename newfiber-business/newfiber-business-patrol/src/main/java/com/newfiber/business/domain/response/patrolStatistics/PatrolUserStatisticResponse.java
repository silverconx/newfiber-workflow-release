package com.newfiber.business.domain.response.patrolStatistics;

import com.newfiber.business.domain.PatrolTask;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Data;

/**
 * @author : X.K
 * @since : 2023/3/3 下午1:35
 */
@Data
public class PatrolUserStatisticResponse {

	@ApiModelProperty("总人数/总巡查员")
	private long totalPatrolUserCount;

	@ApiModelProperty("实时人数")
	private long realtimePatrolUserCount;

	@ApiModelProperty("实时在线任务")
	private List<PatrolTask> realtimePatrolTask;

	@ApiModelProperty("应到人数")
	private long shouldPatrolUserCount;

	@ApiModelProperty("应到任务")
	private List<PatrolTask> shouldPatrolTaskList;

	@ApiModelProperty("已完成人数/实巡查员")
	private long donePatrolUserCount;

	@ApiModelProperty("已完成任务")
	private List<PatrolTask> donePatrolTaskList;

	@ApiModelProperty("缺勤人数")
	private long absencePatrolUserCount;

	@ApiModelProperty("缺勤任务")
	private List<PatrolTask> absencePatrolTaskList;

	@ApiModelProperty("巡查次数")
	private long donePatrolTaskCount;

	@ApiModelProperty("处置事件")
	private long patrolCaseCount;

	@ApiModelProperty("异常停留人数")
	private long abnormalStayUserCount;

}
