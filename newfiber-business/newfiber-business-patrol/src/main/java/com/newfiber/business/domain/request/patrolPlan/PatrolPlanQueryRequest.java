package com.newfiber.business.domain.request.patrolPlan;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.newfiber.common.core.web.request.BaseQueryRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import static cn.hutool.core.date.DatePattern.NORM_DATE_PATTERN;

/**
 * 巡查计划配置对象 pat_patrol_plan
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PatrolPlanQueryRequest extends BaseQueryRequest {

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号")
    private String number;

    /**
     * 巡查类型（数据字典 patrol_type）
     */
    @ApiModelProperty(value = "巡查类型（数据字典 patrol_type）")
    private String patrolType;

    /**
     * 巡查计划名称
     */
    @ApiModelProperty(value = "巡查计划名称")
    private String planName;

    /**
     * 查询条件时间段开始
     */
    @ApiModelProperty(value = "查询条件时间段开始")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date queryFromTime;

    /**
     * 查询条件时间段结束
     */
    @ApiModelProperty(value = "查询条件时间段结束")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date queryToTime;

    /**
     * 巡查基础信息编号
     */
    @ApiModelProperty(value = "巡查基础信息编号")
    private Long basicInfoId;

    /**
     * 是否平均分配（0 否 | 1 是）
     */
    @ApiModelProperty(value = "是否平均分配（0 否 | 1 是）")
    private String averageDistributeFlag;

    /**
     * 频率类型（固定频率 fix_frequency | 固定日期 fix_date | 间隔日期 interval_date）
     */
    @ApiModelProperty(value = "频率类型（固定频率 fix_frequency | 固定日期 fix_date | 间隔日期 interval_date）")
    private String patrolFrequencyType;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    private String status;

}
