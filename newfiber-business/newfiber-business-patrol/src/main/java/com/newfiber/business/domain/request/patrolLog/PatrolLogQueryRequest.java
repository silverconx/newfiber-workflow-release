package com.newfiber.business.domain.request.patrolLog;

import static cn.hutool.core.date.DatePattern.NORM_DATE_PATTERN;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.newfiber.common.core.web.request.BaseQueryRequest;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;


/**
 * 巡查日志对象 pat_patrol_log
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PatrolLogQueryRequest extends BaseQueryRequest {

    /**
     * 巡查id
     */
    @ApiModelProperty(value = "巡查id")
    private Long id;

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号")
    private String number;

    /**
     * 巡查类型（数据字典 patrol_type）
     */
    @ApiModelProperty(value = "巡查类型（数据字典 patrol_type）")
    private String patrolType;

    /**
     * 巡查任务编号
     */
    @ApiModelProperty(value = "巡查任务编号")
    private Long patrolTaskId;

	/**
	 * 日志巡查人
	 */
	@ApiModelProperty(value = "日志巡查人")
	private Long logUserId;

    /**
     * 提交类型（正常 normal | 系统 system）
     */
    @ApiModelProperty(value = "提交类型/提交情况（正常 normal | 系统 system）")
    private String submitType;

    /**
     * 巡查名称
     */
    @ApiModelProperty(value = "巡查名称")
    private String patrolName;

    /**
     * 提交状态
     */
    @ApiModelProperty(value = "提交状态（未提交 draft |  已提交 committed）")
    private String status;

    /**
     * 巡查人
     */
    @ApiModelProperty(value = "巡查人")
    private Long taskUserId;

    /**
     * 巡查河道/巡查内容名称
     */
    @ApiModelProperty(value = "巡查河道/巡查内容名称")
    private Long patrolTargetId;

    /**
     * 开始日期
     */
    @ApiModelProperty(value = "开始日期")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date startDate;

    /**
     * 结束日期
     */
    @ApiModelProperty(value = "结束日期")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date endDate;

    /**
     * 编号列表
     */
    @ApiModelProperty(value = "编号列表")
    private List<Long> idList;

    /**
     * 巡查记录创建时间
     */
    @ApiModelProperty(value = "巡查记录创建时间")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date createTime;

    /**
     * 综合搜索条件
     */
    @ApiModelProperty(value = "综合搜索条件")
    private String queryStr;
}
