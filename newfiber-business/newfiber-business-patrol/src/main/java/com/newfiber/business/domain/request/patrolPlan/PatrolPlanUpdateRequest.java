package com.newfiber.business.domain.request.patrolPlan;

import static cn.hutool.core.date.DatePattern.NORM_DATETIME_PATTERN;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 巡查计划配置对象 pat_patrol_plan
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolPlanUpdateRequest{

    /**
     * 主键
     */
    @NotNull
    @ApiModelProperty(value = "主键", required = true)
    private Long id;

    /**
     * 巡查计划名称
     */
    @NotBlank
    @ApiModelProperty(value = "巡查计划名称", required = true)
    private String planName;

    /**
     * 巡查段编号（全部：*  |  多段用,隔开）
     */
    @NotBlank
    @ApiModelProperty(value = "巡查段编号（全部：*  |  多段用,隔开）", required = true)
    private String patrolSectionIds;

    /**
     * 开始时间
     */
    @NotNull
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    @ApiModelProperty(value = "开始时间", required = true)
    private Date startDatetime;

    /**
     * 结束时间
     */
    @NotNull
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    @ApiModelProperty(value = "结束时间", required = true)
    private Date endDatetime;

    /**
     * 是否平均分配（0 否 | 1 是）
     */
    @NotBlank
    @ApiModelProperty(value = "是否平均分配（0 否 | 1 是）", required = true)
    private String averageDistributeFlag;

    /**
     * 频率类型（固定频率 fix_frequency | 固定日期 fix_date | 间隔日期 interval_date）
     */
    @NotBlank
    @ApiModelProperty(value = "频率类型（固定频率 fix_frequency | 固定日期 fix_date | 间隔日期 interval_date）", required = true)
    private String patrolFrequencyType;

    /**
     * 频率内容
     */
    @NotBlank
    @ApiModelProperty(value = "频率内容", required = true)
    private String patrolFrequencyContent;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

	/**
	 * 状态（0停用 | 1启用）
	 */
	@ApiModelProperty(value = "状态（0停用 | 1启用）")
	private String status;

}
