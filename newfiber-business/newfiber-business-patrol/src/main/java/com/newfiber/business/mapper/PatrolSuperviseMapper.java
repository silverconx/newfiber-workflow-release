package com.newfiber.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import com.newfiber.business.domain.request.patrolSupervise.PatrolSuperviseQueryRequest;
import com.newfiber.business.domain.PatrolSupervise;
import org.apache.ibatis.annotations.Param;

/**
 * 巡查督导Mapper接口
 * 
 * @author X.K
 * @date 2023-02-17
 */
public interface PatrolSuperviseMapper extends BaseMapper<PatrolSupervise>{

    /**
     * 条件查询巡查督导列表
     * 
     * @param request 查询条件
     * @return 巡查督导集合
     */
    List<PatrolSupervise> selectByCondition(@Param("request") PatrolSuperviseQueryRequest request);

}
