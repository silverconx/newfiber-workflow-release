package com.newfiber.business.domain.request.patrolCase;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * 巡查案件对象 pat_patrol_case
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolCaseDeprecateRequest {

	/**
	 * id
	 */
	@NotNull
	@ApiModelProperty(value = "id", required = true)
	private Long id;

	/**
	 * 作废原因
	 */
	@NotBlank
	@ApiModelProperty(value = "作废原因", required = true)
	private String deprecateReason;

}
