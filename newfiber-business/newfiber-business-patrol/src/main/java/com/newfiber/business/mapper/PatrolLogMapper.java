package com.newfiber.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newfiber.business.domain.PatrolLog;
import com.newfiber.business.domain.request.patrolLog.PatrolLogQueryRequest;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 巡查日志Mapper接口
 * 
 * @author X.K
 * @date 2023-02-17
 */
public interface PatrolLogMapper extends BaseMapper<PatrolLog>{

    /**
     * 条件查询巡查日志列表
     * 
     * @param request 查询条件
     * @return 巡查日志集合
     */
    List<PatrolLog> selectByCondition(@Param("request") PatrolLogQueryRequest request);

	/**
	 * 条件查询巡查案件列表
	 *
	 * @param id 查询条件
	 * @return 巡查案件集合
	 */
	PatrolLog selectOneById(@Param("id") Long id);

}
