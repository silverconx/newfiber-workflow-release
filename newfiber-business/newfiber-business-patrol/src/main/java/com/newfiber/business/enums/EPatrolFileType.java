package com.newfiber.business.enums;

import com.newfiber.common.core.web.support.IFileRefType;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author : X.K
 * @since : 2023/3/1 上午9:49
 */
@Getter
@AllArgsConstructor
public enum EPatrolFileType implements IFileRefType {

	/**
	 *
	 */
	PatrolCase("PatrolCase", "", "巡查案件"),

	;

	private final String refType;

	private final String refField;

	private final String refTypeName;

}
