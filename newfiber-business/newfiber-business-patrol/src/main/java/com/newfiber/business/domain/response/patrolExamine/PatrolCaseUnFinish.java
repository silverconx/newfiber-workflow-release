package com.newfiber.business.domain.response.patrolExamine;

import lombok.Data;

/**
 * 巡查考核统计各个巡查问题类型对于案件数量以及未完成案件数量
 * date: 2023/3/24 下午 05:18
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
public class PatrolCaseUnFinish {

    /**
     * 巡查内容名称
     */
    private String caseType;

    /**
     * 巡查内容对应案件数量
     */
    private int caseCount;

    /**
     * 巡查内容对应完成案件数量
     */
    private int caseCountUnDone;
}
