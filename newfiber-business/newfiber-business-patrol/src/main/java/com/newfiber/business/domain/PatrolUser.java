package com.newfiber.business.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 巡查人员对象 pat_patrol_user
 * 
 * @author X.K
 * @date 2023-02-17
 */
@Data
@TableName("pat_patrol_user")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "巡查人员", description = "巡查人员")
public class PatrolUser extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /**
     *  关联类型（巡查段 section | 巡查任务 task）
     */
    @ApiModelProperty(value = "关联类型（巡查段 section | 巡查任务 task）")
    private String refType;

    /**
     *  关联编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "关联编号")
    private Long refId;

    /**
     *  用户类型（巡查人员 patrol | 立案人员 register | 核查人员 approve | 领导人员 leader | 处理人员 handle | 验证人员 verify | 结案人员 close）
     */
    @ApiModelProperty(value = "用户类型（巡查人员 patrol | 立案人员 register | 核查人员 approve | 领导人员 leader | 处理人员 handle | 验证人员 verify | 结案人员 close）")
    private String userType;

    /**
     *  用户编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "用户编号")
    private Long userId;

     // DB Property
    /**
     *  用户姓名
     */
	@TableField(exist = false)
    @ApiModelProperty(value = "用户姓名")
    private String userName;

	/**
	 *  手机号
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "手机号")
	private String phoneNumber;

	/**
	 *  关联名称
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "关联名称")
	private String refName;

}
