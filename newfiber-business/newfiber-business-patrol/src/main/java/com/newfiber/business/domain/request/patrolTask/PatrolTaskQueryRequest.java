package com.newfiber.business.domain.request.patrolTask;

import static cn.hutool.core.date.DatePattern.NORM_DATETIME_PATTERN;
import static cn.hutool.core.date.DatePattern.NORM_DATE_PATTERN;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.newfiber.common.core.web.request.BaseQueryRequest;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;
/**
 * 巡查任务对象 pat_patrol_task
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PatrolTaskQueryRequest extends BaseQueryRequest {

    /**
     * 计划id
     */
    @ApiModelProperty(value = "计划id")
    private Long planId;

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号")
    private String number;

    /**
     * 任务来源（巡查 patrol | 报警 warning）
     */
    @ApiModelProperty(value = "任务来源（巡查 patrol | 报警 warning）")
    private String taskSource;

    /**
     * 巡查类型（数据字典 patrol_type）
     */
    @ApiModelProperty(value = "巡查类型（数据字典 patrol_type）")
    private String patrolType;

    /**
     * 巡查基础信息编号
     */
    @ApiModelProperty(value = "巡查基础信息编号")
    private Long basicInfoId;

    /**
     * 巡查段编号
     */
    @ApiModelProperty(value = "巡查段编号")
    private Long patrolSectionId;

    /**
     * 任务名称
     */
    @ApiModelProperty(value = "任务名称")
    private String taskName;

    /**
     * 巡查人
     */
    @ApiModelProperty(value = "巡查人")
    private String taskUserId;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    private String status;

    /**
     * 状态-字符串形式(用于首页任务日历查询)
     */
    @ApiModelProperty(value = "状态")
    private String statusStr;

    /**
     * 状态(只能用于查询待办已办)
     * 会去查询当前时间之前的任务，未来的任务不会查出来
     */
    @ApiModelProperty(value = "状态")
    private List<String> statusList;

    /**
     * 巡查结束日期（yyyy-mm-dd）
     */
    @ApiModelProperty(value = "巡查结束日期yyyy-mm-dd")
    private String planEndDate;

    /**
     * 开始日期
     */
    @ApiModelProperty(value = "开始日期")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date startDate;

    /**
     * 结束日期
     */
    @ApiModelProperty(value = "结束日期")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date endDate;

    /**
     * 计划开始时间
     */
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    @ApiModelProperty(value = "计划开始时间")
    private Date planStartDatetime;

    /**
     * 计划结束时间
     */
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    @ApiModelProperty(value = "计划结束时间")
    private Date planEndDatetime;

    /**
     * 编号列表
     */
    @ApiModelProperty(value = "编号列表")
    private List<Long> idList;

    /**
     * 排除状态
     */
    @ApiModelProperty(value = "排除状态")
    private String notStatus;

    /**
     * 排除状态
     */
    @ApiModelProperty(value = "排除状态")
    private String notStatusList;

    /**
     * 计划结束时间-上限
     */
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    @ApiModelProperty(value = "计划结束时间-上限")
    private Date planEndDatetimeEnd;

    /**
     * 任务计划开始日期查询-日期范围查找
     */
    @ApiModelProperty(value = "任务计划开始日期查询-开始时间")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date taskPlanStartDateFrom;

    /**
     * 任务计划开始日期查询-日期范围查找
     */
    @ApiModelProperty(value = "任务计划开始日期查询-结束时间")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date taskPlanStartDateTo;

    /**
     * 用于系统首页-任务日历-点击日历选中 待完成日期弹出这个日期里面的任务
     * 查询条件-任务计划开始日期-精确查找
     */
    @ApiModelProperty(value = "任务计划开始日期")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date taskPlanStartDate;

    /**
     * 任务计划完成日期查询-日期范围查找
     */
    @ApiModelProperty(value = "任务计划完成日期查询-开始时间")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date taskPlanEndDateFrom;

    /**
     * 任务计划完成日期查询-日期范围查找
     */
    @ApiModelProperty(value = "任务计划完成日期查询-结束时间")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date taskPlanEndDateTo;

    /**
     * 用于系统首页-任务日历-点击日历选中 未完成日期弹出这个日期里面的任务
     * 查询条件-任务计划结束日期-精确查找
     */
    @ApiModelProperty(value = "任务计划结束日期")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date taskPlanEndDate;

    /**
     * 任务实际完成日期查询-日期范围查找
     */
    @ApiModelProperty(value = "任务实际完成日期查询-开始时间")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date taskRealEndDateFrom;

    /**
     * 任务实际完成日期查询-日期范围查找
     */
    @ApiModelProperty(value = "任务实际完成日期查询-结束时间")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date taskRealEndDateTo;

    /**
     * 用于系统首页-任务日历-点击日历选中 已完成日期弹出这个日期里面的任务
     * 查询条件-任务实际结束日期-精确查找
     */
    @ApiModelProperty(value = "任务实际结束日期")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date taskRealEndDate;

}
