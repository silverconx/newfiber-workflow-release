package com.newfiber.business.enums;

import com.newfiber.common.core.exception.ServiceException;

/**
 * @author : Silver
 * @since : 2019-01-11 22:10
 */
public enum EPatrolUserType {

	/**
	 * 用户类型（巡查人员 patrol | 立案人员 register | 核查人员 approve | 领导人员 leader | 处理人员 handle | 验证人员 verify | 结案人员 close）
	 */
	Patrol("patrol", "巡查人员"),
	Register("register", "立案人员"),
	Approve("approve", "核查人员"),
	Leader("leader", "领导人员"),
	Handle("handle", "处理人员"),
	Verify("verify", "验证人员"),
	Close("close", "结案人员"),

	;

	public static EPatrolUserType match(String code){
		for(EPatrolUserType sectionType : EPatrolUserType.values()){
			if(sectionType.code.equals(code)){
				return sectionType;
			}
		}
		throw new ServiceException("巡查人员类型不匹配");
	}

    EPatrolUserType(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

}
