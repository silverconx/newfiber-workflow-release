package com.newfiber.business.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 巡查督导对象 pat_patrol_supervise
 * 
 * @author X.K
 * @date 2023-02-17
 */
@Data
@TableName("pat_patrol_supervise")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "巡查督导", description = "巡查督导")
public class PatrolSupervise extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /**
     *  任务编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "任务编号")
    private Long patrolTaskId;

    /**
     *  任务截止时间
     */
    @ApiModelProperty(value = "任务截止时间")
    private Date taskEndDatetime;

    /**
     *  是否短信提醒（0 否 | 1 是）
     */
    @ApiModelProperty(value = "是否短信提醒（0 否 | 1 是）")
    private String smsNoticeFlag;

    /**
     * 督导人
     */
    @ApiModelProperty(value = "督导人")
    private String createByName;


}
