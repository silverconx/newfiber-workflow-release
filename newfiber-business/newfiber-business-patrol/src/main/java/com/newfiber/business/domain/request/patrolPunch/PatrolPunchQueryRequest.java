package com.newfiber.business.domain.request.patrolPunch;

import com.newfiber.common.core.web.request.BaseQueryRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 巡查打卡对象 pat_patrol_punch
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PatrolPunchQueryRequest extends BaseQueryRequest {

    /**
     * id
     */
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 巡查任务编号
     */
    @ApiModelProperty(value = "巡查任务编号")
    private Long patrolTaskId;

    /**
     * 巡查日志编号
     */
    @ApiModelProperty(value = "巡查日志编号")
    private Long patrolLogId;

    /**
     * 地址
     */
    @ApiModelProperty(value = "地址")
    private String address;

    /**
     * 经纬度
     */
    @ApiModelProperty(value = "经纬度")
    private String lonLat;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    private String status;

}
