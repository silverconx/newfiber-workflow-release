package com.newfiber.business.domain.response.patrolExamine;

import com.newfiber.common.core.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 巡查考核列表返回数据
 * date: 2023/3/3 下午 05:42
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
public class PatrolExamineQueryResult {
    /**
     * id
     */
    @ApiModelProperty(value = "id")
    private String id;

    /**
     * 巡查人员ID
     */
    @ApiModelProperty(value = "巡查人员ID")
    private Long userId;

    /**
     * 巡查人员姓名
     */
    @Excel(name = "巡查人员姓名")
    @ApiModelProperty(value = "巡查人员姓名")
    private String userName;

    /**
     * 巡查次数
     * 实际巡查次数 / 应巡查次数
     */
    @Excel(name = "巡查次数")
    @ApiModelProperty(value = "巡查次数")
    private String patrolCount;

    /**
     * 巡查时长
     * 实际巡查时长 / 应巡查时长
     */
    @Excel(name = "巡查时长(H)")
    @ApiModelProperty(value = "巡查时长(H)")
    private String patrolTime;

    /**
     * 巡查里程
     * 实际巡查里程 / 应巡查里程
     */
    @Excel(name = "巡查里程(KM)")
    @ApiModelProperty(value = "巡查里程(KM)")
    private String patrolLength;

    /**
     * 问题完成率
     */
    @Excel(name = "问题完成率")
    @ApiModelProperty(value = "问题完成率")
    private String taskFinishRate;
}
