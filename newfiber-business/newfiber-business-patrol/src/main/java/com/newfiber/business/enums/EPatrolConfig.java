package com.newfiber.business.enums;

import com.newfiber.common.core.exception.ServiceException;

/**
 * @author : Silver
 * @since : 2019-01-11 22:10
 */
public enum EPatrolConfig {

    /**
     * 巡查配置key
     */
    BufferScope("buffer_scope", "缓冲区范围"),
	CoverRate("cover_rate", "覆盖率"),
	PatrolStayRadius("patrol-stay-radius", "巡查停留半径"),

    ;

    public static EPatrolConfig match(String code) {
        for (EPatrolConfig sectionType : EPatrolConfig.values()) {
            if (sectionType.code.equals(code)) {
                return sectionType;
            }
        }
        throw new ServiceException("巡查配置key不匹配");
    }

    EPatrolConfig(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

}
