package com.newfiber.business.controller;

import com.newfiber.business.domain.PatrolSupervise;
import com.newfiber.business.domain.request.patrolSupervise.PatrolSuperviseQueryRequest;
import com.newfiber.business.domain.request.patrolSupervise.PatrolSuperviseSaveRequest;
import com.newfiber.business.domain.request.patrolSupervise.PatrolSuperviseUpdateRequest;
import com.newfiber.business.service.IPatrolSuperviseService;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 巡查督导Controller
 * 
 * @author X.K
 * @date 2023-02-17
 */
@RestController
@RequestMapping("/patrolSupervise")
@Api(value = "PAT06-巡查督导", tags = "PAT06-巡查督导")
public class PatrolSuperviseController extends BaseController {

    @Resource
    private IPatrolSuperviseService patrolSuperviseService;

    /**
     * 新增巡查督导
     */
    @PostMapping
    @ApiOperation(value = "新增巡查督导", position = 10)
    @Log(title = "巡查督导", businessType = BusinessType.INSERT)
    public Result<Long> add(@RequestBody PatrolSuperviseSaveRequest request) {
        return success(patrolSuperviseService.insert(request));
    }

    /**
     * 修改巡查督导
     */
    @PutMapping
    @ApiOperation(value = "修改巡查督导", position = 20)
    @Log(title = "巡查督导", businessType = BusinessType.UPDATE)
    public Result<Object> edit(@RequestBody PatrolSuperviseUpdateRequest request) {
        return success(patrolSuperviseService.update(request));
    }

    /**
     * 删除巡查督导
     */
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除巡查督导", notes = "传入ids(,隔开)", position = 30)
    @Log(title = "巡查督导", businessType = BusinessType.DELETE)
    public Result<Object> remove(@PathVariable String ids) {
        return success(patrolSuperviseService.delete(ids));
    }

    /**
     * 详细查询巡查督导
     */
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "详细查询巡查督导", position = 40)
    public Result<PatrolSupervise> detail(@PathVariable("id") Long id) {
        return success(patrolSuperviseService.selectDetail(id));
    }

    /**
     * 分页查询巡查督导
     */
    @GetMapping("/page")
    @ApiOperation(value = "分页查询巡查督导", position = 50)
    public PageResult<List<PatrolSupervise>> page(PatrolSuperviseQueryRequest request) {
        startPage();
        List<PatrolSupervise> list = patrolSuperviseService.selectPage(request);
        return pageResult(list);
    }

    /**
     * 列表查询巡查督导
     */
    @GetMapping("/list")
    @ApiOperation(value = "列表查询巡查督导", position = 60)
    public Result<List<PatrolSupervise>> list(PatrolSuperviseQueryRequest request) {
        List<PatrolSupervise> list = patrolSuperviseService.selectList(request);
        return success(list);
    }

}
