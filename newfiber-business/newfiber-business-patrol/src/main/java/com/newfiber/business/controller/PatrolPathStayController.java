package com.newfiber.business.controller;

import com.newfiber.business.domain.PatrolPathStay;
import com.newfiber.business.domain.request.patrolPathStay.PatrolPathStayQueryRequest;
import com.newfiber.business.domain.request.patrolPathStay.PatrolPathStaySaveRequest;
import com.newfiber.business.domain.request.patrolPathStay.PatrolPathStayUpdateRequest;
import com.newfiber.business.service.IPatrolPathStayService;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 巡查轨迹停留记录Controller
 * 
 * @author X.K
 * @date 2023-03-08
 */
@RestController
@RequestMapping("/patrolPathStay")
@Api(value = "PAT09-巡查轨迹停留记录", tags = "PAT09-巡查轨迹停留记录")
public class PatrolPathStayController extends BaseController {

    @Resource
    private IPatrolPathStayService patrolPathStayService;

    /**
     * 新增巡查轨迹停留记录
     */
    @PostMapping("add")
    @ApiOperation(value = "新增巡查轨迹停留记录", position = 10)
    @Log(title = "巡查轨迹停留记录", businessType = BusinessType.INSERT)
    public Result<Long> add(@RequestBody PatrolPathStaySaveRequest request) {
        return success(patrolPathStayService.insert(request));
    }

    /**
     * 修改巡查轨迹停留记录
     */
    @PutMapping("edit")
    @ApiOperation(value = "修改巡查轨迹停留记录", position = 20)
    @Log(title = "巡查轨迹停留记录", businessType = BusinessType.UPDATE)
    public Result<Object> edit(@RequestBody PatrolPathStayUpdateRequest request) {
        return success(patrolPathStayService.update(request));
    }

    /**
     * 删除巡查轨迹停留记录
     */
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除巡查轨迹停留记录", notes = "传入ids(,隔开)", position = 30)
    @Log(title = "巡查轨迹停留记录", businessType = BusinessType.DELETE)
    public Result<Object> remove(@PathVariable String ids) {
        return success(patrolPathStayService.delete(ids));
    }

    /**
     * 详细查询巡查轨迹停留记录
     */
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "详细查询巡查轨迹停留记录", position = 40)
    public Result<PatrolPathStay> detail(@PathVariable("id") Long id) {
        return success(patrolPathStayService.selectDetail(id));
    }

    /**
     * 分页查询巡查轨迹停留记录
     */
    @GetMapping("/page")
    @ApiOperation(value = "分页查询巡查轨迹停留记录", position = 50)
    public PageResult<List<PatrolPathStay>> page(PatrolPathStayQueryRequest request) {
        startPage();
        List<PatrolPathStay> list = patrolPathStayService.selectPage(request);
        return pageResult(list);
    }

    /**
     * 列表查询巡查轨迹停留记录
     */
    @GetMapping("/list")
    @ApiOperation(value = "列表查询巡查轨迹停留记录", position = 60)
    public Result<List<PatrolPathStay>> list(PatrolPathStayQueryRequest request) {
        List<PatrolPathStay> list = patrolPathStayService.selectList(request);
        return success(list);
    }

    /**
     * 定时任务，计算巡查轨迹
     * date: 2023/4/19 下午 05:42
     * @author: LuFan
     * @since JDK 1.8
     */
    @GetMapping("/calcPatrolStay")
    @ApiOperation(value = "定时任务，计算巡查轨迹", position = 70)
    public Result calcPatrolStay(){
        patrolPathStayService.calcPatrolStay();
        return success(Boolean.TRUE.toString());
    }

}
