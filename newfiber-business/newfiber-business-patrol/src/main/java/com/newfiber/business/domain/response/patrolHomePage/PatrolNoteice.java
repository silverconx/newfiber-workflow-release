package com.newfiber.business.domain.response.patrolHomePage;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

import static cn.hutool.core.date.DatePattern.NORM_DATE_PATTERN;

/**
 * 首页通知列表
 * date: 2023/3/18 下午 05:12
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
public class PatrolNoteice {

    /**
     * 通知类型（巡查任务)
     */
    private String noticeType;

    /**
     * 通知内容
     */
    @ApiModelProperty(value="通知内容")
    private String noticeContent;

    /**
     * 通知时间
     */
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    @ApiModelProperty(value="通知时间")
    private Date noteTime;

}
