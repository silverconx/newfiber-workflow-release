package com.newfiber.business.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 首页任务日历
 * date: 2023/3/18 下午 03:25
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
public class WorkCal {
    @ApiModelProperty(value = "已完成对应日期集合")
    private List<String> toStartDates;

    @ApiModelProperty(value = "已完成对应日期集合")
    private List<String> expireDates;

    @ApiModelProperty(value = "已完成对应日期集合")
    private List<String> doneDates;
}
