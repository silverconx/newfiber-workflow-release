package com.newfiber.business.service;

import java.util.List;

import com.newfiber.business.domain.PatrolExamine;
import com.newfiber.business.domain.request.patrolExamine.PatrolExamineQueryRequest;
import com.newfiber.business.domain.response.patrolExamine.PatrolExamineQueryResult;

import javax.servlet.http.HttpServletResponse;

/**
 * 巡查考核Service接口
 *
 * @author lufan
 * @date 2023-03-03
 */
public interface IPatrolExamineService {

    /**
     * 批量插入考核数据
     *
     * @param patrolExamine
     * @return 结果
     */
    boolean insertBatch(List<PatrolExamine> patrolExamine);

    /**
     * 详细查询巡查考核
     *
     * @param id 主键
     * @return 巡查考核
     */
    PatrolExamine selectDetail(Long id);

    /**
     * 分页查询巡查考核
     *
     * @param request 分页参数
     * @return 巡查考核集合
     */
    List<PatrolExamineQueryResult> selectPage(PatrolExamineQueryRequest request);

    /**
     * 列表查询巡查考核
     *
     * @param request 列表参数
     * @return 巡查考核集合
     */
    List<PatrolExamineQueryResult> selectList(PatrolExamineQueryRequest request);

    /**
     * date: 2023/3/4 下午 04:54
     *
     * @param  patrolExamineQueryRequest
     * @return 结果
     */
    List<PatrolExamine> queryPatrolExamine(PatrolExamineQueryRequest patrolExamineQueryRequest);

    /**
     * 导出
     *
     * @param response
     * @param patrolExamine
     */
    void export(HttpServletResponse response, PatrolExamineQueryRequest patrolExamine);

    /**
     * date: 2023/4/18 下午 07:21
     * @author: LuFan
     * @since JDK 1.8
     */
    void beginExamine();

}
