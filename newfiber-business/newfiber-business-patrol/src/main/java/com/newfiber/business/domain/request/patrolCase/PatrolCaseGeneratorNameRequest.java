package com.newfiber.business.domain.request.patrolCase;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 * 巡查案件对象 pat_patrol_case
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolCaseGeneratorNameRequest {

	/**
	 * 巡查日志编号
	 */
	@ApiModelProperty(value = "巡查日志编号")
	private String patrolLogId;

	/**
	 * 问题类型名称
	 */
	@ApiModelProperty(value = "问题类型名称")
	private String caseType;

	/**
	 * 问题等级
	 */
	@ApiModelProperty(value = "问题等级")
	private String caseLevel;

	public Long getPatrolLogId() {
		if(StringUtils.isNotBlank(patrolLogId)){
			return Long.parseLong(patrolLogId);
		}
		return null;
	}
}
