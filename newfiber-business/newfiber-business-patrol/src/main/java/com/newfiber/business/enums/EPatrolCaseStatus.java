package com.newfiber.business.enums;

import com.newfiber.common.core.exception.ServiceException;

/**
 * @author : Silver
 * @since : 2019-01-11 22:10
 */
public enum EPatrolCaseStatus {

    /**
     * 案件状态
     */
    Report("report", "上报"),
    StartCase("start_case", "立案"),
    Inspect("inspect", "核查"),
    LeadApprove("lead_approve", "领导审核"),
    Handle("handle", "处理"),
    Verify("verify", "验证"),
    CloseCase("close_case", "结案"),
    End("end", "结束"),

    ;

    public static EPatrolCaseStatus match(String code) {
        for (EPatrolCaseStatus sectionType : EPatrolCaseStatus.values()) {
            if (sectionType.code.equals(code)) {
                return sectionType;
            }
        }
        throw new ServiceException("案件状态不匹配");
    }

    EPatrolCaseStatus(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

}
