package com.newfiber.business.domain.request.patrolCase;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.newfiber.system.api.domain.request.SysFileSaveRequest;
import com.newfiber.workflow.support.request.WorkflowSubmitRequest;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import static cn.hutool.core.date.DatePattern.NORM_DATE_PATTERN;

/**
 * 巡查案件对象 pat_patrol_case
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolCaseSubmitRequest extends WorkflowSubmitRequest {

	/**
	 * id
	 */
	@NotNull
	@ApiModelProperty(value = "id", required = true)
	private Long id;

	/**
	 * 期望完成日期
	 */
	@JsonFormat(pattern = NORM_DATE_PATTERN)
	@DateTimeFormat(pattern = NORM_DATE_PATTERN)
	@ApiModelProperty(value = "期望完成日期")
	private Date expectEndDate;

	/**
	 * 文件
	 */
	@ApiModelProperty(value = "文件")
	private List<SysFileSaveRequest> fileSaveRequestList;
}
