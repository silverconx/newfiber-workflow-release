package com.newfiber.business.controller;

import com.newfiber.business.domain.PatrolSection;
import com.newfiber.business.domain.PatrolUser;
import com.newfiber.business.domain.request.patrolUser.PatrolUserQueryRequest;
import com.newfiber.business.domain.request.patrolUser.PatrolUserSaveRequest;
import com.newfiber.business.domain.request.patrolUser.PatrolUserUpdateRequest;
import com.newfiber.business.enums.EPatrolUserRefType;
import com.newfiber.business.service.IPatrolSectionService;
import com.newfiber.business.service.IPatrolUserService;
import com.newfiber.common.core.enums.EBoolean;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import com.newfiber.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 巡查人员Controller
 *
 * @author X.K
 * @date 2023-02-20
 */
@RestController
@RequestMapping("/patrolUser")
@Api(value = "PAT03-巡查人员", tags = "PAT03-巡查人员")
public class PatrolUserController extends BaseController {

	@Resource
	private IPatrolUserService patrolUserService;

	@Resource
	private IPatrolSectionService patrolSectionService;

	/**
	 * 新增巡查人员
	 */
	@PostMapping
	@RequiresPermissions("businessPatrol:patrolUser:add")
	@ApiOperation(value = "新增巡查人员", position = 10)
	@Log(title = "巡查人员", businessType = BusinessType.INSERT)
	public Result<Boolean> add(@RequestBody PatrolUserSaveRequest request) {
		return success(patrolUserService.insert(request));
	}

	/**
	 * 修改巡查人员
	 */
	@PutMapping
	@RequiresPermissions("businessPatrol:patrolUser:edit")
	@ApiOperation(value = "修改巡查人员", position = 20)
	@Log(title = "巡查人员", businessType = BusinessType.UPDATE)
	public Result<Object> edit(@RequestBody PatrolUserUpdateRequest request) {
		return success(patrolUserService.update(request));
	}

	/**
	 * 删除巡查人员
	 */
	@DeleteMapping("/{ids}")
	@RequiresPermissions("businessPatrol:patrolUser:remove")
	@ApiOperation(value = "删除巡查人员", notes = "传入ids(,隔开)", position = 30)
	@Log(title = "巡查人员", businessType = BusinessType.DELETE)
	public Result<Object> remove(@PathVariable String ids) {
		return success(patrolUserService.delete(ids));
	}

	/**
	 * 详细查询巡查人员
	 */
	@GetMapping(value = "/{id}")
	@ApiOperation(value = "详细查询巡查人员", position = 40)
	public Result<PatrolUser> detail(@PathVariable("id") Long id) {
		return success(patrolUserService.selectDetail(id));
	}

	/**
	 * 分页查询巡查人员
	 */
	@GetMapping("/page")
	@ApiOperation(value = "分页查询巡查人员", position = 50)
	public PageResult<List<PatrolUser>> page(PatrolUserQueryRequest request) {
		startPage();
		List<PatrolUser> list = patrolUserService.selectPage(request);
		return pageResult(list);
	}

	/**
	 * 列表查询巡查人员
	 */
	@GetMapping("/list")
	@ApiOperation(value = "列表查询巡查人员", position = 60)
	public Result<List<PatrolUser>> list(PatrolUserQueryRequest request) {
		List<PatrolUser> list = patrolUserService.selectList(request);
		for(PatrolUser patrolUser : list){
			wrapper(patrolUser, request);
		}
		return success(list);
	}

	private void wrapper(PatrolUser patrolUser, PatrolUserQueryRequest request){

		if(EBoolean.True.getStringValue().equals(request.getParseRefNameFlag())){
			switch (EPatrolUserRefType.match(patrolUser.getRefType())){
				case Section:
					PatrolSection patrolSection = patrolSectionService.selectDetail(patrolUser.getRefId());
					patrolUser.setRefName(patrolSection.getPatrolSectionName());
					break;

				case Task:
					break;

				default: break;
			}

		}
	}
}
