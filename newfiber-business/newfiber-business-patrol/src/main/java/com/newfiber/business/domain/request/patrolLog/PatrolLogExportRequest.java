package com.newfiber.business.domain.request.patrolLog;

import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 巡查日志导出数据
 *
 * @author Administrator
 */
@Data
public class PatrolLogExportRequest {

    private String title;
    private String sectionName;
    private String patrolName;
    private String patrolPersonName;
    private String patrolTime;
    private String patrolLocation;
    private String patrolLength;
    private String patrolDescription;
    private String patrolStatus;

    /**
     * 表格数据
     */
    List<List<String>> tableData;

    /**
     * 图片数据
     */
    List<Map> photos;

}
