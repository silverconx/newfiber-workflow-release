package com.newfiber.business.controller;

import com.newfiber.business.domain.PatrolCase;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseDeprecateRequest;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseGeneratorNameRequest;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseQueryCacheRequest;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseQueryMyRequest;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseQueryRequest;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseSaveRequest;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseSubmitRequest;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseUpdateRequest;
import com.newfiber.business.service.IPatrolCaseService;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import com.newfiber.common.security.annotation.RequiresPermissions;
import com.newfiber.common.security.auth.AuthLogic;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 巡查案件Controller
 *
 * @author X.K
 * @date 2023-02-17
 */
@RestController
@RequestMapping("/patrolCase")
@Api(value = "PAT10-巡查案件", tags = "PAT10-巡查案件")
public class PatrolCaseController extends BaseController {

    @Resource
    private IPatrolCaseService patrolCaseService;

    /**
     * 新增巡查案件
     */
    @PostMapping("add")
    @RequiresPermissions("businessPatrol:patrolCase:add")
    @ApiOperation(value = "新增巡查案件", position = 10)
    @Log(title = "巡查案件", businessType = BusinessType.INSERT)
    public Result<Long> add(@RequestBody PatrolCaseSaveRequest request) {
        AuthLogic authLogic = new AuthLogic();
        String currentUser = authLogic.getLoginUser().getUserid().toString();
	    request.setSubmitUserId(currentUser);
        request.setNextTaskApproveUserId(currentUser);
        long id = patrolCaseService.insert(request);

	    patrolCaseService.submitReport(id, request.getReportFlag(), currentUser);

	    return success(id);
    }

    /**
     * 新增巡查案件缓存（临时日志用）
     */
    @PostMapping("addCache")
    @RequiresPermissions("businessPatrol:patrolCase:add")
    @ApiOperation(value = "新增巡查案件缓存（临时日志用）", position = 10)
    @Log(title = "巡查案件", businessType = BusinessType.INSERT)
    public Result<Boolean> addCache(@RequestBody PatrolCaseSaveRequest request) {
        return success(patrolCaseService.insertCache(request));
    }

    /**
     * 提交巡查案件
     */
    @PutMapping("submit")
    @RequiresPermissions("businessPatrol:patrolCase:edit")
    @ApiOperation(value = "提交巡查案件", position = 20)
    @Log(title = "巡查案件", businessType = BusinessType.UPDATE)
    public Result<String> submit(@RequestBody PatrolCaseSubmitRequest request) {
        return success(patrolCaseService.submit(request));
    }

    /**
     * 作废巡查案件
     */
    @PutMapping("deprecate")
    @RequiresPermissions("businessPatrol:patrolCase:edit")
    @ApiOperation(value = "作废巡查案件", position = 20)
    @Log(title = "巡查案件", businessType = BusinessType.UPDATE)
    public Result<Boolean> deprecate(@RequestBody PatrolCaseDeprecateRequest request) {
        return success(patrolCaseService.deprecate(request));
    }

    /**
     * 删除巡查案件
     */
    @DeleteMapping("/{ids}")
    @RequiresPermissions("businessPatrol:patrolCase:remove")
    @ApiOperation(value = "删除巡查案件", notes = "传入ids(,隔开)", position = 30)
    @Log(title = "巡查案件", businessType = BusinessType.DELETE)
    public Result<Object> remove(@PathVariable String ids) {
        return success(patrolCaseService.delete(ids));
    }

    /**
     * 修改巡查案件
     */
    @PutMapping("edit")
    @RequiresPermissions("businessPatrol:patrolCase:edit")
    @ApiOperation(value = "修改巡查案件", position = 20)
    @Log(title = "巡查案件", businessType = BusinessType.UPDATE)
    public Result<Object> edit(@RequestBody PatrolCaseUpdateRequest request) {
        return success(patrolCaseService.update(request));
    }

    /**
     * 详细查询巡查案件
     */
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "详细查询巡查案件", position = 40)
    public Result<PatrolCase> detail(@PathVariable("id") Long id) {
        return success(patrolCaseService.selectDetail(id));
    }

    /**
     * 分页查询巡查案件
     */
    @GetMapping("/page")
    @ApiOperation(value = "分页查询巡查案件", position = 50)
    public PageResult<List<PatrolCase>> page(PatrolCaseQueryRequest request) {
        startPage();
        List<PatrolCase> list = patrolCaseService.selectPage(request);
        return pageResult(list);
    }

	/**
	 * 生成案件名称
	 */
	@GetMapping("/generatorName")
	@ApiOperation(value = "生成案件名称", position = 50)
	public Result<String> generatorName(PatrolCaseGeneratorNameRequest request) {
		String caseName = patrolCaseService.generatorName(request);
		return success(caseName);
	}

    /**
     * 分页查询我的巡查案件
     */
    @GetMapping("/page_my")
    @ApiOperation(value = "分页查询我的巡查案件", position = 50)
    public PageResult<List<PatrolCase>> pageMy(PatrolCaseQueryMyRequest request) {
        List<PatrolCase> list = patrolCaseService.selectPageMy(request);
        return pageResult(list);
    }

    /**
     * 列表查询与我相关的巡查案件（我提交的或者待我审批的）
     */
    @GetMapping("/myCase")
    @ApiOperation(value = "列表查询与我相关的巡查案件", position = 60)
    public PageResult<List<PatrolCase>> myCase() {
        List<PatrolCase> list = patrolCaseService.myCase();
        return pageResult(list);
    }

    /**
     * 列表查询巡查案件
     */
    @GetMapping("/list")
    @ApiOperation(value = "列表查询巡查案件", position = 70)
    public Result<List<PatrolCase>> list(PatrolCaseQueryRequest request) {
        List<PatrolCase> list = patrolCaseService.selectList(request);
        return success(list);
    }

	/**
	 * 列表查询巡查案件
	 */
	@GetMapping("/listCache")
	@ApiOperation(value = "列表查询巡查案件", position = 70)
	public Result<List<PatrolCase>> listCache(PatrolCaseQueryCacheRequest request) {
		List<PatrolCase> list = patrolCaseService.selectListCache(request);
		return success(list);
	}

}
