package com.newfiber.business.service;

import java.util.List;
import com.newfiber.business.domain.PatrolSupervise;
import com.newfiber.business.domain.request.patrolSupervise.PatrolSuperviseSaveRequest;
import com.newfiber.business.domain.request.patrolSupervise.PatrolSuperviseUpdateRequest;
import com.newfiber.business.domain.request.patrolSupervise.PatrolSuperviseQueryRequest;

/**
 * 巡查督导Service接口
 * 
 * @author X.K
 * @date 2023-02-17
 */
public interface IPatrolSuperviseService {

    /**
     * 新增巡查督导
     *
     * @param request 新增参数
     * @return 结果
     */
    long insert(PatrolSuperviseSaveRequest request);

    /**
     * 修改巡查督导
     *
     * @param request 修改参数
     * @return 结果
     */
    boolean update(PatrolSuperviseUpdateRequest request);

    /**
     * 批量删除巡查督导
     *
     * @param  ids 编号(,隔开)
     * @return 结果
     */
    boolean delete(String ids);

    /**
     * 详细查询巡查督导
     *
     * @param id 主键
     * @return 巡查督导
     */
     PatrolSupervise selectDetail(Long id);

     /**
      * 分页查询巡查督导
      *
      * @param request 分页参数
      * @return 巡查督导集合
      */
     List<PatrolSupervise> selectPage(PatrolSuperviseQueryRequest request);

     /**
      * 列表查询巡查督导
      *
      * @param request 列表参数
      * @return 巡查督导集合
      */
     List<PatrolSupervise> selectList(PatrolSuperviseQueryRequest request);

}
