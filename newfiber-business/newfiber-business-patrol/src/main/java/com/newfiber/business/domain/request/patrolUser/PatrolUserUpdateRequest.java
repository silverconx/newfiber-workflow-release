package com.newfiber.business.domain.request.patrolUser;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * 巡查人员对象 pat_patrol_user
 *
 * @author X.K
 * @date 2023-02-21
 */
@Data
public class PatrolUserUpdateRequest{

	/**
	 * 主键
	 */
	@NotNull
	@ApiModelProperty(value = "主键", required = true)
	private Long id;

	/** 状态 */
	private String status;

	/**
	 * 备注
	 */
	@ApiModelProperty(value = "备注")
	private String remark;
}
