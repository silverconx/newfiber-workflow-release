package com.newfiber.business.config;

import com.newfiber.business.controller.websocket.PatrolPathWebNotifySocket;
import com.newfiber.business.controller.websocket.PatrolPathWebReportSocket;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * @author : X.K
 * @since : 2023/3/6 下午2:18
 */
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		registry.addHandler(reportSocket(), "/patrol_path_report").setAllowedOrigins("*");
		registry.addHandler(notifySocket(), "/patrol_path_notify").setAllowedOrigins("*");
	}

	@Bean
	public PatrolPathWebReportSocket reportSocket(){
		return new PatrolPathWebReportSocket();
	}

	@Bean
	public PatrolPathWebNotifySocket notifySocket(){
		return new PatrolPathWebNotifySocket();
	}

}
