package com.newfiber.business.service;

import com.newfiber.business.domain.PatrolPlan;
import com.newfiber.business.domain.request.patrolPlan.PatrolPlanQueryRequest;
import com.newfiber.business.domain.request.patrolPlan.PatrolPlanSaveRequest;
import com.newfiber.business.domain.request.patrolPlan.PatrolPlanUpdateRequest;

import java.util.List;

/**
 * 巡查计划配置Service接口
 *
 * @author X.K
 * @date 2023-02-17
 */
public interface IPatrolPlanService {

    /**
     * 新增巡查计划配置
     *
     * @param request 新增参数
     * @return 结果
     */
    long insert(PatrolPlanSaveRequest request);

    /**
     * 批量删除巡查计划配置
     *
     * @param ids 编号(,隔开)
     * @return 结果
     */
    boolean delete(String ids);

    /**
     * 修改巡查计划配置
     *
     * @param request 修改参数
     * @return 结果
     */
    boolean update(PatrolPlanUpdateRequest request);

    /**
     * 更新任务数量
     *
     * @param planId 计划id
     * @param field  需要更新的字段
     * @param option 更新数量 加/减
     */
    void updateTaskCount(Long planId, String field, String option);

    /**
     * 停止巡查计划配置
     *
     * @param ids 字符串id
     * @return 结果
     */
    boolean stop(String ids);

    /**
     * 启用巡查计划
     *
     * @param planId planId
     * @return 结果
     */
    boolean startUse(Long planId);

    /**
     * 详细查询巡查计划配置
     *
     * @param id 主键
     * @return 巡查计划配置
     */
    PatrolPlan selectDetail(Long id);

    /**
     * 分页查询巡查计划配置
     *
     * @param request 分页参数
     * @return 巡查计划配置集合
     */
    List<PatrolPlan> selectPage(PatrolPlanQueryRequest request);

    /**
     * 列表查询巡查计划配置
     *
     * @param request 列表参数
     * @return 巡查计划配置集合
     */
    List<PatrolPlan> selectList(PatrolPlanQueryRequest request);

}
