package com.newfiber.business.domain.request.patrolBasicInfo;

import com.newfiber.common.core.web.request.BaseQueryRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 巡查基础信息对象 pat_patrol_basic_info
 *
 * @author X.K
 * @date 2023-02-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PatrolBasicInfoQueryRequest extends BaseQueryRequest {

	/**
	 *  编号
	 */
	@ApiModelProperty(value = "编号")
	private Long id;

    /**
     * 巡查类型（数据字典 patrol_type）
     */
    @ApiModelProperty(value = "巡查类型（数据字典 patrol_type）")
    private String patrolType;

    /**
     * 巡查目标
     */
    @ApiModelProperty(value = "巡查目标")
    private String patrolTarget;

	/**
	 *  负责人来源（内部 inner | 外部 outer）
	 */
	@ApiModelProperty(value = "负责人来源（内部 inner | 外部 outer）")
	private String chargeUserSource;

	/**
	 *  负责人（内部为ID | 外部为名称）
	 */
	@ApiModelProperty(value = "负责人（内部为ID | 外部为名称）")
	private String chargeUser;

}
