package com.newfiber.business.controller;

import com.newfiber.business.domain.PatrolPunch;
import com.newfiber.business.domain.request.patrolPunch.PatrolPunchQueryRequest;
import com.newfiber.business.domain.request.patrolPunch.PatrolPunchSaveRequest;
import com.newfiber.business.domain.request.patrolPunch.PatrolPunchUpdateRequest;
import com.newfiber.business.service.IPatrolPunchService;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.log.annotation.Log;
import com.newfiber.common.log.enums.BusinessType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 巡查打卡Controller
 * 
 * @author X.K
 * @date 2023-02-17
 */
@RestController
@RequestMapping("/patrolPunch")
@Api(value = "PAT08-巡查打卡", tags = "PAT08-巡查打卡")
public class PatrolPunchController extends BaseController {

    @Resource
    private IPatrolPunchService patrolPunchService;

    /**
     * 新增巡查打卡
     */
    @PostMapping
    @ApiOperation(value = "新增巡查打卡", position = 10)
    @Log(title = "巡查打卡", businessType = BusinessType.INSERT)
    public Result<Long> add(@RequestBody PatrolPunchSaveRequest request) {
        return success(patrolPunchService.insert(request));
    }

    /**
     * 修改巡查打卡
     */
    @PutMapping
    @ApiOperation(value = "修改巡查打卡", position = 20)
    @Log(title = "巡查打卡", businessType = BusinessType.UPDATE)
    public Result<Object> edit(@RequestBody PatrolPunchUpdateRequest request) {
        return success(patrolPunchService.update(request));
    }

    /**
     * 删除巡查打卡
     */
    @DeleteMapping("/{ids}")
    @ApiOperation(value = "删除巡查打卡", notes = "传入ids(,隔开)", position = 30)
    @Log(title = "巡查打卡", businessType = BusinessType.DELETE)
    public Result<Object> remove(@PathVariable String ids) {
        return success(patrolPunchService.delete(ids));
    }

    /**
     * 详细查询巡查打卡
     */
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "详细查询巡查打卡", position = 40)
    public Result<PatrolPunch> detail(@PathVariable("id") Long id) {
        return success(patrolPunchService.selectDetail(id));
    }

    /**
     * 分页查询巡查打卡
     */
    @GetMapping("/page")
    @ApiOperation(value = "分页查询巡查打卡", position = 50)
    public PageResult<List<PatrolPunch>> page(PatrolPunchQueryRequest request) {
        startPage();
        List<PatrolPunch> list = patrolPunchService.selectPage(request);
        return pageResult(list);
    }

    /**
     * 列表查询巡查打卡
     */
    @GetMapping("/list")
    @ApiOperation(value = "列表查询巡查打卡", position = 60)
    public Result<List<PatrolPunch>> list(PatrolPunchQueryRequest request) {
        List<PatrolPunch> list = patrolPunchService.selectList(request);
        return success(list);
    }

}
