package com.newfiber.business.domain.request.patrolStatistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : X.K
 * @since : 2023/3/3 下午1:35
 */
@Data
public class PatrolUserStatisticRequest {

	/**
	 * 巡查类型（river | pipeline）
	 */
	@ApiModelProperty(value = "巡查类型（river | pipeline）")
	private String patrolType;

}
