package com.newfiber.business.domain.response.patrolStatistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 问题结案率返回结果
 * date: 2023/3/2 上午 11:53
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
@EqualsAndHashCode
public class CaseFinishRateResult {

    @ApiModelProperty(value = "结案")
    private String finished;

    @ApiModelProperty(value = "未结案")
    private String unfinished;
}
