package com.newfiber.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;

import com.newfiber.business.domain.PatrolLog;
import com.newfiber.business.domain.request.patrolPlan.PatrolPlanQueryRequest;
import com.newfiber.business.domain.PatrolPlan;
import org.apache.ibatis.annotations.Param;

/**
 * 巡查计划配置Mapper接口
 * 
 * @author X.K
 * @date 2023-02-17
 */
public interface PatrolPlanMapper extends BaseMapper<PatrolPlan>{

    /**
     * 更新计划完成任务数量，正在进行任务数量
     *
     * @param planId 计划id
     * @param field 需要更新的字段
     * @param option 更新数量 加/减
     */
    void updateFinishedTaskCount(@Param("planId") Long planId, @Param("field") String field, @Param("option") String option);

    /**
     * 条件查询巡查计划配置列表
     * 
     * @param request 查询条件
     * @return 巡查计划配置集合
     */
    List<PatrolPlan> selectByCondition(@Param("request") PatrolPlanQueryRequest request);

    /**
     * 条件查询巡查计划详情
     *
     * @param id 查询条件
     * @return 巡查计划详情
     */
    PatrolPlan selectOneById(@Param("id") Long id);

}
