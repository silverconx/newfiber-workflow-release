package com.newfiber.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newfiber.business.domain.PatrolCase;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseQueryMyRequest;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseQueryRequest;

import java.util.List;

import com.newfiber.business.domain.request.patrolExamine.PatrolExamineQueryRequest;
import com.newfiber.business.domain.response.patrolExamine.PatrolCaseFinish;
import com.newfiber.business.domain.response.patrolExamine.PatrolCaseSource;
import com.newfiber.business.domain.response.patrolExamine.PatrolCaseUnFinish;
import org.apache.ibatis.annotations.Param;

/**
 * 巡查案件Mapper接口
 *
 * @author X.K
 * @date 2023-02-17
 */
public interface PatrolCaseMapper extends BaseMapper<PatrolCase> {

    /**
     * 删除日志相关的案件数据
     *
     * @param taskId 任务id
     * @return 结果
     */
    int deleteCaseByTaskId(@Param("request") Long taskId);

    /**
     * 条件查询巡查案件列表
     *
     * @param request 查询条件
     * @return 巡查案件集合
     */
    List<PatrolCase> selectByCondition(@Param("request") PatrolCaseQueryRequest request);

    /**
     * 条件查询巡查案件列表
     *
     * @param request 查询条件
     * @return 巡查案件集合
     */
    List<PatrolCase> selectByCondition(@Param("request") PatrolCaseQueryMyRequest request);

    /**
     * 条件查询巡查案件列表
     *
     * @param id 查询条件
     * @return 巡查案件集合
     */
    PatrolCase selectOneById(@Param("id") Long id);

    /**
     * 根据巡查类型查询对应案件主要的案件类型
     *
     * @param patrolType
     * @param patrolExamine
     * @return MainCaseTypeCount
     */
    List<String> caseTypeCount(@Param("patrolType") String patrolType, @Param("request") PatrolExamineQueryRequest patrolExamine);

    /**
     * 根据巡查类型查询对应案件各个问题来源以及对于完成数量
     *
     * @param patrolType
     * @param patrolExamine
     * @return PatrolCaseSource
     */
    List<PatrolCaseSource> caseSourceCount(@Param("patrolType") String patrolType, @Param("request") PatrolExamineQueryRequest patrolExamine);

    /**
     * 统计各个巡查内容对应案件数量以及完成的案件的数量
     *
     * @param patrolType
     * @param patrolExamine
     * @return PatrolCaseSource
     */
    List<PatrolCaseFinish> patrolTargetCaseCount(@Param("patrolType") String patrolType, @Param("request") PatrolExamineQueryRequest patrolExamine);

    /**
     * 统计各个巡查案件类型对应案件数量以及完成的案件的数量
     *
     * @param patrolType
     * @param patrolExamine
     * @return PatrolCaseSource
     */
    List<PatrolCaseUnFinish> patrolTargetUnFinishCaseCount(@Param("patrolType") String patrolType, @Param("request") PatrolExamineQueryRequest patrolExamine);
}
