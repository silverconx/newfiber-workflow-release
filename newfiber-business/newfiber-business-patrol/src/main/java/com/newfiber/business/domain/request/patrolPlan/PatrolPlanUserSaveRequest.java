package com.newfiber.business.domain.request.patrolPlan;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolPlanUserSaveRequest {

	/**
	 * 巡查段编号
	 */
	@NotNull
	@ApiModelProperty(value = "巡查段编号", required = true)
	private Long sectionId;

    /**
     * 用户编号
     */
    @NotNull
    @ApiModelProperty(value = "用户编号", required = true)
    private Long userId;

}
