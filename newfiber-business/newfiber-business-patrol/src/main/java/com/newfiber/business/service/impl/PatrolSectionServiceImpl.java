package com.newfiber.business.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.newfiber.business.domain.PatrolSection;
import com.newfiber.business.domain.request.patrolSection.PatrolSectionLine;
import com.newfiber.business.domain.request.patrolSection.PatrolSectionQueryRequest;
import com.newfiber.business.domain.request.patrolSection.PatrolSectionSaveRequest;
import com.newfiber.business.domain.request.patrolSection.PatrolSectionUpdateRequest;
import com.newfiber.business.enums.EPatrolSectionType;
import com.newfiber.business.enums.EPatrolUserRefType;
import com.newfiber.business.mapper.PatrolSectionMapper;
import com.newfiber.business.service.IPatrolSectionService;
import com.newfiber.business.service.IPatrolTaskService;
import com.newfiber.business.service.IPatrolUserService;
import com.newfiber.common.core.exception.ServiceException;
import com.newfiber.common.core.web.domain.BaseEntity;
import com.newfiber.common.core.web.service.BaseServiceImpl;
import java.util.List;
import java.util.Optional;
import javax.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 巡查段配置Service业务层处理
 * 
 * @author X.K
 * @date 2023-02-17
 */
@Service
public class PatrolSectionServiceImpl extends BaseServiceImpl<PatrolSectionMapper, PatrolSection> implements IPatrolSectionService {

    @Resource
    private PatrolSectionMapper patrolSectionMapper;

    @Resource
    private IPatrolUserService patrolUserService;

    @Resource
    private IPatrolTaskService patrolTaskService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public long insert(PatrolSectionSaveRequest request) {
        PatrolSection patrolSection = new PatrolSection();
        BeanUtils.copyProperties(request, patrolSection);
	    patrolSection.setNumber(generatorNumber("PS", NumberFormat.DateSerialNumber));

	    switch (EPatrolSectionType.match(request.getSectionType())){
		    case Line:
			    patrolSection.setSectionDetail(JSONObject.toJSONString(request.getPatrolSectionLine()));
			    break;
		    default: break;
	    }

        save(patrolSection);

	    patrolUserService.save(EPatrolUserRefType.Section, patrolSection.getId(), request.getPatrolUserSaveBriefRequestList());

        return Optional.of(patrolSection).map(BaseEntity::getId).orElse(0L);
    }

    @Override
    public boolean update(PatrolSectionUpdateRequest request) {
		if(patrolTaskService.countBySection(request.getId()) > 0){
			throw new ServiceException("巡查段已绑定任务，无法修改");
		}

        PatrolSection patrolSection = new PatrolSection();
        BeanUtils.copyProperties(request, patrolSection);

	    switch (EPatrolSectionType.match(request.getSectionType())){
		    case Line:
			    patrolSection.setSectionDetail(JSONObject.toJSONString(request.getPatrolSectionLine()));
			    break;
		    default: break;
	    }

	    return updateById(patrolSection);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(String ids) {
	    return deleteLogic(ids);
    }

	@Override
	public long countByBasicInfo(Long basicInfoId) {
		QueryWrapper<PatrolSection> queryWrapper = new QueryWrapper<PatrolSection>().eq("basic_info_id", basicInfoId);
		return count(queryWrapper);
	}

	@Override
    public PatrolSection selectDetail(Long id) {
        PatrolSection patrolSection = patrolSectionMapper.selectOneById(id);
        if(null == patrolSection){
	        throw new ServiceException(String.format("%s ID=%s 的记录不存在", this.getClass().getSimpleName(), id));
        }
        return wrapper(patrolSection);
    }

    @Override
    public List<PatrolSection> selectPage(PatrolSectionQueryRequest request) {
	    List<PatrolSection> patrolSectionList = patrolSectionMapper.selectByCondition(request);
	    for(PatrolSection patrolSection : patrolSectionList){
	    	wrapper(patrolSection);
	    }
	    return patrolSectionList;
    }

    @Override
    public List<PatrolSection> selectList(PatrolSectionQueryRequest request) {
        return patrolSectionMapper.selectByCondition(request);
    }

    private PatrolSection wrapper(PatrolSection patrolSection){
	    switch (EPatrolSectionType.match(patrolSection.getSectionType())){
		    case Line:
			    patrolSection.setPatrolSectionLine(JSONObject.parseObject(patrolSection.getSectionDetail(), PatrolSectionLine.class));
			    break;
		    default: break;
	    }
	    return patrolSection;
    }
}
