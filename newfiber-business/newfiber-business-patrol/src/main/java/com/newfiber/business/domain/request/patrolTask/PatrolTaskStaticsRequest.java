package com.newfiber.business.domain.request.patrolTask;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 巡查任务对象 pat_patrol_task
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolTaskStaticsRequest {

    /**
     * 巡查类型（数据字典 patrol_type）
     */
    @ApiModelProperty(value = "巡查类型（数据字典 patrol_type）")
    private String patrolType;

    /**
     * 巡查人
     */
    @ApiModelProperty(value = "巡查人")
    private String taskUserId;

	/**
	 * 任务名称
	 */
	@ApiModelProperty(value = "任务名称")
	private String taskName;

}
