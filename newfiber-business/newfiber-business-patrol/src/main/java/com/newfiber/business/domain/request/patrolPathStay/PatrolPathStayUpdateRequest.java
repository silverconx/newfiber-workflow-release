package com.newfiber.business.domain.request.patrolPathStay;

import static cn.hutool.core.date.DatePattern.NORM_DATETIME_PATTERN;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 巡查轨迹停留记录对象 pat_patrol_path_stay
 *
 * @author X.K
 * @date 2023-03-08
 */
@Data
public class PatrolPathStayUpdateRequest{

    /**
     * 主键
     */
    @NotNull
    @ApiModelProperty(value = "主键", required = true)
    private Long id;

    /**
     * 关联类型
     */
    @ApiModelProperty(value = "关联类型")
    private String refType;

    /**
     * 关联编号
     */
    @ApiModelProperty(value = "关联编号")
    private Long refId;

    /**
     * 停留经纬度
     */
    @ApiModelProperty(value = "停留经纬度")
    private String stayLonLat;

    /**
     * 停留时间
     */
    @ApiModelProperty(value = "停留时间")
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    private Date stayDatetime;

    /**
     * 停留时长
     */
    @ApiModelProperty(value = "停留时长")
    private Long stayDuration;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    private String status;
}
