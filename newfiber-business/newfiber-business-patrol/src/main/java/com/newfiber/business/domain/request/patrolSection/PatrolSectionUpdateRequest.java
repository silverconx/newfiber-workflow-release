package com.newfiber.business.domain.request.patrolSection;

import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * 巡查段配置对象 pat_patrol_section
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolSectionUpdateRequest{

    /**
     * 主键
     */
    @NotNull
    @ApiModelProperty(value = "主键", required = true)
    private Long id;

    /**
     * 巡查类型（数据字典 patrol_type）
     */
    @ApiModelProperty(value = "巡查类型（数据字典 patrol_type）")
    private String patrolType;

    /**
     * 巡查段类型（点 point | 线 line | 面 area）
     */
    @ApiModelProperty(value = "巡查段类型（点 point | 线 line | 面 area）")
    private String sectionType;

    /**
     * 巡查段详情（JSON）
     */
    @ApiModelProperty(value = "巡查段详情（JSON）")
    private String sectionDetail;

    /**
     * 覆盖率
     */
    @ApiModelProperty(value = "覆盖率")
    private BigDecimal coverRate;

    /**
     * 缓冲区范围
     */
    @ApiModelProperty(value = "缓冲区范围")
    private BigDecimal bufferScope;

    /**
     * 巡查时长
     */
    @ApiModelProperty(value = "巡查时长")
    private BigDecimal patrolDuration;

    /**
     * 巡查区域信息
     */
    @ApiModelProperty(value = "巡查区域信息")
    private String geometry = null;

	/**
	 * 巡查缓冲区域信息
	 */
	@ApiModelProperty(value = "巡查缓冲区域信息")
	private String geometryBuffer = null;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

	/**
	 * 巡查线类型详情
	 */
	@ApiModelProperty(value = "巡查线类型详情")
	private PatrolSectionLine patrolSectionLine;

}
