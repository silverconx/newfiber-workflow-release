package com.newfiber.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newfiber.business.domain.PatrolExamine;
import com.newfiber.business.domain.request.patrolExamine.PatrolExamineQueryRequest;
import com.newfiber.business.domain.response.patrolExamine.PatrolExamineQueryResult;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 巡查考核Mapper接口
 *
 * @author lufan
 * @date 2023-03-03
 */
public interface PatrolExamineMapper extends BaseMapper<PatrolExamine> {

    /**
     * 条件查询巡查考核列表
     *
     * @param request 查询条件
     * @return 巡查考核集合
     */
    List<PatrolExamineQueryResult> selectByCondition(@Param("request") PatrolExamineQueryRequest request);

    /**
     * 定时任务扫描数据
     *
     * @param patrolExamineQueryRequest
     * @return PatrolExamine
     */
    List<PatrolExamine> queryPatrolExamine(@Param("request") PatrolExamineQueryRequest patrolExamineQueryRequest);

}
