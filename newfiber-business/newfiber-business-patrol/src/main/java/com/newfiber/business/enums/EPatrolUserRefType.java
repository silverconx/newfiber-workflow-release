package com.newfiber.business.enums;

import com.newfiber.common.core.exception.ServiceException;

/**
 * @author : Silver
 * @since : 2019-01-11 22:10
 */
public enum EPatrolUserRefType {

	/**
	 * 关联类型（巡查段 section | 巡查任务 task）
	 */
	Section("section", "巡查段"),

	Task("task", "巡查任务"),

	;

	public static EPatrolUserRefType match(String code){
		for(EPatrolUserRefType sectionType : EPatrolUserRefType.values()){
			if(sectionType.code.equals(code)){
				return sectionType;
			}
		}
		throw new ServiceException("巡查人员关联类型不匹配");
	}

    EPatrolUserRefType(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

}
