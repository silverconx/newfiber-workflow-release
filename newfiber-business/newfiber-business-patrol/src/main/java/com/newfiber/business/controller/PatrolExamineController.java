package com.newfiber.business.controller;

import com.newfiber.business.domain.PatrolExamine;
import com.newfiber.business.domain.request.patrolExamine.PatrolExamineQueryRequest;
import com.newfiber.business.domain.response.patrolExamine.PatrolExamineQueryResult;
import com.newfiber.business.service.IPatrolExamineService;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import com.newfiber.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.*;

/**
 * 巡查考核Controller
 *
 * @author lufan
 * @date 2023-03-03
 */
@RestController
@RequestMapping("/patrolExamine")
@Api(value = "PAT12-巡查考核", tags = "PAT12-巡查考核")
public class PatrolExamineController extends BaseController {

    @Resource
    private IPatrolExamineService patPatrolExamineService;

    /**
     * 批量插入巡查考核
     */
    @PostMapping("/insertBatch")
    @ApiOperation(value = "批量插入巡查考核", position = 10)
    public Result<Boolean> insertBatch(@RequestBody List<PatrolExamine> patrolExamine) {
        return success(patPatrolExamineService.insertBatch(patrolExamine));
    }

    /**
     * 详细查询巡查考核
     */
    @GetMapping(value = "/{id}")
    @ApiOperation(value = "详细查询巡查考核", position = 20)
    public Result<PatrolExamine> detail(@PathVariable("id") Long id) {
        return success(patPatrolExamineService.selectDetail(id));
    }

    /**
     * 分页查询巡查考核
     */
    @GetMapping("/page")
    @RequiresPermissions("businessPatrol:examine:page")
    @ApiOperation(value = "分页查询巡查考核", position = 30)
    public PageResult<List<PatrolExamineQueryResult>> page(PatrolExamineQueryRequest request) {
        startPage();
        List<PatrolExamineQueryResult> list = patPatrolExamineService.selectPage(request);
        return pageResult(list);
    }

    /**
     * 列表查询巡查考核
     */
    @GetMapping("/list")
    @RequiresPermissions("businessPatrol:examine:list")
    @ApiOperation(value = "列表查询巡查考核", position = 40)
    public Result<List<PatrolExamineQueryResult>> list(PatrolExamineQueryRequest request) {
        List<PatrolExamineQueryResult> list = patPatrolExamineService.selectList(request);
        return success(list);
    }

    /**
     * 导出巡查考核数据
     */
    @PostMapping("/export")
    @RequiresPermissions("businessPatrol:examine:export")
    @ApiOperation(value = "导出巡查考核数据", position = 50)
    public void export(HttpServletResponse response, PatrolExamineQueryRequest patrolExamine) {
        patPatrolExamineService.export(response, patrolExamine);
    }

    /**
     * 查询移动巡查考核
     */
    @PostMapping("/queryPatrolExamine")
    @RequiresPermissions("businessPatrol:examine:list")
    @ApiOperation(value = "查询移动巡查考核", position = 60)
    public Result<List<PatrolExamine>> queryPatrolExamine(PatrolExamineQueryRequest patrolExamine) {
        List<PatrolExamine> result = patPatrolExamineService.queryPatrolExamine(patrolExamine);
        return success(result);
    }

    /**
     * 巡查考核，每周一，每月1号，每年1月1日 统计一次
     * 定时任务执行频率 每天凌晨0点
     * <p>
     * date: 2023/3/6 下午 02:28
     *
     * @author: LuFan
     * @since JDK 1.8
     */
    @GetMapping("beginExamine")
    @ApiOperation(value = "查询移动巡查考核", position = 70)
    public Result beginExamine(){
        patPatrolExamineService.beginExamine();
        return success(Boolean.TRUE.toString());
    }

}
