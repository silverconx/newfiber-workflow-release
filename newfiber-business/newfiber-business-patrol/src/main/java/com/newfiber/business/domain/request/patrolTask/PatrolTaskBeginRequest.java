package com.newfiber.business.domain.request.patrolTask;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolTaskBeginRequest {

    /**
     * 巡查任务编号
     */
    @ApiModelProperty(value = "巡查任务编号")
    private String taskId;

	/**
	 * 巡查起点
	 */
	@ApiModelProperty(value = "巡查起点")
	private String startPlace;

	public Long getTaskId() {
		if(StringUtils.isNotBlank(taskId)){
			return Long.parseLong(taskId);
		}
		return null;
	}
}
