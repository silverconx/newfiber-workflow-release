package com.newfiber.business.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.newfiber.business.domain.request.patrolPlan.PatrolPlanFixDate;
import com.newfiber.business.domain.request.patrolPlan.PatrolPlanFixFrequency;
import com.newfiber.business.domain.request.patrolPlan.PatrolPlanIntervalDate;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 巡查计划配置对象 pat_patrol_plan
 * 
 * @author X.K
 * @date 2023-02-17
 */
@Data
@TableName("pat_patrol_plan")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "巡查计划配置", description = "巡查计划配置")
public class PatrolPlan extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /**
     *  编号
     */
    @ApiModelProperty(value = "编号")
    private String number;

    /**
     *  巡查类型（数据字典 patrol_type）
     */
    @ApiModelProperty(value = "巡查类型（数据字典 patrol_type）")
    private String patrolType;

    /**
     *  巡查计划名称
     */
    @ApiModelProperty(value = "巡查计划名称")
    private String planName;

    /**
     *  巡查基础信息编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "巡查基础信息编号")
    private Long basicInfoId;

    /**
     *  巡查段编号（全部：*  |  多段用,隔开）
     */
    @ApiModelProperty(value = "巡查段编号（全部：*  |  多段用,隔开）")
    private String patrolSectionIds;

    /**
     *  开始时间
     */
    @ApiModelProperty(value = "开始时间")
    private Date startDate;

    /**
     *  结束时间
     */
    @ApiModelProperty(value = "结束时间")
    private Date endDate;

    /**
     *  是否平均分配（0 否 | 1 是）
     */
    @ApiModelProperty(value = "是否平均分配（0 否 | 1 是）")
    private String averageDistributeFlag;

    /**
     *  频率类型（固定频率 fix_frequency | 固定日期 fix_date | 间隔日期 interval_date）
     */
    @ApiModelProperty(value = "频率类型（固定频率 fix_frequency | 固定日期 fix_date | 间隔日期 interval_date）")
    private String patrolFrequencyType;

    /**
     *  频率内容
     */
    @ApiModelProperty(value = "频率内容")
    private String patrolFrequencyContent;

    /**
     *  任务总数
     */
    @ApiModelProperty(value = "任务总数")
    private Long taskCount;

    /**
     *  任务完成数
     */
    @ApiModelProperty(value = "任务完成数")
    private Long taskDoneCount;

    /**
     *  执行中任务数
     */
    @ApiModelProperty(value = "执行中任务数")
    private Long taskProceedCount;

    /**
     *  任务完成率
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "任务完成率")
    private String taskFinishRate;

    // DB Property

	/**
	 * 固定日期
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "固定日期")
	private PatrolPlanFixDate patrolPlanFixDate;

	/**
	 * 固定频率
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "固定频率")
	private PatrolPlanFixFrequency patrolPlanFixFrequency;

	/**
	 * 间隔日期
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "间隔日期")
	private PatrolPlanIntervalDate patrolPlanIntervalDate;

    /**
     * 巡查目标
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查目标")
    private String patrolTarget;

    /**
     * 巡查段类型（点 point | 线 line | 面 area）
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查段类型（点 point | 线 line | 面 area）")
    private String sectionType;

    /**
     * 巡查段详情（JSON）
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查段详情（JSON）")
    private String sectionDetail;

    /**
     * 巡查段名称
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查段名称")
    private String patrolSectionName;

    @TableField(exist = false)
    @ApiModelProperty(value = "制定人")
    private String createByName;

    public String getPatrolSectionName() {
        return PatrolSection.parsePatrolSectionName(patrolTarget, sectionType, sectionDetail);
    }
}
