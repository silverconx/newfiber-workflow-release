package com.newfiber.business.domain.request.patrolUser;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * 巡查人员对象 pat_patrol_user
 *
 * @author X.K
 * @date 2023-02-21
 */
@Data
public class PatrolUserSaveRequest{

	/**
	 * 关联类型（巡查段 section | 巡查任务 task）
	 */
	@NotBlank
	@ApiModelProperty(value = "关联类型（巡查段 section | 巡查任务 task）", required = true)
	private String refType;

	/**
	 * 关联编号
	 */
	@NotNull
	@ApiModelProperty(value = "关联编号", required = true)
	private Long refId;

	/**
	 * 用户类型（巡查人员 patrol | 立案人员 register | 核查人员 approve | 处理人员 handle | 验证人员 verify | 结案人员 close）
	 */
	@ApiModelProperty(value = "用户类型（巡查人员 patrol | 立案人员 register | 核查人员 approve | 处理人员 handle | 验证人员 verify | 结案人员 close）")
	private String userType;

	/**
	 * 用户编号
	 */
	@ApiModelProperty(value = "用户编号")
	private Long userId;

	/**
	 * 用户编号
	 */
	@ApiModelProperty(value = "用户编号")
	private String userIds;

	/**
	 * 备注
	 */
	@ApiModelProperty(value = "备注")
	private String remark;

}
