package com.newfiber.business.domain.request.patrolPlan;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : X.K
 * @since : 2023/2/20 上午10:34
 */
@Data
public class PatrolPlanFixDate {

	@ApiModelProperty(value = "时间表达式")
	private String cron;

}
