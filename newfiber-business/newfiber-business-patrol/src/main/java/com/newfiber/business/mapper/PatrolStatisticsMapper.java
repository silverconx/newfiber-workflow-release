package com.newfiber.business.mapper;

import com.newfiber.business.domain.response.patrolStatistics.*;
import com.newfiber.business.domain.request.patrolStatistics.PatrolStatisticsQueryRequest;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 巡查统计
 * <p>
 * date: 2023/3/1 下午 02:30
 *
 * @author: LuFan
 * @since JDK 1.8
 */
public interface PatrolStatisticsMapper {

    /**
     * 巡查出勤率统计
     *
     * @param request 通用统计查询入参
     * @return PatrolAttendanceRate
     */
    PatrolAttendanceRate attendanceRate(@Param("request") PatrolStatisticsQueryRequest request);

    /**
     * 优秀巡查人统计
     *
     * @param request 通用统计查询入参
     * @return PatrolAttendanceRate
     */
    List<PatrolPerson> patrolPerson(@Param("request") PatrolStatisticsQueryRequest request);

    /**
     * 统计案件来源
     *
     * @param caseSource caseSource
     * @param request    通用统计查询入参
     * @return PatrolStatisticsCaseResult
     */
    List<Integer> queryCaseSource(@Param("caseSource") String caseSource, @Param("request") PatrolStatisticsQueryRequest request);


    /**
     * 巡查问题统计
     *
     * @param request   caseTypes
     * @param caseTypes 通用统计查询入参
     * @return PatrolStatisticsCaseResult
     */
    List<Integer> queryCaseType(@Param("caseTypes") String caseTypes, @Param("request") PatrolStatisticsQueryRequest request);

    /**
     * 问题类型统计
     *
     * @param request 通用统计查询入参
     * @return CountResult
     */
    List<CaseCountResult> caseType(@Param("request") PatrolStatisticsQueryRequest request);

    /**
     * 问题结案率统计
     *
     * @param request 通用统计查询入参
     * @return CountResult
     */
    CaseFinishRateResult caseFinishRate(@Param("request") PatrolStatisticsQueryRequest request);

    /**
     * 来源
     * 问题来源总数统计，不区分巡查内容
     *
     * @param request 通用统计查询入参
     * @return CaseCountResult
     */
    List<CaseCountResult> caseSourceTotal(@Param("request") PatrolStatisticsQueryRequest request);
}
