package com.newfiber.business.service;

import com.newfiber.business.domain.PatrolPunch;
import com.newfiber.business.domain.request.patrolPunch.PatrolPunchQueryRequest;
import com.newfiber.business.domain.request.patrolPunch.PatrolPunchSaveRequest;
import com.newfiber.business.domain.request.patrolPunch.PatrolPunchUpdateRequest;
import java.util.List;

/**
 * 巡查打卡Service接口
 * 
 * @author X.K
 * @date 2023-02-17
 */
public interface IPatrolPunchService {

    /**
     * 新增巡查打卡
     *
     * @param request 新增参数
     * @return 结果
     */
    long insert(PatrolPunchSaveRequest request);

    /**
     * 修改巡查打卡
     *
     * @param request 修改参数
     * @return 结果
     */
    boolean update(PatrolPunchUpdateRequest request);

    /**
     * 批量删除巡查打卡
     *
     * @param  ids 编号(,隔开)
     * @return 结果
     */
    boolean delete(String ids);

	/**
	 *
	 */
	boolean deleteByTaskId(Long patrolTaskId);

	/**
	 *
	 */
	boolean deleteByLogId(Long patrolLogId);

	/**
     * 详细查询巡查打卡
     *
     * @param id 主键
     * @return 巡查打卡
     */
     PatrolPunch selectDetail(Long id);

     /**
      * 分页查询巡查打卡
      *
      * @param request 分页参数
      * @return 巡查打卡集合
      */
     List<PatrolPunch> selectPage(PatrolPunchQueryRequest request);

     /**
      * 列表查询巡查打卡
      *
      * @param request 列表参数
      * @return 巡查打卡集合
      */
     List<PatrolPunch> selectList(PatrolPunchQueryRequest request);

}
