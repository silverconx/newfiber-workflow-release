package com.newfiber.business.domain.request.patrolTask;

import static cn.hutool.core.date.DatePattern.NORM_DATE_PATTERN;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.newfiber.business.domain.request.patrolPlan.PatrolPlanFixDate;
import com.newfiber.business.domain.request.patrolPlan.PatrolPlanFixFrequency;
import com.newfiber.business.domain.request.patrolPlan.PatrolPlanIntervalDate;
import com.newfiber.business.domain.request.patrolPlan.PatrolPlanUserSaveRequest;
import com.newfiber.business.enums.EPatrolPlanFrequencyType;
import io.swagger.annotations.ApiModelProperty;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 巡查任务对象 pat_patrol_task
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolTaskGeneratorDraftRequest {

	/**
	 * 巡查基础信息编号
	 */
	@NotNull
	@ApiModelProperty(value = "巡查基础信息编号", required = true)
	private Long basicInfoId;

	/**
	 * 巡查段编号（全部：*  |  多段用,隔开）
	 */
	@NotBlank
	@ApiModelProperty(value = "巡查段编号（全部：*  |  多段用,隔开）", required = true)
	private String patrolSectionIds;

	/**
	 * 开始时间
	 */
	@NotNull
	@JsonFormat(pattern = NORM_DATE_PATTERN)
	@DateTimeFormat(pattern = NORM_DATE_PATTERN)
	@ApiModelProperty(value = "开始时间", required = true)
	private Date startDate;

	/**
	 * 结束时间
	 */
	@NotNull
	@JsonFormat(pattern = NORM_DATE_PATTERN)
	@DateTimeFormat(pattern = NORM_DATE_PATTERN)
	@ApiModelProperty(value = "结束时间", required = true)
	private Date endDate;

	/**
	 * 是否平均分配（0 否 | 1 是）
	 */
	@NotBlank
	@ApiModelProperty(value = "是否平均分配（0 否 | 1 是）", required = true)
	private String averageDistributeFlag;

	/**
	 * 频率类型（固定频率 fix_frequency | 固定日期 fix_date | 间隔日期 interval_date）
	 */
	@NotBlank
	@ApiModelProperty(value = "频率类型（固定频率 fix_frequency | 固定日期 fix_date | 间隔日期 interval_date）", required = true)
	private String patrolFrequencyType;

	/**
	 * 巡查人员
	 */
	@NotEmpty
	@ApiModelProperty(value = "巡查人员", required = true)
	private List<PatrolPlanUserSaveRequest> patrolUserList;

	/**
	 * 固定日期
	 */
	@ApiModelProperty(value = "固定日期")
	private PatrolPlanFixDate patrolPlanFixDate;

	/**
	 * 固定频率
	 */
	@ApiModelProperty(value = "固定频率")
	private PatrolPlanFixFrequency patrolPlanFixFrequency;

	/**
	 * 状态（0停用 | 1启用）
	 */
	@ApiModelProperty(value = "状态（0停用 | 1启用）")
	private String status;

	/**
	 * 间隔日期
	 */
	@ApiModelProperty(value = "间隔日期")
	private PatrolPlanIntervalDate patrolPlanIntervalDate;

	public String parseStartTime(EPatrolPlanFrequencyType frequencyType){
		switch (frequencyType){
			case IntervalDate:
				return StringUtils.isBlank(patrolPlanIntervalDate.getStartTime()) ? "00:00:00" : patrolPlanIntervalDate.getStartTime();

			case FixFrequency:
				return StringUtils.isBlank(patrolPlanFixFrequency.getStartTime()) ? "00:00:00" : patrolPlanFixFrequency.getStartTime();

			default: return "00:00:00";
		}
	}

	public String parseEndTime(EPatrolPlanFrequencyType frequencyType){
		switch (frequencyType){
			case IntervalDate:
				return StringUtils.isBlank(patrolPlanIntervalDate.getEndTime()) ? "23:59:59" : patrolPlanIntervalDate.getEndTime();

			case FixFrequency:
				return StringUtils.isBlank(patrolPlanFixFrequency.getEndTime()) ? "23:59:59" : patrolPlanFixFrequency.getEndTime();

			default: return "23:59:59";
		}
	}

	public List<String> parsePatrolSectionIdList(){
		if(StringUtils.isBlank(patrolSectionIds)){
			return Collections.emptyList();
		}

		return Arrays.asList(patrolSectionIds.split(","));
	}
}
