package com.newfiber.business.enums;

import com.newfiber.common.core.exception.ServiceException;

/**
 * @author : Silver
 * @since : 2019-01-11 22:10
 */
public enum EPatrolPlanFixFrequencyType {

	/**
	 * 周期类型（日 day | 周 week | 月 month）
	 */
	Day("day", "日"),

	Week("week", "周"),

	Month("month", "月"),

	;

	public static EPatrolPlanFixFrequencyType match(String code){
		for(EPatrolPlanFixFrequencyType sectionType : EPatrolPlanFixFrequencyType.values()){
			if(sectionType.code.equals(code)){
				return sectionType;
			}
		}
		throw new ServiceException("固定频率巡查周期不匹配");
	}

    EPatrolPlanFixFrequencyType(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

}
