package com.newfiber.business.domain.request.patrolPunch;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * 巡查打卡对象 pat_patrol_punch
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolPunchUpdateRequest{

    /**
     * 主键
     */
    @NotNull
    @ApiModelProperty(value = "主键", required = true)
    private Long id;

    /**
     * 巡查日志编号
     */
    @NotNull
    @ApiModelProperty(value = "巡查日志编号", required = true)
    private Long patrolLogId;

    /**
     * 地址
     */
    @NotBlank
    @ApiModelProperty(value = "地址", required = true)
    private String address;

    /**
     * 经纬度
     */
    @NotBlank
    @ApiModelProperty(value = "经纬度", required = true)
    private String lonLat;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;
}
