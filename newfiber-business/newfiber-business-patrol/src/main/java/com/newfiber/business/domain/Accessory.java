package com.newfiber.business.domain;

import lombok.Data;

/**
 * 资源
 *
 * date: 2023/3/10 下午 04:55
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
public class Accessory {
    private String url;
}
