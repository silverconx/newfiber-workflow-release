package com.newfiber.business.enums;

import com.newfiber.common.core.exception.ServiceException;

/**
 * 案件数据来源
 * date: 2023/3/27 下午 02:15
 * @author: LuFan
 * @since JDK 1.8
 */
public enum EPatrolCaseDataSource {
    /**
     * 案件来源
     */
    Task("task", "巡查任务录入的案件"),
    Log("log", "巡查日志录入的案件"),
    Case("case", "单独录入的案件"),
    ;

    public static EPatrolCaseDataSource match(String code) {
        for (EPatrolCaseDataSource sectionType : EPatrolCaseDataSource.values()) {
            if (sectionType.code.equals(code)) {
                return sectionType;
            }
        }
        throw new ServiceException("案件来源不匹配");
    }

    EPatrolCaseDataSource(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
