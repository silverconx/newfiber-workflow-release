package com.newfiber.business.domain.request.patrolLog;

import cn.hutool.json.JSONUtil;
import com.newfiber.business.domain.Accessory;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import javafx.beans.property.SetProperty;
import lombok.Data;
import nonapi.io.github.classgraph.json.JSONUtils;

/**
 * 巡查日志对象 pat_patrol_log
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolLogSaveRequest {

    /**
     * 巡查任务id
     */
    @NotNull
    @ApiModelProperty(value = "巡查任务id", required = true)
    private Long patrolTaskId;

    /**
     * 巡查名称
     */
    @NotBlank
    @ApiModelProperty(value = "巡查名称", required = true)
    private String patrolName;

    /**
     * 巡查长度
     */
    @ApiModelProperty(value = "巡查长度")
    private BigDecimal patrolLength;

    /**
     * 巡查起点
     */
    @ApiModelProperty(value = "巡查起点")
    private String startPlace;

    /**
     * 巡查终点
     */
    @ApiModelProperty(value = "巡查终点")
    private String endPlace;

    /**
     * 巡查内容/巡查总结
     */
    @ApiModelProperty(value = "巡查内容")
    private String logContent;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 图片
     */
    @ApiModelProperty(value = "图片")
    private String picture;

    /**
     * 图片，前端传过来的数组
     */
    @ApiModelProperty(value = "图片-数组")
    private List<Accessory> pictureListData;

    /**
     * 视频
     */
    @ApiModelProperty(value = "视频")
    private String video;

    /**
     * 音频
     */
    @ApiModelProperty(value = "音频")
    private String audio;


}
