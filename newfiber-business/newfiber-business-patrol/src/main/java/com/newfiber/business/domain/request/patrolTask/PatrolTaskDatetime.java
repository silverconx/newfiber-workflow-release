package com.newfiber.business.domain.request.patrolTask;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author : X.K
 * @since : 2023/2/21 上午10:45
 */
@Data
@AllArgsConstructor
public class PatrolTaskDatetime {
	private Date startDatetime;
	private Date endDatetime;
	private int count = 1;

	public PatrolTaskDatetime(Date startDatetime, Date endDatetime) {
		this.startDatetime = startDatetime;
		this.endDatetime = endDatetime;
	}
}
