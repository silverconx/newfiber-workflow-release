package com.newfiber.business.domain.request.patrolPlan;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : X.K
 * @since : 2023/2/20 上午10:34
 */
@Data
public class PatrolPlanIntervalDate {

	/**
	 * 巡查间隔
	 */
	@ApiModelProperty(value = "巡查间隔")
	private int interval;

	/**
	 * 巡查频率（次）
	 */
	@ApiModelProperty(value = "巡查频率（次）")
	private int frequency;

	/**
	 * 开始时间
	 */
	@ApiModelProperty(value = "开始时间")
	private String startTime;

	/**
	 * 结束时间
	 */
	@ApiModelProperty(value = "结束时间")
	private String endTime;

}
