package com.newfiber.business.domain;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.newfiber.business.domain.request.patrolSection.PatrolSectionLine;
import com.newfiber.business.enums.EPatrolSectionType;
import com.newfiber.common.core.geometry.GeometryTypeHandler;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.StringUtils;

/**
 * 巡查段配置对象 pat_patrol_section
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
@TableName(value = "pat_patrol_section", autoResultMap = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "巡查段配置", description = "巡查段配置")
public class PatrolSection extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号")
    private String number;

    /**
     * 巡查类型（数据字典 patrol_type）
     */
    @ApiModelProperty(value = "巡查类型（数据字典 patrol_type）")
    private String patrolType;

    /**
     * 巡查基础信息id
     */
    @ApiModelProperty(value = "巡查基础信息id")
    private String basicInfoId;

    /**
     * 巡查段类型（点 point | 线 line | 面 area）
     */
    @ApiModelProperty(value = "巡查段类型（点 point | 线 line | 面 area）")
    private String sectionType;

    /**
     * 巡查段详情（JSON）
     */
    @ApiModelProperty(value = "巡查段详情（JSON）")
    private String sectionDetail;

    /**
     * 覆盖率
     */
    @ApiModelProperty(value = "覆盖率")
    private BigDecimal coverRate;

    /**
     * 缓冲区范围
     */
    @ApiModelProperty(value = "缓冲区范围")
    private BigDecimal bufferScope;

    /**
     * 巡查时长
     */
    @ApiModelProperty(value = "巡查时长")
    private BigDecimal patrolDuration;

    /**
     * 巡查区域信息
     */
    @ApiModelProperty(value = "巡查区域信息")
    @TableField(value = "geometry", typeHandler = GeometryTypeHandler.class, updateStrategy = FieldStrategy.IGNORED)
    private String geometry = null;

	/**
	 * 巡查缓冲区域信息
	 */
	@ApiModelProperty(value = "巡查缓冲区域信息")
	@TableField(value = "geometry_buffer", typeHandler = GeometryTypeHandler.class, updateStrategy = FieldStrategy.IGNORED)
	private String geometryBuffer = null;


    // DB Property

    /**
     * 巡查线类型详情
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查线类型详情")
    private PatrolSectionLine patrolSectionLine;

    /**
     * 巡查目标
     * 巡查内容名称
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查目标")
    private String patrolTarget;

	/**
	 * 巡查段名称
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "巡查段名称")
    private String patrolSectionName;

	public static String parsePatrolSectionName(String patrolTarget, String sectionType, String sectionDetail){
		String patrolSectionName = patrolTarget;

		if(StringUtils.isNotBlank(sectionType) && StringUtils.isNotBlank(sectionDetail)){
			switch (EPatrolSectionType.match(sectionType)){
				case Line:
					PatrolSectionLine patrolSectionLine = JSONObject.parseObject(sectionDetail, PatrolSectionLine.class);
					patrolSectionName = patrolSectionName + String.format("(%s-%s)", patrolSectionLine.getStartPlace(), patrolSectionLine.getEndPlace());
					break;

				default: break;
			}
		}

		return patrolSectionName;
	}

	public String getPatrolSectionName() {
		return parsePatrolSectionName(patrolTarget, sectionType, sectionDetail);
	}
}
