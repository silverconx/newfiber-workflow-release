package com.newfiber.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newfiber.business.domain.PatrolBasicInfo;
import com.newfiber.business.domain.request.patrolBasicInfo.PatrolBasicInfoQueryRequest;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 巡查基础信息Mapper接口
 *
 * @author X.K
 * @date 2023-02-10
 */
public interface PatrolBasicInfoMapper extends BaseMapper<PatrolBasicInfo>{

    /**
     * 条件查询巡查基础信息列表
     *
     * @param request 查询条件
     * @return 巡查基础信息集合
     */
    List<PatrolBasicInfo> selectByCondition(@Param("request") PatrolBasicInfoQueryRequest request);

}
