package com.newfiber.business.domain.request.patrolSection;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 巡查段配置对象 pat_patrol_section
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolSectionLine {

    /**
     * 起点
     */
    @ApiModelProperty(value = "起点")
    private String startPlace;

    /**
     * 起点（全称）
     */
    @ApiModelProperty(value = "起点（全称）")
    private String startPlaceFull;

    /**
     * 终点
     */
    @ApiModelProperty(value = "终点")
    private String endPlace;

    /**
     * 终点（全称）
     */
    @ApiModelProperty(value = "终点（全称）")
    private String endPlaceFull;

    /**
     * 长度
     */
    @ApiModelProperty(value = "长度")
    private String length;

}
