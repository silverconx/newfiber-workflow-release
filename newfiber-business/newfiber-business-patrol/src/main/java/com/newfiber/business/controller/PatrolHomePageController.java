package com.newfiber.business.controller;

import com.newfiber.business.domain.WorkCal;
import com.newfiber.business.domain.response.patrolHomePage.PatrolHomePageCount;
import com.newfiber.business.service.IPatrolTaskService;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 首页/工作台Controller
 *
 * @author lufan
 * @date 2023-03-16
 */
@RestController
@RequestMapping("/patrolHomepage")
@Api(value = "PAT00-首页", tags = "PAT00-首页")
public class PatrolHomePageController extends BaseController {

    @Resource
    private IPatrolTaskService patrolTaskService;

    /**
     * 当日，本周，本月巡查统计
     */
    @GetMapping("/patrolCount")
    @ApiOperation(value = "当日，本周，本月巡查统计", position = 10)
    public Result<List<PatrolHomePageCount>> patrolCount() {
        List<PatrolHomePageCount> list = patrolTaskService.patrolCount();
        return success(list);
    }

    /**
     * 工作日历
     */
    @GetMapping("/workCal")
    @ApiOperation(value = "工作日历", position = 20)
    public Result<WorkCal> workCal(Date startDate, Date endDate) {
        WorkCal workCal = patrolTaskService.workCal(startDate, endDate);
        return success(workCal);
    }
}
