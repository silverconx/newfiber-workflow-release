package com.newfiber.business.domain.request.patrolCase;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 巡查案件对象 pat_patrol_case
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolCaseQueryCacheRequest{

    /**
     * 巡查日志编号
     */
    @ApiModelProperty(value = "巡查日志编号")
    private Long patrolLogId;
}
