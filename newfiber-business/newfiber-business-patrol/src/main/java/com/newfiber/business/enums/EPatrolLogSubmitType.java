package com.newfiber.business.enums;

import com.newfiber.common.core.exception.ServiceException;

/**
 * date: 2023/2/23 下午 05:44
 * @author: LuFan
 * @since JDK 1.8
 */
public enum EPatrolLogSubmitType {
    /**
     * 提交类型（正常 normal | 系统 system）
     */
    Normal("normal","正常"),

    System("system","系统"),
    ;

    public static EPatrolLogSubmitType match(String code){
        for(EPatrolLogSubmitType ePatrolLogSubmitType : EPatrolLogSubmitType.values()){
            if(ePatrolLogSubmitType.code.equals(code)){
                return ePatrolLogSubmitType;
            }
        }
        throw new ServiceException("日志提交类型不匹配");
    }

    EPatrolLogSubmitType(String code, String value) {
        this.code = code;
        this.value = value;
    }
    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }
}
