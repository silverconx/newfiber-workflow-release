package com.newfiber.business.enums;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.newfiber.business.service.impl.PatrolCaseServiceImpl;
import com.newfiber.workflow.support.IWorkflowDefinition;
import lombok.Getter;

@Getter
public enum EWorkflowDefinition implements IWorkflowDefinition {

	/**
	 *
	 */
    PatrolCase("PatrolCase", "巡查案件", "t", "id", Long.class, PatrolCaseServiceImpl.class),
;

    EWorkflowDefinition(String workflowKey, String workflowName, String tableAlias, String tablePrimary, Class<?> tableIdType, Class<? extends ServiceImpl<?, ?>> businessService) {
        this.workflowKey = workflowKey;
        this.workflowName = workflowName;
        this.tableAlias = tableAlias;
        this.tablePrimary = tablePrimary;
        this.tableIdType = tableIdType;
        this.businessService = businessService;
    }

    private final String workflowKey;

    private final String workflowName;

    private final String tableAlias;

    private final String tablePrimary;

    private final Class<?> tableIdType;

    private final Class<? extends ServiceImpl<?, ?>> businessService;
}
