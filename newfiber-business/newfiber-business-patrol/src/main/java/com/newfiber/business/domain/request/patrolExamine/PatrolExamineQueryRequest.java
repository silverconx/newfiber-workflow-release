package com.newfiber.business.domain.request.patrolExamine;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import static cn.hutool.core.date.DatePattern.NORM_DATE_PATTERN;

/**
 * 巡查考核筛选时间段
 *
 * @author lufan
 * @date 2023-03-03
 */
@Data
public class PatrolExamineQueryRequest {

    /**
     * 开始日期
     */
    @ApiModelProperty(value = "开始日期")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date startDate;

    /**
     * 结束日期
     */
    @ApiModelProperty(value = "结束日期")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date endDate;

}
