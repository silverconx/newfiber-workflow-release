package com.newfiber.business.enums;

import com.newfiber.common.core.exception.ServiceException;

/**
 * @author : Silver
 * @since : 2019-01-11 22:10
 */
public enum EPatrolTaskSource {

	/**
	 * 任务来源（巡查 patrol | 报警 warning）
	 */
	Patrol("patrol", "巡查"),

	Warning("warning", "报警"),

	;

	public static EPatrolTaskSource match(String code){
		for(EPatrolTaskSource sectionType : EPatrolTaskSource.values()){
			if(sectionType.code.equals(code)){
				return sectionType;
			}
		}
		throw new ServiceException("巡查任务来源不匹配");
	}

    EPatrolTaskSource(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

}
