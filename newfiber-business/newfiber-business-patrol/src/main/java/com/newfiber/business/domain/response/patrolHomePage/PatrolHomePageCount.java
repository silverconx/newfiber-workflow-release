package com.newfiber.business.domain.response.patrolHomePage;

import lombok.Data;

/**
 * 首页通用统计返回数据模型
 * date: 2023/3/16 下午 07:37
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
public class PatrolHomePageCount {
    /**
     * 统计项目
     */
    private String item;

    /**
     * 应完成
     */
    private int shouldDone;

    /**
     * 已完成
     */
    private int done;

    /**
     * 完成率
     */
    private String rate;
}
