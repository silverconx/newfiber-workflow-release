package com.newfiber.business.domain.request.patrolBasicInfo;

import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 * 巡查基础信息对象 pat_patrol_basic_info
 *
 * @author X.K
 * @date 2023-02-10
 */
@Data
public class PatrolBasicInfoUpdateRequest{

    /**
     * 主键
     */
    @NotNull
    @ApiModelProperty(value = "主键", required = true)
    private Long id;

    /**
     * 巡查类型（数据字典 patrol_type）
     */
    @NotBlank
    @ApiModelProperty(value = "巡查类型（数据字典 patrol_type）")
    private String patrolType;

    /**
     * 巡查目标
     */
    @NotBlank
    @ApiModelProperty(value = "巡查目标")
    private String patrolTarget;

    /**
     * 巡查长度
     */
    @NotBlank
    @ApiModelProperty(value = "巡查长度")
    private String patrolTargetLength;

	/**
	 *  负责人来源（内部 inner | 外部 outer）
	 */
	@ApiModelProperty(value = "负责人来源（内部 inner | 外部 outer）")
	private String chargeUserSource;

	/**
	 *  负责人（内部为ID | 外部为名称）
	 */
	@ApiModelProperty(value = "负责人（内部为ID | 外部为名称）")
	private String chargeUser;

	/**
	 *  外部负责人手机号
	 */
	@ApiModelProperty(value = "外部负责人手机号")
	private String chargeUserMobile;

    /**
     * 备注
     */
    @NotBlank
    @ApiModelProperty(value = "备注")
    private String remark;

    /**
     * 排序
     */
    @NotNull
    @ApiModelProperty(value = "排序")
    private Long sort;

}
