package com.newfiber.business.domain.response.patrolStatistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 巡查出勤率统计返回结果
 * date: 2023/3/2 上午 11:10
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
@EqualsAndHashCode
public class PatrolAttendanceRate {

    @ApiModelProperty(value = "异常")
    private int abnormal;

    @ApiModelProperty(value = "正常")
    private int normal;
}
