package com.newfiber.business.enums;

import com.newfiber.common.core.exception.ServiceException;

/**
 * @author : Silver
 * @since : 2019-01-11 22:10
 */
public enum EPatrolPlanFrequencyType {

	/**
	 * 频率类型（固定频率 fix_frequency | 固定日期 fix_date | 间隔日期 interval_date）
	 */
	FixFrequency("fix_frequency", "固定频率"),

	FixDate("fix_date", "固定日期"),

	IntervalDate("interval_date", "间隔日期"),

	;

	public static EPatrolPlanFrequencyType match(String code){
		for(EPatrolPlanFrequencyType sectionType : EPatrolPlanFrequencyType.values()){
			if(sectionType.code.equals(code)){
				return sectionType;
			}
		}
		throw new ServiceException("巡查计划频率不匹配");
	}

    EPatrolPlanFrequencyType(String code, String value) {
        this.code = code;
        this.value = value;
    }

    private final String code;

    private final String value;

    public String getCode() {
        return code;
    }

    public String getValue() {
        return value;
    }

}
