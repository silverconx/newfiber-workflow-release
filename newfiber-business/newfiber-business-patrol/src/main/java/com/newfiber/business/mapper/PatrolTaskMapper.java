package com.newfiber.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newfiber.business.domain.PatrolTask;
import com.newfiber.business.domain.request.patrolExamine.PatrolExamineQueryRequest;
import com.newfiber.business.domain.request.patrolTask.PatrolTaskQueryRequest;
import com.newfiber.business.domain.request.patrolTask.PatrolTaskStaticsRequest;
import com.newfiber.business.domain.response.patrolExamine.PatrolExamineExport;
import com.newfiber.business.domain.response.patrolHomePage.PatrolHomePageCount;
import com.newfiber.business.domain.response.patrolTask.PatrolTaskStatusStatistics;
import java.util.List;

import org.apache.ibatis.annotations.Param;

/**
 * 巡查任务Mapper接口
 *
 * @author X.K
 * @date 2023-02-17
 */
public interface PatrolTaskMapper extends BaseMapper<PatrolTask> {

    /**
     * 条件查询巡查任务列表
     *
     * @param request 查询条件
     * @return 巡查任务集合
     */
    List<PatrolTask> selectByCondition(@Param("request") PatrolTaskQueryRequest request);

	/**
	 * 状态统计
	 */
	List<PatrolTaskStatusStatistics> statusStatistics(@Param("request") PatrolTaskStaticsRequest request);

	/**
	 * 首页/工作台统计
	 *
	 * @return PatrolTask
	 */
	List<PatrolHomePageCount> patrolCount();

	/**
	 * 查询任务详情
	 *
	 * @param id
	 * @return PatrolTask
	 */
	PatrolTask selectOneById(Long id);

	/**
	 * 巡查考核统计巡查任务
	 *
	 * @param request
	 * @return PatrolExamineExport
	 */
	PatrolExamineExport examineCount(@Param("request") PatrolExamineQueryRequest request);

}
