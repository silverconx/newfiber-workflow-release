package com.newfiber.business.domain.request.patrolPlan;

import static cn.hutool.core.date.DatePattern.NORM_DATE_PATTERN;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.newfiber.business.domain.PatrolTask;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 巡查计划配置对象 pat_patrol_plan
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolPlanSaveRequest{

    /**
     * 巡查计划名称
     */
    @NotBlank
    @ApiModelProperty(value = "巡查计划名称", required = true)
    private String planName;

    /**
     * 巡查基础信息编号
     */
    @NotNull
    @ApiModelProperty(value = "巡查基础信息编号", required = true)
    private Long basicInfoId;

    /**
     * 巡查段编号（全部：*  |  多段用,隔开）
     */
    @NotBlank
    @ApiModelProperty(value = "巡查段编号（全部：*  |  多段用,隔开）", required = true)
    private String patrolSectionIds;

    /**
     * 开始时间
     */
    @NotNull
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    @ApiModelProperty(value = "开始时间", required = true)
    private Date startDate;

    /**
     * 结束时间
     */
    @NotNull
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    @ApiModelProperty(value = "结束时间", required = true)
    private Date endDate;

    /**
     * 是否平均分配（0 否 | 1 是）
     */
    @NotBlank
    @ApiModelProperty(value = "是否平均分配（0 否 | 1 是）", required = true)
    private String averageDistributeFlag;

    /**
     * 频率类型（固定频率 fix_frequency | 固定日期 fix_date | 间隔日期 interval_date）
     */
    @NotBlank
    @ApiModelProperty(value = "频率类型（固定频率 fix_frequency | 固定日期 fix_date | 间隔日期 interval_date）", required = true)
    private String patrolFrequencyType;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

	/**
	 * 状态（0停用 | 1启用）
	 */
	@ApiModelProperty(value = "状态（0停用 | 1启用）")
	private String status;

	/**
	 * 巡查人员
	 */
	@NotEmpty
	@ApiModelProperty(value = "巡查人员", required = true)
	private List<PatrolPlanUserSaveRequest> patrolUserList;

	/**
	 * 固定日期
	 */
	@ApiModelProperty(value = "固定日期")
	private PatrolPlanFixDate patrolPlanFixDate;

	/**
	 * 固定频率
	 */
	@ApiModelProperty(value = "固定频率")
	private PatrolPlanFixFrequency patrolPlanFixFrequency;

	/**
	 * 间隔日期
	 */
	@ApiModelProperty(value = "间隔日期")
	private PatrolPlanIntervalDate patrolPlanIntervalDate;

	/**
	 * 巡查计划
	 */
	@ApiModelProperty(value = "巡查计划")
	private List<PatrolTask> patrolTaskList;

}
