package com.newfiber.business.domain.request.patrolPathStay;

import com.newfiber.common.core.web.request.BaseQueryRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 巡查轨迹停留记录对象 pat_patrol_path_stay
 *
 * @author X.K
 * @date 2023-03-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PatrolPathStayQueryRequest extends BaseQueryRequest {

    /**
     * 关联类型
     */
    @ApiModelProperty(value = "关联类型")
    private String refType;

    /**
     * 关联编号
     */
    @ApiModelProperty(value = "关联编号")
    private Long refId;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    private String status;

	/**
	 *  关联人员
	 */
	@ApiModelProperty(value = "关联人员")
	private Long userId;

}
