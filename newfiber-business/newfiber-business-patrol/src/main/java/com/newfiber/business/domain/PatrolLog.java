package com.newfiber.business.domain;

import static cn.hutool.core.date.DatePattern.NORM_DATETIME_PATTERN;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.newfiber.common.core.annotation.Excel;
import com.newfiber.common.core.geometry.GeometryLngLat;
import com.newfiber.common.core.geometry.GeometryTypeHandler;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 巡查日志对象 pat_patrol_log
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
@TableName(value = "pat_patrol_log", autoResultMap = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "巡查日志", description = "巡查日志")
public class PatrolLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @ApiModelProperty(value = "编号")
    private String number;

    /**
     * 巡查类型（数据字典 patrol_type）
     */
    @ApiModelProperty(value = "巡查类型（数据字典 patrol_type）")
    private String patrolType;

    /**
     * 巡查任务编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "巡查任务编号")
    private Long patrolTaskId;

    /**
     * 巡查段编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "巡查段编号")
    private Long patrolSectionId;

    /**
     * 日志巡查人
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "日志巡查人")
    private Long logUserId;

    /**
     * 创建者
     * nick_name
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "创建者")
    private String createByName;


    /**
     * 提交类型（正常 normal | 系统 system）
     */
    @ApiModelProperty(value = "提交类型（正常 normal | 系统 system）")
    private String submitType;

    /**
     * 巡查名称
     */
    @ApiModelProperty(value = "巡查名称")
    private String patrolName;

    /**
     * 巡查长度
     */
    @ApiModelProperty(value = "巡查长度")
    private BigDecimal patrolLength;

    /**
     * 巡查起点
     */
    @ApiModelProperty(value = "巡查起点")
    private String startPlace;

    /**
     * 巡查终点
     */
    @ApiModelProperty(value = "巡查终点")
    private String endPlace;

    /**
     * 巡查开始时间
     */
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    @ApiModelProperty(value = "开始时间")
    private Date startDatetime;

    /**
     * 巡查结束时间
     */
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    @ApiModelProperty(value = "结束时间")
    private Date endDatetime;

    /**
     * 巡查内容/巡查总结
     */
    @ApiModelProperty(value = "巡查内容/巡查总结")
    private String logContent;

    /**
     * 案件数量
     */
    @ApiModelProperty(value = "案件数量/巡查问题数")
    private Long caseCount;

    /**
     * 案件已处理数量/巡查已处理问题数
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "案件已处理数量/巡查已处理问题数")
    private Long caseFinishedCount;

    /**
     * 巡查轨迹
     */
    @ApiModelProperty(value = "巡查轨迹")
    @TableField(value = "patrol_path", typeHandler = GeometryTypeHandler.class, updateStrategy = FieldStrategy.IGNORED)
    private String patrolPath = null;

    /**
     * 配置覆盖率
     */
    @ApiModelProperty(value = "配置覆盖率")
    private Double coverRate;

    /**
     * 实际覆盖率
     */
    @ApiModelProperty(value = "实际覆盖率")
    private Double realCoverRate;

    /**
     * 图片
     */
    @ApiModelProperty(value = "图片")
    private String picture;

    /**
     * 图片，前端传过来的数组
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "图片-数组")
    private List<Accessory> pictureListData;

    public List<Accessory> getPictureListData() {
        return JSONObject.parseArray(picture, Accessory.class);
    }

    /**
     * 视频
     */
    @ApiModelProperty(value = "视频")
    private String video;

    /**
     * 音频
     */
    @ApiModelProperty(value = "音频")
    private String audio;

    // DB Property

    /**
     * 巡查目标
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查目标")
    private String patrolTarget;

    /**
     * 巡查段类型（点 point | 线 line | 面 area）
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查段类型（点 point | 线 line | 面 area）")
    private String sectionType;

    /**
     * 巡查段详情（JSON）
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查段详情（JSON）")
    private String sectionDetail;

    /**
     * 巡查段名称
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查段名称")
    private String patrolSectionName;

    /**
     * 巡查人
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查人")
    private String taskUserName;

    /**
     * 巡查人手机号
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "巡查人手机号")
    private String taskUserPhone;

	/**
	 * 巡查轨迹
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "巡查轨迹")
	private List<GeometryLngLat> patrolPathLngLat;

    /**
     * 巡查缓冲区域信息
     */
    @ApiModelProperty(value = "巡查缓冲区域信息")
    @TableField(exist = false, value = "geometry_buffer", typeHandler = GeometryTypeHandler.class, updateStrategy = FieldStrategy.IGNORED)
    private String geometryBuffer = null;

    public String getPatrolSectionName() {
        return PatrolSection.parsePatrolSectionName(patrolTarget, sectionType, sectionDetail);
    }

}
