package com.newfiber.business.service;

import com.newfiber.business.domain.PatrolPathStay;
import com.newfiber.business.domain.request.patrolPathStay.PatrolPathStayQueryRequest;
import com.newfiber.business.domain.request.patrolPathStay.PatrolPathStaySaveRequest;
import com.newfiber.business.domain.request.patrolPathStay.PatrolPathStayUpdateRequest;
import com.newfiber.business.enums.EPatrolPathRefType;
import java.util.Date;
import java.util.List;

/**
 * 巡查轨迹停留记录Service接口
 * 
 * @author X.K
 * @date 2023-03-08
 */
public interface IPatrolPathStayService {

    /**
     * 新增巡查轨迹停留记录
     *
     * @param request 新增参数
     * @return 结果
     */
    long insert(PatrolPathStaySaveRequest request);

	/**
	 * 新增巡查轨迹停留记录
	 */
	long insert(EPatrolPathRefType refType, Long refId, Long userId, String stayLonLat, Date stayDatetime, Double stayDuration);

    /**
     * 修改巡查轨迹停留记录
     *
     * @param request 修改参数
     * @return 结果
     */
    boolean update(PatrolPathStayUpdateRequest request);

    /**
     * 批量删除巡查轨迹停留记录
     *
     * @param  ids 编号(,隔开)
     * @return 结果
     */
    boolean delete(String ids);

	/**
	 * 巡查人员总数量
	 */
	long patrolStayUserDistinctCount();

    /**
     * 详细查询巡查轨迹停留记录
     *
     * @param id 主键
     * @return 巡查轨迹停留记录
     */
     PatrolPathStay selectDetail(Long id);

     /**
      * 分页查询巡查轨迹停留记录
      *
      * @param request 分页参数
      * @return 巡查轨迹停留记录集合
      */
     List<PatrolPathStay> selectPage(PatrolPathStayQueryRequest request);

     /**
      * 列表查询巡查轨迹停留记录
      *
      * @param request 列表参数
      * @return 巡查轨迹停留记录集合
      */
     List<PatrolPathStay> selectList(PatrolPathStayQueryRequest request);

     /**
      * 定时任务，计算巡查轨迹
      * date: 2023/4/19 下午 07:52
      * @author: LuFan
      * @since JDK 1.8
      */
     void calcPatrolStay();
}
