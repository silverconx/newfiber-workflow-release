package com.newfiber.business.domain.response.patrolExamine;

import lombok.Data;

/**
 * 巡查考核统计各个巡查内容对于案件数量以及完成案件数量
 * date: 2023/3/24 下午 05:18
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
public class PatrolCaseFinish {

    /**
     * 巡查内容名称
     */
    private String patrolTarget;

    /**
     * 巡查内容对应案件数量
     */
    private int caseCount;

    /**
     * 巡查内容对应完成案件数量
     */
    private int caseCountDone;
}
