package com.newfiber.business.domain.request.patrolTask;

import static cn.hutool.core.date.DatePattern.NORM_DATETIME_PATTERN;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 巡查任务对象 pat_patrol_task
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolTaskUpdateRequest{

    /**
     * 主键
     */
    @NotNull
    @ApiModelProperty(value = "主键", required = true)
    private Long id;

    @ApiModelProperty(value = "任务id")
    private String taskId;

    /**
     * 巡查段编号
     */
    @NotNull
    @ApiModelProperty(value = "巡查段编号", required = true)
    private Long patrolSectionId;

    /**
     * 巡查计划编号
     */
    @NotNull
    @ApiModelProperty(value = "巡查计划编号", required = true)
    private Long planId;

    /**
     * 任务名称
     */
    @NotBlank
    @ApiModelProperty(value = "任务名称", required = true)
    private String taskName;

    /**
     * 巡查人
     */
    @NotNull
    @ApiModelProperty(value = "巡查人", required = true)
    private Long taskUserId;

    /**
     * 计划开始时间
     */
    @NotNull
    @ApiModelProperty(value = "计划开始时间", required = true)
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    private Date planStartDatetime;

    /**
     * 计划应结束时间
     */
    @NotNull
    @ApiModelProperty(value = "计划应结束时间", required = true)
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    private Date planEndDatetime;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;
}
