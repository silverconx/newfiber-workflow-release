package com.newfiber.business.domain.request.patrolTask;

import com.newfiber.system.api.domain.request.SysFileSaveRequest;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.List;
import lombok.Data;

/**
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolTaskFinishRequest {

    /**
     * 巡查任务编号
     */
    @ApiModelProperty(value = "巡查任务编号")
    private String patrolTaskId;

	/**
	 * 巡查日志编号
	 */
	@ApiModelProperty(value = "巡查日志编号")
	private String patrolLogId;

    /**
     * 巡查名称
     */
    @ApiModelProperty(value = "巡查名称")
    private String patrolName;

    /**
     * 巡查内容/巡查总结
     */
    @ApiModelProperty(value = "巡查内容/巡查总结")
    private String logContent;

	/**
	 * 巡查终点
	 */
	@ApiModelProperty(value = "巡查终点")
	private String endPlace;

	/**
	 * 巡查长度
	 */
	@ApiModelProperty(value = "巡查长度")
	private BigDecimal patrolLength;

	/**
	 * 文件
	 */
	@ApiModelProperty(value = "文件")
	private List<SysFileSaveRequest> fileSaveRequestList;

	public Long getPatrolTaskId() {
		return Long.parseLong(patrolTaskId);
	}

	public Long getPatrolLogId() {
		return Long.parseLong(patrolLogId);
	}
}
