package com.newfiber.business.service.impl;

import com.newfiber.business.domain.PatrolSupervise;
import com.newfiber.business.domain.request.patrolSupervise.PatrolSuperviseQueryRequest;
import com.newfiber.business.domain.request.patrolSupervise.PatrolSuperviseSaveRequest;
import com.newfiber.business.domain.request.patrolSupervise.PatrolSuperviseUpdateRequest;
import com.newfiber.business.mapper.PatrolSuperviseMapper;
import com.newfiber.business.service.IPatrolSuperviseService;
import com.newfiber.common.core.exception.ServiceException;
import com.newfiber.common.core.web.domain.BaseEntity;
import com.newfiber.common.core.web.service.BaseServiceImpl;
import java.util.List;
import java.util.Optional;
import javax.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 巡查督导Service业务层处理
 * 
 * @author X.K
 * @date 2023-02-17
 */
@Service
public class PatrolSuperviseServiceImpl extends BaseServiceImpl<PatrolSuperviseMapper, PatrolSupervise> implements IPatrolSuperviseService {

    @Resource
    private PatrolSuperviseMapper patrolSuperviseMapper;

    @Override
    public long insert(PatrolSuperviseSaveRequest request) {
        PatrolSupervise patrolSupervise = new PatrolSupervise();
        BeanUtils.copyProperties(request, patrolSupervise);
        save(patrolSupervise);
        return Optional.of(patrolSupervise).map(BaseEntity::getId).orElse(0L);
    }

    @Override
    public boolean update(PatrolSuperviseUpdateRequest request) {
        PatrolSupervise patrolSupervise = new PatrolSupervise();
        BeanUtils.copyProperties(request, patrolSupervise);
        return updateById(patrolSupervise);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(String ids) {
	    return deleteLogic(ids);
    }

    @Override
    public PatrolSupervise selectDetail(Long id) {
        PatrolSupervise patrolSupervise = patrolSuperviseMapper.selectById(id);
        if(null == patrolSupervise){
	        throw new ServiceException(String.format("%s ID=%s 的记录不存在", this.getClass().getSimpleName(), id));
        }
        return patrolSupervise;
    }

    @Override
    public List<PatrolSupervise> selectPage(PatrolSuperviseQueryRequest request) {
        return patrolSuperviseMapper.selectByCondition(request);
    }

    @Override
    public List<PatrolSupervise> selectList(PatrolSuperviseQueryRequest request) {
        return patrolSuperviseMapper.selectByCondition(request);
    }

}
