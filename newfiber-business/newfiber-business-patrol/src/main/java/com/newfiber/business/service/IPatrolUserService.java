package com.newfiber.business.service;

import com.newfiber.business.domain.PatrolUser;
import com.newfiber.business.domain.request.patrolUser.PatrolUserQueryRequest;
import com.newfiber.business.domain.request.patrolUser.PatrolUserSaveBriefRequest;
import com.newfiber.business.domain.request.patrolUser.PatrolUserSaveRequest;
import com.newfiber.business.domain.request.patrolUser.PatrolUserUpdateRequest;
import com.newfiber.business.enums.EPatrolUserRefType;
import com.newfiber.business.enums.EPatrolUserType;
import java.util.List;

/**
 * 巡查人员Service接口
 *
 * @author X.K
 * @date 2023-02-17
 */
public interface IPatrolUserService {

	/**
	 * 保存
	 *
	 * @param refType 巡查段类型
	 * @param refId 巡查id
	 * @param patrolUserSaveBriefRequestList
	 * @return 结果
	 */
	boolean save(EPatrolUserRefType refType, Long refId, List<PatrolUserSaveBriefRequest> patrolUserSaveBriefRequestList);

	/**
	 * 保存
	 *
	 * @param refType 巡查段类型
	 * @param refId 巡查id
	 * @param userType 用户类型
	 * @param userIdList 用户id
	 * @return 结果
	 */
	boolean save(EPatrolUserRefType refType, Long refId, EPatrolUserType userType, List<Long> userIdList);

	/**
	 * 新增巡查人员
	 *
	 * @param request 新增参数
	 * @return 结果
	 */
	boolean insert(PatrolUserSaveRequest request);

	/**
	 * 修改巡查人员
	 *
	 * @param request 修改参数
	 * @return 结果
	 */
	boolean update(PatrolUserUpdateRequest request);

	/**
	 * 批量删除巡查人员
	 *
	 * @param  ids 编号(,隔开)
	 * @return 结果
	 */
	boolean delete(String ids);

	/**
	 * 巡查人员总数量
	 */
	long patrolUserDistinctCount();

	/**
	 * 巡查人员总数量
	 */
	long patrolUserDistinctCount(String patrolType);

	/**
	 * 详细查询巡查人员
	 *
	 * @param id 主键
	 * @return 巡查人员
	 */
	PatrolUser selectDetail(Long id);

	/**
      * 分页查询巡查人员
      *
      * @param request 分页参数
      * @return 巡查人员集合
      */
     List<PatrolUser> selectPage(PatrolUserQueryRequest request);

     /**
      * 列表查询巡查人员
      *
      * @param request 列表参数
      * @return 巡查人员集合
      */
     List<PatrolUser> selectList(PatrolUserQueryRequest request);

}
