package com.newfiber.business.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 巡查打卡对象 pat_patrol_punch
 * 
 * @author X.K
 * @date 2023-02-17
 */
@Data
@TableName("pat_patrol_punch")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "巡查打卡", description = "巡查打卡")
public class PatrolPunch extends BaseEntity{

    private static final long serialVersionUID = 1L;

    /**
     *  巡查任务编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "巡查任务编号")
    private Long patrolTaskId;

    /**
     *  巡查日志编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "巡查日志编号")
    private Long patrolLogId;

    /**
     *  地址
     */
    @ApiModelProperty(value = "地址")
    private String address;

    /**
     *  经纬度
     */
    @ApiModelProperty(value = "经纬度")
    private String lonLat;

    /**
     *  创建人姓名
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "创建人姓名")
    private String createByName;
}
