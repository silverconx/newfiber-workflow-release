package com.newfiber.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;

import com.newfiber.business.domain.PatrolLog;
import com.newfiber.business.domain.request.patrolPunch.PatrolPunchQueryRequest;
import com.newfiber.business.domain.PatrolPunch;
import org.apache.ibatis.annotations.Param;

/**
 * 巡查打卡Mapper接口
 * 
 * @author X.K
 * @date 2023-02-17
 */
public interface PatrolPunchMapper extends BaseMapper<PatrolPunch>{

    /**
     * 条件查询巡查打卡列表
     * 
     * @param request 查询条件
     * @return 巡查打卡集合
     */
    List<PatrolPunch> selectByCondition(@Param("request") PatrolPunchQueryRequest request);

    /**
     * 条件查询巡查打卡详情
     *
     * @param id 查询条件
     * @return 打卡详情
     */
    PatrolPunch selectOneById(@Param("id") Long id);

}
