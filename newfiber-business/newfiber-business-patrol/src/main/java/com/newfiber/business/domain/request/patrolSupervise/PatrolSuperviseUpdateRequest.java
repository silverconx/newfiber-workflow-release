package com.newfiber.business.domain.request.patrolSupervise;

import static cn.hutool.core.date.DatePattern.NORM_DATE_PATTERN;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 巡查督导对象 pat_patrol_supervise
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolSuperviseUpdateRequest{

    /**
     * 主键
     */
    @NotNull
    @ApiModelProperty(value = "主键", required = true)
    private Long id;

    /**
     * 任务截止时间
     */
    @NotNull
    @ApiModelProperty(value = "任务截止时间", required = true)
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date taskEndDatetime;

    /**
     * 是否短信提醒（0 否 | 1 是）
     */
    @NotBlank
    @ApiModelProperty(value = "是否短信提醒（0 否 | 1 是）", required = true)
    private String smsNoticeFlag;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;
}
