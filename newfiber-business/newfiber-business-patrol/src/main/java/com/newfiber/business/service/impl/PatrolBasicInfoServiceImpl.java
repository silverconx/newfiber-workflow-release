package com.newfiber.business.service.impl;

import com.newfiber.business.domain.PatrolBasicInfo;
import com.newfiber.business.domain.request.patrolBasicInfo.PatrolBasicInfoQueryRequest;
import com.newfiber.business.domain.request.patrolBasicInfo.PatrolBasicInfoSaveRequest;
import com.newfiber.business.domain.request.patrolBasicInfo.PatrolBasicInfoUpdateRequest;
import com.newfiber.business.mapper.PatrolBasicInfoMapper;
import com.newfiber.business.service.IPatrolBasicInfoService;
import com.newfiber.common.core.exception.ServiceException;
import com.newfiber.common.core.web.domain.BaseEntity;
import com.newfiber.common.core.web.service.BaseServiceImpl;
import java.util.List;
import java.util.Optional;
import javax.annotation.Resource;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 巡查基础信息Service业务层处理
 * 
 * @author X.K
 * @date 2023-02-10
 */
@Service
public class PatrolBasicInfoServiceImpl extends BaseServiceImpl<PatrolBasicInfoMapper, PatrolBasicInfo> implements IPatrolBasicInfoService {

    @Resource
    private PatrolBasicInfoMapper patrolBasicInfoMapper;

    @Override
    public long insert(PatrolBasicInfoSaveRequest request) {
    	if(duplicateCheck(PatrolBasicInfo::getPatrolTarget, request.getPatrolTarget())){
			throw new ServiceException(String.format("巡查内容已存在：%s", request.getPatrolTarget()));
	    }

        PatrolBasicInfo patrolBasicInfo = new PatrolBasicInfo();
        BeanUtils.copyProperties(request, patrolBasicInfo);
        save(patrolBasicInfo);
        return Optional.of(patrolBasicInfo).map(BaseEntity::getId).orElse(0L);
    }

    @Override
    public boolean update(PatrolBasicInfoUpdateRequest request) {
	    if(duplicateCheck(request.getId(), PatrolBasicInfo::getPatrolTarget, request.getPatrolTarget())){
		    throw new ServiceException(String.format("巡查内容已存在：%s", request.getPatrolTarget()));
	    }

	    PatrolBasicInfo patrolBasicInfo = new PatrolBasicInfo();
        BeanUtils.copyProperties(request, patrolBasicInfo);
        return updateById(patrolBasicInfo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(String ids) {
        return deleteLogic(ids);
    }

    @Override
    public PatrolBasicInfo selectDetail(Long id) {
	    PatrolBasicInfoQueryRequest request = new PatrolBasicInfoQueryRequest();
	    request.setId(id);
	    List<PatrolBasicInfo> patrolBasicInfoList = patrolBasicInfoMapper.selectByCondition(request);
        return CollectionUtils.isNotEmpty(patrolBasicInfoList) ? patrolBasicInfoList.get(0) : null;
    }

    @Override
    public List<PatrolBasicInfo> selectPage(PatrolBasicInfoQueryRequest request) {
        return patrolBasicInfoMapper.selectByCondition(request);
    }

    @Override
    public List<PatrolBasicInfo> selectList(PatrolBasicInfoQueryRequest request) {
        return patrolBasicInfoMapper.selectByCondition(request);
    }

}
