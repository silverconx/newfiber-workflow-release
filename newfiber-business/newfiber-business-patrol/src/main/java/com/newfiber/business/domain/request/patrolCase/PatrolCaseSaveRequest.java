package com.newfiber.business.domain.request.patrolCase;

import static cn.hutool.core.date.DatePattern.NORM_DATE_PATTERN;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.newfiber.system.api.domain.request.SysFileSaveRequest;
import com.newfiber.workflow.support.request.WorkflowStartRequest;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 巡查案件对象 pat_patrol_case
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolCaseSaveRequest extends WorkflowStartRequest {

    /**
     * 巡查类型（数据字典 patrol_type）
     */
    @ApiModelProperty(value = "巡查类型（数据字典 patrol_type）")
    private String patrolType;

    /**
     *  巡查基础信息编号
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "巡查基础信息编号")
    private Long basicInfoId;

    /**
     * 巡查段编号
     */
    @ApiModelProperty(value = "巡查段编号")
    private Long patrolSectionId;

    /**
     * 巡查日志编号（移动端）
     */
    @NotNull
    @ApiModelProperty(value = "巡查日志编号（移动端）", required = true)
    private Long patrolLogId;

    /**
     * 案件名称
     */
    @NotBlank
    @ApiModelProperty(value = "案件名称", required = true)
    private String caseName;

    /**
     * 案件等级（数据字典 case_level）
     */
    @NotBlank
    @ApiModelProperty(value = "案件等级（数据字典 case_level）", required = true)
    private String caseLevel;

    /**
     * 案件类型（数据字典  case_type）
     */
    @NotBlank
    @ApiModelProperty(value = "案件类型（数据字典  case_type）", required = true)
    private String caseType;

    /**
     * 案件内容/问题详情/问题描述
     */
    @NotBlank
    @ApiModelProperty(value = "案件内容/问题详情/问题描述", required = true)
    private String caseContent;

    /**
     * 案件来源
     */
    @ApiModelProperty(value = "案件来源")
    private String caseSource;

    /**
     * 案件数据来源
     * 1巡查任务录入的案件task 2巡查日志录入的案件log 3单独录入的案件case
     */
    @ApiModelProperty(value = "案件数据来源")
    private String caseDataSource;

    /**
     * 截止日期
     */
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    @ApiModelProperty(value = "截止日期")
    private Date deadline;

    /**
     * 案件地址
     */
    @ApiModelProperty(value = "案件地址")
    private String caseAddress;

    /**
     * 经纬度
     */
    @ApiModelProperty(value = "经纬度")
    private String lonLat;

    /**
     * 是否上报（0 否 | 1 是）
     */
    @ApiModelProperty(value = "是否上报（0 否 | 1 是）")
    private String reportFlag;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

	/**
	 * 文件
	 */
	@ApiModelProperty(value = "文件")
	private List<SysFileSaveRequest> fileSaveRequestList;
}
