package com.newfiber.business.domain.response.patrolExamine;

import lombok.Data;

/**
 * 事件来源，数量，完成数量
 * date: 2023/3/24 下午 03:59
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
public class PatrolCaseSource {
    private String caseSource;
    private int total;
    private String endCount;
}
