package com.newfiber.business.domain.request.patrolTask;

import static cn.hutool.core.date.DatePattern.NORM_DATETIME_PATTERN;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 巡查任务对象 pat_patrol_task
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolTaskSaveRequest{

    /**
     * 任务来源（巡查 patrol | 报警 warning）
     */
    @NotBlank
    @ApiModelProperty(value = "任务来源（巡查 patrol | 报警 warning）", required = true)
    private String taskSource;

    /**
     * 巡查类型（数据字典 patrol_type）
     */
    @NotBlank
    @ApiModelProperty(value = "巡查类型（数据字典 patrol_type）", required = true)
    private String patrolType;

    /**
     * 巡查基础信息编号
     */
    @NotNull
    @ApiModelProperty(value = "巡查基础信息编号", required = true)
    private Long basicInfoId;

    /**
     * 巡查段编号
     */
    @NotNull
    @ApiModelProperty(value = "巡查段编号", required = true)
    private Long patrolSectionId;

    /**
     * 任务名称
     */
    @NotBlank
    @ApiModelProperty(value = "任务名称", required = true)
    private String taskName;

    /**
     * 巡查人
     */
    @NotNull
    @ApiModelProperty(value = "巡查人", required = true)
    private Long taskUserId;

    /**
     * 计划开始时间
     */
    @NotNull
    @ApiModelProperty(value = "计划开始时间", required = true)
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    private Date planStartDatetime;

    /**
     * 计划应结束时间
     */
    @NotNull
    @ApiModelProperty(value = "计划应结束时间", required = true)
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    private Date planEndDatetime;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

}
