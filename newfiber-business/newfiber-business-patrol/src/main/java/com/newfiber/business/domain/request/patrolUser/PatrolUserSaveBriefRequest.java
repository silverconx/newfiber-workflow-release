package com.newfiber.business.domain.request.patrolUser;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 巡查人员对象 pat_patrol_user
 *
 * @author X.K
 * @date 2023-02-20
 */
@Data
public class PatrolUserSaveBriefRequest {

	/**
	 * 用户类型（巡查人员 patrol | 立案人员 register | 核查人员 approve | 处理人员 handle | 验证人员 verify | 结案人员 close）
	 */
	@ApiModelProperty(value = "用户类型（巡查人员 patrol | 立案人员 register | 核查人员 approve | 处理人员 handle | 验证人员 verify | 结案人员 close）")
	private String userType;

	/**
	 * 用户编号
	 */
	@ApiModelProperty(value = "用户编号")
	private Long userId;

}
