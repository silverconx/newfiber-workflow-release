package com.newfiber.business.domain.request.patrolStatistics;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

import static cn.hutool.core.date.DatePattern.NORM_DATE_PATTERN;

/**
 * 巡查统计筛选条件
 * <p>
 * date: 2023/3/1 上午 10:09
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
@EqualsAndHashCode
public class PatrolStatisticsQueryRequest {

    /**
     * 巡查类型（数据字典 patrol_type）
     * 全部 河道 管网
     */
    @ApiModelProperty(value = "巡查类型 all/river/pipeline")
    private String patrolType;

    /**
     * 开始日期
     */
    @ApiModelProperty(value = "开始日期")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date startDate;

    /**
     * 结束日期
     */
    @ApiModelProperty(value = "结束日期")
    @JsonFormat(pattern = NORM_DATE_PATTERN)
    @DateTimeFormat(pattern = NORM_DATE_PATTERN)
    private Date endDate;
}
