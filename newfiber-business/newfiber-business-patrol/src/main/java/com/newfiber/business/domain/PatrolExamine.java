package com.newfiber.business.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.newfiber.business.constant.BusinessConstants;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 巡查考核对象 pat_patrol_examine
 *
 * @author lufan
 * @date 2023-03-03
 */
@Data
@TableName("pat_patrol_examine")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "巡查考核", description = "巡查考核")
public class PatrolExamine extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 巡查人员ID
     */
    @ApiModelProperty(value = "巡查人员ID")
    private Long userId;

    /**
     * 巡查人员姓名
     */
    @ApiModelProperty(value = "巡查人员姓名")
    private String userName;

    /**
     * 应巡查次数
     */
    @ApiModelProperty(value = "应巡查次数")
    private Long planPatrolCount;

    /**
     * 实际巡查次数
     */
    @ApiModelProperty(value = "实际巡查次数")
    private Long realPatrolCount;

    /**
     * 应巡查时长（单位：小时）
     */
    @ApiModelProperty(value = "应巡查时长（单位：小时）")
    private Long planPatrolTimes;

    /**
     * 实际巡查时长（单位：小时）
     */
    @ApiModelProperty(value = "实际巡查时长（单位：小时）")
    private Long realPatrolTimes;

    /**
     * 应巡查里程（单位：千米）
     */
    @ApiModelProperty(value = "应巡查里程（单位：千米）")
    private Long planPatrolLength;

    /**
     * 实际巡查里程（单位：千米）
     */
    @ApiModelProperty(value = "实际巡查里程（单位：千米）")
    private Long realPatrolLength;

    /**
     * 应完成问题个数
     */
    @ApiModelProperty(value = "应完成问题个数")
    private Long planFinishCount;

    /**
     * 实际完成问题个数
     */
    @ApiModelProperty(value = "实际完成问题个数")
    private Long realFinishCount;

    /**
     * 问题完成率
     */
    @ApiModelProperty(value = "问题完成率")
    private String taskFinishRate;

    /**
     * 统计日期单元（年/月/周）
     */
    @ApiModelProperty(value = "统计日期单元（年/月/周）")
    private String examineUnit;

    /**
     * 统计日期范围 2023-02-27-2023-03-05
     */
    @ApiModelProperty(value = "统计日期范围")
    private String examineRange;

    /**
     * 统计日期单元（年/月/周）
     */
    @TableField(exist = false)
    @ApiModelProperty(value = "统计日期单元（年/月/周）")
    private String examineUnitStr;

    public String getExamineUnitStr() {
        String unitStr = "";
        switch (this.examineUnit) {
            case BusinessConstants.WEEK:
                unitStr = "周";
                break;
            case BusinessConstants.MONTH:
                unitStr = "月";
                break;
            case BusinessConstants.YEAR:
                unitStr = "年";
                break;
        }
        return unitStr;
    }
}
