package com.newfiber.business.domain.request.patrolCase;

import static cn.hutool.core.date.DatePattern.NORM_DATETIME_PATTERN;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 巡查案件对象 pat_patrol_case
 *
 * @author X.K
 * @date 2023-02-17
 */
@Data
public class PatrolCaseUpdateRequest {

    /**
     * 主键
     */
    @NotNull
    @ApiModelProperty(value = "主键", required = true)
    private Long id;

    /**
     * 案件名称
     */
    @NotBlank
    @ApiModelProperty(value = "案件名称", required = true)
    private String caseName;

    /**
     * 案件等级（数据字典 case_level）
     */
    @NotBlank
    @ApiModelProperty(value = "案件等级（数据字典 case_level）", required = true)
    private String caseLevel;

    /**
     * 案件类型（数据字典  case_type）
     */
    @NotBlank
    @ApiModelProperty(value = "案件类型（数据字典  case_type）", required = true)
    private String caseType;

    /**
     * 案件内容/问题详情/问题描述
     */
    @NotBlank
    @ApiModelProperty(value = "案件内容/问题详情/问题描述", required = true)
    private String caseContent;

    /**
     * 案件来源
     */
    @ApiModelProperty(value = "案件来源")
    private String caseSource;

    /**
     * 截止日期
     */
    @ApiModelProperty(value = "截止日期")
    @JsonFormat(pattern = NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
    private Date deadline;

    /**
     * 案件地址
     */
    @ApiModelProperty(value = "案件地址")
    private String caseAddress;

    /**
     * 经纬度
     */
    @ApiModelProperty(value = "经纬度")
    private String lonLat;

    /**
     * 是否上报（0 否 | 1 是）
     */
    @ApiModelProperty(value = "是否上报（0 否 | 1 是）")
    private String reportFlag;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;

	/**
	 * 图片
	 */
	@ApiModelProperty(value = "图片")
	private String picture;

	/**
	 * 视频
	 */
	@ApiModelProperty(value = "视频")
	private String video;

	/**
	 * 音频
	 */
	@ApiModelProperty(value = "音频")
	private String audio;

}
