package com.newfiber.business.enums;

import com.newfiber.common.core.web.support.IRedisPrefix;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author : X.K
 * @since : 2023/2/28 下午3:03
 */
@Getter
@AllArgsConstructor
public enum EPatrolRedisKey implements IRedisPrefix {

	/**
	 *
	 */
	TodayPatrolTask("TodayPatrolTask", "今日巡查任务"),

	PatrolPath("PatrolPath", "巡查轨迹"),

	PatrolCase("PatrolCase", "巡查案件"),
		;

	private final String moduleKey;

	private final String moduleName;

}
