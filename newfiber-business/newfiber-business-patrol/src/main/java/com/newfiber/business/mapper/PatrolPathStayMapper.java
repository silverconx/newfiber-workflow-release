package com.newfiber.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import com.newfiber.business.domain.request.patrolPathStay.PatrolPathStayQueryRequest;
import com.newfiber.business.domain.PatrolPathStay;
import org.apache.ibatis.annotations.Param;

/**
 * 巡查轨迹停留记录Mapper接口
 * 
 * @author X.K
 * @date 2023-03-08
 */
public interface PatrolPathStayMapper extends BaseMapper<PatrolPathStay>{

    /**
     * 条件查询巡查轨迹停留记录列表
     * 
     * @param request 查询条件
     * @return 巡查轨迹停留记录集合
     */
    List<PatrolPathStay> selectByCondition(@Param("request") PatrolPathStayQueryRequest request);

}
