package com.newfiber.business.domain.response.patrolExamine;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 导出数据实例
 * word文档数据
 * date: 2023/3/23 下午 06:46
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
public class PatrolExamineExport implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 标题
     */
    public String title;

    /**
     * 日期
     */
    public String date;

    /**
     * 问题来源
     */
    public String caseSource;

    /**
     * 问题来源数量
     */
    public String caseSourceCount;

    /**
     * 已解决问题数量
     */
    public String caseFinishCount;

    /**
     * 未解决问题数量
     */
    public String caseNotFinishCount;

    /**
     * 巡查内容
     */
    public String patrolTarget;

    /**
     * 巡查内容数量
     */
    public String patrolTargetCount;

    /**
     * 巡查内容完成数量
     */
    public String patrolTargetCountDone;

    /**
     * 上报案件数量
     */
    public String caseCount;

    /**
     * 实巡查人数
     */
    public String riverUserCount;
    public String pipelineUserCount;

    /**
     * 实巡查次数
     */
    public String riverPatrolTime;
    public String pipelinePatrolTime;

    /**
     * 上报事件数量
     */
    public String riverCaseReportCount;
    public String pipelineCaseReportCount;

    /**
     * 上报事件主要类型
     */
    public String riverCaseReportMainCaseType;
    public String pipelineCaseReportMainCaseType;

    /**
     * 接收事件个数
     */
    public String riverCaseReceived;
    public String pipelineCaseReceived;
    /**
     * 事件来源，数量，完成数量
     */
    public List<PatrolCaseSource> patrolCaseSource;
    public String patrolRiverCaseSourceStr;
    public String patrolPipelineCaseSourceStr;

    /**
     * 河道
     * 巡查问题类型对于案件完成数量
     */
    public List<PatrolCaseFinish> patrolCaseFinish;
    public String patrolRiverCaseFinishStr;

    /**
     * 管网
     * 巡查问题类型对于案件完成数量
     */
    public List<PatrolCaseUnFinish> patrolCaseUnFinish;
    public String patrolPipelineCaseUnFinishStr;

    /**
     * 表格数据
     */
    List<List<String>> tableData;

}
