package com.newfiber.business.controller;

import com.newfiber.business.domain.request.patrolStatistics.PatrolStatisticsQueryRequest;
import com.newfiber.business.domain.request.patrolStatistics.PatrolUserStatisticRequest;
import com.newfiber.business.domain.response.patrolStatistics.CaseCountResult;
import com.newfiber.business.domain.response.patrolStatistics.CaseFinishRateResult;
import com.newfiber.business.domain.response.patrolStatistics.PatrolAttendanceRate;
import com.newfiber.business.domain.response.patrolStatistics.PatrolPerson;
import com.newfiber.business.domain.response.patrolStatistics.PatrolStatisticsCaseResult;
import com.newfiber.business.domain.response.patrolStatistics.PatrolUserStatisticResponse;
import com.newfiber.business.service.IPatrolStatisticsService;
import com.newfiber.common.core.web.controller.BaseController;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.security.annotation.RequiresPermissions;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 巡查统计Controller
 * <p>
 * date: 2023/3/1 上午 09:57
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@RestController
@RequestMapping("/patrolStatistics")
@Api(value = "PAT11-巡查统计", tags = "PAT11-巡查统计")
public class PatrolStatisticsController extends BaseController {

    @Resource
    private IPatrolStatisticsService patrolStatisticsService;

    /**
     * 巡查出勤率统计
     */
    @PostMapping(value = "/attendanceRate")
    @RequiresPermissions("businessPatrol:patrolStatistics:detail")
    @ApiOperation(value = "巡查出勤率统计", position = 10)
    public Result<PatrolAttendanceRate> attendanceRate(@RequestBody PatrolStatisticsQueryRequest request) {
        return success(patrolStatisticsService.attendanceRate(request));
    }

    /**
     * 优秀巡查人统计
     */
    @PostMapping(value = "/patrolPerson")
    @RequiresPermissions("businessPatrol:patrolStatistics:detail")
    @ApiOperation(value = "优秀巡查人统计", position = 20)
    public Result<List<PatrolPerson>> patrolPerson(@RequestBody PatrolStatisticsQueryRequest request) {
        return success(patrolStatisticsService.patrolPerson(request));
    }

    /**
     * 问题来源统计
     */
    @PostMapping(value = "/caseSource")
    @RequiresPermissions("businessPatrol:patrolStatistics:detail")
    @ApiOperation(value = "问题来源统计", position = 30)
    public Result<PatrolStatisticsCaseResult> caseSource(@RequestBody PatrolStatisticsQueryRequest request) {
        return success(patrolStatisticsService.caseSource(request));
    }

    /**
     * 项目问题统计
     */
    @PostMapping(value = "/patrolCaseType")
    @RequiresPermissions("businessPatrol:patrolStatistics:detail")
    @ApiOperation(value = "项目问题统计", position = 40)
    public Result<PatrolStatisticsCaseResult> patrolCaseType(@RequestBody PatrolStatisticsQueryRequest request) {
        return success(patrolStatisticsService.patrolCaseType(request));
    }

    /**
     * 问题类型统计
     */
    @PostMapping(value = "/caseType")
    @RequiresPermissions("businessPatrol:patrolStatistics:detail")
    @ApiOperation(value = "问题类型统计", position = 50)
    public Result<List<CaseCountResult>> caseType(@RequestBody PatrolStatisticsQueryRequest request) {
        return success(patrolStatisticsService.caseType(request));
    }

    /**
     * 问题结案率
     */
    @PostMapping(value = "/caseFinishRate")
    @RequiresPermissions("businessPatrol:patrolStatistics:detail")
    @ApiOperation(value = "问题结案率", position = 60)
    public Result<CaseFinishRateResult> caseFinishRate(@RequestBody PatrolStatisticsQueryRequest request) {
        return success(patrolStatisticsService.caseFinishRate(request));
    }

    /**
     * 来源
     */
    @PostMapping(value = "/caseSourceTotal")
    @RequiresPermissions("businessPatrol:patrolStatistics:detail")
    @ApiOperation(value = "来源", position = 60)
    public Result<List<CaseCountResult>> caseSourceTotal(@RequestBody PatrolStatisticsQueryRequest request) {
        return success(patrolStatisticsService.caseSourceTotal(request));
    }

	/**
	 * 巡查人员统计
	 */
	@PostMapping(value = "/patrolUserStatistic")
	@RequiresPermissions("businessPatrol:patrolStatistics:detail")
	@ApiOperation(value = "巡查人员统计", position = 60)
	public Result<PatrolUserStatisticResponse> patrolUserStatistic(@RequestBody PatrolUserStatisticRequest request) {
		return success(patrolStatisticsService.patrolUserStatistic(request));
	}

}
