package com.newfiber.business.service;

import com.newfiber.business.domain.PatrolCase;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseDeprecateRequest;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseGeneratorNameRequest;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseQueryCacheRequest;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseQueryMyRequest;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseQueryRequest;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseSaveRequest;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseSubmitRequest;
import com.newfiber.business.domain.request.patrolCase.PatrolCaseUpdateRequest;
import java.util.List;

/**
 * 巡查案件Service接口
 *
 * @author X.K
 * @date 2023-02-17
 */
public interface IPatrolCaseService {

    /**
     * 新增巡查案件
     *
     * @param request 新增参数
     * @return 结果
     */
    long insert(PatrolCaseSaveRequest request);

	/**
	 * 新增巡查案件
	 *
	 * @param request 新增参数
	 * @return 结果
	 */
	boolean insertCache(PatrolCaseSaveRequest request);

    /**
     * 修改巡查案件
     *
     * @param request 修改参数
     * @return 结果
     */
    boolean update(PatrolCaseUpdateRequest request);

	/**
	 * 启动临时日志的缓存案件
	 */
	boolean startCachedCaseByLog(Long patrolLogId);

    /**
     * 提交巡查案件
     *
     * @param request 修改参数
     * @return 结果
     */
    String submit(PatrolCaseSubmitRequest request);

	/**
	 * 提交是否上报
	 */
	void submitReport(long id, String reportFlag, String currentUser);

    /**
     * 作废巡查案件
     *
     * @param request 修改参数
     * @return 结果
     */
    boolean deprecate(PatrolCaseDeprecateRequest request);

    /**
     * 批量删除巡查案件
     *
     * @param ids 编号(,隔开)
     * @return 结果
     */
    boolean delete(String ids);

    /**
     * 根据taskId删除case
     *
     * @param taskId
     * @return 结果
     */
    boolean deleteCaseByTaskId(Long taskId);

	/**
	 *
	 */
	long count(List<Long> taskIdList);

	/**
	 *
	 */
	String generatorName(PatrolCaseGeneratorNameRequest request);

    /**
     * 详细查询巡查案件
     *
     * @param id 主键
     * @return 巡查案件
     */
    PatrolCase selectDetail(Long id);

    /**
     * 分页查询巡查案件
     *
     * @param request 分页参数
     * @return 巡查案件集合
     */
    List<PatrolCase> selectPage(PatrolCaseQueryRequest request);

    /**
     * 分页查询巡查案件
     *
     * @param request 分页参数
     * @return 巡查案件集合
     */
    List<PatrolCase> selectPageMy(PatrolCaseQueryMyRequest request);

	/**
	 * 列表查询与我相关的巡查案件
	 *
	 * @return 与我相关的巡查案件
	 */
	List<PatrolCase> myCase();

    /**
     * 列表查询巡查案件
     *
     * @param request 列表参数
     * @return 巡查案件集合
     */
    List<PatrolCase> selectList(PatrolCaseQueryRequest request);

	/**
	 * 列表查询巡查案件
	 *
	 * @param request 列表参数
	 * @return 巡查案件集合
	 */
	List<PatrolCase> selectListCache(PatrolCaseQueryCacheRequest request);

}
