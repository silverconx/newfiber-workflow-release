package com.newfiber.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.newfiber.business.domain.PatrolSection;
import com.newfiber.business.domain.request.patrolSection.PatrolSectionQueryRequest;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 巡查段配置Mapper接口
 * 
 * @author X.K
 * @date 2023-02-17
 */
public interface PatrolSectionMapper extends BaseMapper<PatrolSection>{

    /**
     * 条件查询巡查段配置列表
     * 
     * @param request 查询条件
     * @return 巡查段配置集合
     */
    List<PatrolSection> selectByCondition(@Param("request") PatrolSectionQueryRequest request);

	/**
	 * 条件查询巡查段配置列表
	 *
	 * @param id 查询条件
	 * @return 巡查段配置集合
	 */
	PatrolSection selectOneById(@Param("id") Long id);

}
