import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.EnumUtil;
import cn.hutool.core.util.HexUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.ServiceLoaderUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SmUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.SM2;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import cn.hutool.poi.excel.ExcelUtil;
import com.alibaba.fastjson2.JSONObject;
import com.newfiber.business.domain.request.patrolPlan.PatrolPlanFixDate;
import com.newfiber.business.domain.request.patrolPlan.PatrolPlanFixFrequency;
import com.newfiber.business.domain.request.patrolPlan.PatrolPlanIntervalDate;
import com.newfiber.business.domain.request.patrolTask.PatrolTaskDatetime;
import com.newfiber.business.domain.request.patrolTask.PatrolTaskGeneratorDraftRequest;
import com.newfiber.business.service.impl.PatrolTaskServiceImpl;
import com.newfiber.common.core.utils.CronUtils;
import com.newfiber.workflow.support.IWorkflowDefinition;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.junit.jupiter.api.Test;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * @author : X.K
 * @since : 2023/2/20 下午2:40
 */
@Slf4j
public class CommonTest {

	@Test
	public void cronTest(){
//		CronUtils.getExecutionTime("0 0 0 1/1 * ? ", 20).forEach(System.out::println);
		CronUtils.getExecutionTime("0 0 0 1/1 * ? ", DateUtil.parse("2023-02-25 00:00:00")).forEach(System.out::println);
	}

	@Test
	public void calcTaskDatetimeTest(){
		PatrolTaskServiceImpl patrolTaskService = new PatrolTaskServiceImpl();
		PatrolTaskGeneratorDraftRequest request = new PatrolTaskGeneratorDraftRequest();
		request.setPatrolFrequencyType("fix_frequency");
		request.setStartDate(DateUtil.parseDate("2023-02-20"));
		request.setEndDate(DateUtil.parseDate("2023-04-20"));

		PatrolPlanFixDate fixDate = new PatrolPlanFixDate();
		fixDate.setCron("0 0 0 1/1 * ? ");
		request.setPatrolPlanFixDate(fixDate);

		PatrolPlanFixFrequency fixFrequency = new PatrolPlanFixFrequency();
		fixFrequency.setIntervalType("day");
		fixFrequency.setFrequency(2);
		fixFrequency.setStartTime("09:00:00");
		fixFrequency.setEndTime("18:00:00");
		request.setPatrolPlanFixFrequency(fixFrequency);

		PatrolPlanIntervalDate intervalDate = new PatrolPlanIntervalDate();
		intervalDate.setInterval(2);
		intervalDate.setFrequency(2);
		intervalDate.setStartTime("09:00:00");
		intervalDate.setEndTime("18:00:00");
		request.setPatrolPlanIntervalDate(intervalDate);

		List<PatrolTaskDatetime> patrolTaskDatetimeList = patrolTaskService.calcTaskDatetime(request);
		patrolTaskDatetimeList.forEach(t -> System.out.println(JSONObject.toJSONString(t)));

		log.info("success");
	}

	@Test
	public void calcDateTest(){
		Date startDatetime = DateUtil.parseDate("2023-08-01");
		Date endDatetime = DateUtil.parseDate("2023-08-31");

		List<? extends Date> dateList = DateUtil.rangeToList(startDatetime, endDatetime, DateField.DAY_OF_WEEK);
		dateList.forEach(t -> System.out.println(DateUtil.formatDateTime(t)));
	}

	@Test
	public void splitTest(){
		String sectionIds = "123";
		System.out.println(Arrays.asList(sectionIds.split(",")).size());
	}

	@Test
	public void serverLoadTest(){
		List<IWorkflowDefinition> workflowDefinitionList = ServiceLoaderUtil.loadList(IWorkflowDefinition.class);

		ServiceLoader<IWorkflowDefinition> loader = ServiceLoader.load(IWorkflowDefinition.class);
		if(loader.iterator().hasNext()){
			System.out.println(loader.iterator().next().getWorkflowKey());
		}
	}

	@Test
	public void interfaceScanTest(){
		Set<Class<?>> classes = ClassUtil.scanPackage("com.newfiber");
		List<IWorkflowDefinition> workflowDefinitionList = new ArrayList<>();

		for(Class<?> clazz : classes){
//			clazz instanceof IWorkflowDefinition;
//			ClassUtil
			if(clazz.isEnum() && null != clazz.getSuperclass() && ReflectUtil.hasField(clazz, "workflowKey")){
				;
				System.out.println(EnumUtil.getFieldValues((Class<? extends Enum<?>>)clazz, "workflowKey"));
//				IWorkflowDefinition workflowDefinition = (IWorkflowDefinition) ; /
				System.out.println(clazz.getSimpleName() + ":" + clazz.getSuperclass().getSimpleName());
			}

//			if(clazz.getClass().getSuperclass() instanceof IWorkflowDefinition)){
//
//			}

		}
	}

	@Test
	public void sm2Test(){
		String content = "我是Hanley.";
		final SM2 sm2 = SmUtil.sm2();
		String sign = sm2.signHex(HexUtil.encodeHexStr(content));

		boolean verify = sm2.verifyHex(HexUtil.encodeHexStr(content), sign);

		//随机生成的密钥对进行加密解密
		String text = "this is a test str";
		// 公钥加密，私钥解密
		KeyType publicKey = KeyType.PublicKey;
		System.out.println(publicKey.toString());
		KeyType privateKey = KeyType.PrivateKey;
		System.out.println(privateKey.toString());
		String encryptStr = sm2.encryptBcd(text, publicKey);
		String decryptStr = StrUtil.utf8Str(sm2.decryptFromBcd(encryptStr, privateKey));
		System.out.println("encode" + "    " + encryptStr);
		System.out.println("decode" + "    " + decryptStr);
	}

	@Test
	public void sm3Test(){

	}

	@Test
	public void sm4Test(){
		String content = "test中文";
		SymmetricCrypto sm4 = SmUtil.sm4();

		String encryptHex = sm4.encryptHex(content);
		String decryptStr = sm4.decryptStr(encryptHex, CharsetUtil.CHARSET_UTF_8);

		System.out.println("encode" + "    " + encryptHex);
		System.out.println("decode" + "    " + decryptStr);
	}

	@Test
	public void bigExcelTest() throws Exception{
		String excelPath = "/Users/silver/Downloads/层位数据筛选.xlsx";

		new ReadBigDataXlsxFile(excelPath, new Consumer<Map<String,String>>() {
			@Override
			public void accept(Map<String,String> o) {
				System.out.println(o.size());
			}
		}
		).processAllSheets();

	}


	public Map<String, CustomSheet> bigExcelTest2(String excelPath) throws Exception{
		OPCPackage opc = OPCPackage.open(excelPath, PackageAccess.READ);
		XSSFReader reader = new XSSFReader(opc);
		XSSFReader.SheetIterator iterator = (XSSFReader.SheetIterator) reader.getSheetsData();
		SharedStringsTable sst = reader.getSharedStringsTable();
		StylesTable st = reader.getStylesTable();
		CustomHandler handler = new CustomHandler(sst, st);
		XMLReader xml = XMLReaderFactory.createXMLReader();
		Map<String, CustomSheet> map = new HashMap<>();
		xml.setContentHandler(handler);
		while (iterator.hasNext()) {
			System.out.println("=====================================================================================================================");
			InputStream is = iterator.next();
			InputSource in = new InputSource(is);
			xml.parse(in);
			String name = iterator.getSheetName();
			System.out.println(name);
			CustomSheet sheet = handler.getData();
			sheet.setName(name);
//			if("标灰尖灭线".equals(name)){
//				sheet.getRows().removeAll(map.get("Sheet1").getRows());
//			}
			map.put(name, sheet);
			is.close();
		}
		System.out.println(map.size());
		return map;
	}

	@Test
	public void excelTest() throws Exception{
		String excelPath = "/Users/silver/Downloads/层位数据筛选.xlsx";
		String excelWritePath = "/Users/silver/Downloads/层位数据筛选-已处理.xlsx";

//		List<List<Object>> sheetOneData = ExcelUtil.getReader(excelPath, 0).read();
//		List<List<Object>> sheetTwoData = ExcelUtil.getReader(excelPath, 1).read();
		List<List<Object>> sheetThreeData = ExcelUtil.getReader(excelPath, 2).read(0, 711509);
		List<List<Object>> sheetTargetData = ExcelUtil.getReader(excelPath, 3).read();

		Map<String, CustomSheet> excelDataMap = bigExcelTest2(excelPath);

//		List<PrepareData> prepareDataList = new ArrayList<>();
//		prepareDataList.addAll(PrepareData.parse("Sheet1", excelDataMap.get("Sheet1")));
//		prepareDataList.addAll(PrepareData.parse("Sheet2", excelDataMap.get("Sheet2")));
//		prepareDataList.addAll(PrepareData.parse("Sheet3", excelDataMap.get("Sheet3")));
//
		CustomSheet targetSheet = excelDataMap.get("标灰尖灭线");

		List<PrepareData> targetDataList = new ArrayList<>();
		for(CustomRow customRow : targetSheet.getRows()){
			String targetX = customRow.getCells().get(0).getValue();

			CustomRow targetRow = findNearestZ(excelDataMap, targetX, null);
			if(null != targetRow){
				System.out.println(targetRow.getCells().get(2).getValue());
			}

//			prepareData.setX(targetX);
//			targetDataList.add(prepareData);
//			targetRow.add(2, prepareData.getZ());
//			targetRow.add(3, prepareData.getSheet());
		}

//		for(List<Object> targetRow : sheetTargetData){
//			Double targetX = Double.parseDouble(targetRow.get(0).toString());
//
//			PrepareData prepareData = findNearestZ(prepareDataList, targetX);
//			targetRow.add(2, prepareData.getZ());
//			targetRow.add(3, prepareData.getSheet());
//		}

		System.out.println(targetDataList.size());
//		ExcelUtil.getWriter(excelWritePath).write(sheetTargetData);
	}

	@Test
	public void excelBigParse() throws Exception{
		String excelPath1 = "/Users/silver/Downloads/层位数据筛选-Sheet1.xlsx";
		String excelPath2 = "/Users/silver/Downloads/层位数据筛选-Sheet2.xlsx";
		String excelPath3 = "/Users/silver/Downloads/层位数据筛选-Sheet3.xlsx";
		String huimieExcelPath = "/Users/silver/Downloads/层位数据筛选-毁灭.xlsx";

		String excelWritePath = "/Users/silver/Downloads/层位数据筛选-已处理.xlsx";

		Map<String, CustomSheet> excelDataSheet1Map = bigExcelTest2(excelPath1);
		Map<String, CustomSheet> excelDataSheet2Map = bigExcelTest2(excelPath2);
		Map<String, CustomSheet> excelDataSheet3Map = bigExcelTest2(excelPath3);
		excelDataSheet1Map.put("Sheet2", excelDataSheet2Map.get("Sheet2"));
		excelDataSheet1Map.put("Sheet3", excelDataSheet3Map.get("Sheet3"));

		CustomSheet targetSheet = bigExcelTest2(huimieExcelPath).get("标灰尖灭线");

		List<PrepareData> targetDataList = new ArrayList<>();
		for(CustomRow customRow : targetSheet.getRows()){
			String targetX = customRow.getCells().get(0).getValue();
			String targetY = customRow.getCells().get(1).getValue();

			CustomRow targetRow = findNearestZ(excelDataSheet1Map, targetX, targetY);
			if(null != targetRow){
				System.out.println(targetRow.getCells().get(2).getValue());
				targetDataList.add(new PrepareData("", customRow.getCells().get(0).getValue(), customRow.getCells().get(1).getValue(), targetRow.getCells().get(2).getValue()));
			}
		}

		ExcelUtil.getWriter(excelWritePath).write(targetDataList).flush();
		System.out.println(targetDataList.size());
	}

	private PrepareData findNearestZ(List<PrepareData> prepareDataList, Double targetX){
		// X相同
		List<PrepareData> parsedData = prepareDataList.stream().filter(t -> t.getX().equals(targetX)).collect(Collectors.toList());

		if(CollectionUtils.isEmpty(parsedData)){
			return PrepareData.nan();
		}

		// Y排序
		parsedData = parsedData.stream().sorted(Comparator.comparing(PrepareData::getY)).collect(Collectors.toList());

		return parsedData.get(0);
	}

	private CustomRow findNearestZ(Map<String, CustomSheet> excelDataMap, String targetX, String targetY){
		// X相同
		List<CustomRow> sameXDataList = new ArrayList<>();
		sameXDataList.addAll(excelDataMap.get("Sheet1").getRows().stream().filter(t -> t.getCells().get(0).getValue().split("\\.")[0].equals(targetX)).collect(Collectors.toList()));
		sameXDataList.addAll(excelDataMap.get("Sheet2").getRows().stream().filter(t -> t.getCells().get(0).getValue().split("\\.")[0].equals(targetX)).collect(Collectors.toList()));
		sameXDataList.addAll(excelDataMap.get("Sheet3").getRows().stream().filter(t -> t.getCells().get(0).getValue().split("\\.")[0].equals(targetX)).collect(Collectors.toList()));

		if(CollectionUtils.isEmpty(sameXDataList)){
			return null;
		}

		// Y排序
		sameXDataList = sameXDataList.stream().sorted(Comparator.comparing(t ->
			Math.abs((Double.parseDouble(targetY) - Double.parseDouble(t.getCells().get(1).getValue()))))).collect(Collectors.toList());

		return sameXDataList.get(0);
	}

	@Data
	@AllArgsConstructor
	static class PrepareData{
		String sheet;
		String x;
		String y;
		String z;

		public static List<PrepareData> parse(String sheet, List<List<Object>> sheetData){
			List<PrepareData> prepareDataList = new ArrayList<>();
			for(List<Object> targetRow : sheetData){
				String targetX = targetRow.get(0).toString();
				String targetY = targetRow.get(1).toString();
				String targetZ = targetRow.get(2).toString();

				prepareDataList.add(new PrepareData(sheet, targetX, targetY, targetZ));
			}
			return prepareDataList;
		}

		public static List<PrepareData> parse(String sheet, CustomSheet customSheet){
			List<PrepareData> prepareDataList = new ArrayList<>(customSheet.getRows().size());
			for(CustomRow customRow : customSheet.getRows()){
				List<CustomCell> cells = customRow.getCells();
				if(cells.size() < 3){
					continue;
				}
				String targetX = cells.get(0).getValue();
				String targetY = cells.get(1).getValue();
				String targetZ = cells.get(2).getValue();

				prepareDataList.add(new PrepareData(sheet, targetX, targetY, targetZ));
			}
			return prepareDataList;
		}


		public static PrepareData nan(){
			return new PrepareData("NaN", "-1", "-1", "-1");
		}
	}
}


class CustomHandler extends DefaultHandler {

	private CustomRow row;

	private CustomSheet data;

	private SharedStringsTable sharedStringsTable;

	private StylesTable stylesTable;

	private boolean vIsOpen;

	private XssfDataType nextDataType;

	private boolean isCellNull = false;

	private int lastColumnNumber = -1;

	private StringBuffer value;

	private int cellNumber;

	private int rowNumber = -1;

	private short formatIndex;
	private String formatString;
	private final DataFormatter formatter = new DataFormatter();
	private SimpleDateFormat sdf = null;
	private DecimalFormat df = new DecimalFormat("###########");

	public CustomHandler(SharedStringsTable sharedStringsTable, StylesTable stylesTable) {
		this.sharedStringsTable = sharedStringsTable;
		this.stylesTable = stylesTable;
		this.data = new CustomSheet();
		this.data.setRows(new ArrayList<>());
		this.value = new StringBuffer();
	}

	@Override
	public void startElement (String uri, String localName,
		String qName, Attributes attributes)
		throws SAXException {
		if ("inlineStr".equals(qName) || "v".equals(qName)) {
			vIsOpen = true;
			value.setLength(0);
		} else if ("row".equals(qName)) {
			rowNumber = Integer.parseInt(attributes.getValue("r"));
			this.row = new CustomRow(rowNumber);
			this.row.setCells(new ArrayList<CustomCell>());
		}else if ("c".equals(qName)) {
			String r = attributes.getValue("r");
			int firstDigit = -1;
			//get the index of the cell
			for (int c = 0; c < r.length(); c++) {
				if (Character.isDigit(r.charAt(c))) {
					firstDigit = c;
					break;
				}
			}
			cellNumber = nameToColumn(r.substring(0, firstDigit));
			this.nextDataType = XssfDataType.NUMBER;
			this.formatIndex = -1;
			this.formatString = null;
			String cellType = attributes.getValue("t");
			String cellStyleStr = attributes.getValue("s");
			if ("b".equals(cellType))
				nextDataType = XssfDataType.BOOL;
			else if ("e".equals(cellType))
				nextDataType = XssfDataType.ERROR;
			else if ("inlineStr".equals(cellType))
				nextDataType = XssfDataType.INLINESTR;
			else if ("s".equals(cellType))
				nextDataType = XssfDataType.SSTINDEX;
			else if ("str".equals(cellType))
				nextDataType = XssfDataType.FORMULA;
			else if (cellStyleStr != null) {
				// It's a number, but almost certainly one
				// with a special style or format
				int styleIndex = Integer.parseInt(cellStyleStr);
				XSSFCellStyle style = stylesTable.getStyleAt(styleIndex);
				this.formatIndex = style.getDataFormat();
				this.formatString = style.getDataFormatString();
				if (this.formatString == null)
					this.formatString = BuiltinFormats
						.getBuiltinFormat(this.formatIndex);
			}
		}
	}

	@Override
	public void endElement (String uri, String localName, String qName)
		throws SAXException {
		String thisStr = null;

		// v => contents of a cell
		if ("v".equals(qName)) {
			// Do now, as characters() may be called more than once
			switch (nextDataType) {
				case BOOL:
					char first = value.charAt(0);
					thisStr = first == '0' ? "FALSE" : "TRUE";
					break;
				case ERROR:
					thisStr = "\"ERROR:" + value.toString() + '"';
					break;
				case FORMULA:
					// A formula could result in a string value,
					// so always add double-quote characters.
					thisStr = value.toString();
					break;
				case INLINESTR:
					// TODO: have seen an example of this, so it's untested
					XSSFRichTextString rtsi = new XSSFRichTextString(
						value.toString());
					thisStr = rtsi.toString();
					break;
				case SSTINDEX:
					String sstIndex = value.toString();
					try {
						int idx = Integer.parseInt(sstIndex);
						XSSFRichTextString rtss = new XSSFRichTextString(
							sharedStringsTable.getEntryAt(idx));
						thisStr = rtss.toString();
					} catch (NumberFormatException ex) {
						System.out.println("Failed to parse SST index '" + sstIndex
							+ "': " + ex.toString());
					}
					break;
				case NUMBER:

					String n = value.toString();
					// Date or not
					if (formatIndex == 14 || formatIndex == 31 || formatIndex == 57 || formatIndex == 58
						|| (176 <= formatIndex && formatIndex <= 178) ||
						(182 <= formatIndex && formatIndex <= 196)
						|| (210 <= formatIndex && formatIndex <= 213) || (208 == formatIndex)) {
						sdf = new SimpleDateFormat("yyyy-MM-dd");
						Date date = org.apache.poi.ss.usermodel.DateUtil.getJavaDate(Double.parseDouble(n));
						thisStr = sdf.format(date);
					} else if (formatIndex == 20 || formatIndex == 32 || formatIndex == 183 ||
						(200 <= formatIndex && formatIndex <= 209)) {//时间
						sdf = new SimpleDateFormat("HH:mm");
						Date date = org.apache.poi.ss.usermodel.DateUtil.getJavaDate(Double.parseDouble(n));
						thisStr = sdf.format(date);
					} else {
						if (n.contains("E")) {
							String[] split = n.split("\\+");
							String e = split[0].replaceAll("E|e", "");
							thisStr = e.replace(".", "");
						} else {
							thisStr = n;
						}
					}
					break;
				default:
					thisStr = "(TODO: Unexpected type: " + nextDataType + ")";
					break;
			}
			if (lastColumnNumber == -1) {
				lastColumnNumber = 0;
			}
			//判断单元格的值是否为空
			if (thisStr == null || "".equals(isCellNull)) {
				isCellNull = true;
			}

			this.row.getCells().add(new CustomCell(cellNumber, thisStr));
			if (rowNumber > -1)
				lastColumnNumber = rowNumber;
		}else if ("row".equals(qName)) {
			if (cellNumber > 0) {
				if (lastColumnNumber == -1) {
					lastColumnNumber = 0;
				}
				data.getRows().add(this.row);
			}
		}
	}

	public CustomSheet getData() {
		return data;
	}

	public void setData(CustomSheet data) {
		this.data = data;
	}

	public void characters(char[] ch, int start, int length)
		throws SAXException {
		value = new StringBuffer();
		value.append(ch, start, length);
	}

	private int nameToColumn(String name) {
		int column = -1;
		for (int i = 0; i < name.length(); ++i) {
			int c = name.charAt(i);
			column = (column + 1) * 26 + c - 'A';
		}
		return column;
	}

}
