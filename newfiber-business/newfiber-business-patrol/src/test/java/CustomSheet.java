import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

class CustomSheet {
	private String name;

	private List<CustomRow> rows;

	public CustomSheet(String name, List<CustomRow> rows) {
		this.name = name;
		this.rows = rows;
	}

	public CustomSheet() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<CustomRow> getRows() {
		return rows;
	}

	public void setRows(List<CustomRow> rows) {
		this.rows = rows;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		CustomSheet that = (CustomSheet) o;

		return Objects.equals(rows, that.rows);
	}

	@Override
	public int hashCode() {
		return rows != null ? rows.hashCode() : 0;
	}

	@Override
	public String toString() {
		return "CustomSheet{" +
			"name='" + name + '\'' +
			", rows=" + rows +
			'}';
	}

	public void sort() {
		Collections.sort(this.rows, new Comparator<CustomRow>() {
			@Override
			public int compare(CustomRow o1, CustomRow o2) {
				return o1.getNo() - o2.getNo();
			}
		});
	}
}

class CustomRow {
	private int no;

	private List<CustomCell> cells;

	public CustomRow(int no, List<CustomCell> cells) {
		this.no = no;
		this.cells = cells;
	}

	public CustomRow(int no) {
		this.no = no;
	}

	public CustomRow() {
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		CustomRow row = (CustomRow) o;

		return cells.equals(row.cells);
	}

	@Override
	public int hashCode() {
		return cells.hashCode();
	}

	@Override
	public String toString() {
		return "CustomRow{" +
			"no=" + no +
			", cells=" + cells +
			'}';
	}

	public void sort() {
		Collections.sort(this.cells, new Comparator<CustomCell>() {
			@Override
			public int compare(CustomCell o1, CustomCell o2) {
				return o1.getNo() - o2.getNo();
			}
		});
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public List<CustomCell> getCells() {
		return cells;
	}

	public void setCells(List<CustomCell> cells) {
		this.cells = cells;
	}
}

class CustomCell {
	private int no;

	private String value;

	public CustomCell(int no, String value) {
		this.no = no;
		this.value = value;
	}

	public CustomCell() {
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		CustomCell cell = (CustomCell) o;

		return value.equals(cell.value);
	}

	@Override
	public int hashCode() {
		return value.hashCode();
	}

	@Override
	public String toString() {
		return "CustomCell{" +
			"no=" + no +
			", value='" + value + '\'' +
			'}';
	}
}

enum XssfDataType {
	BOOL, ERROR, FORMULA, INLINESTR, SSTINDEX, NUMBER,
}
