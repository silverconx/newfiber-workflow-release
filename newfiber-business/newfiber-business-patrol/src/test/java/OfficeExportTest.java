import cn.hutool.core.io.FileUtil;
import cn.hutool.poi.word.PicType;
import cn.hutool.poi.word.Word07Writer;
import com.newfiber.business.domain.response.patrolExamine.PatrolExamineExport;
import com.newfiber.utils.OfficeUtil;
import com.newfiber.utils.WordBaseService;
import com.newfiber.utils.WordUtil;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.awt.*;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static cn.hutool.core.date.DatePattern.CHINESE_DATE_PATTERN;

//@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class OfficeExportTest {

    // 模板路径
    private static String templateBasePath = "/template/examineExport.docx";

    // 导出文件到本地路径
    private static String exportFile = "E:\\移动巡查\\移动巡查导出.docx";

    /**
     * 使用Word07Writer导出巡查考核
     */
    @Test
    public void writeExamineFile() {
        String filePath = "C:\\Users\\Administrator\\Desktop\\移动巡查\\demo.docx";
        Word07Writer writer = new Word07Writer();
        PatrolExamineExport patrolExamineExport = new PatrolExamineExport();
        patrolExamineExport.setTitle("条子河");
        patrolExamineExport.setPatrolTarget("条子河");
        patrolExamineExport.setRiverCaseReceived("23");
        patrolExamineExport.setCaseSource("举报热线");
        patrolExamineExport.setCaseSourceCount("324");
        patrolExamineExport.setRiverPatrolTime("23");
        patrolExamineExport.setRiverUserCount("58");
        patrolExamineExport.setPatrolTargetCount("743");
        patrolExamineExport.setCaseCount("50");
        patrolExamineExport.setCaseFinishCount("39");
        patrolExamineExport.setCaseNotFinishCount("587");
        patrolExamineExport.setPatrolTargetCountDone("536");
        writer.addText(ParagraphAlignment.CENTER, new Font("方正小标宋简体", Font.BOLD, 15), "中心城区“" + patrolExamineExport.getTitle() + "”一体化运维");
        writer.addText(ParagraphAlignment.CENTER, new Font("方正小标宋简体", Font.BOLD, 15), "数据日报");
        writer.addText(ParagraphAlignment.LEFT, new Font("方正小标宋简体", Font.BOLD, 14), "一、城市河道");
        writer.addText(ParagraphAlignment.LEFT, new Font("方正小标宋简体", Font.PLAIN, 10), "   当日河道实巡查人数 "
                + patrolExamineExport.getRiverUserCount() + "人，巡查次数"
                + patrolExamineExport.getRiverPatrolTime() + "次，上报事件数量"
                + patrolExamineExport.getCaseCount() + "个（主要是河岸垃圾类问题）。当日智慧水务平台共接收事件"
                + patrolExamineExport.getRiverCaseReceived() + "个，事件来源为"
                + patrolExamineExport.getCaseSource()
                + patrolExamineExport.getCaseSourceCount() + "个，已完成"
                + patrolExamineExport.getCaseFinishCount() + "个。接收事件中：\n 涉及"
                + patrolExamineExport.getPatrolTarget()
                + patrolExamineExport.getPatrolTargetCount() + "个，已完成"
                + patrolExamineExport.getPatrolTargetCountDone() + "个。");
        writer.addText(ParagraphAlignment.LEFT, new Font("方正小标宋简体", Font.BOLD, 14), "二、排水管网");
        writer.addText(ParagraphAlignment.LEFT, new Font("方正小标宋简体", Font.PLAIN, 10), "   当日河道实巡查人数 "
                + patrolExamineExport.getRiverUserCount() + "人，巡查次数"
                + patrolExamineExport.getRiverPatrolTime() + "次，上报事件数量"
                + patrolExamineExport.getCaseCount() + "个（主要是河岸垃圾类问题）。当日智慧水务平台共接收事件"
                + patrolExamineExport.getRiverCaseReceived() + "个，事件来源为"
                + patrolExamineExport.getCaseSource()
                + patrolExamineExport.getCaseSourceCount() + "个，已完成"
                + patrolExamineExport.getCaseFinishCount() + "个。接收事件中：\n 涉及"
                + patrolExamineExport.getPatrolTarget()
                + patrolExamineExport.getPatrolTargetCount() + "个，已完成"
                + patrolExamineExport.getPatrolTargetCountDone() + "个。");
        writer.addText(ParagraphAlignment.LEFT, new Font("方正小标宋简体", Font.PLAIN, 14), "   排水管网未完成事件具体如下：");
        List<List<String>> rowList = new ArrayList<>();
        List<String> colList = new ArrayList<>();
        AtomicInteger i = new AtomicInteger(1);

        colList.add(String.valueOf(i.getAndIncrement()));
        colList.add("问题类型");
        colList.add("巡查项目");
        colList.add("问题描述");
        rowList.add(colList);
        rowList.add(colList);
        rowList.add(colList);
        rowList.add(colList);
        rowList.add(colList);
        rowList.add(colList);
        rowList.add(colList);
        writer.addTable(rowList);
        writer.flush(FileUtil.file(filePath));
        writer.close();
    }

    /**
     * 使用Word07Writer导出巡查日志
     */
    @Test
    public void exportLog() {
        String filePath = "C:\\Users\\Administrator\\Desktop\\移动巡查\\demo2.docx";
        Word07Writer writer = new Word07Writer();
        writer.addText(ParagraphAlignment.LEFT, new Font("方正小标宋简体", Font.BOLD, 15), "巡查记录");
        // 准备数据
        List<List<String>> rowList = new ArrayList<>();
        List<String> data1 = new ArrayList<>();
        data1.add("巡查段名称");
        data1.add("条子河");
        rowList.add(data1);

        List<String> data2 = new ArrayList<>();
        data2.add("巡查名称");
        data2.add("2023-01-14条子河日常巡查");
        rowList.add(data2);

        List<String> data3 = new ArrayList<>();
        data3.add("巡查人");
        data3.add("张山");
        rowList.add(data3);

        List<String> data4 = new ArrayList<>();
        data4.add("巡查时间段");
        data4.add("2023-03-01 12:30 2023-03-01 15:33");
        rowList.add(data4);

        List<String> data5 = new ArrayList<>();
        data5.add("巡查位置");
        data5.add("南河起点-南河终点");
        rowList.add(data5);

        List<String> data6 = new ArrayList<>();
        data6.add("巡查里程");
        data6.add("3km");
        rowList.add(data6);

        List<String> data7 = new ArrayList<>();
        data7.add("巡查描述");
        data7.add("河道巡查发现了问题");
        rowList.add(data7);

        List<String> data8 = new ArrayList<>();
        data8.add("提交状态");
        data8.add("正常提交");
        rowList.add(data8);

        // 插入表格
        writer.addTable(rowList);

        // 插入图片
//        File file = new File("C:\\Users\\Administrator\\Desktop\\移动巡查\\工作台UI图.png");
//        String urlStr = "https://p6.itc.cn/q_70/images01/20220727/1f0b91f068744223823ca585f8ad0446.jpeg";
        String urlStr = "https://p6.itc.cn/q_70/images01/20220727/1f0b91f068744223823ca585f8ad0446.jpeg";
        InputStream fileStream = OfficeUtil.getFile(urlStr);

//        writer.addPicture(file, 200, 100);
        writer.addPicture(fileStream, PicType.JPEG, "demo", 200, 100);

        // 写出文件流
        writer.flush(FileUtil.file(filePath));
        writer.close();
    }

    /**
     * 使用office模块提供的功能导出
     */
    @Test
    public void export() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat(CHINESE_DATE_PATTERN);
        PatrolExamineExport patrolExamineExport = new PatrolExamineExport();
        patrolExamineExport.setTitle("条子河");
        patrolExamineExport.setDate(sdf.format(new Date()));
        patrolExamineExport.setPatrolTarget("条子河");
        patrolExamineExport.setRiverCaseReceived("23");
        patrolExamineExport.setCaseSource("举报热线");
        patrolExamineExport.setCaseSourceCount("324");
        patrolExamineExport.setRiverPatrolTime("23");
        patrolExamineExport.setRiverUserCount("58");
        patrolExamineExport.setPatrolTargetCount("743");
        patrolExamineExport.setCaseCount("50");
        patrolExamineExport.setCaseFinishCount("39");
        patrolExamineExport.setCaseNotFinishCount("587");
        patrolExamineExport.setPatrolTargetCountDone("536");

        List<List<String>> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            List<String> body = new ArrayList<>();
            body.add("问题类型");
            body.add("长安路（鸡公山大街-中山街）");
            body.add("patrolCase");
            list.add(body);
        }
        WordBaseService wordBaseService = new WordBaseService();
        patrolExamineExport.setTableData(list);

        byte[] bytes = wordBaseService.toWordList(patrolExamineExport, templateBasePath);
        if (bytes != null) {
            OfficeUtil.exportWord(bytes, exportFile);
        }
    }

}
