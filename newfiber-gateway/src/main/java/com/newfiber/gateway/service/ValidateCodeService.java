package com.newfiber.gateway.service;

import com.newfiber.common.core.exception.CaptchaException;
import java.io.IOException;

/**
 * 验证码处理
 *
 * @author newfiber
 */
public interface ValidateCodeService{
    /**
     * 生成验证码
     */
    Object createCaptcha() throws IOException, CaptchaException;

    /**
     * 校验验证码
     */
    public void checkCaptcha(String key, String value) throws CaptchaException;
}
