package com.newfiber.gateway.service.impl;

import com.google.code.kaptcha.Producer;
import com.newfiber.common.core.constant.Constants;
import com.newfiber.common.core.constant.HttpStatus;
import com.newfiber.common.core.exception.CaptchaException;
import com.newfiber.common.core.utils.StringUtils;
import com.newfiber.common.core.utils.sign.Base64;
import com.newfiber.common.core.utils.uuid.IdUtils;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.redis.service.RedisService;
import com.newfiber.gateway.config.properties.CaptchaProperties;
import com.newfiber.gateway.enums.EGatewayRedisKey;
import com.newfiber.gateway.service.ValidateCodeService;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;
import javax.imageio.ImageIO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FastByteArrayOutputStream;

/**
 * 验证码实现处理
 *
 * @author newfiber
 */
@Service
public class ValidateCodeServiceImpl implements ValidateCodeService{
    @Resource(name = "captchaProducer")
    private Producer captchaProducer;

    @Resource(name = "captchaProducerMath")
    private Producer captchaProducerMath;

    @Autowired
    private RedisService redisService;

    @Autowired
    private CaptchaProperties captchaProperties;

    /**
     * 生成验证码
     */
    @Override
    public Object createCaptcha() throws IOException, CaptchaException{
        boolean captchaEnabled = captchaProperties.getEnabled();
        if (!captchaEnabled){
            return Result.success();
        }

        // 保存验证码信息
        String uuid = IdUtils.simpleUUID();

        String capStr, code = null;
        BufferedImage image = null;

        String captchaType = captchaProperties.getType();
        // 生成验证码
        if ("math".equals(captchaType)){
            String capText = captchaProducerMath.createText();
            capStr = capText.substring(0, capText.lastIndexOf("@"));
            code = capText.substring(capText.lastIndexOf("@") + 1);
            image = captchaProducerMath.createImage(capStr);
        }
        else if ("char".equals(captchaType)){
            capStr = code = captchaProducer.createText();
            image = captchaProducer.createImage(capStr);
        }

        redisService.setCacheObject(EGatewayRedisKey.LoginCaptchaCodes, uuid, code, Constants.CAPTCHA_EXPIRATION, TimeUnit.MINUTES);
        // 转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        try{
            ImageIO.write(image, "jpg", os);
        }
        catch (IOException e){
            return Result.error(e.getMessage());
        }

	    HashMap<String, Object> ajax = new HashMap<>();
	    ajax.put("code", HttpStatus.SUCCESS);
	    ajax.put("msg", "操作成功");
	    ajax.put("captchaEnabled", captchaEnabled);
	    ajax.put("uuid", uuid);
	    ajax.put("img", Base64.encode(os.toByteArray()));
	    return ajax;
    }

    /**
     * 校验验证码
     */
    @Override
    public void checkCaptcha(String code, String uuid) throws CaptchaException{
        if (StringUtils.isEmpty(code)){
            throw new CaptchaException("验证码不能为空");
        }
        if (StringUtils.isEmpty(uuid)){
            throw new CaptchaException("验证码已失效");
        }
        String captcha = redisService.getCacheObject(EGatewayRedisKey.LoginCaptchaCodes, uuid);
        redisService.deleteObject(EGatewayRedisKey.LoginCaptchaCodes, uuid);

        if (!code.equalsIgnoreCase(captcha)){
            throw new CaptchaException("验证码错误");
        }
    }

}
