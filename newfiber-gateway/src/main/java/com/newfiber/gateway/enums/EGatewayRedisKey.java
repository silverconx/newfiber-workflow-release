package com.newfiber.gateway.enums;

import com.newfiber.common.core.web.support.IRedisPrefix;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author : X.K
 * @since : 2023/2/28 下午3:03
 */
@Getter
@AllArgsConstructor
public enum EGatewayRedisKey implements IRedisPrefix {

	/**
	 *
	 */
	LoginCaptchaCodes("captcha_codes", "登录验证码"),
	Token("login_tokens", "token"),
		;

	private final String moduleKey;

	private final String moduleName;

}
