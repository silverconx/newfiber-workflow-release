package com.newfiber.gateway.handler;

import com.newfiber.common.core.exception.CaptchaException;
import com.newfiber.gateway.service.ValidateCodeService;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.HandlerFunction;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

/**
 * 验证码获取
 *
 * @author newfiber
 */
@Component
public class ValidateCodeHandler implements HandlerFunction<ServerResponse>{
    @Autowired
    private ValidateCodeService validateCodeService;

    @Override
    public Mono<ServerResponse> handle(ServerRequest serverRequest){
	    Object ajax;
        try{
            ajax = validateCodeService.createCaptcha();
        }
        catch (CaptchaException | IOException e){
            return Mono.error(e);
        }
        return ServerResponse.status(HttpStatus.OK).body(BodyInserters.fromValue(ajax));
    }
}
