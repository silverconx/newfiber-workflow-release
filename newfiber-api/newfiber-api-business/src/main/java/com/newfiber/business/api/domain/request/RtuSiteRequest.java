package com.newfiber.business.api.domain.request;

import lombok.Data;

@Data
public class RtuSiteRequest {

    /**
     *  监测对象类型（河道 river | 湖泊 lake | 管网 pipeline | 港渠 channel |内涝 waterlogging | 典型地块 typical_land）
     * */
    private String type;
}
