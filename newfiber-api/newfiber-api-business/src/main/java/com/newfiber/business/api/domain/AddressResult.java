package com.newfiber.business.api.domain;

import lombok.Data;

/**
 * 获取地址详细信息
 * date: 2023/4/6 下午 07:23
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
public class AddressResult {
    private String status;
    private Regeocode regeocode;
    private String info;
    private String infocode;
}
