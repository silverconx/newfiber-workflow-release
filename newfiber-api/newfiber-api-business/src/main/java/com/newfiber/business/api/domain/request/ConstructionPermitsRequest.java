package com.newfiber.business.api.domain.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class ConstructionPermitsRequest implements Serializable {

    private String projectNo;

    public ConstructionPermitsRequest(){

    }

    public ConstructionPermitsRequest(String projectNo) {
        this.projectNo = projectNo;
    }
}
