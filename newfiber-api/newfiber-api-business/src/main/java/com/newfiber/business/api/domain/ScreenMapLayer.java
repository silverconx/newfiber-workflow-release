package com.newfiber.business.api.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 */
@Data
@ApiModel(value = "大屏图层图例")
@AllArgsConstructor
@NoArgsConstructor
public class ScreenMapLayer implements Serializable {

    @ApiModelProperty(value = "id")
    private Long id ;

    @ApiModelProperty(value = "编码")
    private String code ;

    @ApiModelProperty(value = "名称")
    private String name ;

    @ApiModelProperty(value = "类型")
    private String layerType;

	@ApiModelProperty(value = "类型")
	private String layerName;

    @ApiModelProperty(value = "经度")
    private String lon ;

    @ApiModelProperty(value = "纬度")
    private String lat ;

	@ApiModelProperty(value = "图标")
    private String iconUrl;

    @ApiModelProperty(value = "geometrys")
    private String geometrys ;

	/**
	 * 在线状态（online 在线 | offline离线）
	 */
	@ApiModelProperty(value = "在线状态（online 在线 | offline 离线）")
	private String rtuOnlineStatus;

	/**
	 * 故障状态（正常 normal | 低电压 low_battery  | 低信号 low_signal | 异常值 exception | 离线 offline）
	 */
	@ApiModelProperty(value = "故障状态（正常 normal | 低电压 low_battery  | 低信号 low_signal | 异常值 exception | 离线 offline）")
	private String rtuFaultStatus;

    @ApiModelProperty(value = "实时监测数据")
    private Map<String,Object> datas ;

	public ScreenMapLayer(String layerType, String layerName) {
		this.layerType = layerType;
		this.layerName = layerName;
	}
}
