package com.newfiber.business.api;

import com.newfiber.business.api.domain.ProjectEcharts;
import com.newfiber.business.api.domain.ProjectTypeStatistics;
import com.newfiber.business.api.domain.RemoteProjectInfo;
import com.newfiber.business.api.domain.request.ConstructionPermitsRequest;
import com.newfiber.business.api.factory.RemoteProjectConstructionPermitsFallbackFactory;
import com.newfiber.common.core.constant.ServiceNameConstants;
import com.newfiber.common.core.web.domain.Result;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@FeignClient(contextId = "remoteProjectConstructionPermitsService", value = ServiceNameConstants.BUSINESS_SERVICE, fallbackFactory = RemoteProjectConstructionPermitsFallbackFactory.class)

public interface RemoteProjectConstructionPermitsService {


    /**
     * 查询工程信息
     * date: 2023/4/7 下午 02:17
     *
     * @author: LuFan
     * @since JDK 1.8
     */
    @PostMapping(value = "/projectConstructionPermits/continueExecuteWorkFlowByProjectNo")
    Result<Boolean> continueExecuteWorkFlowByProjectNo(@RequestBody ConstructionPermitsRequest constructionPermitsRequest);


    /**
     * 获取项目开工/完工情况echarts数据
     * date: 2023/9/14 下午 05:33
     * @author: 张鸿志
     * @since JDK 1.8
     */
    @GetMapping("/projectInfoNew/getProjectBuildStatus/{year}")
    Result<List<ProjectTypeStatistics>> getProjectBuildStatus(@PathVariable("year") String year);


    /**
     * 获取项目的echarts图-  供海绵绩效考核调用
     * date: 2023/9/14 下午 05:33
     * @author: 张鸿志
     * @since JDK 1.8
     */
    @GetMapping("/projectInfoNew/selectProjectEcharts")
    Result<Map<String,List<ProjectEcharts>>> selectProjectEcharts();


    /**
     * 增加项目预审次数
     * date: 2023/10/10 下午 04:42
     * @author: 张鸿志
     * @since JDK 1.8
     */
    @PostMapping(value = "/projectInfoNew/addInquiryCountByProjectNo")
    Result<Boolean> addInquiryCountByProjectNo(@RequestParam(name = "projectNo") String projectNo);


}
