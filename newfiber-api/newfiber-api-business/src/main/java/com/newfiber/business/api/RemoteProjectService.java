package com.newfiber.business.api;

import com.newfiber.business.api.domain.RemoteProjectInfo;
import com.newfiber.business.api.factory.RemoteProjectFallbackFactory;
import com.newfiber.common.core.constant.ServiceNameConstants;
import com.newfiber.common.core.web.domain.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Map;

/**
 * 工程服务
 * date: 2023/4/7 下午 02:02
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@FeignClient(contextId = "remoteProjectService", value = ServiceNameConstants.BUSINESS_SERVICE, fallbackFactory = RemoteProjectFallbackFactory.class)
public interface RemoteProjectService {

    /**
     * 查询工程信息
     * date: 2023/4/7 下午 02:17
     *
     * @author: LuFan
     * @since JDK 1.8
     */
    @PostMapping(value = "/projectInfoNew/projectList")
    List<RemoteProjectInfo> getProjectList();
}
