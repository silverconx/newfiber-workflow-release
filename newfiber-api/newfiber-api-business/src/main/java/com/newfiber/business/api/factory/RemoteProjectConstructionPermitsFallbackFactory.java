package com.newfiber.business.api.factory;

import com.newfiber.business.api.RemoteProjectConstructionPermitsService;
import com.newfiber.business.api.domain.ProjectEcharts;
import com.newfiber.business.api.domain.ProjectTypeStatistics;
import com.newfiber.business.api.domain.request.ConstructionPermitsRequest;
import com.newfiber.common.core.web.domain.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class RemoteProjectConstructionPermitsFallbackFactory implements FallbackFactory<RemoteProjectConstructionPermitsService> {
    @Override
    public RemoteProjectConstructionPermitsService create(Throwable throwable) {
        log.error("海绵服务调用失败:{}", throwable.getMessage());
        return new RemoteProjectConstructionPermitsService() {
            @Override
            public Result<Boolean> continueExecuteWorkFlowByProjectNo(ConstructionPermitsRequest constructionPermitsRequest) {
                log.error("调用项目施工许可审批流程失败！:{}", throwable.getMessage());
                return null;
            }

            @Override
            public Result<List<ProjectTypeStatistics>> getProjectBuildStatus(String year) {
                log.error("获取项目开工/完工情况echarts数据失败！:{}", throwable.getMessage());
                return null;
            }

            @Override
            public Result<Map<String, List<ProjectEcharts>>> selectProjectEcharts() {
                log.error("获取项目的echarts图失败！:{}", throwable.getMessage());
                return null;
            }

            @Override
            public Result<Boolean> addInquiryCountByProjectNo(String projectNo) {
                log.error("增加项目编号为【"+projectNo+"】预审次数失败！:{}", throwable.getMessage());
                return null;
            }
        };
    }
}
