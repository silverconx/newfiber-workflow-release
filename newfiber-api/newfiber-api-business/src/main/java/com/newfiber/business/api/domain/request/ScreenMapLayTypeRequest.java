package com.newfiber.business.api.domain.request;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author : X.K
 * @since : 2023/10/7 上午10:47
 */
@Data
@AllArgsConstructor
public class ScreenMapLayTypeRequest {

	private final String layerType;

	private final String layerName;

	private final String businessType;

}
