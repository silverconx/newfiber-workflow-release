package com.newfiber.business.api.factory;

import com.newfiber.business.api.RemoteRtuSiteInfoService;
import com.newfiber.business.api.domain.RemoteRtuSiteInfo;
import com.newfiber.business.api.domain.request.RemoteWaterLoggingRecordInfo;
import com.newfiber.business.api.domain.request.RtuSiteRequest;
import com.newfiber.business.api.domain.request.WaterLoggingRequest;
import com.newfiber.common.core.web.page.PageResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Component
public class RemoteRtuSiteInfoFallbackFactory implements FallbackFactory<RemoteRtuSiteInfoService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteRtuSiteInfoFallbackFactory.class);

    @Override
    public RemoteRtuSiteInfoService create(Throwable throwable){
        log.error("Rtu站点信息调用失败:{}", throwable.getMessage());

        return new RemoteRtuSiteInfoService(){
            @Override
            public List<RemoteRtuSiteInfo> getRtuSiteList(@RequestBody RtuSiteRequest request) {
                log.error("获取Rtu站点信息失败:{}", throwable.getMessage());
                return null;
            }

            @Override
            public PageResult<List<RemoteWaterLoggingRecordInfo>> queryRecords(@RequestBody WaterLoggingRequest request) {
                log.error("获取渍水信息失败:{}", throwable.getMessage());
                return null;
            }
        };
    }
}
