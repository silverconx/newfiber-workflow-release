package com.newfiber.business.api.factory;

import com.newfiber.business.api.RemoteAddressService;
import com.newfiber.business.api.domain.AddressResult;
import com.newfiber.common.core.web.domain.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class RemoteAddressFallbackFactory implements FallbackFactory<RemoteAddressService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteAddressFallbackFactory.class);

    @Override
    public RemoteAddressService create(Throwable throwable){
        log.error("地址服务调用失败:{}", throwable.getMessage());

        return new RemoteAddressService(){
            @Override
            public Result<AddressResult> getAddressByLonLat(String lonLat){
                return Result.error("获取地址信息失败:" + throwable.getMessage());
            }
        };
    }
}
