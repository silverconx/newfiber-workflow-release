package com.newfiber.business.api;

import com.newfiber.business.api.domain.AddressResult;
import com.newfiber.business.api.factory.RemoteAddressFallbackFactory;
import com.newfiber.common.core.constant.ServiceNameConstants;
import com.newfiber.common.core.web.domain.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * 地址服务
 * date: 2023/4/7 下午 02:02
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@FeignClient(contextId = "remoteAddressService", value = ServiceNameConstants.BUSINESS_SERVICE, fallbackFactory = RemoteAddressFallbackFactory.class)
public interface RemoteAddressService {

    /**
     * 根据经纬度查地址信息
     * date: 2023/4/7 下午 02:17
     *
     * @author: LuFan
     * @since JDK 1.8
     */
    @GetMapping(value = "/address/getAddressByLonLat/{lonLat}")
    public Result<AddressResult> getAddressByLonLat(@PathVariable("lonLat") String lonLat);
}
