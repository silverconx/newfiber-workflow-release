package com.newfiber.business.api.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RemoteProjectInfo  {

    private Long id;
    private String projectNo;
    private String projectName;
    private Integer projectStatus;
    private Date completeTime;

/*    public RemoteProjectInfo(String projectNo, String projectName,Integer projectStatus, Date completeTime) {
        this.projectNo = projectNo;
        this.projectName = projectName;
        this.projectStatus = projectStatus;
        this.completeTime = completeTime;
    }*/
}
