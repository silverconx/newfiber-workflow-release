package com.newfiber.business.api.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
public class ProjectEcharts implements Serializable {


    @ApiModelProperty(value = "年份")
    private String year;

    @ApiModelProperty(value = "第一个数据")
    private Integer oneCount;

    @ApiModelProperty(value = "资金总数")
    private BigDecimal totalInvest;

//    @ApiModelProperty(value = "第二个数据")
//    private Integer twoCount;

//    @ApiModelProperty(value = "百分比")
//    private Double brokenLine;


    @ApiModelProperty(value = "项目信息数据")
    private List<SimpleProjectInfo> simpleProjectInfoList;


}
