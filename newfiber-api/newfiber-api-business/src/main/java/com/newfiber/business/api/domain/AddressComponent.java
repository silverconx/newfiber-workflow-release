package com.newfiber.business.api.domain;

import lombok.Data;

/**
 * 详细地区信息
 *
 * date: 2023/4/6 下午 07:24
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
public class AddressComponent {
    private String city;
    private String province;
    private String adcode;
    private String district;
    private String towncode;
    private String streetNumber;
    private String country;
    private String township;
    private String businessAreas;
    private String building;
    private String neighborhood;
    private String citycode;
}
