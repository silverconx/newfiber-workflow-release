package com.newfiber.business.api.domain.request;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class WaterLoggingRequest {

    /**
     *  持续时长
     * */
    private Integer durationTime;

    /**
     *  最小水位
     * */
    private BigDecimal minLevel;

    /**
     *  年份
     * */
    private String year;

    /**
     *  年份
     * */
    private String stCode;
}
