package com.newfiber.business.api.domain;

import lombok.Data;

/**
 * 地区信息
 *
 * date: 2023/4/6 下午 07:23
 * @author: LuFan
 * @since JDK 1.8
 */
@Data
public class Regeocode {
    private AddressComponent AddressComponent;
    private String formatted_address;
}
