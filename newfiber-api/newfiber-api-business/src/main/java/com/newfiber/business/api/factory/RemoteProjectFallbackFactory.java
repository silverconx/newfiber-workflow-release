package com.newfiber.business.api.factory;

import com.newfiber.business.api.RemoteProjectService;
import com.newfiber.business.api.domain.RemoteProjectInfo;
import com.newfiber.common.core.web.domain.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class RemoteProjectFallbackFactory implements FallbackFactory<RemoteProjectService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteProjectFallbackFactory.class);

    @Override
    public RemoteProjectService create(Throwable throwable){
        log.error("工程项目调用失败:{}", throwable.getMessage());

        return new RemoteProjectService(){
            @Override
            public List<RemoteProjectInfo> getProjectList(){
                log.error("获取工程项目信息失败:{}", throwable.getMessage());
                return null;
            }
        };
    }
}
