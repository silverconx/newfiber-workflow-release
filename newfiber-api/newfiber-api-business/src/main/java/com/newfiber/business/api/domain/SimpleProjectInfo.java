package com.newfiber.business.api.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SimpleProjectInfo implements Serializable {

    @ApiModelProperty(value = "项目id")
    private Long id;


    @ApiModelProperty(value = "项目编号")
    private String projectNo;

    @ApiModelProperty(value = "项目名称")
    private String projectName;

    @ApiModelProperty(value = "项目位置")
    private String projectLocation;
}
