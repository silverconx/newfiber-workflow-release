package com.newfiber.business.api.factory;

import com.newfiber.business.api.RemoteDocumentService;
import com.newfiber.common.core.web.domain.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;


@Component
public class RemoteDocumentServiceFallbackFactory  implements FallbackFactory<RemoteDocumentService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteDocumentServiceFallbackFactory.class);

    @Override
    public RemoteDocumentService create(Throwable throwable) {
        log.error("同步文件信息失败:{}", throwable.getMessage());

        return new RemoteDocumentService(){
            @Override
            public Result<Boolean> saveDocumentFiles(String refType, String refField, Long refId) {
                log.error("同步文件信息失败:{}", throwable.getMessage());
                return null;
            }
        };
    }
}
