package com.newfiber.business.api.service;

import com.newfiber.business.api.domain.ScreenMapLayer;
import com.newfiber.business.api.domain.request.ScreenMapLayTypeRequest;
import java.util.List;

/**
 * 大屏图层
 */
public interface IScreenMapLayerService {

	/**
	 * 解析图层数据
	 */
	List<ScreenMapLayer> parseScreenMapLayer(List<ScreenMapLayTypeRequest> mapLayTypeRequestList);

}
