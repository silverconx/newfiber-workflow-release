package com.newfiber.business.api;

import com.newfiber.business.api.factory.RemoteDocumentServiceFallbackFactory;
import com.newfiber.common.core.constant.ServiceNameConstants;
import com.newfiber.common.core.web.domain.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 工程服务
 * date: 2023/4/7 下午 02:02
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@FeignClient(contextId = "remoteDocumentService", value = ServiceNameConstants.BUSINESS_SERVICE, fallbackFactory = RemoteDocumentServiceFallbackFactory.class)
public interface RemoteDocumentService {

    /**
     * 保存内置文件
     *
     * @return 结果
     */
    @PostMapping(value = "/documentFile/saveDocumentFiles")
    Result<Boolean> saveDocumentFiles(@RequestParam("refType") String refType, @RequestParam("refField") String refField, @RequestParam("refId") Long refId);
}
