package com.newfiber.business.api.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RemoteRtuSiteInfo {

    private String stCode;
    private String stName;
    private String startTime;
}
