package com.newfiber.business.api.domain.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RemoteWaterLoggingRecordInfo {

    /**
     *  站点编码
     */
    @ApiModelProperty(value = "站点编码")
    private String stCode;

    /**
     *  站点编码
     */
    @ApiModelProperty(value = "站点编码")
    private String stName;

    /**
     *  内涝开始时间
     */
    @ApiModelProperty(value = "内涝开始时间")
    private Date startTime;

    /**
     *  内涝结束时间
     */
    @ApiModelProperty(value = "内涝结束时间")
    private Date endTime;

    /**
     *  持续时长
     */
    @ApiModelProperty(value = "持续时长")
    private Integer durationTime;

    /**
     *  最大水深
     */
    @ApiModelProperty(value = "最大水深")
    private BigDecimal maxLevel;

    /**
     *  最大水深
     */
    @ApiModelProperty(value = "最小水深")
    private BigDecimal minLevel;
}
