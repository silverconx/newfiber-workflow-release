package com.newfiber.business.api;

import com.newfiber.business.api.domain.RemoteRtuSiteInfo;
import com.newfiber.business.api.domain.request.RemoteWaterLoggingRecordInfo;
import com.newfiber.business.api.domain.request.RtuSiteRequest;
import com.newfiber.business.api.domain.request.WaterLoggingRequest;
import com.newfiber.business.api.factory.RemoteProjectFallbackFactory;
import com.newfiber.business.api.factory.RemoteRtuSiteInfoFallbackFactory;
import com.newfiber.common.core.constant.ServiceNameConstants;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.common.core.web.page.PageResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * Rtu站点信息查询
 * date: 2023/4/7 下午 02:02
 *
 * @author: LuFan
 * @since JDK 1.8
 */
@FeignClient(contextId = "remoteRtuSiteInfoService", value = ServiceNameConstants.BUSINESS_SERVICE, fallbackFactory = RemoteRtuSiteInfoFallbackFactory.class)
public interface RemoteRtuSiteInfoService {

    /**
     * Rtu站点信息查询
     * date: 2023/4/7 下午 02:17
     *
     * @author: LuFan
     * @since JDK 1.8
     */
    @PostMapping(value = "/rtuSiteInfo/queryByTargetType")
    List<RemoteRtuSiteInfo> getRtuSiteList(@RequestBody RtuSiteRequest request);

    /**
     * 内涝点记录查询
     * @since JDK 1.8
     */
    @PostMapping(value = "/waterloggingRecord/queryRecords")
    PageResult<List<RemoteWaterLoggingRecordInfo>> queryRecords(@RequestBody WaterLoggingRequest request);
}
