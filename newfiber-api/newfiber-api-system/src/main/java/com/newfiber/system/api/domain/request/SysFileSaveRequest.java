package com.newfiber.system.api.domain.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author : X.K
 * @since : 2023/3/1 上午11:12
 */
@Data
public class SysFileSaveRequest {

	/**
	 * 关联类型
	 */
	@ApiModelProperty(value = "关联类型")
	private String refType;

	/**
	 * 关联字段
	 */
	@ApiModelProperty(value = "关联字段")
	private String refField;

	/**
	 * 文件名称
	 */
	@ApiModelProperty(value = "文件名称")
	private String name;

	/**
	 *  名称（带扩展名）
	 */
	@ApiModelProperty(value = "名称（带扩展名）")
	private String originalName;

	/**
	 *  扩展名
	 */
	@ApiModelProperty(value = "扩展名")
	private String extension;

	/**
	 * 文件地址
	 */
	@ApiModelProperty(value = "文件地址")
	private String url;

	/**
	 *  大小
	 */
	@ApiModelProperty(value = "大小")
	private Long size;
}
