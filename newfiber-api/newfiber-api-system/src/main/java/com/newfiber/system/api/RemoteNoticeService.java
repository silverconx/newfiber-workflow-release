package com.newfiber.system.api;

import com.newfiber.common.core.constant.ServiceNameConstants;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.system.api.domain.SysNotice;
import com.newfiber.system.api.factory.RemoteNoticeFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 * @author newfiber
 */
@FeignClient(contextId = "remoteNoticeService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteNoticeFallbackFactory.class)
public interface RemoteNoticeService {

	/**
	 *
	 */
	@PostMapping("/notice/add")
	Result<String> noticeAdd(@RequestBody SysNotice sysNotice);

}
