package com.newfiber.system.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author : X.K
 * @since : 2023/2/28 下午3:03
 */
@Getter
@AllArgsConstructor
public enum ENoticeRefType {

	/**
	 *
	 */
	RtuMonitorDataExport("RtuMonitorDataExport", "", "RTU监测数据导出"),

		;

	private final String refType;

	private final String refField;

	private final String refName;

}
