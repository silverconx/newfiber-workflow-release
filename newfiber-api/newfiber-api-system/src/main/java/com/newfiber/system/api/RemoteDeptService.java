package com.newfiber.system.api;

import com.newfiber.common.core.constant.SecurityConstants;
import com.newfiber.common.core.constant.ServiceNameConstants;
import com.newfiber.common.core.domain.R;
import com.newfiber.system.api.domain.SysDept;
import com.newfiber.system.api.factory.RemoteDeptFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(contextId = "remoteDeptService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteDeptFallbackFactory.class)
public interface RemoteDeptService {

    /**
     * Title:通过部门id 集合获取对应的部门信息数据
     * @param deptIds
     * @param source
     * @return
     */
    @PostMapping("/dept/by/ids")
    public R<List<SysDept>> getDeptByIds(@RequestBody List<Long> deptIds, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * Title: 获取部门列表
     * @return
     */
    @GetMapping("/dept/getList")
    public R<List<SysDept>> getDeptList();
}


