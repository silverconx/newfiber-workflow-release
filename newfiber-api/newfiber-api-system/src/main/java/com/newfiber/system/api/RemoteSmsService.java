package com.newfiber.system.api;

import com.newfiber.common.core.constant.ServiceNameConstants;
import com.newfiber.common.core.domain.R;
import com.newfiber.system.api.factory.RemoteUserFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 短信服务
 * 
 * @author newfiber
 */
@FeignClient(contextId = "remoteSmsService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteUserFallbackFactory.class)
public interface RemoteSmsService {

	/**
	 * 发送信息
	 * @param code 短信配置编号
	 * @param params 自定义短信参数
	 * @param phones 手机号集合
	 */
    @PostMapping("/sms/endpoint/send-message")
    R<String> sendMessage(@RequestParam("code") String code, @RequestParam("params") String params, @RequestParam("phones") String phones);
}
