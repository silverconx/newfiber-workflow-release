package com.newfiber.system.api.factory;

import com.newfiber.common.core.web.domain.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import com.newfiber.common.core.domain.R;
import com.newfiber.system.api.RemoteLogService;
import com.newfiber.system.api.domain.SysLogininfor;
import com.newfiber.system.api.domain.SysOperLog;

import java.util.List;

/**
 * 日志服务降级处理
 * 
 * @author newfiber
 */
@Component
public class RemoteLogFallbackFactory implements FallbackFactory<RemoteLogService>{
    private static final Logger log = LoggerFactory.getLogger(RemoteLogFallbackFactory.class);

    @Override
    public RemoteLogService create(Throwable throwable){
        log.error("日志服务调用失败:{}", throwable.getMessage());
        return new RemoteLogService(){
            @Override
            public Result saveLog(SysOperLog sysOperLog, String source){
                return null;
            }

            @Override
            public List<SysOperLog> queryList(SysOperLog sysOperLog, String source) {
                return null;
            }

            @Override
            public R<Boolean> saveLogininfor(SysLogininfor sysLogininfor, String source){
                return null;
            }
        };

    }
}
