package com.newfiber.system.api;

import com.newfiber.common.core.web.domain.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import com.newfiber.common.core.constant.SecurityConstants;
import com.newfiber.common.core.constant.ServiceNameConstants;
import com.newfiber.common.core.domain.R;
import com.newfiber.system.api.domain.SysLogininfor;
import com.newfiber.system.api.domain.SysOperLog;
import com.newfiber.system.api.factory.RemoteLogFallbackFactory;

import java.util.List;

/**
 * 日志服务
 * 
 * @author newfiber
 */
@FeignClient(contextId = "remoteLogService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteLogFallbackFactory.class)
public interface RemoteLogService{
    /**
     * 保存系统日志
     *
     * @param sysOperLog 日志实体
     * @param source 请求来源
     * @return 结果
     */
    @PostMapping("/operlog")
    public Result saveLog(@RequestBody SysOperLog sysOperLog, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 查询操作日志
     * @return 结果
     */
    @PostMapping("/queryList")
    public List<SysOperLog> queryList(@RequestBody SysOperLog sysOperLog, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 保存访问记录
     *
     * @param sysLogininfor 访问实体
     * @param source 请求来源
     * @return 结果
     */
    @PostMapping("/logininfor")
    public R<Boolean> saveLogininfor(@RequestBody SysLogininfor sysLogininfor, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
