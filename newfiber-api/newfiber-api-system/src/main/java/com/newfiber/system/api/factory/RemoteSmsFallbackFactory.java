package com.newfiber.system.api.factory;

import com.newfiber.common.core.domain.R;
import com.newfiber.system.api.RemoteSmsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * 短信服务降级处理
 * 
 * @author newfiber
 */
@Component
public class RemoteSmsFallbackFactory implements FallbackFactory<RemoteSmsService>{
    private static final Logger log = LoggerFactory.getLogger(RemoteSmsService.class);

    @Override
    public RemoteSmsService create(Throwable throwable){
        log.error("短信服务调用失败:{}", throwable.getMessage());
        return new RemoteSmsService(){
	        @Override
	        public R<String> sendMessage(String code, String params, String phones) {
		        return R.fail("发送短信失败:" + throwable.getMessage());
	        }
        };
    }
}
