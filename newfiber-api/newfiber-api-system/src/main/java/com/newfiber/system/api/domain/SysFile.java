package com.newfiber.system.api.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.newfiber.common.core.web.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 文件信息
 * 
 * @author newfiber
 */
@Data
@TableName("sys_file")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "附件", description = "附件")
public class SysFile extends BaseEntity {

	/**
	 *  关联类型
	 */
	@ApiModelProperty(value = "关联类型")
	private String refType;

	/**
	 *  关联字段
	 */
	@ApiModelProperty(value = "关联字段")
	private String refField;

	/**
	 *  关联编号
	 */
	@ApiModelProperty(value = "关联编号")
	private Long refId;

	/**
     * 文件名称
     */
	@ApiModelProperty(value = "文件名称")
    private String name;

	/**
	 *  名称（带扩展名）
	 */
	@ApiModelProperty(value = "名称（带扩展名）")
	private String originalName;

	/**
	 *  扩展名
	 */
	@ApiModelProperty(value = "扩展名")
	private String extension;

	/**
	 * 文件地址
	 */
	@ApiModelProperty(value = "文件地址")
	private String url;

	/**
	 *  大小
	 */
	@ApiModelProperty(value = "大小")
	private Long size;
}
