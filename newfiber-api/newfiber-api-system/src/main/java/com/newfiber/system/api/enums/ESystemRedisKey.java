package com.newfiber.system.api.enums;

import com.newfiber.common.core.web.support.IRedisPrefix;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author : X.K
 * @since : 2023/2/28 下午3:03
 */
@Getter
@AllArgsConstructor
public enum ESystemRedisKey implements IRedisPrefix {

	/**
	 *
	 */
	Dict("sys_dict", "数据字典"),
	Config("sys_config", "系统配置"),
	Token("login_tokens", "token"),
	LoginCaptchaCodes("captcha_codes", "登录验证码"),
	PwdErrCnt("pwd_err_cnt", "登录错误次数"),
	SmsCaptcha("sms_captcha", "短信"),
	User("sys_users", "用户"),
	USER_NAME("sys_user_names","用户名称")

		;

	private final String moduleKey;

	private final String moduleName;

}
