package com.newfiber.system.api.factory;

import com.newfiber.common.core.web.domain.Result;
import com.newfiber.system.api.RemoteNoticeService;
import com.newfiber.system.api.domain.SysNotice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author newfiber
 */
@Component
public class RemoteNoticeFallbackFactory implements FallbackFactory<RemoteNoticeService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteNoticeFallbackFactory.class);

    @Override
    public RemoteNoticeService create(Throwable throwable) {
        log.error("通知服务调用失败:{}", throwable.getMessage());
        return new RemoteNoticeService() {
	        @Override
	        public Result<String> noticeAdd(SysNotice sysNotice) {
		        return Result.error("发送通知失败:" + throwable.getMessage());
	        }
        };
    }
}
