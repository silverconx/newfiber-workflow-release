package com.newfiber.system.api.factory;

import com.newfiber.common.core.domain.R;
import com.newfiber.system.api.RemoteDeptService;
import com.newfiber.system.api.domain.SysDept;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RemoteDeptFallbackFactory implements FallbackFactory<RemoteDeptService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteDeptFallbackFactory.class);

    @Override
    public RemoteDeptService create(Throwable throwable) {
        log.error("部门服务调用失败:{}", throwable.getMessage());

        return new RemoteDeptService(){
            @Override
            public R<List<SysDept>> getDeptByIds(List<Long> deptIds, String source){
                return R.fail("获取部门信息失败:" + throwable.getMessage());
            }

            @Override
            public R<List<SysDept>> getDeptList() {
                return R.fail("获取部门列表失败:" + throwable.getMessage());
            }

        };
    }
}
