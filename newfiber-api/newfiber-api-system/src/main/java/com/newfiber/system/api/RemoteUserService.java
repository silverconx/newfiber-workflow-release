package com.newfiber.system.api;

import com.newfiber.common.core.web.domain.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.*;
import com.newfiber.common.core.constant.SecurityConstants;
import com.newfiber.common.core.constant.ServiceNameConstants;
import com.newfiber.common.core.domain.R;
import com.newfiber.system.api.domain.SysUser;
import com.newfiber.system.api.factory.RemoteUserFallbackFactory;
import com.newfiber.system.api.model.LoginUser;

import java.util.List;

/**
 * 用户服务
 *
 * @author newfiber
 */
@FeignClient(contextId = "remoteUserService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteUserFallbackFactory.class)
public interface RemoteUserService {
    /**
     * 通过用户名查询用户信息
     *
     * @param username 用户名
     * @param source   请求来源
     * @return 结果
     */
    @GetMapping("/user/info/{username}")
    public R<LoginUser> getUserInfo(@PathVariable("username") String username, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 注册用户信息
     *
     * @param sysUser 用户信息
     * @param source  请求来源
     * @return 结果
     */
    @PostMapping("/user/register")
    public R<Boolean> registerUserInfo(@RequestBody SysUser sysUser, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);


    /**
     * 新增用户信息
     *
     * @param sysUser 用户信息
     * @param source  请求来源
     * @return 结果
     */

    @PostMapping("/user/add")
    public Result add(@RequestBody SysUser sysUser, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);

    /**
     * 获取用户信息
     *
     * @param userIds   用户对象
     * @param userNames 用户对象
     * @param source    请求来源
     * @return 结果
     */
    @GetMapping("/user/list")
    public Result<List<SysUser>> getUserInfoList(@RequestParam("userIds") Long[] userIds, @RequestParam("userNames") String[] userNames
            , @RequestHeader(SecurityConstants.FROM_SOURCE) String source, @RequestParam("deptIds") Long[] deptIds);

}
