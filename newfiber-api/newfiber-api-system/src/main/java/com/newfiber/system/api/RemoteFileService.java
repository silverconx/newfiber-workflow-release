package com.newfiber.system.api;

import com.newfiber.common.core.constant.ServiceNameConstants;
import com.newfiber.common.core.domain.R;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.system.api.domain.SysFile;
import com.newfiber.system.api.domain.request.SysFileSaveRequest;
import com.newfiber.system.api.factory.RemoteFileFallbackFactory;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件服务
 *
 * @author newfiber
 */
@FeignClient(contextId = "remoteFileService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteFileFallbackFactory.class)
public interface RemoteFileService {
    /**
     * 上传文件
     *
     * @param file 文件信息
     * @return 结果
     */
    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    R<SysFile> upload(@RequestPart(value = "file") MultipartFile file);

//	/**
//	 * 上传文件
//	 *
//	 * @param file 文件信息
//	 * @return 结果
//	 */
//	@PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
//	R<SysFile> upload(IFileRefType fileRefType, Long refId, @RequestPart(value = "file") MultipartFile file);

    /**
     * 新增文件
     *
     * @return 结果
     */
    @PostMapping(value = "/add")
    Result<Long> add(@RequestParam("refId") Long refId, @RequestBody SysFileSaveRequest request);

    /**
     * 批量新增文件
     *
     * @return 结果
     */
    @PostMapping(value = "/addBatch")
    Result<Boolean> addBatch(@RequestParam("refId") Long refId, @RequestBody List<SysFileSaveRequest> request);

    /**
     * 批量新增文件
     *
     * @return 结果
     */
    @PostMapping(value = "/addBatchByType")
    Result<Boolean> addBatch(@RequestParam("refType") String refType, @RequestParam("refId") Long refId, @RequestBody List<SysFileSaveRequest> request);

    /**
     * 更新附件
     *
     * @return 结果
     */
    @PostMapping(value = "/update")
    Result<Boolean> update(@RequestParam("refType") String refType, @RequestParam("refId") Long refId, @RequestBody List<SysFileSaveRequest> request);

    /**
     * 列表查询文件
     *
     * @return 结果
     */
    @GetMapping(value = "/listByRef/{refId}")
    Result<List<SysFile>> list(@PathVariable("refId") Long id);

    @GetMapping(value = "/selectByRefIds/{refIds}")
    @ApiOperation(value = "根据关联主键查询附件",position =  110)
    Result<List<SysFile>> selectByRefIds(@PathVariable("refIds") String refIds);

}
