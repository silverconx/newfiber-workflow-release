package com.newfiber.system.api.domain;

import static cn.hutool.core.date.DatePattern.NORM_DATETIME_PATTERN;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.newfiber.common.core.web.domain.BaseEntity;
import com.newfiber.common.core.xss.Xss;
import com.newfiber.system.api.enums.ENoticeRefType;
import com.newfiber.system.api.enums.ENoticeType;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 通知公告表 sys_notice
 * 
 * @author newfiber
 */
@Data
public class SysNotice extends BaseEntity{
    private static final long serialVersionUID = 1L;

	@JsonSerialize(using = ToStringSerializer.class)
	@ApiModelProperty(value = "主键id")
	@TableId(value = "notice_id", type = IdType.AUTO)
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private Long id;

    /** 公告ID */
    private Long noticeId;

    /** 关联类型 */
    private String refType;

	/** 关联类型 */
	private String refField;

    /** 关联编号 */
    private Long refId;

    /** 公告标题 */
    private String noticeTitle;

    /** 公告类型（1通知 2公告） */
    private String noticeType;

    /** 公告内容 */
    private String noticeContent;

    /**
     * 浏览量
     */
    private Integer readCount;

    /**
     * 已读人员数量
     */
    private Integer readPersonCount;

    /**
     * 未读人员数量
     */
    private Integer unreadPersonCount;

	/** 附件地址 */
	private String fileInfoJson;

    /** 公告状态（0正常 1关闭） */
    private String status;


    // DB Property

	/**
	 * 通知人
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "通知人")
	private String noticePerson;

	/**
	 * 是否已读（0未读 1已读)
	 */
	@TableField(exist = false)
	@ApiModelProperty(value = "是否已读（0未读 1已读)")
	private String readStatus;

	/** 创建时间 */
	@TableField(exist = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
	private Date createTimeStart;

	/** 创建时间 */
	@TableField(exist = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = NORM_DATETIME_PATTERN)
	private Date createTimeEnd;

	@TableField(exist = false)
	private SysFile fileInfo;

    public static SysNotice build(ENoticeRefType noticeRefType, String noticeTitle, ENoticeType noticeType, String noticeContent){
	    SysNotice sysNotice = new SysNotice();
	    sysNotice.setRefType(noticeRefType.getRefType());
	    sysNotice.setRefField(noticeRefType.getRefField());
	    sysNotice.setNoticeTitle(noticeTitle);
	    sysNotice.setNoticeType(noticeType.getType());
	    sysNotice.setNoticeContent(noticeContent);
	    return sysNotice;
    }

    public SysNotice() {
    }

    public Long getNoticeId(){
        return noticeId;
    }

    public void setNoticeId(Long noticeId){
        this.noticeId = noticeId;
    }

    public void setNoticeTitle(String noticeTitle){
        this.noticeTitle = noticeTitle;
    }

    @Xss(message = "公告标题不能包含脚本字符")
    @NotBlank(message = "公告标题不能为空")
    @Size(min = 0, max = 50, message = "公告标题不能超过50个字符")
    public String getNoticeTitle(){
        return noticeTitle;
    }

    public void setNoticeType(String noticeType){
        this.noticeType = noticeType;
    }

    public String getNoticeType(){
        return noticeType;
    }

    public void setNoticeContent(String noticeContent){
        this.noticeContent = noticeContent;
    }

    public String getNoticeContent(){
        return noticeContent;
    }

    public Integer getReadCount() {
        return readCount;
    }

    public void setReadCount(Integer readCount) {
        this.readCount = readCount;
    }

    public Integer getReadPersonCount() {
        return readPersonCount;
    }

    public void setReadPersonCount(Integer readPersonCount) {
        this.readPersonCount = readPersonCount;
    }

    public Integer getUnreadPersonCount() {
        return unreadPersonCount;
    }

    public void setUnreadPersonCount(Integer unreadPersonCount) {
        this.unreadPersonCount = unreadPersonCount;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return status;
    }

    public String getRefType() {
        return refType;
    }

    public void setRefType(String refType) {
        this.refType = refType;
    }

    public Long getRefId() {
        return refId;
    }

    public void setRefId(Long refId) {
        this.refId = refId;
    }

	public SysFile getFileInfo() {
    	if(StringUtils.isNotBlank(fileInfoJson)){
    		return JSONObject.parseObject(fileInfoJson, SysFile.class);
	    }
		return fileInfo;
	}

	@Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("noticeId", getNoticeId())
            .append("noticeTitle", getNoticeTitle())
            .append("noticeType", getNoticeType())
            .append("noticeContent", getNoticeContent())
            .append("readCount", getReadCount())
            .append("readPersonCount", getReadPersonCount())
            .append("unreadPersonCount", getUnreadPersonCount())
            .append("status", getStatus())
            .append("refType", getRefType())
            .append("refId", getRefId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
