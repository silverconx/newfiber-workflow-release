package com.newfiber.system.api.factory;

import com.newfiber.common.core.domain.R;
import com.newfiber.common.core.web.domain.Result;
import com.newfiber.system.api.RemoteFileService;
import com.newfiber.system.api.domain.SysFile;
import com.newfiber.system.api.domain.request.SysFileSaveRequest;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件服务降级处理
 *
 * @author newfiber
 */
@Component
public class RemoteFileFallbackFactory implements FallbackFactory<RemoteFileService> {
    private static final Logger log = LoggerFactory.getLogger(RemoteFileFallbackFactory.class);

    @Override
    public RemoteFileService create(Throwable throwable) {
        log.error("文件服务调用失败:{}", throwable.getMessage());
        return new RemoteFileService() {
            @Override
            public R<SysFile> upload(MultipartFile file) {
                return R.fail("上传文件失败:" + throwable.getMessage());
            }

//	        @Override
//	        public R<SysFile> upload(IFileRefType fileRefType, Long refId, MultipartFile file) {
//		        return R.fail("上传文件失败:" + throwable.getMessage());
//	        }

            @Override
            public Result<Long> add(Long refId, SysFileSaveRequest request) {
                return Result.error("添加文件失败:" + throwable.getMessage());
            }

            @Override
            public Result<Boolean> addBatch(Long refId, List<SysFileSaveRequest> request) {
                return Result.error("添加文件失败:" + throwable.getMessage());
            }

            @Override
            public Result<Boolean> addBatch(String refType, Long refId, List<SysFileSaveRequest> request) {
                return Result.error("添加文件失败:" + throwable.getMessage());
            }

            @Override
            public Result<Boolean> update(String refType, Long refId, List<SysFileSaveRequest> request) {
                return Result.error("更新文件失败:" + throwable.getMessage());
            }

            @Override
            public Result<List<SysFile>> list(Long id) {
                return Result.error("文件查询失败:" + throwable.getMessage());
            }

            @Override
            public Result<List<SysFile>> selectByRefIds(String refIds) {
                return Result.error("文件查询失败:" + throwable.getMessage());
            }
        };
    }
}
