package com.newfiber.system.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author : X.K
 * @since : 2023/2/28 下午3:03
 */
@Getter
@AllArgsConstructor
public enum ESystemConstant {

	/**
	 */
	SystemCode("newfiber-workflow-release", "系统编号"),

		;

	private final String key;

	private final String value;


}
