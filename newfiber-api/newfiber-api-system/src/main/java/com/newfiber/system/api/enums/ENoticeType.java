package com.newfiber.system.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author : X.K
 * @since : 2023/2/28 下午3:03
 */
@Getter
@AllArgsConstructor
public enum ENoticeType {

	/**
	 * 公告类型（1通知 2公告）
	 */
	TongZhi("1", "通知"),
	GongGao("2", "公告"),

		;

	private final String type;

	private final String name;


}
